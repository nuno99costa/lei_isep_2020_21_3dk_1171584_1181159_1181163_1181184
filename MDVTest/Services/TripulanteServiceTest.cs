using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.Tripulantes;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class TripulanteServiceTest
    {
        private TripulanteService service;

        private Mock<ITripulanteRepository> repoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Tripulante tripMock;

        private TripulanteDto expected;

        [SetUp]
        public void Setup()
        {
            repoMock = new Mock<ITripulanteRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            //--------------------------APOIO----------------------------
            var data = DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            expected = new TripulanteDto{NMecanografico="1234567890",NCCidadao="1", NCConducao="1", NIF="1", Nome="Antonio", TipoTripulante="1", DataEntradaEmpresa="12/12/2012",DataLicencaConducao="12/12/2012", DataNascimento="12/12/2012", DataSaidaEmpresa="12/12/2012"};
            tripMock = new Tripulante(expected.NMecanografico, expected.Nome, data,expected.NCCidadao, expected.NCConducao, expected.NIF, expected.TipoTripulante, data, data, data);


            //-------------------------------------------------------------
            service = new TripulanteService(unitOfWorkMock.Object, repoMock.Object);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            repoMock.Setup(s => s.GetByIdAsync(tripMock.Id)).ReturnsAsync(tripMock);

            var result = await service.GetByIdAsync(tripMock.Id);

            repoMock.Verify(s => s.GetByIdAsync(tripMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.NMecanografico, result.NMecanografico);
            Assert.AreEqual(expected.Nome, result.Nome);
            Assert.AreEqual(expected.NCCidadao, result.NCCidadao);
            Assert.AreEqual(expected.NCConducao, result.NCConducao);

        }

        [Test]
        public async Task getAllAsyncTest(){
            List<Tripulante> mockValue = new List<Tripulante>{tripMock};
            repoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<TripulanteDto> expected3 = new List<TripulanteDto>{expected};

            var result = await service.GetAllAsync();

            repoMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expected3[0].NMecanografico, result[0].NMecanografico);
            Assert.AreEqual(expected3[0].Nome, result[0].Nome);
            Assert.AreEqual(expected3[0].NCCidadao, result[0].NCCidadao);
            Assert.AreEqual(expected3[0].NCConducao, result[0].NCConducao);
        }

        [Test]
        public async Task AddAsyncTest(){
            repoMock.Setup(s => s.AddAsync(It.IsAny<Tripulante>()));

            var result = await service.AddAsync(expected);

            repoMock.Verify(s => s.AddAsync(It.IsAny<Tripulante>()), Times.Once);
            Assert.AreEqual(expected.NMecanografico, result.NMecanografico);
            Assert.AreEqual(expected.Nome, result.Nome);
            Assert.AreEqual(expected.NCCidadao, result.NCCidadao);
            Assert.AreEqual(expected.NCConducao, result.NCConducao);
        }

        [Test]
        public async Task DeleteAsyncTest(){
            repoMock.Setup(s => s.GetByIdAsync(tripMock.Id)).ReturnsAsync(tripMock);
            repoMock.Setup(s => s.Remove(tripMock));

            var result = await service.DeleteAsync(tripMock.Id);

            repoMock.Verify(s => s.GetByIdAsync(tripMock.Id), Times.Once);
            repoMock.Verify(s => s.Remove(tripMock), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.NMecanografico, result.NMecanografico);
            Assert.AreEqual(expected.Nome, result.Nome);
            Assert.AreEqual(expected.NCCidadao, result.NCCidadao);
            Assert.AreEqual(expected.NCConducao, result.NCConducao);
        }
    }
}