
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Linhas;
using MDV.Domain.Percursos;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class BlocoTrabalhoServiceTest
    {
        private BlocoTrabalhoService service;

        private Mock<IBlocoTrabalhoRepository> blocoRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IViagemRepository> viagemRepoMock;
        private Mock<IViagemService> viagemServiceMock;
        private Mock<IServicoViaturaRepository> svRepoMock;

        //Lista de viagens auxiliar (só tem uma viagem)
        //private List<Viagem> aux = new List<Viagem> { new Viagem("11111111-1111-1111-1111-111222222222", new PercursoId("11111111-1111-2222-1111-111111111111"), new LinhaId("11111111-2222-2222-1111-111111111111"), new List<HorarioPassagem> { }) };

        //DTO resultado esperado comum a varios testes. De notar que o id da viagem é igual a este ^^^
        private BlocoTrabalhoDto expected;

        private Viagem viagemMock;

        private ServicoViatura svMock;

        private BlocoTrabalho blocoMock;

        [SetUp]
        public void Setup()
        {
            blocoRepoMock = new Mock<IBlocoTrabalhoRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            viagemRepoMock = new Mock<IViagemRepository>();

            viagemServiceMock = new Mock<IViagemService>();

            svRepoMock = new Mock<IServicoViaturaRepository>();

            //--------------------------APOIO----------------------------
            viagemMock = new Viagem("22111111-1111-1111-1111-111111111122", new PercursoId("ola"), new LinhaId("adeus"), new List<HorarioPassagem>(), 0);

            svMock = new ServicoViatura("1234567891",3000, new List<Viagem>{viagemMock});

            blocoMock = new BlocoTrabalho("11111111-1111-1111-1111-111111111111", 123,456,svMock);

            expected  = new BlocoTrabalhoDto {Id = blocoMock.Id.AsString(), Inicio = blocoMock.inicio, Fim = blocoMock.fim, ServicoViaturaId=svMock.Id.AsString() };
            //-------------------------------------------------------------
            service = new BlocoTrabalhoService(unitOfWorkMock.Object, blocoRepoMock.Object, viagemRepoMock.Object, svRepoMock.Object, viagemServiceMock.Object);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            blocoRepoMock.Setup(s => s.GetBlocoByIdEagerLoading(blocoMock.Id)).ReturnsAsync(blocoMock);

            var result = await service.GetByIdAsync(blocoMock.Id);

            blocoRepoMock.Verify(s => s.GetBlocoByIdEagerLoading(blocoMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Inicio, result.Inicio);
            Assert.AreEqual(expected.Fim, result.Fim);
            Assert.AreEqual(expected.ServicoViaturaId, result.ServicoViaturaId);

        }

        [Test]
        public async Task getAllAsyncTest(){
            List<BlocoTrabalho> mockValue = new List<BlocoTrabalho>{blocoMock};
            blocoRepoMock.Setup(s => s.GetAllBlocosEagerLoading()).ReturnsAsync(mockValue);

            List<BlocoTrabalhoDto> expected2 = new List<BlocoTrabalhoDto>{expected};

            var result = await service.GetAllAsync();

            blocoRepoMock.Verify(s => s.GetAllBlocosEagerLoading(), Times.Once);
            Assert.AreEqual(expected2[0].Id, result[0].Id);
            Assert.AreEqual(expected2[0].Fim, result[0].Fim);
            Assert.AreEqual(expected2[0].Inicio, result[0].Inicio);
            Assert.AreEqual(expected2[0].ServicoViaturaId, result[0].ServicoViaturaId);
        }

        [Test]
        public async Task AddAsyncTest(){
            blocoRepoMock.Setup(s => s.AddAsync(It.IsAny<BlocoTrabalho>()));

            svRepoMock.Setup(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id)).ReturnsAsync(svMock);

            var result = await service.AddAsync(expected);

            blocoRepoMock.Verify(s => s.AddAsync(It.IsAny<BlocoTrabalho>()), Times.Once);
            svRepoMock.Verify(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id), Times.Once);
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Inicio, result.Inicio);
            Assert.AreEqual(expected.Fim, result.Fim);
            Assert.AreEqual(expected.ServicoViaturaId, result.ServicoViaturaId);
        }

        [Test]
        public async Task DeleteAsyncTest(){
            blocoRepoMock.Setup(s => s.GetByIdAsync(blocoMock.Id)).ReturnsAsync(blocoMock);
            blocoRepoMock.Setup(s => s.Remove(blocoMock));

            var result = await service.DeleteAsync(blocoMock.Id);

            blocoRepoMock.Verify(s => s.GetByIdAsync(blocoMock.Id), Times.Once);
            blocoRepoMock.Verify(s => s.Remove(blocoMock), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Inicio, result.Inicio);
            Assert.AreEqual(expected.Fim, result.Fim);
            Assert.AreEqual(expected.ServicoViaturaId, result.ServicoViaturaId);
        }
    }
}