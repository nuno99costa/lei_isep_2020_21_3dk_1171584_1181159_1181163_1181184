using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.Viaturas;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class ViaturaServiceTest
    {
        private ViaturaService service;

        private Mock<IViaturaRepository> vRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Viatura viaturaMock;

        private ViaturaDto expected;

        [SetUp]
        public void Setup()
        {
            vRepoMock = new Mock<IViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            //--------------------------APOIO----------------------------
            viaturaMock = new Viatura("1", "12-AB-34", "1", DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")));
            expected = new ViaturaDto{Vin="1", Data=DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")), Matricula = "12-AB-34", TipoViatura = "1"};

            //-------------------------------------------------------------
            service = new ViaturaService(unitOfWorkMock.Object, vRepoMock.Object);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            vRepoMock.Setup(s => s.GetByIdAsync(viaturaMock.Id)).ReturnsAsync(viaturaMock);

            var result = await service.GetByIdAsync(viaturaMock.Id);

            vRepoMock.Verify(s => s.GetByIdAsync(viaturaMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Vin, result.Vin);
            Assert.AreEqual(expected.Data, result.Data);
            Assert.AreEqual(expected.TipoViatura, result.TipoViatura);
            Assert.AreEqual(expected.Matricula, result.Matricula);

        }

        [Test]
        public async Task getAllAsyncTest(){
            List<Viatura> mockValue = new List<Viatura>{viaturaMock};
            vRepoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<ViaturaDto> expected3 = new List<ViaturaDto>{expected};

            var result = await service.GetAllAsync();

            vRepoMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expected3[0].Vin, result[0].Vin);
            Assert.AreEqual(expected3[0].Data, result[0].Data);
            Assert.AreEqual(expected3[0].TipoViatura, result[0].TipoViatura);
            Assert.AreEqual(expected3[0].Matricula, result[0].Matricula);
        }

        [Test]
        public async Task AddAsyncTest(){
            vRepoMock.Setup(s => s.AddAsync(It.IsAny<Viatura>()));

            var result = await service.AddAsync(expected);

            vRepoMock.Verify(s => s.AddAsync(It.IsAny<Viatura>()), Times.Once);
            Assert.AreEqual(expected.Vin, result.Vin);
            Assert.AreEqual(expected.Data, result.Data);
            Assert.AreEqual(expected.TipoViatura, result.TipoViatura);
            Assert.AreEqual(expected.Matricula, result.Matricula);
        }

        [Test]
        public async Task DeleteAsyncTest(){
            vRepoMock.Setup(s => s.GetByIdAsync(viaturaMock.Id)).ReturnsAsync(viaturaMock);
            vRepoMock.Setup(s => s.Remove(viaturaMock));

            var result = await service.DeleteAsync(viaturaMock.Id);

            vRepoMock.Verify(s => s.GetByIdAsync(viaturaMock.Id), Times.Once);
            vRepoMock.Verify(s => s.Remove(viaturaMock), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Vin, result.Vin);
            Assert.AreEqual(expected.Data, result.Data);
            Assert.AreEqual(expected.TipoViatura, result.TipoViatura);
            Assert.AreEqual(expected.Matricula, result.Matricula);
        }
    }
}