using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.Shared;
using MDV.Mappers;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class ServicoTripulanteServiceTest
    {
        private ServicoTripulanteService service;

        private Mock<IServicoTripulanteRepository> stRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IBlocoTrabalhoRepository> blocoRepoMock;

        //Há dois tipos de DTOs. O que difere é o formato da Data.
        private ServicoTripulanteDto expected;

        private ServicoTripulante stMock;

        [SetUp]
        public void Setup()
        {
            stRepoMock = new Mock<IServicoTripulanteRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            blocoRepoMock = new Mock<IBlocoTrabalhoRepository>();

            //--------------------------APOIO----------------------------
            stMock = new ServicoTripulante("1234567890", 0, new List<BlocoTrabalho>{});

            expected  = ServicoTripulanteMapper.toDto(stMock);

            //-------------------------------------------------------------
            service = new ServicoTripulanteService(unitOfWorkMock.Object, stRepoMock.Object, blocoRepoMock.Object);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            stRepoMock.Setup(s => s.GetServicoTripulanteByIdEagerLoading(stMock.Id)).ReturnsAsync(stMock);

            var result = await service.GetByIdAsync(stMock.Id);

            stRepoMock.Verify(s => s.GetServicoTripulanteByIdEagerLoading(stMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Duracao, result.Duracao);
            Assert.AreEqual(expected.IdsBlocosTrabalho, result.IdsBlocosTrabalho);
            


        }

        [Test]
        public async Task getAllAsyncTest(){
            List<ServicoTripulante> mockValue = new List<ServicoTripulante>{stMock};
            stRepoMock.Setup(s => s.GetAllServicosTripulanteEagerLoading()).ReturnsAsync(mockValue);

            List<ServicoTripulanteDto> expected3 = new List<ServicoTripulanteDto>{expected};

            var result = await service.GetAllAsync();

            stRepoMock.Verify(s => s.GetAllServicosTripulanteEagerLoading(), Times.Once);
            Assert.AreEqual(expected3[0].Id, result[0].Id);
            Assert.AreEqual(expected3[0].Duracao, result[0].Duracao);
            Assert.AreEqual(expected3[0].IdsBlocosTrabalho, result[0].IdsBlocosTrabalho);
           
        }

        [Test]
        public async Task AddAsyncTest(){
            stRepoMock.Setup(s => s.AddAsync(It.IsAny<ServicoTripulante>()));

            var result = await service.AddAsync(expected);

            stRepoMock.Verify(s => s.AddAsync(It.IsAny<ServicoTripulante>()), Times.Once);
 
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Duracao, result.Duracao);
            Assert.AreEqual(expected.IdsBlocosTrabalho, result.IdsBlocosTrabalho);
        }

        [Test]
        public async Task DeleteAsyncTest(){
            stRepoMock.Setup(s => s.GetByIdAsync(stMock.Id)).ReturnsAsync(stMock);
            stRepoMock.Setup(s => s.Remove(stMock));

            var result = await service.DeleteAsync(stMock.Id);

            stRepoMock.Verify(s => s.GetByIdAsync(stMock.Id), Times.Once);
            stRepoMock.Verify(s => s.Remove(stMock), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Id, result.Id);
            Assert.AreEqual(expected.Duracao, result.Duracao);
            Assert.AreEqual(expected.IdsBlocosTrabalho, result.IdsBlocosTrabalho);
        }
    }
}