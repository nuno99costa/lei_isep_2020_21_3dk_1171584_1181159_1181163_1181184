using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MDV.Domain.Linhas;
using MDV.Domain.Nos;
using MDV.Domain.Percursos;
using MDV.Domain.Segmentos;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using MDV.Utils.Communication;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class ViagemServiceTest
    {
        private ViagemService service;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IViagemRepository> viagemRepoMock;

        private Mock<ICommunicationService> comServiceMock;

        private Mock<IServicoViaturaRepository> svRepoMock;

        private ServicoViatura svMock;

        private Viagem viagemMock;

        private ViagemDto expected;

        

        [SetUp]
        public void Setup()
        {
            svRepoMock = new Mock<IServicoViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            viagemRepoMock = new Mock<IViagemRepository>();

            comServiceMock = new Mock<ICommunicationService>();

            //--------------APOIO-----------------

            viagemMock = new Viagem("22111111-1111-1111-1111-111111111122", new PercursoId("ola"), new LinhaId("adeus"), new List<HorarioPassagem>{new HorarioPassagem(new NoId("1"), 0),new HorarioPassagem(new NoId("2"), 3000) }, 0 );

            svMock = new ServicoViatura("1234567891", 3000 ,new List<Viagem>{viagemMock});

            service = new ViagemService(unitOfWorkMock.Object, viagemRepoMock.Object, comServiceMock.Object, svRepoMock.Object);

            expected = ViagemMapper.toDto(viagemMock);

        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            viagemRepoMock.Setup(s => s.GetByIdAsync(viagemMock.Id)).ReturnsAsync(viagemMock);

            var result = await service.GetByIdAsync(viagemMock.Id);

            viagemRepoMock.Verify(s => s.GetByIdAsync(viagemMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.ViagemId, result.ViagemId);
            Assert.AreEqual(expected.IdPercurso, result.IdPercurso);
            Assert.AreEqual(expected.IdLinha, result.IdLinha);
            Assert.AreEqual(expected.HoraSaida, result.HoraSaida);
            Assert.AreEqual(expected.HorariosPassagem, result.HorariosPassagem);

        }

        [Test]
        public async Task getAllAsyncTest(){
            List<Viagem> mockValue = new List<Viagem>{viagemMock};

            viagemRepoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<ViagemDto> expected3 = new List<ViagemDto>{expected};

            var result = await service.GetAllAsync();

            viagemRepoMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expected3[0].ViagemId, result[0].ViagemId);
            Assert.AreEqual(expected3[0].IdLinha, result[0].IdLinha);
            Assert.AreEqual(expected3[0].IdPercurso, result[0].IdPercurso);
            Assert.AreEqual(expected3[0].HorariosPassagem, result[0].HorariosPassagem);
            Assert.AreEqual(expected3[0].HoraSaida, result[0].HoraSaida);
        }

        [Test]
        public async Task getBySVIdAsyncTest()
        {

            List<ViagemDto> expected3 = new List<ViagemDto>{expected};

            svRepoMock.Setup(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id)).ReturnsAsync(svMock);

            var result = await service.getByServicoViaturaIdAsync(svMock.Id);

            svRepoMock.Verify(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id), Times.Once);

            Assert.AreEqual(expected3[0].ViagemId, result[0].ViagemId);
            Assert.AreEqual(expected3[0].IdLinha, result[0].IdLinha);
            Assert.AreEqual(expected3[0].IdPercurso, result[0].IdPercurso);
            Assert.AreEqual(expected3[0].HorariosPassagem, result[0].HorariosPassagem);
            Assert.AreEqual(expected3[0].HoraSaida, result[0].HoraSaida);

        }

        [Test]
        public async Task AddAsyncTest()
        {

            CreatingViagemDto param = new CreatingViagemDto{ViagemId = expected.ViagemId, IdPercurso = expected.IdPercurso, IdLinha = expected.IdLinha, IdNoSaida = expected.HorariosPassagem[0].IdNo.AsString(), HorarioSaida = expected.HoraSaida};

            List<SegmentoDto> mockSegmento = new List<SegmentoDto>{new SegmentoDto{Id = "1", distanciaNos = 1, idNoInicial = viagemMock.HorariosPassagem[0].IdNo.AsString(), idNoFinal = viagemMock.HorariosPassagem[1].IdNo.AsString(), nomeSegmento = "1_2", tempoNos = 3000}};
            comServiceMock.Setup(s => s.GetSegmentosAsync(param.IdPercurso, "ola")).ReturnsAsync(mockSegmento);

            viagemRepoMock.Setup(s => s.AddAsync(It.IsAny<Viagem>()));

            var result = await service.AddAsync(param, "ola");
            result.HorariosPassagem.OrderBy(s => s.IdNo).ToList();

            Assert.AreEqual(expected.ViagemId, result.ViagemId);
            Assert.AreEqual(expected.IdPercurso, result.IdPercurso);
            Assert.AreEqual(expected.IdLinha, result.IdLinha);
            Assert.AreEqual(expected.HoraSaida, result.HoraSaida);
            Assert.AreEqual(expected.HorariosPassagem[0].IdNo, result.HorariosPassagem[0].IdNo);
            Assert.AreEqual(expected.HorariosPassagem[1].IdNo, result.HorariosPassagem[1].IdNo);
            Assert.AreEqual(expected.HorariosPassagem[0].Horario, result.HorariosPassagem[0].Horario);
            Assert.AreEqual(expected.HorariosPassagem[1].Horario, result.HorariosPassagem[1].Horario);
            
        }


    }
}