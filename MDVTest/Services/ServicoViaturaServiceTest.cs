using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Linhas;
using MDV.Domain.Nos;
using MDV.Domain.Percursos;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using MDV.Mappers;
using Moq;
using NUnit.Framework;

namespace MDVTest.Services
{
    [TestFixture]
    public class ServicoViaturaServiceTest
    {
        private ServicoViaturaService service;

        private Mock<IServicoViaturaRepository> svRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IViagemRepository> viagemRepoMock;

        private Mock<IBlocoTrabalhoRepository> blocoRepoMock;

        //Há dois tipos de DTOs. O que difere é o formato da Data.
        private ServicoViaturaDto expected;

        private ServicoViaturaDto expected2;

        private Viagem viagemMock;

        private Viatura viaturaMock;

        private ServicoViatura svMock;

        [SetUp]
        public void Setup()
        {
            svRepoMock = new Mock<IServicoViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            viagemRepoMock = new Mock<IViagemRepository>();

            blocoRepoMock = new Mock<IBlocoTrabalhoRepository>();

            //--------------------------APOIO----------------------------
            viagemMock = new Viagem("22111111-1111-1111-1111-111111111122", new PercursoId("ola"), new LinhaId("adeus"), new List<HorarioPassagem> { new HorarioPassagem(new NoId("1"), 0), new HorarioPassagem(new NoId("1"), 3000) }, 0);

            svMock = new ServicoViatura("1234567891", 3000, new List<Viagem> { viagemMock });

            expected = new ServicoViaturaDto { Id = svMock.Id.AsString(), ViagensIds = ViagemMapper.toStringIDs(svMock.Viagens), DuracaoSegundos = svMock.DuracaoSegundos };

            expected2 = ServicoViaturaMapper.toDto(svMock);
            //-------------------------------------------------------------
            service = new ServicoViaturaService(unitOfWorkMock.Object, svRepoMock.Object, viagemRepoMock.Object, blocoRepoMock.Object);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            svRepoMock.Setup(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id)).ReturnsAsync(svMock);

            var result = await service.GetByIdAsync(svMock.Id);

            svRepoMock.Verify(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected2.Id, result.Id);
            Assert.AreEqual(expected2.DuracaoSegundos, result.DuracaoSegundos);
            Assert.AreEqual(expected2.ViagensIds, result.ViagensIds);


        }

        [Test]
        public async Task getAllAsyncTest()
        {
            List<ServicoViatura> mockValue = new List<ServicoViatura> { svMock };
            svRepoMock.Setup(s => s.GetAllServicosViaturaEagerLoading()).ReturnsAsync(mockValue);

            List<ServicoViaturaDto> expected3 = new List<ServicoViaturaDto> { expected2 };

            var result = await service.GetAllAsync();

            svRepoMock.Verify(s => s.GetAllServicosViaturaEagerLoading(), Times.Once);
            Assert.AreEqual(expected3[0].Id, result[0].Id);
            Assert.AreEqual(expected3[0].DuracaoSegundos, result[0].DuracaoSegundos);
            Assert.AreEqual(expected3[0].ViagensIds, result[0].ViagensIds);

        }

        [Test]
        public async Task AddAsyncTest()
        {
            svRepoMock.Setup(s => s.AddAsync(It.IsAny<ServicoViatura>()));

            viagemRepoMock.Setup(s => s.GetByIdAsync(viagemMock.Id)).ReturnsAsync(viagemMock);

            var result = await service.AddAsync(expected);

            svRepoMock.Verify(s => s.AddAsync(It.IsAny<ServicoViatura>()), Times.Once);
            viagemRepoMock.Verify(s => s.GetByIdAsync(viagemMock.Id), Times.Once);
            Assert.AreEqual(expected2.Id, result.Id);
            Assert.AreEqual(expected2.DuracaoSegundos, result.DuracaoSegundos);
            Assert.AreEqual(expected2.ViagensIds, result.ViagensIds);
        }

        [Test]
        public async Task DeleteAsyncTest()
        {
            svRepoMock.Setup(s => s.GetByIdAsync(svMock.Id)).ReturnsAsync(svMock);
            svRepoMock.Setup(s => s.Remove(svMock));

            var result = await service.DeleteAsync(svMock.Id);

            svRepoMock.Verify(s => s.GetByIdAsync(svMock.Id), Times.Once);
            svRepoMock.Verify(s => s.Remove(svMock), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected2.Id, result.Id);
            Assert.AreEqual(expected2.DuracaoSegundos, result.DuracaoSegundos);
            Assert.AreEqual(expected2.ViagensIds, result.ViagensIds);
        }
    }
}