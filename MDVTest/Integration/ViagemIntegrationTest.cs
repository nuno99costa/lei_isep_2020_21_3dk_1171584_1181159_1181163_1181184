using NUnit.Framework;
using MDV.Controllers;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System;
using System.Threading.Tasks;
using MDV.Domain.Viagens;
using MDV.Domain.Nos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Components;
using MDV.Domain.Shared;
using MDV.Utils.Communication;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Viaturas;
using MDV.Domain.Percursos;
using System.Globalization;
using MDV.Domain.Linhas;
using MDV.Domain.Segmentos;

namespace MDVTest.Integration{

    [TestFixture]
    public class ViagemIntegrationTest{

        private ViagensController controller;

        private ViagemService service;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IViagemRepository> viagemRepoMock;

        private Mock<ICommunicationService> comServiceMock;

        private Mock<IServicoViaturaRepository> svRepoMock;

        private ServicoViatura svMock;

        private Viagem viagemMock;

        private ViagemDto expected;

        private Viatura viaturaMock;

        private Mock<HttpContext> contextMock;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            svRepoMock = new Mock<IServicoViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            viagemRepoMock = new Mock<IViagemRepository>();

            comServiceMock = new Mock<ICommunicationService>();

            //--------------APOIO-----------------

            viaturaMock = new Viatura("123", "12-12-12", "1", DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")));

            viagemMock = new Viagem("22111111-1111-1111-1111-111111111122", new PercursoId("ola"), new LinhaId("adeus"), new List<HorarioPassagem>{new HorarioPassagem(new NoId("1"), 0),new HorarioPassagem(new NoId("2"), 3000) }, 0 );

            svMock = new ServicoViatura("1234567891", 3000 , new List<Viagem>{viagemMock});

            service = new ViagemService(unitOfWorkMock.Object, viagemRepoMock.Object, comServiceMock.Object, svRepoMock.Object);

            expected = ViagemMapper.toDto(viagemMock);

            contextMock = new Mock<HttpContext>();
            controller = new ViagensController(service);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["auth-token"] = "ola";

        }

        [Test]
        public async Task getByIdAsyncTest(){

            viagemRepoMock.Setup(s => s.GetByIdAsync(viagemMock.Id)).ReturnsAsync(viagemMock);

            //Chamar o metodo
            var result = await controller.GetGetById((expected.ViagemId));

            viagemRepoMock.Verify(s => s.GetByIdAsync(viagemMock.Id), Times.Once);

            //Verificar resultado
            Assert.AreEqual(expected.ViagemId, result.Value.ViagemId);
            Assert.AreEqual(expected.IdPercurso, result.Value.IdPercurso);
            Assert.AreEqual(expected.IdLinha, result.Value.IdLinha);
            Assert.AreEqual(expected.HoraSaida, result.Value.HoraSaida);
            Assert.AreEqual(expected.HorariosPassagem, result.Value.HorariosPassagem);
        }

        [Test]
        public async Task getAllTest(){

             List<Viagem> mockValue = new List<Viagem>{viagemMock};

            viagemRepoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<ViagemDto> expected3 = new List<ViagemDto>{expected};


            var result = await controller.GetAll();

            viagemRepoMock.Verify(s => s.GetAllAsync(), Times.Once);
        }
    
        [Test]
        public async Task CreateTest(){

            CreatingViagemDto param = new CreatingViagemDto{ViagemId = expected.ViagemId, IdPercurso = expected.IdPercurso, IdLinha = expected.IdLinha, IdNoSaida = expected.HorariosPassagem[0].IdNo.AsString(), HorarioSaida = expected.HoraSaida};

            List<SegmentoDto> mockSegmento = new List<SegmentoDto>{new SegmentoDto{Id = "1", distanciaNos = 1, idNoInicial = viagemMock.HorariosPassagem[0].IdNo.AsString(), idNoFinal = viagemMock.HorariosPassagem[1].IdNo.AsString(), nomeSegmento = "1_2", tempoNos = 3000}};
            comServiceMock.Setup(s => s.GetSegmentosAsync(param.IdPercurso, "ola")).ReturnsAsync(mockSegmento);

            viagemRepoMock.Setup(s => s.AddAsync(It.IsAny<Viagem>()));

            
            var result = await controller.Create(param);

            comServiceMock.Verify(s => s.GetSegmentosAsync(param.IdPercurso, "ola"), Times.Once);

            viagemRepoMock.Verify(s => s.AddAsync(It.IsAny<Viagem>()), Times.Once);


        }



    }
}