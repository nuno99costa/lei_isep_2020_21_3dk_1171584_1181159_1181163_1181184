using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.Shared;
using MDV.Domain.Tripulantes;
using MDV.Mappers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace MDVTest.Integration{

    [TestFixture]
    public class TripulanteIntegrationTest{

        private TripulantesController controller;

        private TripulanteService service;

        private Mock<ITripulanteRepository> repoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Tripulante tripMock;

        private TripulanteDto expected;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
             repoMock = new Mock<ITripulanteRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            //--------------------------APOIO----------------------------
            var data = DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            expected = new TripulanteDto{NMecanografico="1234567890",NCCidadao="1", NCConducao="1", NIF="1", Nome="Antonio", TipoTripulante="1", DataEntradaEmpresa="12/12/2012",DataLicencaConducao="12/12/2012", DataNascimento="12/12/2012", DataSaidaEmpresa="12/12/2012"};
            tripMock = new Tripulante(expected.NMecanografico, expected.Nome, data,expected.NCCidadao, expected.NCConducao, expected.NIF, expected.TipoTripulante, data, data, data);

            //-------------------------------------------------------------
            service = new TripulanteService(unitOfWorkMock.Object, repoMock.Object);
            //----------------------------------------------------------
            controller = new TripulantesController(service);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            repoMock.Setup(s => s.GetByIdAsync(tripMock.Id)).ReturnsAsync(tripMock);
            //Chamar o metodo
            var result = await controller.GetGetById(expected.NMecanografico);

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            repoMock.Verify(s => s.GetByIdAsync(tripMock.Id), Times.Once);

        }

        [Test]
        public async Task getAllTest(){

            List<Tripulante> mockValue = new List<Tripulante>{tripMock};
            repoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<TripulanteDto> expected3 = new List<TripulanteDto>{expected};

            var result = await controller.GetAll();

            repoMock.Verify(s => s.GetAllAsync(), Times.Once);
        }
    
        [Test]
        public async Task CreateTest(){

            repoMock.Setup(s => s.AddAsync(It.IsAny<Tripulante>()));

            var result = await controller.Create(expected);

            repoMock.Verify(s => s.AddAsync(It.IsAny<Tripulante>()), Times.Once);


        }


    }
}