using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Linhas;
using MDV.Domain.Nos;
using MDV.Domain.Percursos;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using MDV.Mappers;
using Moq;
using NUnit.Framework;

namespace MDVTest.Integration
{
    [TestFixture]
    public class ServicoViaturaIntegrationTest
    {
        private ServicosViaturaController controller;
        private ServicoViaturaService service;

        private Mock<IServicoViaturaRepository> svRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IViagemRepository> viagemRepoMock;

        private Mock<IBlocoTrabalhoRepository> blocoRepoMock;


        //Há dois tipos de DTOs. O que difere é o formato da Data.
        private ServicoViaturaDto expected;

        private ServicoViaturaDto expected2;

        private Viagem viagemMock;

        private ServicoViatura svMock;

        [SetUp]
        public void Setup()
        {
            svRepoMock = new Mock<IServicoViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            viagemRepoMock = new Mock<IViagemRepository>();
            blocoRepoMock = new Mock<IBlocoTrabalhoRepository>();
            //--------------------------APOIO----------------------------
            viagemMock = new Viagem("22111111-1111-1111-1111-111111111122", new PercursoId("ola"), new LinhaId("adeus"), new List<HorarioPassagem>{new HorarioPassagem(new NoId("1"), 0),new HorarioPassagem(new NoId("1"), 3000) }, 0);

            svMock = new ServicoViatura("1234567891", 3000, new List<Viagem> { viagemMock });

            expected = new ServicoViaturaDto { Id = svMock.Id.AsString(), ViagensIds = ViagemMapper.toStringIDs(svMock.Viagens), DuracaoSegundos = svMock.DuracaoSegundos };

            expected2 = ServicoViaturaMapper.toDto(svMock);
            //-------------------------------------------------------------
            service = new ServicoViaturaService(unitOfWorkMock.Object, svRepoMock.Object, viagemRepoMock.Object, blocoRepoMock.Object);

            controller = new ServicosViaturaController(service);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            svRepoMock.Setup(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id)).ReturnsAsync(svMock);

            //Chamar o metodo
            var result = await controller.GetGetById(svMock.Id.AsString());

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            svRepoMock.Verify(s => s.GetServicoViaturaByIdEagerLoading(svMock.Id), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value.Id, expected2.Id);
        }

        [Test]
        public async Task getAllTest()
        {

            List<ServicoViatura> mockValue = new List<ServicoViatura> { svMock };
            svRepoMock.Setup(s => s.GetAllServicosViaturaEagerLoading()).ReturnsAsync(mockValue);

            List<ServicoViaturaDto> expected3 = new List<ServicoViaturaDto> { expected2 };

            var result = await controller.GetAll();

            svRepoMock.Verify(s => s.GetAllServicosViaturaEagerLoading(), Times.Once);
            Assert.AreEqual(expected3[0].Id, result.Value[0].Id);
       
            Assert.AreEqual(expected3[0].DuracaoSegundos, result.Value[0].DuracaoSegundos);
            Assert.AreEqual(expected3[0].ViagensIds, result.Value[0].ViagensIds);
          
        }

        [Test]
        public async Task CreateTest()
        {

            svRepoMock.Setup(s => s.AddAsync(It.IsAny<ServicoViatura>()));

            viagemRepoMock.Setup(s => s.GetByIdAsync(viagemMock.Id)).ReturnsAsync(viagemMock);

            var result = await controller.Create(expected);

            svRepoMock.Verify(s => s.AddAsync(It.IsAny<ServicoViatura>()), Times.Once);
            viagemRepoMock.Verify(s => s.GetByIdAsync(viagemMock.Id), Times.Once);

            Assert.AreEqual(expected2.Id, result.Value.Id);
    
            Assert.AreEqual(expected2.DuracaoSegundos, result.Value.DuracaoSegundos);
            Assert.AreEqual(expected2.ViagensIds, result.Value.ViagensIds);


        }
    }
}