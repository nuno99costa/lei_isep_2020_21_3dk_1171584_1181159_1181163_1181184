using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.Shared;
using MDV.Domain.Viaturas;
using MDV.Mappers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace MDVTest.Integration{

    [TestFixture]
    public class ViaturaIntegrationTest{

        private ViaturasController controller;

        private ViaturaService service;

        private Mock<IViaturaRepository> vRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Viatura viaturaMock;

        private ViaturaDto expected;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            vRepoMock = new Mock<IViaturaRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            //--------------------------APOIO----------------------------
            viaturaMock = new Viatura("1", "12-AB-34", "1", DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")));
            expected = new ViaturaDto{Vin="1", Data=DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")), Matricula = "12-AB-34", TipoViatura = "1"};

            //-------------------------------------------------------------
            service = new ViaturaService(unitOfWorkMock.Object, vRepoMock.Object);
            //----------------------------------------------------------
            controller = new ViaturasController(service);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            vRepoMock.Setup(s => s.GetByIdAsync(viaturaMock.Id)).ReturnsAsync(viaturaMock);
            //Chamar o metodo
            var result = await controller.GetGetById(expected.Vin);

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            vRepoMock.Verify(s => s.GetByIdAsync(viaturaMock.Id), Times.Once);

        }

        [Test]
        public async Task getAllTest(){

            List<Viatura> mockValue = new List<Viatura>{viaturaMock};
            vRepoMock.Setup(s => s.GetAllAsync()).ReturnsAsync(mockValue);

            List<ViaturaDto> expected3 = new List<ViaturaDto>{expected};

            var result = await controller.GetAll();

            vRepoMock.Verify(s => s.GetAllAsync(), Times.Once);
        }
    
        [Test]
        public async Task CreateTest(){

            vRepoMock.Setup(s => s.AddAsync(It.IsAny<Viatura>()));

            var result = await controller.Create(expected);

            vRepoMock.Verify(s => s.AddAsync(It.IsAny<Viatura>()), Times.Once);


        }


    }
}