using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.Shared;
using MDV.Mappers;
using Moq;
using NUnit.Framework;

namespace MDVTest.Integration
{
    [TestFixture]
    public class ServicoTripulanteIntegrationTest
    {
        private ServicosTripulanteController controller;
        private ServicoTripulanteService service;

        private Mock<IServicoTripulanteRepository> stRepoMock;

        private Mock<IUnitOfWork> unitOfWorkMock;

        private Mock<IBlocoTrabalhoRepository> blocoRepoMock;

        //Há dois tipos de DTOs. O que difere é o formato da Data.
        private ServicoTripulanteDto expected;

        private ServicoTripulante stMock;


        [SetUp]
        public void Setup()
        {
           stRepoMock = new Mock<IServicoTripulanteRepository>();

            unitOfWorkMock = new Mock<IUnitOfWork>();
            unitOfWorkMock.Setup(s => s.CommitAsync()).ReturnsAsync(1);

            blocoRepoMock = new Mock<IBlocoTrabalhoRepository>();

            //--------------------------APOIO----------------------------
            stMock = new ServicoTripulante("1234567890", 0, new List<BlocoTrabalho>{});

            expected  = ServicoTripulanteMapper.toDto(stMock);

            //-------------------------------------------------------------
            service = new ServicoTripulanteService(unitOfWorkMock.Object, stRepoMock.Object, blocoRepoMock.Object);
            controller = new ServicosTripulanteController(service);
        }

        [Test]
        public async Task getByIdAsyncTest()
        {

            stRepoMock.Setup(s => s.GetServicoTripulanteByIdEagerLoading(stMock.Id)).ReturnsAsync(stMock);

            var result = await controller.GetGetById(stMock.Id.AsString());

            stRepoMock.Verify(s => s.GetServicoTripulanteByIdEagerLoading(stMock.Id), Times.Once);

            //Por alguma razão os dois objetos comparados nao dão true, mas individualmente cada elemento é já dá
            Assert.AreEqual(expected.Id, result.Value.Id);
            Assert.AreEqual(expected.Duracao, result.Value.Duracao);
            Assert.AreEqual(expected.IdsBlocosTrabalho, result.Value.IdsBlocosTrabalho);
            
        }

        [Test]
        public async Task getAllTest()
        {

            List<ServicoTripulante> mockValue = new List<ServicoTripulante>{stMock};
            stRepoMock.Setup(s => s.GetAllServicosTripulanteEagerLoading()).ReturnsAsync(mockValue);

            List<ServicoTripulanteDto> expected3 = new List<ServicoTripulanteDto>{expected};

            var result = await controller.GetAll();

            stRepoMock.Verify(s => s.GetAllServicosTripulanteEagerLoading(), Times.Once);
            Assert.AreEqual(expected3[0].Id, result.Value[0].Id);
            Assert.AreEqual(expected3[0].Duracao, result.Value[0].Duracao);
            Assert.AreEqual(expected3[0].IdsBlocosTrabalho, result.Value[0].IdsBlocosTrabalho);
        }

        [Test]
        public async Task CreateTest()
        {

            stRepoMock.Setup(s => s.AddAsync(It.IsAny<ServicoTripulante>()));

            var result = await controller.Create(expected);

            stRepoMock.Verify(s => s.AddAsync(It.IsAny<ServicoTripulante>()), Times.Once);
 
            Assert.AreEqual(expected.Id, result.Value.Id);
            Assert.AreEqual(expected.Duracao, result.Value.Duracao);
            Assert.AreEqual(expected.IdsBlocosTrabalho, result.Value.IdsBlocosTrabalho);


        }
    }
}