using NUnit.Framework;
using MDV.Controllers;
using Moq;
using MDV.Domain.BlocosTrabalho;
using System.Collections.Generic;
using System.Threading;
using System;
using System.Threading.Tasks;

namespace MDVTest.Controllers{

    [TestFixture]
    public class BlocoTrabalhoControllerTest{

        private BlocosTrabalhoController controller;

        private Mock<IBlocoTrabalhoService> blocoServiceMock;

        private BlocoTrabalhoDto expected = new BlocoTrabalhoDto {Id = "11111111-1111-1111-1111-111111111111", Inicio = 123, Fim = 456, ServicoViaturaId="1234567891" };

        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            blocoServiceMock = new Mock<IBlocoTrabalhoService>();

            controller = new BlocosTrabalhoController(blocoServiceMock.Object);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            Console.WriteLine("-------------------GetBlocoByIdController-----------------------");

            var id = "11111111-1111-1111-1111-111111111111";
            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            blocoServiceMock.Setup(p => p.GetByIdAsync(new BlocoTrabalhoId(id))).ReturnsAsync(expected);

            //Chamar o metodo
            var result = await controller.GetGetById(id);

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            blocoServiceMock.Verify(s => s.GetByIdAsync(new BlocoTrabalhoId(id)), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value, expected);
        }

        [Test]
        public async Task getAllTest(){

            Console.WriteLine("-------------------GetAllBlocoController-----------------------");

             List<BlocoTrabalhoDto> expectedList = new List<BlocoTrabalhoDto>{expected};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            blocoServiceMock.Setup(p => p.GetAllAsync()).ReturnsAsync(expectedList);

            var result = await controller.GetAll();

            blocoServiceMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expectedList, result.Value);
        }
    
        [Test]
        public async Task CreateTest(){
            Console.WriteLine("-------------------CreateBlocoController-----------------------");

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            blocoServiceMock.Setup(p => p.AddAsync(expected)).ReturnsAsync(expected);
            
            var result = await controller.Create(expected);

            blocoServiceMock.Verify(s => s.AddAsync(expected), Times.Once);

            //PROBLEMA A TIRAR A COM A STORA: COMO TESTAR CreatedAtAction? Mudei no controller para isto rodar. Deve ser corrigido
            Assert.AreEqual(expected, result.Value);


        }


    }
}