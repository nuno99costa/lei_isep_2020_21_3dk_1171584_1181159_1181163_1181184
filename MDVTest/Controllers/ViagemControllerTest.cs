using NUnit.Framework;
using MDV.Controllers;
using Moq;
using System.Collections.Generic;
using System.Threading;
using System;
using System.Threading.Tasks;
using MDV.Domain.Viagens;
using MDV.Domain.Nos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Components;

namespace MDVTest.Controllers{

    [TestFixture]
    public class ViagemControllerTest{

        private ViagensController controller;

        private Mock<IViagemService> viagemServiceMock;

        private Mock<HttpContext> contextMock;

        private ViagemDto expected;

        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            expected = new ViagemDto {ViagemId = "11111111-1111-1111-1111-111111111111", IdLinha = "1", IdPercurso = "1", HorariosPassagem= new List<HorarioPassagem>{new HorarioPassagem(new NoId("1"), 0) }, HoraSaida = 0};
            viagemServiceMock = new Mock<IViagemService>();

            contextMock = new Mock<HttpContext>();
            controller = new ViagensController(viagemServiceMock.Object);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.ControllerContext.HttpContext.Request.Headers["auth-token"] = "ola";

        }

        [Test]
        public async Task getByIdAsyncTest(){

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            viagemServiceMock.Setup(p => p.GetByIdAsync(new ViagemId(expected.ViagemId))).ReturnsAsync(expected);

            //Chamar o metodo
            var result = await controller.GetGetById((expected.ViagemId));

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            viagemServiceMock.Verify(s => s.GetByIdAsync(new ViagemId(expected.ViagemId)), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value, expected);
        }

        [Test]
        public async Task getAllTest(){

             List<ViagemDto> expectedList = new List<ViagemDto>{expected};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            viagemServiceMock.Setup(p => p.GetAllAsync()).ReturnsAsync(expectedList);

            var result = await controller.GetAll();

            viagemServiceMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expectedList, result.Value);
        }
    
        [Test]
        public async Task CreateTest(){

            CreatingViagemDto param = new CreatingViagemDto{ViagemId = expected.ViagemId, IdPercurso = expected.IdPercurso, IdLinha = expected.IdLinha, IdNoSaida = expected.HorariosPassagem[0].IdNo.AsString(), HorarioSaida = expected.HoraSaida};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            viagemServiceMock.Setup(p => p.AddAsync(param, "ola")).ReturnsAsync(expected);
            
            var result = await controller.Create(param);

            viagemServiceMock.Verify(s => s.AddAsync(param, "ola"), Times.Once);

            //PROBLEMA A TIRAR A COM A STORA: COMO TESTAR CreatedAtAction? Mudei no controller para isto rodar. Deve ser corrigido
            Assert.AreEqual(expected, (result.Result as CreatedAtActionResult).Value);


        }



    }
}