using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosTripulante;
using MDV.Mappers;
using Moq;
using NUnit.Framework;

namespace MDVTest.Controllers{

    [TestFixture]
    public class ServicoTripulanteControllerTest{

        private ServicosTripulanteController controller;

        private Mock<IServicoTripulanteService> stServiceMock;

        private ServicoTripulante stAuxiliar;

        private ServicoTripulanteDto expected;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            stServiceMock = new Mock<IServicoTripulanteService>();

            //------------Variaveis de apoio-------------------------
            stAuxiliar = new ServicoTripulante("1234567890", 3000, new List<BlocoTrabalho>{});


            expected  = ServicoTripulanteMapper.toDto(stAuxiliar);

            //----------------------------------------------------------
            controller = new ServicosTripulanteController(stServiceMock.Object);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            stServiceMock.Setup(p => p.GetByIdAsync(stAuxiliar.Id)).ReturnsAsync(expected);

            //Chamar o metodo
            var result = await controller.GetGetById(stAuxiliar.Id.AsString());

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            stServiceMock.Verify(s => s.GetByIdAsync(stAuxiliar.Id), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value, expected);
        }

        [Test]
        public async Task getAllTest(){

             List<ServicoTripulanteDto> expectedList = new List<ServicoTripulanteDto>{expected};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            stServiceMock.Setup(p => p.GetAllAsync()).ReturnsAsync(expectedList);

            var result = await controller.GetAll();

            stServiceMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expectedList, result.Value);
        }
    
        [Test]
        public async Task CreateTest(){

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            stServiceMock.Setup(p => p.AddAsync(expected)).ReturnsAsync(expected);
            
            var result = await controller.Create(expected);

            stServiceMock.Verify(s => s.AddAsync(expected), Times.Once);

            //PROBLEMA A TIRAR A COM A STORA: COMO TESTAR CreatedAtAction? Mudei no controller para isto rodar. Deve ser corrigido
            Assert.AreEqual(expected, result.Value);


        }


    }
}