using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.Tripulantes;
using MDV.Mappers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace MDVTest.Controllers{

    [TestFixture]
    public class TripulanteControllerTest{

        private TripulantesController controller;

        private Mock<ITripulanteService> serviceMock;

        private TripulanteDto expected;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            serviceMock = new Mock<ITripulanteService>();

            //------------Variaveis de apoio-------------------------
            expected = new TripulanteDto{NMecanografico="1",NCCidadao="1", NCConducao="1", NIF="1", Nome="Antonio", TipoTripulante="1", DataEntradaEmpresa="12/12/2012",DataLicencaConducao="12/12/2012", DataNascimento="12/12/2012", DataSaidaEmpresa="12/12/2012"};

            //----------------------------------------------------------
            controller = new TripulantesController(serviceMock.Object);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            serviceMock.Setup(p => p.GetByIdAsync(new TripulanteId(expected.NMecanografico))).ReturnsAsync(expected);

            //Chamar o metodo
            var result = await controller.GetGetById(expected.NMecanografico);

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            serviceMock.Verify(s => s.GetByIdAsync(new TripulanteId(expected.NMecanografico)), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value, expected);
        }

        [Test]
        public async Task getAllTest(){

             List<TripulanteDto> expectedList = new List<TripulanteDto>{expected};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            serviceMock.Setup(p => p.GetAllAsync()).ReturnsAsync(expectedList);

            var result = await controller.GetAll();

            serviceMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expectedList, result.Value);
        }
    
        [Test]
        public async Task CreateTest(){

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            serviceMock.Setup(p => p.AddAsync(expected)).ReturnsAsync(expected);
            
            var result = await controller.Create(expected);

            serviceMock.Verify(s => s.AddAsync(expected), Times.Once);

            //PROBLEMA A TIRAR A COM A STORA: COMO TESTAR CreatedAtAction? Mudei no controller para isto rodar. Deve ser corrigido
            Assert.AreEqual(expected, (result.Result as CreatedAtActionResult).Value);


        }


    }
}