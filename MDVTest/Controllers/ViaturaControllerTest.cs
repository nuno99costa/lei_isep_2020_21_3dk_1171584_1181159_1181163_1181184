using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using MDV.Controllers;
using MDV.Domain.Viaturas;
using MDV.Mappers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace MDVTest.Controllers{

    [TestFixture]
    public class ViaturaControllerTest{

        private ViaturasController controller;

        private Mock<IViaturaService> vServiceMock;

        private ViaturaDto expected;


        // ----------- Setup dos mocks e instancia a testar --------- Definir returns gerais dos mocks ----------
        [SetUp]
        public void Setup()
        {
            vServiceMock = new Mock<IViaturaService>();

            //------------Variaveis de apoio-------------------------
            expected = new ViaturaDto{Vin="1", Data=DateTime.ParseExact("12/12/2012", "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")), Matricula = "12-AB-34", TipoViatura = "1"};

            //----------------------------------------------------------
            controller = new ViaturasController(vServiceMock.Object);

        }

        [Test]
        public async Task getByIdAsyncTest(){

            vServiceMock.Setup(p => p.GetByIdAsync(new ViaturaId(expected.Vin))).ReturnsAsync(expected);

            //Chamar o metodo
            var result = await controller.GetGetById(expected.Vin);

            //Verificar se o mock foi chamado (como os spies no sinon no node js)
            vServiceMock.Verify(s => s.GetByIdAsync(new ViaturaId(expected.Vin)), Times.Once);

            //Verificar resultado
            Assert.AreEqual(result.Value, expected);
        }

        [Test]
        public async Task getAllTest(){

             List<ViaturaDto> expectedList = new List<ViaturaDto>{expected};

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            vServiceMock.Setup(p => p.GetAllAsync()).ReturnsAsync(expectedList);

            var result = await controller.GetAll();

            vServiceMock.Verify(s => s.GetAllAsync(), Times.Once);
            Assert.AreEqual(expectedList, result.Value);
        }
    
        [Test]
        public async Task CreateTest(){

            //Fazer o mock retornar. ReturnsAsync para metodos que são assincronos!
            vServiceMock.Setup(p => p.AddAsync(expected)).ReturnsAsync(expected);
            
            var result = await controller.Create(expected);

            vServiceMock.Verify(s => s.AddAsync(expected), Times.Once);

            //PROBLEMA A TIRAR A COM A STORA: COMO TESTAR CreatedAtAction? Mudei no controller para isto rodar. Deve ser corrigido
            Assert.AreEqual(expected, (result.Result as CreatedAtActionResult).Value);


        }


    }
}