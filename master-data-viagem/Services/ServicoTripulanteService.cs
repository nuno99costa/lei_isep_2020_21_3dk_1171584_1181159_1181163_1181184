using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.Shared;
using MDV.Domain.Tripulantes;
using MDV.Mappers;

namespace MDV.Domain.BlocosTrabalho
{
    public class ServicoTripulanteService : IServicoTripulanteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoTripulanteRepository _repo;

        private readonly IBlocoTrabalhoRepository _blocoTrabalhoRepo;

        //Construtor com injetaveis
        public ServicoTripulanteService(IUnitOfWork unitOfWork, IServicoTripulanteRepository repo, IBlocoTrabalhoRepository repoBlocos)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._blocoTrabalhoRepo = repoBlocos;
        }

        public async Task<List<ServicoTripulanteDto>> GetAllAsync()
        {
            List<ServicoTripulante> list = await this._repo.GetAllServicosTripulanteEagerLoading();

            List<ServicoTripulanteDto> listDto = new List<ServicoTripulanteDto>();
            foreach (ServicoTripulante servicoTripulante in list)
            {
                listDto.Add(ServicoTripulanteMapper.toDto(servicoTripulante));
            }

            return listDto;
        }

        //Vai ao repositório buscar o serviço de Tripulante com id passado por parametro
        public async Task<ServicoTripulanteDto> GetByIdAsync(ServicoTripulanteId id)
        {
            ServicoTripulante servicoTripulante = await this._repo.GetServicoTripulanteByIdEagerLoading(id);

            if (servicoTripulante == null) return null;

            return ServicoTripulanteMapper.toDto(servicoTripulante);
        }

        public async Task<ServicoTripulanteDto> AddAsync(ServicoTripulanteDto dto)
        {

            if (String.IsNullOrEmpty(dto.Id))
            {
                throw new BusinessRuleValidationException("ID em falta!");
            }


            List<BlocoTrabalho> blocos = await this.getBlocos(dto.IdsBlocosTrabalho);
            blocos = blocos.OrderBy(s => s.inicio).ToList();
            int duracao = this.validarBlocos(blocos);

            ServicoTripulante servicoTripulante = new ServicoTripulante(dto.Id, duracao, blocos);

            await this._repo.AddAsync(servicoTripulante);

            await this._unitOfWork.CommitAsync();

            return ServicoTripulanteMapper.toDto(servicoTripulante);
        }

        private async Task<List<BlocoTrabalho>> getBlocos(List<string> idsBlocosTrabalho)
        {
            List<BlocoTrabalho> blocos = new List<BlocoTrabalho>();

            foreach(string id in idsBlocosTrabalho){
                BlocoTrabalho current = await this._blocoTrabalhoRepo.GetBlocoByIdEagerLoading(new BlocoTrabalhoId(id));
                if (current == null)
                {
                    throw new BusinessRuleValidationException("Bloco não existe: " + id.ToString());
                }
                blocos.Add(current);
            }

            return blocos;
        }

        private int validarBlocos(List<BlocoTrabalho> blocos){
            int duracao = 0;
            int i;

            for(i = 0; i < blocos.Count; i++){

                if(i != (blocos.Count - 1)){
                    if(blocos[i].fim != blocos[i + 1].inicio) throw new BusinessRuleValidationException("Os blocos têm de ser seguidos");
                }

                duracao += (blocos[i].fim - blocos[i].inicio);
            }

            if(duracao > (8 * 3600)) throw new BusinessRuleValidationException("Um serviço de tripulante não pode ter duração maior que 8 horas");

            return duracao;
        }

        public async Task<ServicoTripulanteDto> DeleteAsync(ServicoTripulanteId id)
        {
            var servicoTripulante = await this._repo.GetByIdAsync(id);

            if (servicoTripulante == null) return null;

            this._repo.Remove(servicoTripulante);
            await this._unitOfWork.CommitAsync();

            return ServicoTripulanteMapper.toDto(servicoTripulante);
        }

    }


}