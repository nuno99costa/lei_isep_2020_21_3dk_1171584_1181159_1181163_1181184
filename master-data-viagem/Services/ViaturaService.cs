using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;

namespace MDV.Domain.Viaturas
{
    public class ViaturaService : IViaturaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViaturaRepository _repo;

        public ViaturaService(IUnitOfWork unitOfWork, IViaturaRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<ViaturaDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<ViaturaDto> listDto = list.ConvertAll<ViaturaDto>(viatura => new ViaturaDto { Vin = viatura.Id.AsString(), Matricula = viatura.Matricula, TipoViatura = viatura.TipoViatura, Data = viatura.Data });

            return listDto;
        }

        public async Task<ViaturaDto> GetByIdAsync(ViaturaId id)
        {
            Viatura viatura = await this._repo.GetByIdAsync(id);

            if (viatura == null) return null;

            return new ViaturaDto { Vin = viatura.Id.AsString(), Matricula = viatura.Matricula, TipoViatura = viatura.TipoViatura, Data = viatura.Data };
        }

        public async Task<ViaturaDto> AddAsync(ViaturaDto dto)
        {
            Viatura viatura;

            viatura = new Viatura(dto.Vin, dto.Matricula, dto.TipoViatura, dto.Data);

            await this._repo.AddAsync(viatura);

            await this._unitOfWork.CommitAsync();

            return new ViaturaDto { Vin = viatura.Id.AsString(), Matricula = viatura.Matricula, TipoViatura = viatura.TipoViatura, Data = viatura.Data };
        }

        public async Task<ViaturaDto> DeleteAsync(ViaturaId id)
        {
            var viatura = await this._repo.GetByIdAsync(id);

            if (viatura == null) return null;

            this._repo.Remove(viatura);

            await this._unitOfWork.CommitAsync();

            return new ViaturaDto { Vin = viatura.Id.AsString(), Matricula = viatura.Matricula, TipoViatura = viatura.TipoViatura, Data = viatura.Data };
        }
    }
}