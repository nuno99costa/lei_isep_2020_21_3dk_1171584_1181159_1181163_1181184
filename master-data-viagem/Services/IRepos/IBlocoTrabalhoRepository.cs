using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;

namespace MDV.Domain.BlocosTrabalho
{
    public interface IBlocoTrabalhoRepository : IRepository<BlocoTrabalho, BlocoTrabalhoId>
    {
        Task<List<BlocoTrabalho>> GetAllBlocosEagerLoading();
        Task<BlocoTrabalho> GetBlocoByIdEagerLoading(BlocoTrabalhoId id);

        Task<List<BlocoTrabalho>> getBlocosByServicoViatura(ServicoViaturaId id);
        Task<List<BlocoTrabalho>> GetAllBlocosEagerLoadingWithTrips();
    }
}