using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Shared;

namespace MDV.Domain.Viagens
{
    public interface IViagemRepository : IRepository<Viagem, ViagemId>
    {
    
    }
}