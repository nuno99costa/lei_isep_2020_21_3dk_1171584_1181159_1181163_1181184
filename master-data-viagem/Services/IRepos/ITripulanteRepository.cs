using MDV.Domain.Shared;

namespace MDV.Domain.Tripulantes
{

    public interface ITripulanteRepository : IRepository<Tripulante, TripulanteId>
    {
        
    }
}