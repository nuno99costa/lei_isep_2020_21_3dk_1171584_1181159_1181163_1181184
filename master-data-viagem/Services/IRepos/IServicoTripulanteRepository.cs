using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Shared;

namespace MDV.Domain.ServicosTripulante
{
    public interface IServicoTripulanteRepository : IRepository<ServicoTripulante, ServicoTripulanteId>
    {
        Task<List<ServicoTripulante>> GetAllServicosTripulanteEagerLoading();
        Task<ServicoTripulante> GetServicoTripulanteByIdEagerLoading(ServicoTripulanteId id);
    }
}