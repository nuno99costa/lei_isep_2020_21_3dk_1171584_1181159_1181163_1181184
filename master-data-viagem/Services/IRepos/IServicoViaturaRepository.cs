using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Shared;

namespace MDV.Domain.ServicosViatura
{
    public interface IServicoViaturaRepository : IRepository<ServicoViatura, ServicoViaturaId>
    {
        Task<List<ServicoViatura>> GetAllServicosViaturaEagerLoading();
        Task<ServicoViatura> GetServicoViaturaByIdEagerLoading(ServicoViaturaId id);
    }
}