using MDV.Domain.Shared;

namespace MDV.Domain.Viaturas
{

    public interface IViaturaRepository : IRepository<Viatura, ViaturaId>
    {
        
    }
}