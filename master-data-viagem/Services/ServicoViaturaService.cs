using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using MDV.Mappers;

namespace MDV.Domain.BlocosTrabalho
{
    public class ServicoViaturaService : IServicoViaturaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IServicoViaturaRepository _repo;

        private readonly IViagemRepository _viagemRepo;
        private readonly IBlocoTrabalhoRepository blocoTrabalhoRepository;

        //Construtor com injetaveis
        public ServicoViaturaService(IUnitOfWork unitOfWork, IServicoViaturaRepository repo, IViagemRepository repoViagem, IBlocoTrabalhoRepository blocoRepo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._viagemRepo = repoViagem;
            this.blocoTrabalhoRepository = blocoRepo;

        }

        //Vai ao repositório buscar todos os blocos de trabalho e devolve uma lista de DTOs
        public async Task<List<ServicoViaturaDto>> GetAllAsync()
        {
            List<ServicoViatura> list = await this._repo.GetAllServicosViaturaEagerLoading();

            List<ServicoViaturaDto> listDto = new List<ServicoViaturaDto>();
            foreach (ServicoViatura servicoViatura in list)
            {
                listDto.Add(ServicoViaturaMapper.toDto(servicoViatura));
            }

            return listDto;
        }

        //Vai ao repositório buscar o serviço de viatura com id passado por parametro
        public async Task<ServicoViaturaDto> GetByIdAsync(ServicoViaturaId id)
        {
            ServicoViatura servicoViatura = await this._repo.GetServicoViaturaByIdEagerLoading(id);

            if (servicoViatura == null) return null;

            return ServicoViaturaMapper.toDto(servicoViatura);
        }

        //Adiciona um bloco de trabalho recebendo um DTO.
        public async Task<ServicoViaturaDto> AddAsync(ServicoViaturaDto dto)
        {

            if (String.IsNullOrEmpty(dto.Id))
            {
                throw new BusinessRuleValidationException("ID em falta!");
            }

            //Vai buscar as viagens e valida se todas existem
            List<Viagem> viagens = await getViagens(dto.ViagensIds);

            int duracao = 0;

            foreach (Viagem v in viagens)
            {
                v.sortHorarios();
                int primeiroInstante = v.HorariosPassagem[0].Horario;
                int ultimoInstante = v.HorariosPassagem[v.HorariosPassagem.Count - 1].Horario;
                duracao = duracao + (ultimoInstante - primeiroInstante);
            }

            if (duracao > (24 * 3600)) throw new BusinessRuleValidationException("A duração de um serviço de viatura não pode ser maior que 24h");

            ServicoViatura servicoViatura = new ServicoViatura(dto.Id, duracao, viagens);

            await this._repo.AddAsync(servicoViatura);

            await this._unitOfWork.CommitAsync();

            return ServicoViaturaMapper.toDto(servicoViatura);
        }

        public async Task<ServicoViaturaDto> DeleteAsync(ServicoViaturaId id)
        {
            var servicoViatura = await this._repo.GetByIdAsync(id);

            if (servicoViatura == null) return null;

            this._repo.Remove(servicoViatura);
            await this._unitOfWork.CommitAsync();

            return ServicoViaturaMapper.toDto(servicoViatura);
        }

        private async Task<List<Viagem>> getViagens(List<string> ids)
        {

            List<Viagem> viagens = new List<Viagem>();
            foreach (string id in ids)
            {
                Viagem viagem = await _viagemRepo.GetByIdAsync(new ViagemId(id));
                if (viagem == null)
                {
                    throw new BusinessRuleValidationException("Viagem não existe: " + id.ToString());
                }
                viagens.Add(viagem);
            }

            return viagens;
        }

        public async Task<List<ServicoViaturaPlaneamentoDto>> GetAllForPlaneamento()
        {
            List<ServicoViatura> servicos = await this._repo.GetAllServicosViaturaEagerLoading();
            List<ServicoViaturaPlaneamentoDto> result = new List<ServicoViaturaPlaneamentoDto>();
            foreach (ServicoViatura sv in servicos)
            {
                List<BlocoTrabalho> blocos = await this.blocoTrabalhoRepository.getBlocosByServicoViatura(sv.Id);
                blocos = blocos.OrderBy(s => s.inicio).ToList();
                result.Add(new ServicoViaturaPlaneamentoDto{Id = sv.Id.AsString(), BlocosIds = blocos.ConvertAll<string>(s => s.Id.AsString())});
            }

            return result;

        }
    }


}