using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;
using MDV.Mappers;
using System;
using System.Globalization;

namespace MDV.Domain.Tripulantes
{
    public class TripulanteService : ITripulanteService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripulanteRepository _repo;

        public TripulanteService(IUnitOfWork unitOfWork, ITripulanteRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<TripulanteDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<TripulanteDto> listDto = list.ConvertAll<TripulanteDto>(tripulante => TripulanteMapper.toDto(tripulante));

            return listDto;
        }

        public async Task<TripulanteDto> GetByIdAsync(TripulanteId id)
        {
            Tripulante tripulante = await this._repo.GetByIdAsync(id);

            if (tripulante == null) return null;

            return TripulanteMapper.toDto(tripulante);
        }

        public async Task<TripulanteDto> AddAsync(TripulanteDto dto)
        {
            Tripulante tripulante;

            //if(dto.NMecanografico.Length != 10) throw new BusinessRuleValidationException("Número mecanográfico tem de ter 10 carateres");

            DateTime? dataSaida;

            if(dto.DataSaidaEmpresa == null){
                dataSaida = null;
            } else {
                dataSaida = DateTime.ParseExact(dto.DataSaidaEmpresa, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US"));
            }

            tripulante = new Tripulante(dto.NMecanografico, 
            dto.Nome, 
            DateTime.ParseExact(dto.DataNascimento, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")), 
            dto.NCCidadao, 
            dto.NCConducao, 
            dto.NIF, 
            dto.TipoTripulante, 
            DateTime.ParseExact(dto.DataEntradaEmpresa, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")),
             dataSaida, 
             DateTime.ParseExact(dto.DataLicencaConducao, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("en-US")));

            await this._repo.AddAsync(tripulante);

            await this._unitOfWork.CommitAsync();

            return TripulanteMapper.toDto(tripulante);
        }

        public async Task<TripulanteDto> DeleteAsync(TripulanteId id)
        {
            var tripulante = await this._repo.GetByIdAsync(id);

            if (tripulante == null) return null;

            this._repo.Remove(tripulante);

            await this._unitOfWork.CommitAsync();

            return TripulanteMapper.toDto(tripulante);
        }
    }
}