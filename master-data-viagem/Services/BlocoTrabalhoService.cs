using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Viagens;
using System.Linq;
using MDV.Domain.ServicosViatura;
using MDV.Mappers;
using Microsoft.AspNetCore.Mvc;

namespace MDV.Domain.BlocosTrabalho
{
    public class BlocoTrabalhoService : IBlocoTrabalhoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBlocoTrabalhoRepository _repo;

        private readonly IViagemRepository _viagemRepo;

        private readonly IServicoViaturaRepository _servicoViaturaRepo;

        private readonly IViagemService viagemService;

        //Construtor com injetaveis
        public BlocoTrabalhoService(IUnitOfWork unitOfWork, IBlocoTrabalhoRepository repo, IViagemRepository repoViagem, IServicoViaturaRepository repoServicoViatura, IViagemService viagemService)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._viagemRepo = repoViagem;
            this._servicoViaturaRepo = repoServicoViatura;
            this.viagemService = viagemService;
        }

        //Vai ao repositório buscar todos os blocos de trabalho e devolve uma lista de DTOs
        public async Task<List<BlocoTrabalhoDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllBlocosEagerLoading();

            List<BlocoTrabalhoDto> listDto = list?.ConvertAll<BlocoTrabalhoDto>(blocoTrabalho =>  BlocoTrabalhoMapper.toDto(blocoTrabalho));

            return listDto;
        }

        //Vai ao repositório buscar o bloco de trabalho com id passado por parametro
        public async Task<BlocoTrabalhoDto> GetByIdAsync(BlocoTrabalhoId id)
        {
            BlocoTrabalho bloco = await this._repo.GetBlocoByIdEagerLoading(id);

            if (bloco == null) return null;

            return BlocoTrabalhoMapper.toDto(bloco);
        }

        //Adiciona um bloco de trabalho recebendo um DTO.
        public async Task<BlocoTrabalhoDto> AddAsync(BlocoTrabalhoDto dto)
        {

            ServicoViatura servicoViatura = await this._servicoViaturaRepo.GetServicoViaturaByIdEagerLoading(new ServicoViaturaId(dto.ServicoViaturaId));
            BlocoTrabalho bloco;
            if (dto.Id == null)
            {
                bloco = new BlocoTrabalho(dto.Inicio, dto.Fim, servicoViatura);
            }
            else
            {
                bloco = new BlocoTrabalho(dto.Id, dto.Inicio, dto.Fim, servicoViatura);
            }

            await this._repo.AddAsync(bloco);

            await this._unitOfWork.CommitAsync();

            return BlocoTrabalhoMapper.toDto(bloco);
        }

        public async Task<BlocoTrabalhoDto> DeleteAsync(BlocoTrabalhoId id)
        {
            var bloco = await this._repo.GetByIdAsync(id);

            if (bloco == null) return null;

            this._repo.Remove(bloco);
            await this._unitOfWork.CommitAsync();

            return BlocoTrabalhoMapper.toDto(bloco);
        }

        public async Task<List<BlocoTrabalhoDto>> CriarVariosBlocosAsync(CreatingBlocosDto dto)
        {
            ServicoViatura sv = await this._servicoViaturaRepo.GetServicoViaturaByIdEagerLoading(new ServicoViaturaId(dto.ServicoViaturaId));
            
            BlocoTrabalho bloco; 

            List<Viagem> viagens = sv.Viagens.OrderBy(s => s.HoraSaida).ToList();;
            
            viagens[0].sortHorarios();
            viagens[viagens.Count - 1].sortHorarios();

            int instanteInicio = viagens[0].HorariosPassagem[0].Horario; //Começa na hora de saida da primeira viagem
            int instanteFim = instanteInicio + dto.DuracaoMaxima;

            int ultimaViagemIndex = viagens.Count - 1;
            Viagem ultimaViagem = viagens[ultimaViagemIndex];
            int fimUltimaViagem = ultimaViagem.HorariosPassagem[ultimaViagem.HorariosPassagem.Count - 1].Horario;

            List<BlocoTrabalhoDto> result = new List<BlocoTrabalhoDto>();

            int contador = 0;
            int duracaoApos = dto.DuracaoMaxima;
            int duracaoFinal = 0;

            while(contador < dto.numeroMaximoBlocos){

                if(duracaoApos > sv.DuracaoSegundos){
                    instanteFim = fimUltimaViagem;
                    duracaoFinal += instanteFim - instanteInicio; //O instante de fim é o final da ultima viagem. 
                    bloco = new BlocoTrabalho(instanteInicio, instanteFim, sv);
                    await this._repo.AddAsync(bloco);
                    result.Add(BlocoTrabalhoMapper.toDto(bloco));
                    break;
                }

                bloco = new BlocoTrabalho(instanteInicio, instanteFim, sv);
                await this._repo.AddAsync(bloco);
                result.Add(BlocoTrabalhoMapper.toDto(bloco));
                instanteInicio = instanteFim;
                instanteFim = instanteInicio + dto.DuracaoMaxima;
                duracaoApos += dto.DuracaoMaxima;
                duracaoFinal += dto.DuracaoMaxima;
            }

            if(duracaoFinal > (4 * 3600)) throw new BusinessRuleValidationException("O bloco de trabalho não pode ter mais de 4 horas de duração");

            await this._unitOfWork.CommitAsync();

            return result;


        }

        public async Task<ActionResult<List<BlocoTrabalhoDto>>> GetByServicoViaturaId(ServicoViaturaId servicoViaturaId)
        {
            ServicoViatura sv = await this._servicoViaturaRepo.GetServicoViaturaByIdEagerLoading(servicoViaturaId);

            if(sv == null) throw new BusinessRuleValidationException("Serviço de viatura não encontrado");

            List<BlocoTrabalho> blocos = await this._repo.getBlocosByServicoViatura(sv.Id);

            return blocos.ConvertAll<BlocoTrabalhoDto>(b => BlocoTrabalhoMapper.toDto(b));
        }

        public async Task<ActionResult<List<BlocoTrabalhoPlaneamentoDto>>> GetBlocosPlaneamento()
        {
            List<BlocoTrabalhoPlaneamentoDto> result = new List<BlocoTrabalhoPlaneamentoDto>();
            List<BlocoTrabalho> blocos = await this._repo.GetAllBlocosEagerLoadingWithTrips();
            foreach(BlocoTrabalho b in blocos){
                List<Viagem> viagens = b.servicoViatura.Viagens;
                viagens = viagens.OrderBy(s => s.HoraSaida).ToList();
                result.Add(new BlocoTrabalhoPlaneamentoDto{Id = b.Id.AsString(), Inicio = b.inicio, Fim = b.fim, idsViagens = viagens.ConvertAll<string>(s => s.Id.AsString())});
            }
            return result;
        }
    }


}
