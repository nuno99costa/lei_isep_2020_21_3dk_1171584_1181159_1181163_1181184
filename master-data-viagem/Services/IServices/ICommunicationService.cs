using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Segmentos;

namespace MDV.Utils.Communication{
    public interface ICommunicationService{
        Task<List<SegmentoDto>> GetSegmentosAsync(string percursoId, string token);

    }
}