using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV.Domain.ServicosViatura{
    public interface IServicoViaturaService{
        Task<List<ServicoViaturaDto>> GetAllAsync();

        Task<List<ServicoViaturaPlaneamentoDto>> GetAllForPlaneamento();

        Task<ServicoViaturaDto> GetByIdAsync(ServicoViaturaId id);

        Task<ServicoViaturaDto> AddAsync(ServicoViaturaDto dto);

        Task<ServicoViaturaDto> DeleteAsync(ServicoViaturaId id);

    }
}