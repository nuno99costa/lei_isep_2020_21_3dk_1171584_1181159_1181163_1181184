using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV.Domain.ServicosTripulante{
    public interface IServicoTripulanteService{
        Task<List<ServicoTripulanteDto>> GetAllAsync();

        Task<ServicoTripulanteDto> GetByIdAsync(ServicoTripulanteId id);

        Task<ServicoTripulanteDto> AddAsync(ServicoTripulanteDto dto);

        Task<ServicoTripulanteDto> DeleteAsync(ServicoTripulanteId id);

    }
}