using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.ServicosViatura;
using Microsoft.AspNetCore.Mvc;

namespace MDV.Domain.BlocosTrabalho{
    public interface IBlocoTrabalhoService{
        Task<List<BlocoTrabalhoDto>> GetAllAsync();

        Task<BlocoTrabalhoDto> GetByIdAsync(BlocoTrabalhoId id);

        Task<BlocoTrabalhoDto> AddAsync(BlocoTrabalhoDto dto);

        Task<BlocoTrabalhoDto> DeleteAsync(BlocoTrabalhoId id);

        Task<List<BlocoTrabalhoDto>> CriarVariosBlocosAsync(CreatingBlocosDto dto);
        Task<ActionResult<List<BlocoTrabalhoDto>>> GetByServicoViaturaId(ServicoViaturaId servicoViaturaId);
        Task<ActionResult<List<BlocoTrabalhoPlaneamentoDto>>> GetBlocosPlaneamento();
    }
}