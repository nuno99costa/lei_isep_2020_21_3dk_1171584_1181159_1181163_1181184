using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Viaturas;

namespace MDV.Domain.Viaturas{
    public interface IViaturaService{
        Task<List<ViaturaDto>> GetAllAsync();

        Task<ViaturaDto> GetByIdAsync(ViaturaId id);

        Task<ViaturaDto> AddAsync(ViaturaDto dto);

        Task<ViaturaDto> DeleteAsync(ViaturaId id);

    }
}