
using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.ServicosViatura;
using MDV.Planeamento;
using Microsoft.AspNetCore.Mvc;

namespace MDV.Domain.Viagens{
    public interface IViagemService{
        Task<List<ViagemDto>> GetAllAsync();

        Task<ViagemDto> GetByIdAsync(ViagemId id);

        Task<ViagemDto> AddAsync(CreatingViagemDto dto, string token);

        Task<ViagemDto> DeleteAsync(ViagemId id);

        Task<List<ViagemDto>> getByServicoViaturaIdAsync(ServicoViaturaId id);
        Task<ActionResult<List<DadosPlaneamentoDto>>> getDadosPlaneamento();

    }
}