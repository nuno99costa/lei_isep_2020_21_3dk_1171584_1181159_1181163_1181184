using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Tripulantes;

namespace MDV.Domain.Tripulantes{
    public interface ITripulanteService{
        Task<List<TripulanteDto>> GetAllAsync();

        Task<TripulanteDto> GetByIdAsync(TripulanteId id);

        Task<TripulanteDto> AddAsync(TripulanteDto dto);

        Task<TripulanteDto> DeleteAsync(TripulanteId id);

    }
}