using System.Collections.Generic;
using System.Threading.Tasks;

namespace MDV.Filesystem{
    public interface IFilesystemService{
        Task<string> inserirGLX(string xml, string token);
    }
}