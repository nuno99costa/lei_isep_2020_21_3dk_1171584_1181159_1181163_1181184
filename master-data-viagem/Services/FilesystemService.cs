using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using System.Xml;
using System;
using MDV.Domain.Nos;
using MDV.Domain.Linhas;
using MDV.Domain.Percursos;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosViatura;
using MDV.Domain.ServicosTripulante;

namespace MDV.Filesystem
{
    public class FilesystemService : IFilesystemService
    {
        private readonly IViagemService viagemService;

        private readonly IViagemRepository viagemRepository;

        private readonly IServicoViaturaService servicoViaturaService;

        private readonly IServicoTripulanteRepository stRepo;

        private readonly IBlocoTrabalhoService blocoService;

        private readonly IBlocoTrabalhoRepository blocoRepo;

        private IUnitOfWork unit;

        public FilesystemService(IUnitOfWork unitOfWork, IViagemService viagemService, IViagemRepository viagemRepository, IServicoViaturaService sv, IBlocoTrabalhoService blocoService, IServicoTripulanteRepository stRepo, IBlocoTrabalhoRepository blocoRepo)
        {
            this.viagemService = viagemService;
            this.viagemRepository = viagemRepository;
            this.unit = unitOfWork;
            this.servicoViaturaService = sv;
            this.blocoService = blocoService;
            this.stRepo = stRepo;
            this.blocoRepo = blocoRepo;
        }

        public async Task<string> inserirGLX(string xml, string token)
        {
            try
            {


                //Carregar a string xml
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);

                XmlNode scheduleNode = doc.ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[1].ChildNodes[0];

                //-----------------------------------------------Carregar viagens-------------------------------------------------
                XmlNodeList tripNodes = scheduleNode.ChildNodes[0].ChildNodes;
                Dictionary<string, XmlNode> viagens = new Dictionary<string, XmlNode>();
                foreach (XmlNode trip in tripNodes)
                {
                    if (trip.Attributes["Line"] == null) continue;
                    string id = trip.Attributes["key"].Value;
                    viagens.Add(id, trip);
                }

                await this.carregarViagens(viagens);

                //-----------------------------------------------Carregar Serviços de viatura e blocos de trabalho--------------------------------------
                XmlNodeList vehicleDutiesNodes = scheduleNode.ChildNodes[2].ChildNodes;
                XmlNodeList workBlocksNodes = scheduleNode.ChildNodes[1].ChildNodes;

                Dictionary<string, XmlNode> blocos = new Dictionary<string, XmlNode>();
                foreach (XmlNode bloco in workBlocksNodes)
                {
                    string id = bloco.Attributes["key"].Value;
                    blocos.Add(id, bloco);
                }

                Dictionary<string, XmlNode> servicosViatura = new Dictionary<string, XmlNode>();
                foreach (XmlNode servicoViatura in vehicleDutiesNodes)
                {
                    string id = servicoViatura.Attributes["key"].Value;
                    servicosViatura.Add(id, servicoViatura);
                }

                await this.carregarServicosViaturaEBlocos(servicosViatura, blocos, viagens);
                //-----------------------------------------------Carregar Serviços de tripulante----------------------
                XmlNodeList driverDutiesNodes = scheduleNode.ChildNodes[3].ChildNodes;
                Dictionary<string, XmlNode> servicosTripulante = new Dictionary<string, XmlNode>();
                foreach (XmlNode sTrip in driverDutiesNodes)
                {
                    string id = sTrip.Attributes["key"].Value;
                    servicosTripulante.Add(id, sTrip);
                }
                await this.carregarServicosTripulante(servicosTripulante);

                await this.unit.CommitAsync();

                return "Inserido";
            }
            catch (BusinessRuleValidationException ex)
            {
                return ex.Message;
            }
            catch (Exception ex)
            {
                return ex.StackTrace;
            }
        }

        private async Task carregarServicosTripulante(Dictionary<string, XmlNode> servicosTripulante)
        {
            foreach (KeyValuePair<string, XmlNode> st in servicosTripulante)
            {
                string id = st.Value.Attributes["key"].Value;
                List<BlocoTrabalho> blocosST = new List<BlocoTrabalho>();
                int duracao = 0;
                foreach (XmlNode bloco in st.Value.ChildNodes[0].ChildNodes)
                {
                    BlocoTrabalho b = await this.blocoRepo.GetBlocoByIdEagerLoading(new BlocoTrabalhoId(bloco.Attributes["key"].Value));
                    blocosST.Add(b);
                    duracao += (b.fim - b.inicio);
                }

                ServicoTripulante dto = new ServicoTripulante(id, duracao, blocosST);
                await this.stRepo.AddAsync(dto);
            }
        }

        private async Task carregarViagens(Dictionary<string, XmlNode> trips)
        {


            foreach (KeyValuePair<string, XmlNode> trip in trips)
            {
                string id = trip.Value.Attributes["key"].Value;
                string line = trip.Value.Attributes["Line"].Value;
                string path = trip.Value.Attributes["Path"].Value;
                List<HorarioPassagem> horarios = new List<HorarioPassagem>();
                foreach (XmlNode passingTime in trip.Value.ChildNodes[0].ChildNodes)
                {
                    string idNoInicio = passingTime.Attributes["Node"].Value;
                    int horaInicio = Int32.Parse(passingTime.Attributes["Time"].Value);
                    horarios.Add(new HorarioPassagem(new NoId(idNoInicio), horaInicio));
                }
                Viagem v = new Viagem(id, new PercursoId(path), new LinhaId(line), horarios, Int32.Parse(trip.Value.ChildNodes[0].ChildNodes[0].Attributes["Time"].Value));
                await this.viagemRepository.AddAsync(v);


            }

            await this.unit.CommitAsync();
        }

        private async Task carregarServicosViaturaEBlocos(Dictionary<string, XmlNode> servicosViatura, Dictionary<string, XmlNode> blocos, Dictionary<string, XmlNode> viagensGuardadas)
        {
            foreach (KeyValuePair<string, XmlNode> sv in servicosViatura)
            {
                string id = sv.Value.Attributes["key"].Value;

                //Buscar as viagens dos blocos
                List<string> trips = await this.getViagensSV(sv.Value.ChildNodes[0].ChildNodes, blocos, viagensGuardadas);

                ServicoViaturaDto dto = new ServicoViaturaDto { Id = id, ViagensIds = trips, DuracaoSegundos = 0 };
                await this.servicoViaturaService.AddAsync(dto);

                await this.carregarBlocos(sv.Value.ChildNodes[0].ChildNodes, blocos, id);
            }

            await this.unit.CommitAsync();
        }

        private Task<List<string>> getViagensSV(XmlNodeList blocosSV, Dictionary<string, XmlNode> blocos, Dictionary<string, XmlNode> viagens)
        {
            List<string> result = new List<string>();
            foreach (XmlNode blocoSV in blocosSV)
            {
                string idBloco = blocoSV.Attributes["key"].Value;
                XmlNode bloco = blocos.GetValueOrDefault(idBloco);
                foreach(XmlNode viagem in bloco.ChildNodes[0].ChildNodes){
                    string viagemId = viagem.Attributes["key"].Value;
                    if(viagens.ContainsKey(viagemId)) result.Add(viagemId);
                }
            }
            return Task.Run(() => result);
        }

        private async Task carregarBlocos(XmlNodeList blocosSV, Dictionary<string, XmlNode> blocos, string idSV)
        {

            foreach (XmlNode blocoSV in blocosSV)
            {
                string idBloco = blocoSV.Attributes["key"].Value;
                XmlNode bloco = blocos.GetValueOrDefault(idBloco);

                string id = bloco.Attributes["key"].Value;
                int inicio = Int32.Parse(bloco.Attributes["StartTime"].Value);
                int fim = Int32.Parse(bloco.Attributes["EndTime"].Value);
                BlocoTrabalhoDto dto = new BlocoTrabalhoDto { Id = id, Inicio = inicio, Fim = fim, ServicoViaturaId = idSV };
                await this.blocoService.AddAsync(dto);


            }

        }


    }
}