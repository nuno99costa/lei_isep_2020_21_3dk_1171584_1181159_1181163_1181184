using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;
using MDV.Domain.Percursos;
using MDV.Domain.Linhas;
using System;
using MDV.Domain.Nos;
using MDV.Domain.Segmentos;
using MDV.Domain.Communication;
using Microsoft.AspNetCore.Mvc;
using MDV.Domain.ServicosViatura;
using MDV.Utils.Communication;
using MDV.Planeamento;
using System.Linq;

namespace MDV.Domain.Viagens
{
    public class ViagemService : IViagemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IViagemRepository _repo;

        private ICommunicationService communicationService;

        private readonly IServicoViaturaRepository _svRepository;

        public ViagemService(IUnitOfWork unitOfWork, IViagemRepository repo, ICommunicationService communicationService, IServicoViaturaRepository svRepo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this.communicationService = communicationService;
            this._svRepository = svRepo;
        }

        public async Task<List<ViagemDto>> GetAllAsync()
        {
            List<Viagem> list = await this._repo.GetAllAsync();

            List<ViagemDto> listDto = new List<ViagemDto>();

            foreach (Viagem viagem in list)
            {
                listDto.Add(ViagemMapper.toDto(viagem));
            }

            return listDto;
        }

        public async Task<ViagemDto> GetByIdAsync(ViagemId id)
        {
            Viagem viagem = await this._repo.GetByIdAsync(id);

            if (viagem == null) return null;

            return ViagemMapper.toDto(viagem);
        }

        public async Task<List<ViagemDto>> getByServicoViaturaIdAsync(ServicoViaturaId servicoViaturaId)
        {
            ServicoViatura sv = await this._svRepository.GetServicoViaturaByIdEagerLoading(servicoViaturaId);

            List<ViagemDto> result = new List<ViagemDto>();
            foreach (Viagem viagem in sv.Viagens)
            {
                result.Add(ViagemMapper.toDto(viagem));
            }

            return result;
        }

        public async Task<ViagemDto> AddAsync(CreatingViagemDto dto, string token)
        {

            List<HorarioPassagem> horariosPassagem = await this.calcularHorariosPassagem(dto, token);

            Viagem viagem;
            if (dto.ViagemId == null)
            {
                viagem = new Viagem(new PercursoId(dto.IdPercurso), new LinhaId(dto.IdLinha), horariosPassagem, horariosPassagem[0].Horario);
            }
            else
            {
                viagem = new Viagem(dto.ViagemId.ToString(), new PercursoId(dto.IdPercurso), new LinhaId(dto.IdLinha), horariosPassagem, horariosPassagem[0].Horario);
            }

            await this._repo.AddAsync(viagem);

            await this._unitOfWork.CommitAsync();

            return ViagemMapper.toDto(viagem);
        }


        public async Task<ViagemDto> DeleteAsync(ViagemId id)
        {
            var viagem = await this._repo.GetByIdAsync(id);

            if (viagem == null) return null;

            this._repo.Remove(viagem);
            await this._unitOfWork.CommitAsync();

            return ViagemMapper.toDto(viagem);
        }



        //Vai buscar todos os segmentos do percurso ao MDR
        public async Task<List<SegmentoDto>> GetSegmentosAsync(string percursoId, string token)
        {
            return await this.communicationService.GetSegmentosAsync(percursoId, token);
        }

        private async Task<List<HorarioPassagem>> calcularHorariosPassagem(CreatingViagemDto dto, string token)
        {
            //Cria a lista que vamos retornar
            List<HorarioPassagem> horariosPassagem = new List<HorarioPassagem>();

            //Vai buscar todos os segmentos do percurso
            List<SegmentoDto> segmentos = await this.GetSegmentosAsync(dto.IdPercurso, token);

            //Para o primeiro nó do percurso, ou seja, o nó de sáida 
            HorarioPassagem horarioSaida = new HorarioPassagem(new NoId(dto.IdNoSaida), dto.HorarioSaida);

            horariosPassagem.Add(horarioSaida);

            int index;
            int horaSaida = dto.HorarioSaida; //Hora de saída no primeiro nó do percurso

            for (index = 0; index < segmentos.Count; index++)
            {
                NoId no = new NoId(segmentos[index].idNoFinal);
                horaSaida += segmentos[index].tempoNos;
                HorarioPassagem horario = new HorarioPassagem(no, horaSaida);

                horariosPassagem.Add(horario);
            }

            return horariosPassagem;
        }

        public async Task<ActionResult<List<DadosPlaneamentoDto>>> getDadosPlaneamento()
        {
            var viagens = await this._repo.GetAllAsync();
            List<DadosPlaneamentoDto> result = new List<DadosPlaneamentoDto>();

            foreach (Viagem v in viagens)
            {
                List<int> horarios = new List<int>();
                foreach (HorarioPassagem h in v.HorariosPassagem)
                {
                    horarios.Add(h.Horario);
                }
                horarios.Sort();
                result.Add(new DadosPlaneamentoDto { ViagemId = v.Id.AsString(), IdPercurso = v.IdPercurso.AsString(), horarios = horarios });
            }

            return result;
        }
    }
}