using System.Threading.Tasks;
using System.Collections.Generic;
using MDV.Domain.Shared;
using MDV.Domain.Percursos;
using MDV.Domain.Linhas;
using System;
using MDV.Domain.Nos;
using System.Net.Http;
using MDV.Domain.Segmentos;
using Newtonsoft.Json;
using MDV.Utils.Communication;

namespace MDV.Domain.Communication
{
    public class CommunicationService : ICommunicationService
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly String mdrURI = "https://lei-isep-lapr5-g05-mdr.herokuapp.com";

        public CommunicationService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<List<SegmentoDto>> GetSegmentosAsync(string percursoId, string token)
        {

            using (var client = new HttpClient())
            {
                
                    client.BaseAddress = new Uri(mdrURI);

                    String endereco = String.Concat("/api/percurso/segmentos/", percursoId);



                    client.DefaultRequestHeaders.Add("auth-token", token);

                    var result = await client.GetAsync(endereco);

                    string resultContent = await result.Content.ReadAsStringAsync();

                    List<SegmentoDto> dtos = JsonConvert.DeserializeObject<List<SegmentoDto>>(resultContent);

                    return dtos;
                
            }

        }
    }
}