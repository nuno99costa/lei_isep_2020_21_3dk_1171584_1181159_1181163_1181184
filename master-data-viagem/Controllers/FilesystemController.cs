using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Filesystem;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FilesystemController : ControllerBase
    {
        private readonly IFilesystemService _service;
        
        public FilesystemController(IFilesystemService service)
        {
            _service = service;
        }

        [HttpPost]
        [Consumes("text/plain")]
        public async Task<ActionResult<string>> InserirGLX([FromBody] string xml)
        {
            string token = this.HttpContext.Request.Headers["auth-token"];
            var result = await this._service.inserirGLX(xml, token);

            return result;
        }

    }
}