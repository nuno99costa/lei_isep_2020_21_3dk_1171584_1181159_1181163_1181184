using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosViatura;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicosViaturaController : ControllerBase
    {
        private readonly IServicoViaturaService _service;

        public ServicosViaturaController(IServicoViaturaService service)
        {
            _service = service;
        }

        // GET: api/ServicosViatura
        [HttpGet]
        public async Task<ActionResult<List<ServicoViaturaDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/BlocosTrabalho/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServicoViaturaDto>> GetGetById(string id)
        {
            var servicoViatura = await _service.GetByIdAsync(new ServicoViaturaId(id));

            if (servicoViatura == null) return NotFound();

            return servicoViatura;
        }

        // GET: api/BlocosTrabalho/5
        [HttpGet("planeamento")]
        public async Task<ActionResult<List<ServicoViaturaPlaneamentoDto>>> GetAllForPlaneamento()
        {
            return await _service.GetAllForPlaneamento();
        }

        // POST: api/BlocosTrabalho
        [HttpPost]
        public async Task<ActionResult<ServicoViaturaDto>> Create(ServicoViaturaDto dto)
        {
            var servicoViatura = await _service.AddAsync(dto);

            return servicoViatura;
        }

        //api/BlocosTrabalho/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<ServicoViaturaDto>> HardDelete(string id)
        {
            try
            {
                var servicoViatura = await _service.DeleteAsync(new ServicoViaturaId(id));

                if (servicoViatura == null) return NotFound();

                return Ok(servicoViatura);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}