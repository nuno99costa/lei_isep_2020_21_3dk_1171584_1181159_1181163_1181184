using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.Tripulantes;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripulantesController : ControllerBase
    {
        private readonly ITripulanteService _service;
        
        public TripulantesController(ITripulanteService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TripulanteDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TripulanteDto>> GetGetById(string id)
        {
            var tripulante = await _service.GetByIdAsync(new TripulanteId(id));

            if (tripulante == null) return NotFound();

            return tripulante;
        }

        [HttpPost]
        public async Task<ActionResult<TripulanteDto>> Create(TripulanteDto dto)
        {
            var tripulante = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = tripulante.NMecanografico }, tripulante);
        }

        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<TripulanteDto>> HardDelete(string id)
        {
            try
            {
                var tripulante = await _service.DeleteAsync(new TripulanteId(id));

                if (tripulante == null) return NotFound();

                return Ok(tripulante);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}