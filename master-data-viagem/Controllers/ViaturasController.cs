using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.Viaturas;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ViaturasController : ControllerBase
    {
        private readonly IViaturaService _service;
        
        public ViaturasController(IViaturaService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViaturaDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ViaturaDto>> GetGetById(string id)
        {
            var viatura = await _service.GetByIdAsync(new ViaturaId(id));

            if (viatura == null) return NotFound();

            return viatura;
        }

        [HttpPost]
        public async Task<ActionResult<ViaturaDto>> Create(ViaturaDto dto)
        {
            var viatura = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = viatura.Vin }, viatura);
        }

        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<ViaturaDto>> HardDelete(string id)
        {
            try
            {
                var viatura = await _service.DeleteAsync(new ViaturaId(id));

                if (viatura == null) return NotFound();

                return Ok(viatura);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}