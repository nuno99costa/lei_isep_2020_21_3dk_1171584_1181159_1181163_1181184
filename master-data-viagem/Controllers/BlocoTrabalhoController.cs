using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosViatura;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlocosTrabalhoController : ControllerBase
    {
        private readonly IBlocoTrabalhoService _service;

        public BlocosTrabalhoController(IBlocoTrabalhoService service)
        {
            _service = service;
        }

        // GET: api/BlocosTrabalho
        [HttpGet]
        public async Task<ActionResult<List<BlocoTrabalhoDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/BlocosTrabalho/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BlocoTrabalhoDto>> GetGetById(string id)
        {
            var blocoTrabalho = await _service.GetByIdAsync(new BlocoTrabalhoId(id));

            if (blocoTrabalho == null) return NotFound();

            return blocoTrabalho;
        }

        [HttpGet("planeamento")]
        public async Task<ActionResult<List<BlocoTrabalhoPlaneamentoDto>>> GetBlocosPlaneamento()
        {
            return await _service.GetBlocosPlaneamento();
        }

        // POST: api/BlocosTrabalho
        [HttpPost]
        public async Task<ActionResult<BlocoTrabalhoDto>> Create(BlocoTrabalhoDto dto)
        {
            try
            {
                var blocoTrabalho = await _service.AddAsync(dto);

                return blocoTrabalho;
            }
            catch (BusinessRuleValidationException ex)
            {
                throw ex;
            }

        }

        [HttpPost("Multiple")]
        public async Task<ActionResult<List<BlocoTrabalhoDto>>> CreateMultiple(CreatingBlocosDto dto){
            try
            {
                var blocoTrabalho = await _service.CriarVariosBlocosAsync(dto);

                return blocoTrabalho;
            }
            catch (BusinessRuleValidationException ex)
            {
                throw ex;
            }
        }

        [HttpGet("ServicoViatura/{id}")]
        public async Task<ActionResult<List<BlocoTrabalhoDto>>> getByServicoViaturaId(string id){
            return await this._service.GetByServicoViaturaId(new ServicoViaturaId(id));
        }

        //api/BlocosTrabalho/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<BlocoTrabalhoDto>> HardDelete(string id)
        {
            try
            {
                var blocoTrabalho = await _service.DeleteAsync(new BlocoTrabalhoId(id));

                if (blocoTrabalho == null) return NotFound();

                return Ok(blocoTrabalho);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
