using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using Microsoft.AspNetCore.Cors;
using MDV.Domain.Segmentos;
using MDV.Domain.ServicosViatura;
using Microsoft.AspNetCore.Http;
using MDV.Planeamento;

namespace MDV.Controllers
{
    [EnableCors("_MDVCorsPolicy")]
    [Route("api/[controller]")]
    [ApiController]
    public class ViagensController : ControllerBase
    {
        private readonly IViagemService _service;

        public ViagensController(IViagemService service)
        {
            _service = service;
        }

        // GET: api/Viagens
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ViagemDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Viagens/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ViagemDto>> GetGetById(string id)
        {
            var viagem = await _service.GetByIdAsync(new ViagemId(id));

            if (viagem == null) return NotFound();

            return viagem;
        }

        [HttpGet("servicoViatura/{id}")]
        public async Task<ActionResult<List<ViagemDto>>> GetByServicoViaturaId(string id){
            return await _service.getByServicoViaturaIdAsync(new ServicoViaturaId(id));
        }

        // POST: api/Viagens
        [HttpPost]
        public async Task<ActionResult<CreatingViagemDto>> Create(CreatingViagemDto dto)
        {
            string token = this.HttpContext.Request.Headers["auth-token"];
            var viagem = await _service.AddAsync(dto, token);

            return CreatedAtAction(nameof(GetGetById), new { id = viagem.ViagemId }, viagem);
        }

        //api/Viagens/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<ViagemDto>> HardDelete(string id)
        {
            try
            {
                var viagem = await _service.DeleteAsync(new ViagemId(id));

                if (viagem == null) return NotFound();

                return Ok(viagem);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        [HttpGet("planeamento")]
        public async Task<ActionResult<List<DadosPlaneamentoDto>>> getDadosPlaneamento(){
            return await this._service.getDadosPlaneamento();
        }
    }
}
