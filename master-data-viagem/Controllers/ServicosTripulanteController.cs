using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using MDV.Domain.ServicosTripulante;

namespace MDV.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicosTripulanteController : ControllerBase
    {
        private readonly IServicoTripulanteService _service;

        public ServicosTripulanteController(IServicoTripulanteService service)
        {
            _service = service;
        }

        // GET: api/ServicosTripulante
        [HttpGet]
        public async Task<ActionResult<List<ServicoTripulanteDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/BlocosTrabalho/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ServicoTripulanteDto>> GetGetById(string id)
        {
            var servicoTripulante = await _service.GetByIdAsync(new ServicoTripulanteId(id));

            if (servicoTripulante == null) return NotFound();

            return servicoTripulante;
        }

        // POST: api/BlocosTrabalho
        [HttpPost]
        public async Task<ActionResult<ServicoTripulanteDto>> Create(ServicoTripulanteDto dto)
        {
            var servicoTripulante = await _service.AddAsync(dto);

            return servicoTripulante;
        }

        //api/BlocosTrabalho/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<ServicoTripulanteDto>> HardDelete(string id)
        {
            try
            {
                var servicoTripulante = await _service.DeleteAsync(new ServicoTripulanteId(id));

                if (servicoTripulante == null) return NotFound();

                return Ok(servicoTripulante);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}