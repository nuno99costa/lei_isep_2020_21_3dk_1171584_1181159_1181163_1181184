using MDV.Domain.ServicosTripulante;

namespace MDV.Mappers{
    public class ServicoTripulanteMapper{
        public static ServicoTripulanteDto toDto(ServicoTripulante st){
            return new ServicoTripulanteDto { Id = st.Id.AsString(), 
                                            Duracao = st.Duracao, 
                                            IdsBlocosTrabalho = st.BlocosTrabalho.ConvertAll<string>(st => st.Id.AsString())
                                            };
        }
    }
}