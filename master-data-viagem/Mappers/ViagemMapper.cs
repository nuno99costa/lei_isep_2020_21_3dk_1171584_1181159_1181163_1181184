

using System;
using System.Collections.Generic;

namespace MDV.Domain.Viagens{

    public class ViagemMapper{
        public static List<string> toStringIDs(List<Viagem> viagens){
            List<string> lista = new List<string>();
            foreach(Viagem v in viagens){
                lista.Add(v.Id.AsString());
            }

            return lista;
        }

        public static ViagemDto toDto(Viagem viagem)
        {
            return new ViagemDto { ViagemId = viagem.Id.AsString(), IdPercurso = viagem.IdPercurso.AsString(), IdLinha = viagem.IdLinha.AsString(), HorariosPassagem = viagem.HorariosPassagem };
        }
    }

}