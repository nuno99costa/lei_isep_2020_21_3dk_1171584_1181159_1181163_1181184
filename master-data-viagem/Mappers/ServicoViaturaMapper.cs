using System.Globalization;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Viagens;

namespace MDV.Mappers{
    public class ServicoViaturaMapper{
        public static ServicoViaturaDto toDto(ServicoViatura servicoViatura){
            return new ServicoViaturaDto { Id = servicoViatura.Id.AsString(), 
            DuracaoSegundos = servicoViatura.DuracaoSegundos, 
            ViagensIds = ViagemMapper.toStringIDs(servicoViatura.Viagens) };
        }
    }
}