using MDV.Domain.BlocosTrabalho;

namespace MDV.Mappers{
    public class BlocoTrabalhoMapper{
        public static BlocoTrabalhoDto toDto(BlocoTrabalho bloco){
            return new BlocoTrabalhoDto { Id = bloco.Id.AsString(), Inicio = bloco.inicio, Fim = bloco.fim, ServicoViaturaId = bloco.servicoViatura.Id.AsString() };
        }
    }
}