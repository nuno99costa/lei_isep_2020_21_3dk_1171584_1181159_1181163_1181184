using MDV.Domain.Tripulantes;

namespace MDV.Mappers{
    public class TripulanteMapper{
        public static TripulanteDto toDto(Tripulante tripulante){
            string dataSaida;
            if(!tripulante.DataSaidaEmpresa.HasValue){
                dataSaida = "Nao definido...Ainda";
            }else{
                dataSaida = tripulante.DataSaidaEmpresa.Value.ToString("dd/MM/yyyy");
            }
            return new TripulanteDto
            {
                NMecanografico = tripulante.Id.AsString(),
                Nome = tripulante.Nome,
                DataNascimento = tripulante.DataNascimento.ToString("dd/MM/yyyy"),
                NCCidadao = tripulante.NCCidadao,
                NCConducao = tripulante.NCConducao,
                NIF = tripulante.NIF,
                TipoTripulante = tripulante.TipoTripulante,
                DataEntradaEmpresa = tripulante.DataEntradaEmpresa.ToString("dd/MM/yyyy"),
                DataSaidaEmpresa = dataSaida,
                DataLicencaConducao = tripulante.DataLicencaConducao.ToString("dd/MM/yyyy")
            };
        }
    }
}