using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace MDV.Domain.Viagens
{
    public class ViagemId : EntityId
    {
        [JsonConstructor]

        public ViagemId(){

        }
        
        public ViagemId(object value) : base(value)
        {
        }

        public ViagemId(String value) : base(value)
        {
        }

        override
        protected Object createFromString(String text)
        {
            return text;
        }

        override
        public String AsString()
        {
            return (String)base.ObjValue;
        }

    }
}