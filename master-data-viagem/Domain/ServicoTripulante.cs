using System;
using System.Collections.Generic;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Shared;
using MDV.Domain.Tripulantes;

namespace MDV.Domain.ServicosTripulante{
    public class ServicoTripulante : Entity<ServicoTripulanteId>, IAggregateRoot{


        public int Duracao{get; private set;}

        public List<BlocoTrabalho> BlocosTrabalho{get; private set;}

        public ServicoTripulante()
        {
        }

        public ServicoTripulante(string Id, int duracao, List<BlocoTrabalho> blocosTrabalho)
        {
            this.Id = new ServicoTripulanteId(Id);
            this.Duracao = duracao;
            this.BlocosTrabalho = blocosTrabalho;
        }
    }
}