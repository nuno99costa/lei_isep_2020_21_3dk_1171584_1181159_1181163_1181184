using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace MDV.Domain.Nos
{
    public class NoId : EntityId
    {

        public NoId(String value) : base(value)
        {
        }

        override
        protected String createFromString(String text)
        {
            return text;
        }

        override
        public String AsString()
        {
            return this.Value;
        }

    }
}