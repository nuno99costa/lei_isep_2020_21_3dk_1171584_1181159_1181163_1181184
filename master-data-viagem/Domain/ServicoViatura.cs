using System;
using System.Collections.Generic;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;

namespace MDV.Domain.ServicosViatura{
    public class ServicoViatura : Entity<ServicoViaturaId>, IAggregateRoot{

        public int DuracaoSegundos{get; private set;}

        public List<Viagem> Viagens{get; private set;}

        //-----------------------------------AUXILIARES PARA O ENTITY FRAMEWORK CORE-------------------------------------
        public List<BlocoTrabalho> blocosTrabalho;
        //---------------------------------------------------------------------------------------------------------------

        public ServicoViatura()
        {
        }

        public ServicoViatura(string Id, int duracao, List<Viagem> viagens)
        {
            this.Id = new ServicoViaturaId(Id);
            this.DuracaoSegundos = duracao;
            this.Viagens = viagens;
        }
    }
}