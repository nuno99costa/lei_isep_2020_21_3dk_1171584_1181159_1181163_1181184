using System;
using System.Text.Json.Serialization;
using MDV.Domain.Shared;

namespace MDV.Domain.ServicosViatura{
    public class ServicoViaturaId : EntityId
    {

                [JsonConstructor]

        public ServicoViaturaId(){

        }

        public ServicoViaturaId(String value) : base(value)
        {
        }
        public override string AsString()
        {
            return this.Value;
        }

        protected override object createFromString(string text)
        {
            /* if(text.Length != 10){
                throw new BusinessRuleValidationException("Código do serviço de viatura tem que ser de tamanho 10.");
            }else{
                return text;
            } */
            return text;
        }
    }
}