using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace  MDV.Domain.Tripulantes
{
    
    public class TripulanteId : EntityId
    {
        
        [JsonConstructor]
        public TripulanteId(object value) : base(value)
        {
        }

        public TripulanteId(String value) : base(value)
        {
        }  

        override
        protected Object createFromString(String text)
        {
            return text;
        } 

        override
        public String AsString()
        {
            return (String)base.ObjValue;
        }

    }
}