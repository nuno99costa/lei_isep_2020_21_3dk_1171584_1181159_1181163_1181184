using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace  MDV.Domain.Viaturas
{
    
    public class ViaturaId : EntityId
    {
        
        [JsonConstructor]
        public ViaturaId(object value) : base(value)
        {
        }

        public ViaturaId(String value) : base(value)
        {
        }  

        override
        protected Object createFromString(String text)
        {
            return text;
        } 

        override
        public String AsString()
        {
            return (String)base.ObjValue;
        }

    }
}