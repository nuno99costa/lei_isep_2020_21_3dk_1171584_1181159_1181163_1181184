using System;
using MDV.Domain.Shared;

namespace MDV.Domain.Percursos
{
    public class PercursoId : EntityId
    {

        public PercursoId(String value) : base(value)
        {
        }

        override
        protected String createFromString(String text)
        {
            return text;
        }

        override
        public String AsString()
        {
            return this.Value;
        }

    }
}