using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace  MDV.Domain.ServicosTripulante
{
    
    public class ServicoTripulanteId : EntityId
    {
        
        [JsonConstructor]
        public ServicoTripulanteId(){
        }

        public ServicoTripulanteId(String value) : base(value)
        {
        }  

        override
        protected Object createFromString(String text)
        {
            return text;
        } 

        override
        public String AsString()
        {
            return (String)base.ObjValue;
        }

    }
}