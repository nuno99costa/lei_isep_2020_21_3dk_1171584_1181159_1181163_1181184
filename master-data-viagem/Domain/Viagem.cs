using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Linhas;
using MDV.Domain.Percursos;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;

namespace MDV.Domain.Viagens
{
    public class Viagem : Entity<ViagemId>, IAggregateRoot
    {
        public PercursoId IdPercurso { get; private set; }

        public LinhaId IdLinha { get; private set; }

        public List<HorarioPassagem> HorariosPassagem { get; private set; }

        public int HoraSaida{get; private set;}

        //------------------------------------AUXILIARES PARA O ENTITY FRAMEWORK CORE----------------------------
        public ServicoViatura servicoViatura;

        //---------------------------------------------------------------------------------------------------------

        public Viagem()
        {
        }
        public Viagem(String Id, PercursoId IdPercurso, LinhaId IdLinha, List<HorarioPassagem> HorariosPassagem, int horaSaida)
        {
            this.Id = new ViagemId(Id);
            this.IdPercurso = IdPercurso;
            this.IdLinha = IdLinha;
            this.HorariosPassagem = HorariosPassagem;
            this.HoraSaida = horaSaida;
        }

        public Viagem(PercursoId IdPercurso, LinhaId IdLinha, List<HorarioPassagem> HorariosPassagem, int horaSaida)
        {
            this.Id = new ViagemId(Guid.NewGuid().ToString());
            this.IdPercurso = IdPercurso;
            this.IdLinha = IdLinha;
            this.HorariosPassagem = HorariosPassagem;
            this.HoraSaida = horaSaida;
        }

        public void sortHorarios(){
            this.HorariosPassagem = this.HorariosPassagem.OrderBy(o => o.Horario).ToList<HorarioPassagem>();
        }
    }
}
