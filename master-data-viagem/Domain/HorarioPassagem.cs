using MDV.Domain.Nos;
using MDV.Domain.Shared;

namespace MDV.Domain.Viagens
{
    public class HorarioPassagem : IValueObject
    {

        public NoId IdNo { get; private set; }

        public int Horario { get; private set; } //tempo em segundos depois da meia noite

        private HorarioPassagem()
        {
        }

        public HorarioPassagem(NoId IdNo, int Horario)
        {
            this.IdNo = IdNo;
            this.Horario = Horario;
        }
    }
}