using System;
using System.Collections.Generic;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;
using MDV.Domain.Viagens;

namespace MDV.Domain.BlocosTrabalho
{
    public class BlocoTrabalho : Entity<BlocoTrabalhoId>, IAggregateRoot
    {
        public int inicio { get; private set; }

        public int fim { get; private set; }

        public ServicoViatura servicoViatura{get; private set;}

        //----------------------Auxiliar EF Core --------------------------------

        public ServicoTripulante servicoTripulante;

        public BlocoTrabalho()
        {
        }

        public BlocoTrabalho(String Id, int inicio, int fim, ServicoViatura servicoViatura)
        {
            this.Id = new BlocoTrabalhoId(Id);
            this.inicio = inicio;
            this.fim = fim;
            this.servicoViatura = servicoViatura;
        }
        public BlocoTrabalho(int inicio, int fim, ServicoViatura servicoViatura)
        {
            this.Id = new BlocoTrabalhoId(Guid.NewGuid().ToString());
            this.inicio = inicio;
            this.fim = fim;
            this.servicoViatura = servicoViatura;
        }

    }
}