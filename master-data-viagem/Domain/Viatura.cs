using System;
using System.Collections.Generic;
using MDV.Domain.ServicosViatura;
using MDV.Domain.Shared;

namespace MDV.Domain.Viaturas
{

    public class Viatura : Entity<ViaturaId>, IAggregateRoot
    {

        public string Vin { get; private set; }

        public string Matricula { get; private set; }
        
        public string TipoViatura { get; private set; }

        public DateTime Data { get; private set; }

        //--------------------AUXILIAR PARA EF CORE-------------------------------

        //----------------------------------------------------------------------------

        public Viatura()
        {

        }

        public Viatura(string Vin, string Matricula, string TipoViatura, DateTime Data){
            this.Id = new ViaturaId(Vin);
            this.Vin = Vin;
            this.Matricula = Matricula;
            this.TipoViatura = TipoViatura;
            this.Data = Data;
        }

    }
}