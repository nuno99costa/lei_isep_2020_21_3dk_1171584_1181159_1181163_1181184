using MDV.Domain.Shared;
using MDV.Domain.Viagens;

namespace MDV.Domain.BlocosTrabalho
{
    public class BlocosTrabalhoViagem
    {
        public BlocoTrabalhoId BlocoTrabalhoId { get; private set; }

        public BlocoTrabalho BlocoTrabalho {get; private set; }

        public ViagemId ViagemId {get; private set; }

        public Viagem Viagem {get; private set; }

        public BlocosTrabalhoViagem(){

        }

        public BlocosTrabalhoViagem(BlocoTrabalhoId blocoTrabalhoId, BlocoTrabalho blocoTrabalho, ViagemId viagemId, Viagem viagem){
            this.BlocoTrabalho = blocoTrabalho;
            this.BlocoTrabalhoId = blocoTrabalhoId;
            this.ViagemId = viagemId;
            this.Viagem = viagem;
        }
    }
}