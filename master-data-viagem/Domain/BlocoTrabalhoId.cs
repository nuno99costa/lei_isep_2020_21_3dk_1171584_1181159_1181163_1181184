using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace MDV.Domain.BlocosTrabalho
{
    public class BlocoTrabalhoId : EntityId
    {
        [JsonConstructor]
        public BlocoTrabalhoId(object value) : base(value)
        {
        }

        public BlocoTrabalhoId(String value) : base(value)
        {
        }

        override
        protected Object createFromString(String text)
        {
            return text;
        }

        override
        public String AsString()
        {
            return this.Value;
        }

    }
}