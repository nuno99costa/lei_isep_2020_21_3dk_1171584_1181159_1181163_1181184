using System;
using MDV.Domain.Shared;
using Newtonsoft.Json;

namespace MDV.Domain.Linhas
{
    public class LinhaId : EntityId
    {

        public LinhaId(String value) : base(value)
        {
        }

        override
        protected String createFromString(String text)
        {
            return text;
        }

        override
        public String AsString()
        {
            return this.Value;
        }

    }
}