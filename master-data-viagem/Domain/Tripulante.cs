using System;
using System.Collections.Generic;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.Shared;

namespace MDV.Domain.Tripulantes
{

    public class Tripulante : Entity<TripulanteId>, IAggregateRoot
    {

        public string NMecanografico { get; private set; }

        public string Nome { get; private set; }

        public DateTime DataNascimento { get; private set; }
        
        public string NCCidadao { get; private set; }
        
        public string NCConducao { get; private set; }
        
        public string NIF { get; private set; }
        
        //Tipo de Tripulante deve ser uma string??? Usamos o id do tipo?
        public string TipoTripulante { get; private set; }
        
        public DateTime DataEntradaEmpresa { get; private set; }
        
        public DateTime? DataSaidaEmpresa { get; private set; }
        
        public DateTime DataLicencaConducao { get; private set; }

        //--------------------AUXILIAR EF CORE---------------------------
        
        public Tripulante()
        {

        }

        //faz sentido pedir a data de saida de empresa? Permitimos que esse parâmetro seja nulo?
        public Tripulante(string NMecanografico, string nome, DateTime dataNascimento, string ncCidadao, string ncConducao, string nif, string tipoTripulante, DateTime dataEntradaEmpresa, DateTime? dataSaidaEmpresa, DateTime dataLicencaConducao)
        {
            this.Id = new TripulanteId(NMecanografico);
            this.NMecanografico = NMecanografico;
            this.Nome = nome;
            this.DataNascimento = dataNascimento;
            this.NCCidadao = ncCidadao;
            this.NCConducao = ncConducao;
            this.NIF = nif;
            this.TipoTripulante = tipoTripulante;
            this.DataEntradaEmpresa = dataEntradaEmpresa;
            this.DataSaidaEmpresa = dataSaidaEmpresa;
            this.DataLicencaConducao = dataLicencaConducao;
        }
    }
}