﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class NullableDateTimeTripulante : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlocosTrabalho_Viagens_viagemId",
                table: "BlocosTrabalho");

            migrationBuilder.DropIndex(
                name: "IX_BlocosTrabalho_viagemId",
                table: "BlocosTrabalho");

            migrationBuilder.DropColumn(
                name: "viagemId",
                table: "BlocosTrabalho");

            migrationBuilder.AlterColumn<DateTime>(
                name: "DataSaidaEmpresa",
                table: "Tripulantes",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "datetime2");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "DataSaidaEmpresa",
                table: "Tripulantes",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified),
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "viagemId",
                table: "BlocosTrabalho",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BlocosTrabalho_viagemId",
                table: "BlocosTrabalho",
                column: "viagemId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlocosTrabalho_Viagens_viagemId",
                table: "BlocosTrabalho",
                column: "viagemId",
                principalTable: "Viagens",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
