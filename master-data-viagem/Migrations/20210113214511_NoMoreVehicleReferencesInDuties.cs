﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class NoMoreVehicleReferencesInDuties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ServicosTripulante_Tripulantes_TripulanteId",
                table: "ServicosTripulante");

            migrationBuilder.DropForeignKey(
                name: "FK_ServicosViatura_Viaturas_ViaturaId",
                table: "ServicosViatura");

            migrationBuilder.DropIndex(
                name: "IX_ServicosViatura_ViaturaId",
                table: "ServicosViatura");

            migrationBuilder.DropIndex(
                name: "IX_ServicosTripulante_TripulanteId",
                table: "ServicosTripulante");

            migrationBuilder.DropColumn(
                name: "Data",
                table: "ServicosViatura");

            migrationBuilder.DropColumn(
                name: "ViaturaId",
                table: "ServicosViatura");

            migrationBuilder.DropColumn(
                name: "TripulanteId",
                table: "ServicosTripulante");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Data",
                table: "ServicosViatura",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "ViaturaId",
                table: "ServicosViatura",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TripulanteId",
                table: "ServicosTripulante",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ServicosViatura_ViaturaId",
                table: "ServicosViatura",
                column: "ViaturaId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicosTripulante_TripulanteId",
                table: "ServicosTripulante",
                column: "TripulanteId");

            migrationBuilder.AddForeignKey(
                name: "FK_ServicosTripulante_Tripulantes_TripulanteId",
                table: "ServicosTripulante",
                column: "TripulanteId",
                principalTable: "Tripulantes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_ServicosViatura_Viaturas_ViaturaId",
                table: "ServicosViatura",
                column: "ViaturaId",
                principalTable: "Viaturas",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }
    }
}
