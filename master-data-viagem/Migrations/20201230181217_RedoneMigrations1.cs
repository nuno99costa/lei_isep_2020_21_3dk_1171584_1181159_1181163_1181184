﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class RedoneMigrations1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Tripulantes",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    NMecanografico = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Nome = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataNascimento = table.Column<DateTime>(type: "datetime2", nullable: false),
                    NCCidadao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NCConducao = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NIF = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TipoTripulante = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DataEntradaEmpresa = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataSaidaEmpresa = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DataLicencaConducao = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tripulantes", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Viaturas",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Vin = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Matricula = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TipoViatura = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Data = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Viaturas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ServicosViatura",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Data = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DuracaoSegundos = table.Column<int>(type: "int", nullable: false),
                    ViaturaId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicosViatura", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServicosViatura_Viaturas_ViaturaId",
                        column: x => x.ViaturaId,
                        principalTable: "Viaturas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "Viagens",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    IdPercurso = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IdLinha = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    servicoViaturaId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Viagens", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Viagens_ServicosViatura_servicoViaturaId",
                        column: x => x.servicoViaturaId,
                        principalTable: "ServicosViatura",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "BlocosTrabalho",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    inicio = table.Column<int>(type: "int", nullable: false),
                    fim = table.Column<int>(type: "int", nullable: false),
                    viagemId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    servicoViaturaId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlocosTrabalho", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlocosTrabalho_ServicosViatura_servicoViaturaId",
                        column: x => x.servicoViaturaId,
                        principalTable: "ServicosViatura",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlocosTrabalho_Viagens_viagemId",
                        column: x => x.viagemId,
                        principalTable: "Viagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateTable(
                name: "HorarioPassagem",
                columns: table => new
                {
                    IdNo = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Horario = table.Column<int>(type: "int", nullable: false),
                    ViagemId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HorarioPassagem", x => new { x.ViagemId, x.IdNo, x.Horario });
                    table.ForeignKey(
                        name: "FK_HorarioPassagem_Viagens_ViagemId",
                        column: x => x.ViagemId,
                        principalTable: "Viagens",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlocosTrabalho_servicoViaturaId",
                table: "BlocosTrabalho",
                column: "servicoViaturaId");

            migrationBuilder.CreateIndex(
                name: "IX_BlocosTrabalho_viagemId",
                table: "BlocosTrabalho",
                column: "viagemId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicosViatura_ViaturaId",
                table: "ServicosViatura",
                column: "ViaturaId");

            migrationBuilder.CreateIndex(
                name: "IX_Viagens_servicoViaturaId",
                table: "Viagens",
                column: "servicoViaturaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlocosTrabalho");

            migrationBuilder.DropTable(
                name: "HorarioPassagem");

            migrationBuilder.DropTable(
                name: "Tripulantes");

            migrationBuilder.DropTable(
                name: "Viagens");

            migrationBuilder.DropTable(
                name: "ServicosViatura");

            migrationBuilder.DropTable(
                name: "Viaturas");
        }
    }
}
