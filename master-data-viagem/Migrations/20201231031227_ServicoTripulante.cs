﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class ServicoTripulante : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "servicoTripulanteId",
                table: "BlocosTrabalho",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "ServicosTripulante",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Duracao = table.Column<int>(type: "int", nullable: false),
                    TripulanteId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServicosTripulante", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServicosTripulante_Tripulantes_TripulanteId",
                        column: x => x.TripulanteId,
                        principalTable: "Tripulantes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.SetNull);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlocosTrabalho_servicoTripulanteId",
                table: "BlocosTrabalho",
                column: "servicoTripulanteId");

            migrationBuilder.CreateIndex(
                name: "IX_ServicosTripulante_TripulanteId",
                table: "ServicosTripulante",
                column: "TripulanteId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlocosTrabalho_ServicosTripulante_servicoTripulanteId",
                table: "BlocosTrabalho",
                column: "servicoTripulanteId",
                principalTable: "ServicosTripulante",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlocosTrabalho_ServicosTripulante_servicoTripulanteId",
                table: "BlocosTrabalho");

            migrationBuilder.DropTable(
                name: "ServicosTripulante");

            migrationBuilder.DropIndex(
                name: "IX_BlocosTrabalho_servicoTripulanteId",
                table: "BlocosTrabalho");

            migrationBuilder.DropColumn(
                name: "servicoTripulanteId",
                table: "BlocosTrabalho");
        }
    }
}
