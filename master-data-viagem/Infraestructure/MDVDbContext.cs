using Microsoft.EntityFrameworkCore;
using MDV.Domain.Categories;
using MDV.Domain.Products;
using MDV.Domain.Families;
using MDV.Domain.Viagens;
using MDV.Infrastructure.Categories;
using MDV.Infrastructure.Products;
using MDV.Infrastructure.Viagens;
using MDV.Infrastructure.BlocosTrabalho;
using MDV.Domain.BlocosTrabalho;
using MDV.Infrastructure.Viaturas;
using MDV.Domain.Viaturas;
using MDV.Domain.ServicosViatura;
using MDV.Infrastructure.ServicosViatura;
using MDV.Infrastructure.Tripulantes;
using MDV.Domain.Tripulantes;
using MDV.Domain.ServicosTripulante;

namespace MDV.Infrastructure
{
    public class MDVDbContext : DbContext
    {

        public DbSet<Tripulante> Tripulantes { get; set; }
        public DbSet<Viatura> Viaturas { get; set; }

        public DbSet<Viagem> Viagens { get; set; }
        public DbSet<ServicoViatura> ServicosViatura { get; set; }
        public DbSet<BlocoTrabalho> BlocosTrabalho { get; set; }

        public DbSet<ServicoTripulante> ServicosTripulante{get; set;}

        public MDVDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.ApplyConfiguration(new ViaturaEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TripulanteEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ViagemEntityTypeConfiguration());    
            modelBuilder.ApplyConfiguration(new ServicoViaturaEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new BlocoTrabalhoEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ServicoTripulanteEntityTypeConfiguration());


        

        }
    }
}