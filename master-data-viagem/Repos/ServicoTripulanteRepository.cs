using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.ServicosTripulante;
using MDV.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDV.Infrastructure.ServicosTripulante{
    public class ServicoTripulanteRepository : BaseRepository<ServicoTripulante, ServicoTripulanteId>, IServicoTripulanteRepository{
        private MDVDbContext context;

        public ServicoTripulanteRepository(MDVDbContext context) : base(context.ServicosTripulante)
        {
            this.context = context;
        }

        public async Task<List<ServicoTripulante>> GetAllServicosTripulanteEagerLoading()
        {
            return await context.ServicosTripulante.Include(s => s.BlocosTrabalho).ToListAsync();
        }

        public async Task<ServicoTripulante> GetServicoTripulanteByIdEagerLoading(ServicoTripulanteId id)
        {
            return await this.context.ServicosTripulante.Include(s => s.BlocosTrabalho).SingleOrDefaultAsync(s => s.Id.Equals(id));
        }
    }
}