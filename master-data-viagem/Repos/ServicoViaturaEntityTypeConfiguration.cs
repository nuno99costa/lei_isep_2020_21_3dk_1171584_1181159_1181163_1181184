using MDV.Domain.ServicosViatura;
using MDV.Domain.Viagens;
using MDV.Domain.Viaturas;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MDV.Infrastructure.ServicosViatura
{
    internal class ServicoViaturaEntityTypeConfiguration : IEntityTypeConfiguration<ServicoViatura>
    {
        public void Configure(EntityTypeBuilder<ServicoViatura> builder)
        {
            //builder.ToTable("Categories", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);

            builder.HasMany<Viagem>(sv => sv.Viagens)
                .WithOne(v => v.servicoViatura).OnDelete(DeleteBehavior.SetNull);
            
                
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}