using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosViatura;
using MDV.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;


namespace MDV.Infrastructure.BlocosTrabalho
{
    public class BlocoTrabalhoRepository : BaseRepository<BlocoTrabalho, BlocoTrabalhoId>, IBlocoTrabalhoRepository
    {
        private MDVDbContext context;

        public BlocoTrabalhoRepository(MDVDbContext context) : base(context.BlocosTrabalho)
        {
            this.context = context;
        }

        public async Task<List<BlocoTrabalho>> GetAllBlocosEagerLoading()
        {
            return await context.BlocosTrabalho.Include(s => s.servicoViatura).ToListAsync();
        }

        public async Task<List<BlocoTrabalho>> GetAllBlocosEagerLoadingWithTrips()
        {
            return await context.BlocosTrabalho.Include(s => s.servicoViatura).ThenInclude(s => s.Viagens).ToListAsync();;
        }

        public async Task<BlocoTrabalho> GetBlocoByIdEagerLoading(BlocoTrabalhoId id)
        {
            return await this.context.BlocosTrabalho.Include(s => s.servicoViatura).SingleOrDefaultAsync(s => s.Id.Equals(id));
        }

        public async Task<List<BlocoTrabalho>> getBlocosByServicoViatura(ServicoViaturaId id){
            return await this.context.BlocosTrabalho.FromSqlRaw("Select * from BlocosTrabalho where servicoViaturaId = {0}", id.AsString()).Include(s => s.servicoViatura).ToListAsync();
        }
    }
} 