using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.Viagens;
using MDV.Infrastructure.Shared;

namespace MDV.Infrastructure.Viagens
{
    public class ViagemRepository : BaseRepository<Viagem, ViagemId>, IViagemRepository
    {

        private MDVDbContext context;
        public ViagemRepository(MDVDbContext context) : base(context.Viagens)
        {
            this.context = context;
        }

    }
}