using System.Collections.Generic;
using System.Threading.Tasks;
using MDV.Domain.ServicosViatura;
using MDV.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace MDV.Infrastructure.ServicosViatura{
    public class ServicoViaturaRepository : BaseRepository<ServicoViatura, ServicoViaturaId>, IServicoViaturaRepository{
        private MDVDbContext context;

        public ServicoViaturaRepository(MDVDbContext context) : base(context.ServicosViatura)
        {
            this.context = context;
        }

        public async Task<List<ServicoViatura>> GetAllServicosViaturaEagerLoading()
        {
            return await context.ServicosViatura.Include(s => s.Viagens).ToListAsync();
        }

        public async Task<ServicoViatura> GetServicoViaturaByIdEagerLoading(ServicoViaturaId id)
        {
            return await this.context.ServicosViatura.Include(s => s.Viagens).SingleOrDefaultAsync(s => s.Id.Equals(id));
        }
    }
}