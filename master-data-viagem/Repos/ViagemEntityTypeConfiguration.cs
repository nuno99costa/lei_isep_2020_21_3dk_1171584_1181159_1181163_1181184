using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV.Domain.Viagens;

namespace MDV.Infrastructure.Viagens
{
    internal class ViagemEntityTypeConfiguration : IEntityTypeConfiguration<Viagem>
    {
        public void Configure(EntityTypeBuilder<Viagem> builder)
        {
            //builder.ToTable("Viagens", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);

            builder.OwnsMany<HorarioPassagem>("HorariosPassagem", a =>
                {
                    a.WithOwner().HasForeignKey("ViagemId");
                    a.Property(ca => ca.IdNo);
                    a.Property(ca => ca.Horario);
                    a.HasKey("ViagemId", "IdNo", "Horario");
                });

                
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}