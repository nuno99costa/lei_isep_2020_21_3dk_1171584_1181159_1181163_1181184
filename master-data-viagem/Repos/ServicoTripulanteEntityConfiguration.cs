using MDV.Domain.BlocosTrabalho;
using MDV.Domain.ServicosTripulante;
using MDV.Domain.Tripulantes;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MDV.Infrastructure.ServicosViatura
{
    internal class ServicoTripulanteEntityTypeConfiguration : IEntityTypeConfiguration<ServicoTripulante>
    {
        public void Configure(EntityTypeBuilder<ServicoTripulante> builder)
        {
            //builder.ToTable("Categories", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);

            builder.HasMany<BlocoTrabalho>(st => st.BlocosTrabalho)
                .WithOne(bt => bt.servicoTripulante).OnDelete(DeleteBehavior.SetNull);         
                
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}