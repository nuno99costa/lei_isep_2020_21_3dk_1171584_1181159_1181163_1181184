using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV.Domain.Viagens;
using MDV.Domain.BlocosTrabalho;
using MDV.Domain.Shared;
using MDV.Domain.ServicosViatura;

namespace MDV.Infrastructure.BlocosTrabalho
{
    internal class BlocoTrabalhoEntityTypeConfiguration : IEntityTypeConfiguration<BlocoTrabalho>
    {
        public void Configure(EntityTypeBuilder<BlocoTrabalho> builder)
        {
            
            builder.HasKey(b => b.Id);

            builder.HasOne<ServicoViatura>(s => s.servicoViatura).WithMany(sv => sv.blocosTrabalho).OnDelete(DeleteBehavior.Cascade);
            //builder.Property<bool>("_active").HasColumnName("Active");
        }
    }
}