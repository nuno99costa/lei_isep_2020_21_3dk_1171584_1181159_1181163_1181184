using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MDV.Domain.Tripulantes;

namespace MDV.Infrastructure.Tripulantes
{

    internal class TripulanteEntityTypeConfiguration : IEntityTypeConfiguration<Tripulante>
    {

        public void Configure(EntityTypeBuilder<Tripulante> builder)
        {
            builder.HasKey(b => b.Id);
        }
    }
}