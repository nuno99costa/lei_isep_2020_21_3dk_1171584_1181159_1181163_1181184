using System.Net;
using System.Threading.Tasks;
using MDV.Domain.Shared;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace MDV.Middleware{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (BusinessRuleValidationException ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, BusinessRuleValidationException exception)
        {
            var code = HttpStatusCode.BadRequest; // 500 if unexpected

        

            var result = JsonConvert.SerializeObject(new { error = exception.Message });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}