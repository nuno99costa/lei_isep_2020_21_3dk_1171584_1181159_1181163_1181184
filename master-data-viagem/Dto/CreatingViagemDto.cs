using System;

namespace MDV.Domain.Viagens
{
    public class CreatingViagemDto
    {
        public string ViagemId { get; set; }

        public string IdPercurso { get; set; }

        public string IdLinha { get; set; }

        public string IdNoSaida { get; set; }

        public int HorarioSaida { get; set; }
    }
}