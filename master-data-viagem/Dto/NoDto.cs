using System;

namespace MDV.Domain.Nos
{
    public class NoDto
    {
        public string Id { get; set; }

        public string nomeCompleto { get; set; }

        public string abreviatura { get; set; }

        public Boolean isPontoRendicao { get; set; }

        public Boolean isEstacaoRecolha { get; set; }

        public int latitude { get; set; }

        public int longitude { get; set; }
    }
}