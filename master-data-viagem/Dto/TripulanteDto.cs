using System;
using System.Collections.Generic;

namespace MDV.Domain.Tripulantes
{
    
    public class TripulanteDto{

        public string NMecanografico { get; set; }

        public string Nome { get; set; }

        public string DataNascimento { get; set; }
        
        public string NCCidadao { get; set; }
        
        public string NCConducao { get; set; }
        
        public string NIF { get; set; }
        
        //Ver domínio.
        public string TipoTripulante { get; set; }
        
        public string DataEntradaEmpresa { get; set; }
        
        public string DataSaidaEmpresa { get; set; }
        
        public string DataLicencaConducao { get; set; }
    }

}