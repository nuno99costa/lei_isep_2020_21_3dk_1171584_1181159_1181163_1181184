using MDV.Domain.Categories;

namespace MDV.Domain.Products
{
    public class CreatingProductDto
    {
        public string Description { get;  set; }

        public string CategoryId { get;   set; }


        public CreatingProductDto(string description, string catId)
        {
            this.Description = description;
            this.CategoryId = catId;
        }
    }
}