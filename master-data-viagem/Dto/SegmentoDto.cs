using System;

namespace MDV.Domain.Segmentos
{
    public class SegmentoDto
    {
        public string Id { get; set; }

        public string nomeSegmento { get; set; }

        public string idNoInicial { get; set; }

        public string idNoFinal { get; set; }

        public int distanciaNos { get; set; }

        public int tempoNos { get; set; }
    }
}