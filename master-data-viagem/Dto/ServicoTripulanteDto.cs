using System.Collections.Generic;

namespace MDV.Domain.ServicosTripulante{
    public class ServicoTripulanteDto{
        
        public string Id;

        public List<string> IdsBlocosTrabalho;

        public int Duracao;
        
    }
}