using System;
using System.Collections.Generic;
using MDV.Domain.Viagens;

namespace MDV.Domain.BlocosTrabalho
{
    public class CreatingBlocosDto
    {
        public int DuracaoMaxima { get; set;}

        public int numeroMaximoBlocos {get; set;}

        public string ServicoViaturaId {get; set;}
        
       }

}