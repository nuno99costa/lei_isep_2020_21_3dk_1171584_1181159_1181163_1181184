using System;
using System.Collections.Generic;
using MDV.Domain.Viagens;

namespace MDV.Domain.BlocosTrabalho
{
    public class BlocoTrabalhoDto
    {
        public string Id {get; set; }
        
        public int Inicio { get; set;}

        public int Fim { get; set; }

        public string ServicoViaturaId {get; set;}
        
       }

}