using System.Collections.Generic;

namespace MDV.Domain.ServicosViatura{
    public class ServicoViaturaDto{
        public string Id;

        public int DuracaoSegundos;

        public List<string> ViagensIds;
    }
}