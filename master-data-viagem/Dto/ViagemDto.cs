using System;
using System.Collections.Generic;

namespace MDV.Domain.Viagens
{
    public class ViagemDto
    {
        public string ViagemId { get; set; }

        public string IdPercurso { get; set; }

        public string IdLinha { get; set; }

        public List<HorarioPassagem> HorariosPassagem { get; set; }

        public int HoraSaida{get; set;}
    }
}
