using System;
using System.Collections.Generic;

namespace MDV.Domain.Viaturas
{
    
    public class ViaturaDto{

        public string Vin { get; set; }

        public string Matricula { get; set; }

        public string TipoViatura { get; set; }

        public DateTime Data { get; set; }
    }

}