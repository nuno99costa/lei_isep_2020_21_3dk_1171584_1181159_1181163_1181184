using System;

namespace MDV.Domain.Linhas
{
    public class LinhaDto
    {
        public string Id { get; set; }

        public string codigo { get; set; }

        public string nome { get; set; }

        public string noInicial { get; set; }

        public string noFinal { get; set; }

        public string[] viaturasPermitidas { get; set; }

        public string[] viaturasProibidas { get; set; }

        public string[] tripulantesPermitidos { get; set; }

        public string[] tripulantesProibidos { get; set; }
    }
}