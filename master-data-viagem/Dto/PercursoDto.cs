using System;

namespace MDV.Domain.Percursos
{
    public class PercursoDto
    {
        public string Id { get; set; }

        public string nomePercurso { get; set; }

        public string linhaId { get; set; }

        public string orientacao { get; set; }

        public string[] listaSegmentos { get; set; }

        public string noInicialId { get; set; }

        public string noFinalId { get; set; }

        public int distanciaTotal { get; set; }

        public int tempoTotal { get; set; }
    }
}