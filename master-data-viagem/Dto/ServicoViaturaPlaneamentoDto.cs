using System.Collections.Generic;

namespace MDV.Domain.ServicosViatura{
    public class ServicoViaturaPlaneamentoDto{
        public string Id;

        public List<string> BlocosIds;
    }
}