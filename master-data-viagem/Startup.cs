﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MDV.Infrastructure;
using MDV.Infrastructure.Viagens;
using MDV.Infrastructure.Shared;
using MDV.Domain.Shared;
using MDV.Domain.Categories;
using MDV.Domain.Products;
using MDV.Domain.Families;
using MDV.Domain.Viagens;
using MDV.Domain.BlocosTrabalho;
using MDV.Infrastructure.BlocosTrabalho;

using MDV.Domain.Communication;
using MDV.Middleware.Authentication;
using Microsoft.IdentityModel.Logging;
using MDV.Domain.Viaturas;
using MDV.Infrastructure.Viaturas;
using MDV.Domain.ServicosViatura;
using MDV.Infrastructure.ServicosViatura;
using MDV.Middleware;
using Microsoft.AspNetCore.Http;
using MDV.Domain.Tripulantes;
using MDV.Infrastructure.Tripulantes;
using MDV.Infrastructure.ServicosTripulante;
using MDV.Domain.ServicosTripulante;
using MDV.Utils.Communication;
using MDV.Filesystem;

namespace MDV
{
    public class Startup
    {
        readonly string MDVCorsPolicy = "_MDVCorsPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;
            services.Configure<CookiePolicyOptions>(options =>
{
    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
    options.CheckConsentNeeded = context => false;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});
            services.AddCors(options =>
            {
                options.AddPolicy(name: MDVCorsPolicy,
                                builder =>
                                {
                                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                                });
            });

            /* services.AddDbContext<MDVDbContext>(opt =>
                opt.UseSqlServer("Server=tcp:lapr5mdv.database.windows.net,1433;Initial Catalog=MDVDB;Persist Security Info=False;User ID=lapr5mdv;Password=Lapr5Grupo5;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;")); */

            services.AddDbContext<MDVDbContext>(opt => opt.UseSqlServer("Server=tcp:mdv-lapr5-grupo5.database.windows.net,1433;Initial Catalog=MDV-Grupo5;Persist Security Info=False;User ID=Lapr5Grupo5;Password=Grupo5Lapr5;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;")
                .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());
            ConfigureMyServices(services);

            

            services.AddDistributedMemoryCache();
            services.AddSession();
            services.AddControllers().AddNewtonsoftJson();
            services.AddControllers(o => o.InputFormatters.Insert(o.InputFormatters.Count, new TextPlainInputFormatter()));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseSession();




            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(MDVCorsPolicy);
            app.UseMiddleware<AuthenticationMiddleware>(); //Chama custom middleware
            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            /* services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<CategoryService>();

            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ProductService>();

            services.AddTransient<IFamilyRepository, FamilyRepository>();
            services.AddTransient<FamilyService>();
 */
            services.AddTransient<IViaturaRepository, ViaturaRepository>();
            services.AddTransient<IViaturaService, ViaturaService>();

            services.AddTransient<ITripulanteRepository, TripulanteRepository>();
            services.AddTransient<ITripulanteService, TripulanteService>();

            services.AddTransient<ICommunicationService, CommunicationService>();

            services.AddTransient<IViagemRepository, ViagemRepository>();
            services.AddTransient<IViagemService, ViagemService>();

            services.AddTransient<IServicoViaturaRepository, ServicoViaturaRepository>();
            services.AddTransient<IServicoViaturaService, ServicoViaturaService>();

            services.AddTransient<IBlocoTrabalhoRepository, BlocoTrabalhoRepository>();
            services.AddTransient<IBlocoTrabalhoService, BlocoTrabalhoService>();

            services.AddTransient<IServicoTripulanteRepository, ServicoTripulanteRepository>();
            services.AddTransient<IServicoTripulanteService, ServicoTripulanteService>();

            services.AddTransient<IFilesystemService, FilesystemService>();



        }
    }
}
