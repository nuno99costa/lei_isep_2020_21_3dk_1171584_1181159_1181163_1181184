:- module(search_methods,[getRoute/5]).

:- use_module(data).

sorted(Nlinha,L2):- 
	horario(Nlinha,X),sort(X,L2).

getRoute(NoI,NoF,TempoI,L,TempoF):-
	bestfs(NoI,NoF,L),
	calcularTempo(L,TempoI,TempoF),!.

bestfs(Orig,Dest,Cam):-
	bestfs2(Dest,(0,[Orig]),Cam,_),!.

bestfs2(Dest,(Custo,[Dest|T]),Cam,Custo):-
	!,
	reverse([Dest|T],Cam).

bestfs2(Dest,(_,LA),Cam,Custo):-
	LA=[Act|_],
	findall((EstX,[X|LA]),
		(data:edge(Act,X,_),\+member(X,LA),
		estimativa(X,Dest,EstX)),Novos),
		sort(Novos,NovosOrd),
		proximo(NovosOrd,Melhor),
	bestfs2(Dest,(_,Melhor),Cam,Custo).

proximo([(_,Melhor)|_],Melhor).
proximo([_|L],Melhor):-
	proximo(L,Melhor).

estimativa(Node1,Node2,Estimativa):-
	data:node(_,Node1,_,_,X1,Y1),
	data:node(_,Node2,_,_,X2,Y2),
	distancia(X1, Y1, X2, Y2, Dist),
	Estimativa is Dist.

distancia(X1, Y1, X2, Y2, Res):-
    P is 0.017453292519943295,
    A is (0.5 - cos((X2 - X1) * P) / 2 + cos(X1 * P) * cos(X2 * P) * (1 - cos((Y2 - Y1) * P)) / 2),
    Res is (12742 * asin(sqrt(A)) * 1000).

%assumimos que o motorista demora 2 minutos a mudar de autocarro numa paragem
procurar_horario(Linha,Pos,HorarioInicial,Res):-
	HorarioInicial1 is HorarioInicial + 120,
	listar_horarios_linha(Linha,L),
	procurar_horario1(Pos,HorarioInicial1,L,Res).

procurar_horario1(Pos,HorarioInicial,[H|T],Res):-
	nth1(Pos,H,Sec),
	(Sec > HorarioInicial,!,Res = H;procurar_horario1(Pos,HorarioInicial,T,Res)).

listar_horarios_linha(Linha,Res):-
	findall(Hor, data:schedule(_,Linha, Hor), L),sort(L,Res).

calcularTempo([],Elem1,Elem1):-!.

calcularTempo([NoI,NoF|T],TempoAtual,TempoFinal):-
	data:edge(NoI,NoF,Nlinha),
	getTempo(NoI,NoF,Nlinha,TempoAtual,TempoX),
	calcularTempo(T,TempoX,TempoFinal).

getTempo(NoI,NoF,Linha,CInicial,CFinal):-
	data:route(_,Linha,L,_,_),
	nth1(PosI,L,NoI),
	procurar_horario(Linha,PosI,CInicial,CX),
	nth1(PosF,L,NoF),
	nth1(PosF,CX,CFinal).