:- module(constraints,[]).

:- use_module(data).

lunch_time_hard_weight([],_,Current, Current).
lunch_time_hard_weight([Driver|T], ReducedSchedule,Current,P3):-
    find_from_reduced_schedule(Driver,ReducedSchedule,DriverSchedule), check_lunch_break(DriverSchedule,0,N),
    FreeTime is 14400 - N,
    ((FreeTime < 3600, MissingTime is 3600 - FreeTime);(MissingTime is 0)),
    Penalization is MissingTime*8, Current1 is Current + Penalization,
    lunch_time_hard_weight(T, ReducedSchedule, Current1, P3).

check_lunch_break([],A,A).
check_lunch_break([t(StartTime, EndTime, WorkblockList)|T],TimeWorked,N):-
    (StartTime =< 39600, EndTime > 39600,Time1 is EndTime - 39600,TimeWorked1 is TimeWorked + Time1,
     ((TimeWorked1>10800,
     (retract(lunch_schedule(LunchSchedule));LunchSchedule=[]),
     append(LunchSchedule,WorkblockList,UpdatedLunchSchedule),
     asserta(lunch_schedule(UpdatedLunchSchedule)));(true)),
     check_lunch_break(T,TimeWorked1,N))
    ;
    (StartTime > 39600, EndTime < 54000, Time1 is EndTime - StartTime,TimeWorked1 is TimeWorked + Time1,
     ((TimeWorked1>10800,
     (retract(lunch_schedule(LunchSchedule));LunchSchedule=[]),
     append(LunchSchedule,WorkblockList,UpdatedLunchSchedule),
     asserta(lunch_schedule(UpdatedLunchSchedule)));(true)),

     check_lunch_break(T,TimeWorked1,N))
    ;
    (StartTime < 54000, EndTime > 54000, Time1 is 54000 - StartTime,TimeWorked1 is TimeWorked + Time1,
     ((TimeWorked1>10800,
     (retract(lunch_schedule(LunchSchedule));LunchSchedule=[]),
     append(LunchSchedule,WorkblockList,UpdatedLunchSchedule),
     asserta(lunch_schedule(UpdatedLunchSchedule)));(true)),

     check_lunch_break(T,TimeWorked1,N))
    ;
    (TimeWorked1 is TimeWorked,
     check_lunch_break(T,TimeWorked1,N)).

dinner_time_hard_weight([],_,Current, Current).
dinner_time_hard_weight([Driver|T], ReducedSchedule,Current,P3):-
    find_from_reduced_schedule(Driver,ReducedSchedule,DriverSchedule), check_dinner_break(DriverSchedule,0,N),
    FreeTime is 14400 - N,
    ((FreeTime < 3600, MissingTime is 3600 - FreeTime);(MissingTime is 0)),
    Penalization is MissingTime*8, Current1 is Current + Penalization,
    dinner_time_hard_weight(T, ReducedSchedule, Current1, P3).

check_dinner_break([],A,A).
check_dinner_break([t(StartTime, EndTime, WorkblockList)|T],TimeWorked,N):-
    (StartTime =< 64800, EndTime > 64800,Time1 is EndTime - 64800,TimeWorked1 is TimeWorked + Time1,

     ((TimeWorked1>10800,
     (retract(dinner_schedule(DinnerSchedule));DinnerSchedule=[]),
     append(DinnerSchedule,WorkblockList,UpdatedDinnerSchedule),
     asserta(dinner_schedule(UpdatedDinnerSchedule)));(true)),

     check_dinner_break(T,TimeWorked1,N))
    ;
    (StartTime > 64800, EndTime < 79200, Time1 is EndTime - StartTime,TimeWorked1 is TimeWorked + Time1,

     ((TimeWorked1>10800,
     (retract(dinner_schedule(DinnerSchedule));DinnerSchedule=[]),
     append(DinnerSchedule,WorkblockList,UpdatedDinnerSchedule),
     asserta(dinner_schedule(UpdatedDinnerSchedule)));(true)),

     check_dinner_break(T,TimeWorked1,N))
    ;
    (StartTime < 79200, EndTime > 79200, Time1 is 79200 - StartTime,TimeWorked1 is TimeWorked + Time1,

     ((TimeWorked1>10800,
     (retract(dinner_schedule(DinnerSchedule));DinnerSchedule=[]),
     append(DinnerSchedule,WorkblockList,UpdatedDinnerSchedule),
     asserta(dinner_schedule(UpdatedDinnerSchedule)));(true)),

     check_dinner_break(T,TimeWorked1,N))
    ;
    (TimeWorked1 is TimeWorked,check_dinner_break(T,TimeWorked1,N)).

schedule_preference_soft_weight([],_,Current, Current).
schedule_preference_soft_weight([[Driver,StartTime,EndTime]|T], ReducedSchedule,Current,P3):-
    find_from_reduced_schedule(Driver,ReducedSchedule,DriverSchedule), check_driver_schedule_preference(DriverSchedule,0,N,StartTime,EndTime),
    Penalization is N*1, Current1 is Current + Penalization,
    schedule_preference_soft_weight(T, ReducedSchedule, Current1, P3).

check_driver_schedule_preference([],A,A,_,_).
check_driver_schedule_preference([t(StartTime, EndTime, _)|T],A,N,StartPref,EndPref):-
    ((StartTime < StartPref,EndTime > StartPref,A1 is StartPref - StartTime)
    ;
    (StartTime < StartPref,EndTime =< StartPref, A1 is EndTime - StartTime)
    ;
    (A1 is 0)),

    ((EndTime > EndPref, StartTime < EndPref, A2 is EndTime - EndPref)
    ;
    (EndTime > EndPref, StartTime >= EndPref, A2 is EndTime - StartTime)
    ;
    (A2 is 0)),

    A3 is A1 + A2 + A,
    check_driver_schedule_preference(T,A3,N,StartPref,EndPref).


four_consecutive_hours_hard_weight([],_,Current, Current).
four_consecutive_hours_hard_weight([Driver|T],ReducedSchedule,Current,P1):-
    find_from_reduced_schedule(Driver,ReducedSchedule,DriverSchedule), check_four_hours(DriverSchedule,0,N),
    Penalization is N*10, Current1 is Current + Penalization,
    four_consecutive_hours_hard_weight(T, ReducedSchedule, Current1, P1).


check_four_hours([],A,A).
check_four_hours([t(StartTime, EndTime, ListWorkblocks)|T],A,N):-
    Duration is EndTime - StartTime,
    ((Duration >= 14400,
      (retract(four_hour_agenda(FourHourAgenda));FourHourAgenda=[]),
      append(FourHourAgenda,ListWorkblocks,FourHourAgenda1),
      asserta(four_hour_agenda(FourHourAgenda1)),
      check_one_hour_rest(EndTime,T,N2),
      ExcessTemp is Duration - 14400,
      Excess is ExcessTemp + N2,
      A1 is A + Excess)
    ;
    (A1 is A)),
    check_four_hours(T, A1, N).

check_one_hour_rest(_,[],0).
check_one_hour_rest(EndTime,[t(NextStartTime, _, _)|_], N2):-
    Diff is NextStartTime - EndTime,
    ((Diff < 3600, N2 is 3600 - Diff)
    ;
    (N2 is 0)).

eight_total_hours_hard_weight([],_,Current, Current).
eight_total_hours_hard_weight([Driver|T],ReducedSchedule,Current,P2):-
    find_from_reduced_schedule(Driver,ReducedSchedule,DriverSchedule), check_eigth_hours(DriverSchedule,0,N),
    Penalization is N*10, Current1 is Current + Penalization,
    eight_total_hours_hard_weight(T, ReducedSchedule, Current1, P2).


check_eigth_hours([],Duration,Res):- ((Duration > 28800, Res is Duration - 28800);(Res is 0)).
check_eigth_hours([t(StartTime, EndTime, _)|T],A,N):-
    Duration is EndTime - StartTime,
    A1 is A + Duration,
    check_eigth_hours(T, A1, N).

find_from_reduced_schedule(_,[],[]).
find_from_reduced_schedule(Driver,[t(StarTime,EndTime,Driver)|T],[t(StarTime,EndTime,Driver)|Lista]):-
    find_from_reduced_schedule(Driver, T, Lista).

find_from_reduced_schedule(Driver,[t(_,_,_)|T], Lista):- find_from_reduced_schedule(Driver, T, Lista).