:- module(server,[server/1]).

%% Nossos modules
:- use_module(data).
:- use_module(search_methods).
:- use_module(alg_genet).

% Bibliotecas 
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/http_unix_daemon)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_open)).
:- use_module(library(http/http_cors)).
:- use_module(library(date)).
:- use_module(library(random)).

% Bibliotecas JSON
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).

%CORS Settings
:- set_setting(http:cors, [*]).

% Relacao entre pedidos HTTP e predicados que os processam
:- http_handler('/api/planeamento/obterPercurso', acceptRoute, []).
:- http_handler('/api/planeamento/escalonar', acceptStaging, []).
:- http_handler('/api/planeamento/recarregarDados', reloadKnowledgeBase, []).

%:- json_object resposta(array:list(string), HoraChegada:int).

% Criacao de servidor HTTP no porto 'Port'
server(Port) :-    
    http_server(http_dispatch, [port(Port)]),!,
    data:loadKnowledgeBase(),!,
    write('KNOWLEDGE BASE LOADED').

%https://prolog.nuno99costa.xyz/api/planeamento/recarregarDados

reloadKnowledgeBase(_Request):-
    data:removeKnowledgeBase(),
    data:loadKnowledgeBase(),
    format('Status: 200~n'),
    format('Content-type: text/plain~n~n'),
    format('Reload successful').

%https://prolog.nuno99costa.xyz/api/planeamento/escalonar

acceptStaging(Request):-
    http_parameters(Request,[ vdid(Vehicle_duty_id, [string]),
                              nret(NRetirar,[integer]),
                              custo(Custo,[integer]),
                              trel(TRelativo,[integer]),
                              nger(NGeracoes,[integer]),
                              dpop(DPop,[integer]),
                              pcruz(ProbCruz,[between(0,100)]),
                              pmut(ProbMut,[between(0,100)])]),
    alg_genet:getStaging(Vehicle_duty_id, NRetirar,Custo,TRelativo,NGeracoes,DPop, ProbCruz, ProbMut, MelhorGer),
    prolog_to_json(MelhorGer, JSONObject),
    cors_enable,
    reply_json(JSONObject, [json_object(dict)]).

%https://prolog.nuno99costa.xyz/api/planeamento/obterPercurso

acceptRoute(Request) :-
    http_parameters(Request,
                    [noI(NoI,[string]),
                     noF(NoF,[string]),
                     t(Hora, [between(0,1000000)])]),
    search_methods:getRoute(NoI,NoF,Hora,Caminho,TempoF),
    cors_enable,
    format('Content-type: text/json~n~n', []),
    format('{"caminho":~w,"tempofinal":~w}', [Caminho,TempoF]).