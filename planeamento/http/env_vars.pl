:- module(env_vars,[nodes_url/1, routes_url/1, schedules_url/1, workblocks_url/1, vehicle_duties_url/1]).

nodes_url("https://lei-isep-lapr5-g05-mdr.herokuapp.com/api/no/all/ordenados/codigo").
routes_url("https://lei-isep-lapr5-g05-mdr.herokuapp.com/api/percurso/dadosPercurso").
schedules_url("https://mdv-grupo5-lapr5.azurewebsites.net/api/viagens/planeamento").
workblocks_url("https://mdv-grupo5-lapr5.azurewebsites.net/api/BlocosTrabalho/planeamento").
vehicle_duties_url("https://mdv-grupo5-lapr5.azurewebsites.net/api/ServicosViatura/planeamento").