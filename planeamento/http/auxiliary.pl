:- module(auxiliary,[remove_last_element/2]).

remove_last_element([X|Xs], Ys):-                 % use auxiliary predicate ...
   remove_last_element1(Xs, Ys, X).            % ... which lags behind by one item

remove_last_element1([], [], _).
remove_last_element1([X1|Xs], [X0|Ys], X0):-  
   remove_last_element1(Xs, Ys, X1).           % lag behind by one