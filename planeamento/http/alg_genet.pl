:- module(alg_genet,[getStaging/9]).

:- use_module(data).
:- use_module(auxiliary,[remove_last_element/2]).
:- use_module(constraints).

:-dynamic generations/1.
:-dynamic population/1.
:-dynamic prob_geneticCrossing/1.
:-dynamic prob_geneticMutation/1.
:-dynamic num/1.
:-dynamic time_limit/1.
:-dynamic acceptable_cost/1.
:-dynamic numDrivers/1.
:-dynamic available_drivers/1.
:-dynamic best_generation/1.
:-dynamic last_generation/2.
:-dynamic vehicle_duty_workblocks/1.
:-dynamic vehicle_duty_id/1.

preferenceHorario(276, 36000, 72000).

contratoHorario(276, 36000, 72000).

preferenceHorario(5188, 45000, 75000).

contratoHorario(5188, 45000, 75000).

preferenceHorario(16690, 33000, 61000).

contratoHorario(16690, 33000, 61000).

preferenceHorario(18107, 25000, 42000).

contratoHorario(18107, 25000, 42000).

getStaging(VDID,NR,C,TR,NG,DP, P1, P2, MelhorGer):-
	(retract(num(_));true), asserta(num(NR)),
	(retract(custo(_));true), asserta(custo(C)),
	(retract(time_limit(_));true), asserta(time_limit(TR)),		
	(retract(generations(_));true), asserta(generations(NG)),
	(retract(population(_));true), asserta(population(DP)),
	PC is P1/100,
	(retract(prob_geneticCrossing(_));true),	asserta(prob_geneticCrossing(PC)),
	PM is P2/100,
	(retract(prob_geneticMutation(_));true), asserta(prob_geneticMutation(PM)),
	(retract(vehicle_duty_id(_));true), asserta(vehicle_duty_id(VDID)),
	data:vehicle_duty(VDID,List),length(List,NumWorkblocks),
	(retract(vehicle_duty_workblocks(_));true), asserta(vehicle_duty_workblocks(NumWorkblocks)),
	generate(MelhorGer*_),!.

generate(Result):- 
       time_limit(TMax),
       catch((R = success, call_with_time_limit(TMax,generate)),
          time_limit_exceeded,
          (R = timeout, write('Time limit exceeded.'))),
        best_generation(M),nth0(0,M,Result).

generate:-
	generate_population(Pop),
	evaluate_population(Pop,PopAv),
	sort_population(PopAv,PopOrd),
	generations(NG),
	asserta(last_generation(PopOrd,0)),
	generate_generation(0,NG,PopOrd).

%% GENERATE POPULATION

generate_population(Pop):-
	population(PopSize),vehicle_duty_id(VDID),data:vehicle_duty(VDID,ListWorkblocks),length(ListWorkblocks,NumWorkblocks),
	(retract(numDrivers(_));true), asserta(numDrivers(NumWorkblocks)),
	findall(X,data:driver_schedule(X,_,_,_,_),List1),
	(retract(available_drivers(_));true), asserta(available_drivers(List1)),!,
	generate_population(PopSize,Pop1),
	available_drivers(ListaCondutores),
	auxiliary:remove_last_element(Pop1,Pop).

generate_population(0,[_]):-!.

generate_population(PopSize,[Ind|Resto]):-
	PopSize1 is PopSize-1,
	generate_population(PopSize1,Resto),
	generate_individual(Ind).

generate_population(PopSize,List):-
	generate_population(PopSize,List).

%% GENERATE INDIVIDUAL

generate_individual1([],[_]):-!.
generate_individual1([WH|WT],[IH|IT]):-
	data:workblock(WH,_,StartTime,EndTime), WorkblockLength is EndTime - StartTime,
	findall((L,A),(data:driver_work_tuple(L,A),WorkblockLength<A),List1),
	length(List1,ListLength1),ListLength is ListLength1 - 1,
	random(0,ListLength,N),
	nth0(N,List1,(IH,_)),
	generate_individual1(WT,IT).

generate_individual(L):-
	vehicle_duty_id(VDID),
	data:vehicle_duty(VDID,L1), 
	generate_individual1(L1,L2),
	auxiliary:remove_last_element(L2,L).

%% EVALUATE

evaluate_population([],[]).
evaluate_population([Individual|T],[Individual*Value|T1]):-
	make_triplets(Individual,_,ReducedSchedule),
	evaluate(ReducedSchedule,Value),
	evaluate_population(T,T1).


evaluate(ReducedSchedule,V):-
	available_drivers(DriverList),
	constraints:four_consecutive_hours_hard_weight(DriverList,ReducedSchedule,0,P1),
	constraints:eight_total_hours_hard_weight(DriverList,ReducedSchedule,0,P2),
	constraints:lunch_time_hard_weight(DriverList,ReducedSchedule,0,P3),
	constraints:dinner_time_hard_weight(DriverList,ReducedSchedule,0,P4),

	findall([Driver,StartTime,EndTime],
	data:driver_schedule(Driver,StartTime,EndTime,_,_),DriverPreferenceList),
	constraints:schedule_preference_soft_weight(DriverPreferenceList,ReducedSchedule,0,P5),
	V is P1 + P2 + P3 + P4 + P5.






make_triplets(Ind,CompleteSchedule,ReducedSchedule):-
	(retractall(complete_schedule(_));true),
	(retractall(reduced_schedule(_));true),
	vehicle_duty_workblocks(Nworkblocks),
	Nworkblocks1 is Nworkblocks+1,
	vehicle_duty_id(VDID),
	data:vehicle_duty(VDID,Workblocks),
	make_complete_schedule(1,Nworkblocks1,Ind,Workblocks),
	complete_schedule(CompleteSchedule),
	make_reduced_schedule(CompleteSchedule),
	reduced_schedule(ReducedSchedule).

make_complete_schedule(Num,Num,_,_):-!.

make_complete_schedule(Num,Ntotal,Ind,Workblocks):-
	(retract(complete_schedule(Schedule));Schedule = [],true),
	nth1(Num,Ind,Driver),
	nth1(Num,Workblocks,Workblock),
	data:workblock(Workblock,_,StartTime,EndTime),
	append(Schedule,[t(StartTime,EndTime,Driver)],UpdatedSchedule),
	asserta(complete_schedule(UpdatedSchedule)),Num1 is Num + 1,!,make_complete_schedule(Num1,Ntotal,Ind,Workblocks).

make_reduced_schedule([H|T]):-join_triplets(H,T,Nova),asserta(reduced_schedule(Nova)).

join_triplets(t(Ti,Tf,M),[],[t(Ti,Tf,M)]).
join_triplets(t(T1,T2,M),[t(T2,T3,M)|L],L1):-!,join_triplets(t(T1,T3,M),L,L1).
join_triplets(t(Ti,Tf,M),[t(T3,T4,M1)|L],[t(Ti,Tf,M)|L1]):-join_triplets(t(T3,T4,M1),L,L1).


check_next(_,[],_,EndTime,EndTime).
check_next(Driver,[t(StartTimeNext,EndTimeNext,NextDriver)|T],T1,EndTime,EndTime1):-
	(same(Driver,NextDriver),
	check_next(Driver,T,T1,EndTimeNext,EndTime1));
	(EndTime1 is EndTime,append(t(StartTimeNext,EndTimeNext,NextDriver),T,T1)).


sort_population(PopAv,PopAvOrd):-
	bsort(PopAv,PopAvOrd).

bsort([X],[X]):-!.
bsort([X|Xs],Ys):-
	bsort(Xs,Zs),
	btroca([X|Zs],Ys).

btroca([X],[X]):-!.

btroca([X*VX,Y*VY|L1],[Y*VY|L2]):-
	VX>VY,!,
	btroca([X*VX|L1],L2).

btroca([X|L1],[X|L2]):-btroca(L1,L2).

%% GENERATIONS

generate_generation(G,G,[_|_]):-!.

generate_generation(N,G,Pop):-
	process_population(Pop,NPopAv),
	append(NPopAv, Pop, FatherAndSon),
	sort_population(FatherAndSon,NPopOrd),
	num(PRet),
	NBest is PRet,
	take(NPopOrd, NBest, LBest),
	remove_list(NPopOrd, LBest, LRest),
	shuffle(LRest, LRestMult),
	sort(0,@=<,LRestMult,LRestMultSorted),
	fix_list(LRestMultSorted,LRestSorted), population(P),
	NResto is P - NBest,
	take(LRestSorted, NResto, LRestOrd1),
	append(LBest,LRestOrd1,AppendList),
	sort_population(AppendList, AppendedList),
    (retract(best_generation(_));true), asserta(best_generation(AppendedList)),
	N1 is N+1,
	Limit is 3,
	(retract(last_generation(LastGen,RepeatNumbers));true),
        (same(AppendedList,LastGen),
	RepeatNumbers1 is RepeatNumbers+1,
	asserta(last_generation(AppendedList,RepeatNumbers1))
	;
	asserta(last_generation(AppendedList,0))),

       last_generation(_,NTimes),
       ((check_if_fine_this_way(Pop)
	);NTimes >= Limit;generate_generation(N1,G,AppendedList)).

getValues([],[]).
getValues([_*X|Pop],[X|Values]):-getValues(Pop,Values).

check_if_fine_this_way([_*V|_]):- acceptable_cost(C), V<C.

same(L, L).

process_population(Pop, NPopAv):-
	random_permutation(Pop, RandPop),
	genetic_crossing(RandPop,NPop1),
	genetic_mutation(NPop1,NPop),
	evaluate_population(NPop,NPopAv).

take(Src,N,L) :- findall(E, (nth1(I,Src,E), I =< N), L).

remove_list([], _, []).
remove_list([X|Tail], L2, Result):- member(X, L2),nth1(_, L2, X, L3), !, remove_list(Tail, L3, Result).
remove_list([X|Tail], L2, [X|Result]):- remove_list(Tail, L2, Result).


shuffle([],[]).
shuffle([H*X|T], [X1*H*X|Res]):- random(0.0,1.0,R), X1 is R*X, shuffle(T,Res).

fix_list([],[]).
fix_list([_*H*X|T], [H*X|Res]):- fix_list(T, Res).


genetic_crossing([],[]).
genetic_crossing([Ind*_],[Ind]).
genetic_crossing([Ind1*_,Ind2*_|T],[NInd1,NInd2|T1]):-
	create_crossing_points(P1,P2),
	prob_geneticCrossing(PCrossing),random(0.0,1.0,Pc),
	((Pc =< PCrossing,!,
        cross(Ind1,Ind2,P1,P2,NInd1),
	    cross(Ind2,Ind1,P1,P2,NInd2))
	;
	(NInd1=Ind1,NInd2=Ind2)),
	genetic_crossing(T,T1).


create_crossing_points(P1,P2):-
	create_crossing_points1(P1,P2).

create_crossing_points1(P1,P2):-
	vehicle_duty_workblocks(N),
	NTemp is N+1,
	random(1,NTemp,P11),
	random(1,NTemp,P21),
	P11\==P21,!,
	((P11<P21,!,P1=P11,P2=P21);(P1=P21,P2=P11)).
create_crossing_points1(P1,P2):-
	create_crossing_points1(P1,P2).

cross(Ind1,Ind2,P1,P2,NInd11):-
	sublista(Ind1,P1,P2,Sub1),
	vehicle_duty_workblocks(NumT),
	R is NumT-P2,
	rotate_right(Ind2,R,Ind21),
	elimina(Ind21,Sub1,Sub2),
	P3 is P2 + 1,
	insere(Sub2,Sub1,P3,NInd1),
	eliminah(NInd1,NInd11).

sublista(L1,I1,I2,L):-
	I1 < I2,!,
	sublista1(L1,I1,I2,L).

sublista(L1,I1,I2,L):-
	sublista1(L1,I2,I1,L).

sublista1([X|R1],1,1,[X|H]):-!,
	preencheh(R1,H).

sublista1([X|R1],1,N2,[X|R2]):-!,
	N3 is N2 - 1,
	sublista1(R1,1,N3,R2).

sublista1([_|R1],N1,N2,[h|R2]):-
	N3 is N1 - 1,
	N4 is N2 - 1,
	sublista1(R1,N3,N4,R2).




preencheh([],[]).

preencheh([_|R1],[h|R2]):-
	preencheh(R1,R2).



rotate_right(L,K,L1):-
	vehicle_duty_workblocks(N),
	T is N - K,
	rr(T,L,L1).

rr(0,L,L):-!.

rr(N,[X|R],R2):-
	N1 is N - 1,
	append(R,[X],R1),
	rr(N1,R1,R2).


%Houve mudanças:
elimina([],_,[]):-!.

elimina([X|R1],L,[X|R2]):-
	not(member(X,L)),!,
	elimina(R1,L,R2).

elimina([X|R1],L,R2):-
	nth1(_,L,X,L2),
	elimina(R1,L2,R2).

insere([],L,_,L):-!.
insere([X|R],L,N,L2):-
	vehicle_duty_workblocks(T),
	((N>T,!,N1 is N mod T);N1 = N),
	insere1(X,N1,L,L1),
	N2 is N + 1,
	insere(R,L1,N2,L2).


insere1(X,1,L,[X|L]):-!.
insere1(X,N,[Y|L],[Y|L1]):-
	N1 is N-1,
	insere1(X,N1,L,L1).

eliminah([],[]).

eliminah([h|R1],R2):-!,
	eliminah(R1,R2).

eliminah([X|R1],[X|R2]):-
	eliminah(R1,R2).

genetic_mutation([],[]).
genetic_mutation([Ind|T],[NInd|T1]):-
	prob_geneticMutation(PMut),
	random(0.0,1.0,Pm),
	((Pm < PMut,!,genetic_mutation1(Ind,NInd));NInd = Ind),
	genetic_mutation(T,T1).

genetic_mutation1(Ind,NInd):-
	((same1(Ind),NInd = Ind);
	(create_crossing_points(P1,P2),
	nth1(P1,Ind,VP1),
	nth1(P2,Ind,VP2),
	((same(VP1,VP2),genetic_mutation1(Ind,NInd));
	genetic_mutation22(Ind,P1,P2,NInd)))).


same1([]).   % You only need this one if you want the empty list to succeed
same1([_]).
same1([X,X|T]) :- same1([X|T]),!.

genetic_mutation22([G1|Ind],1,P2,[G2|NInd]):-
	!, P21 is P2-1,
	genetic_mutation23(G1,P21,Ind,G2,NInd).

genetic_mutation22([G|Ind],P1,P2,[G|NInd]):-
	P11 is P1-1, P21 is P2-1,
	genetic_mutation22(Ind,P11,P21,NInd).

genetic_mutation23(G1,1,[G2|Ind],G2,[G1|Ind]):-!.
genetic_mutation23(G1,P,[G|Ind],G2,[G|NInd]):-
	P1 is P-1,
	genetic_mutation23(G1,P1,Ind,G2,NInd).