:- module(data,[loadKnowledgeBase/0, removeKnowledgeBase/0, node/6, schedule/2, edge/3, route/5, workblock/4, vehicle_duty/2, driver_schedule/5, driver_work_triplet/3]).

:- use_module(library(http/http_open)).
:- use_module(library(http/json_convert)).
:- use_module(library(http/http_json)).
:- use_module(library(http/json)).

:- use_module(env_vars,[schedules_url/1, nodes_url/1, routes_url/1, workblocks_url/1, vehicle_duties_url/1]).

%% Base Conhecimento Primária
:- dynamic node/6.
:- dynamic route/5.
:- dynamic schedule/2.
:- dynamic workblock/4.
:- dynamic vehicle_duty/2.
:- dynamic driver_schedule/5.

%% Base Conhecimento Secundária
:- dynamic edge/3.
:- dynamic driver_work_tuple/2.
:- dynamic driver_work_triplet/3.

loadKnowledgeBase():-
    load_schedules(),
    load_nodes(),
    load_routes(),
    load_workblocks(),
    load_vehicle_duty(),
    load_driver_schedules(),
    load_driver_workblock_tuples(),
    spawn_connections(),!.

removeKnowledgeBase():-
    retractall(node(_,_,_,_,_,_)),
    retractall(route(_,_,_,_,_)),
    retractall(schedule(_,_,_)),
    retractall(edge(_,_,_)),
    retractall(driver_schedule(_,_,_,_,_)),
    retractall(workblock(_,_,_,_)),
    retractall(vehicle_duty(_,_)).

retract_driver_workblock_tuples:-retractall(driver_work_tuple(_,_)).

%% SCHEDULES

load_schedules:-
            get_schedule_data(Data),
            assert_schedules(Data).

get_schedule_data(Data) :-
    env_vars:schedules_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json'),request_header('auth-token'='secret-shhh')]),
        json_read_dict(In, Data),
        close(In)
    ).

assert_schedules(L):-assert_schedules_rec(L).
assert_schedules_rec([]):-!.
assert_schedules_rec([ScheduleJSON|Data]):-
            asserta(
                schedule( 
                    ScheduleJSON.get(viagemId),
                    ScheduleJSON.get(idPercurso), 
                    ScheduleJSON.get(horarios)
                )
            ),
            assert_schedules_rec(Data).

%% EDGES

spawn_connections:- retractall(edge(_,_,_)),
    findall(_,
        ((node(_,Node1,true,false,_,_);node(_,Node1,false,true,_,_)),
        (node(_,Node2,true,false,_,_);node(_,Node2,false,true,_,_)),
        Node1\==Node2,
        route(_,N,LNode,_,_),
        member_order(Node1,Node2,LNode),
        assertz(edge(Node1,Node2,N))),_).

member_order(Node1,Node2,[Node1|L]):- member(Node2,L),!.
member_order(Node1,Node2,[_|L]):- member_order(Node1,Node2,L).

%% NODES

load_nodes():-
            get_node_data(Data),
            assert_nodes(Data).

get_node_data(Data) :-
    env_vars:nodes_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json'),request_header('auth-token'='secret-shhh')]),
        json_read_dict(In, Data),
        close(In)
    ).

assert_nodes(L):-assert_nodes_rec(L).
assert_nodes_rec([]):-!.
assert_nodes_rec([Node|Data]):-
            asserta(
                node( 
                    Node.get(nomeCompleto),
                    Node.get(abreviatura), 
                    Node.get(isPontoRendicao), 
                    Node.get(isEstacaoRecolha), 
                    Node.get(latitude), 
                    Node.get(longitude)
                )
            ),
            assert_nodes_rec(Data).


%% ROUTES

load_routes():-
            get_route_data(Data),
            assert_routes(Data).

get_route_data(Data) :-
    env_vars:routes_url(URL1),
    setup_call_cleanup(
        http_open(URL1, In, [request_header('Accept'='application/json'),request_header('auth-token'='secret-shhh')]),
        json_read_dict(In, Data),
        close(In)
    ).

assert_routes(L):-assert_routes_rec(L).
assert_routes_rec([]):-!.
assert_routes_rec([Route|Data]):-
            asserta(
                route(
                    Route.get(nomeLinha),       %% SUBSTITUIR POR ID DA LINHA
                    Route.get(idPercurso),
                    Route.get(abreviaturasNos), %% SUBSTITUIR POR IDS DOS NOS
                    Route.get(tempo),
                    Route.get(distancia)
                )
            ),
            assert_routes_rec(Data).

%% VEHICLE DUTIES

load_vehicle_duty:-
            get_vehicle_duty_data(Data),
            assert_vehicle_duties(Data).

get_vehicle_duty_data(Data) :-
    env_vars:vehicle_duties_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json'),request_header('auth-token'='secret-shhh')]),
        json_read_dict(In, Data),
        close(In)
    ).

assert_vehicle_duties(L):-assert_vehicle_duties_rec(L).
assert_vehicle_duties_rec([]):-!.
assert_vehicle_duties_rec([ScheduleJSON|Data]):-
            asserta(
                vehicle_duty( 
                    ScheduleJSON.get(id),
                    ScheduleJSON.get(blocosIds)
                )
            ),
            assert_vehicle_duties_rec(Data).

%% WORKBLOCKS

load_workblocks:-
            get_workblock_data(Data),
            assert_workblocks(Data).

get_workblock_data(Data) :-
    env_vars:workblocks_url(URL),
    setup_call_cleanup(
        http_open(URL, In, [request_header('Accept'='application/json'),request_header('auth-token'='secret-shhh')]),
        json_read_dict(In, Data),
        close(In)
    ).

assert_workblocks(L):-assert_workblocks_rec(L).
assert_workblocks_rec([]):-!.
assert_workblocks_rec([ScheduleJSON|Data]):-
            asserta(
                workblock( 
                    ScheduleJSON.get(id),
                    ScheduleJSON.get(idsViagens), 
                    ScheduleJSON.get(inicio), 
                    ScheduleJSON.get(fim)
                )
            ),
            assert_workblocks_rec(Data).

%% DRIVER SCHEDULE

load_driver_schedules:-
    asserta(driver_schedule(276,25200,61200,28800, [21600,7200])),
    asserta(driver_schedule(527, 25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(889, 25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(1055, 25200, 61200, 28800, [14400,14400])),
    asserta(driver_schedule(1461, 25200, 61200, 28800, [14400,14400])),
    asserta(driver_schedule(1640, 25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(2049, 25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(5188,33300,69300, 28800, [7200,21600])),
    asserta(driver_schedule(6616,33300, 69300, 28800, [14400,14400])),
    asserta(driver_schedule(6697,33300, 69300, 28800, [21600,7200])),
    asserta(driver_schedule(11018,41400,77400, 28800, [21600,7200])),
    asserta(driver_schedule(11692,41400, 77400, 28800, [21600,7200])),
    asserta(driver_schedule(14893,45000,81000, 28800, [10800,18000])),
    asserta(driver_schedule(16458,50400,86400, 28800, [14400,14400])),
    asserta(driver_schedule(16690, 50400, 86400, 28800, [7200,21600])),
    asserta(driver_schedule(16763, 50400, 86400, 28800, [14400,14400])),
    asserta(driver_schedule(17015, 50400, 86400, 28800, [10800,18000])),
    asserta(driver_schedule(17552,54000,90000,28800, [10800,18000])),
    asserta(driver_schedule(17623,25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(17630,25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(17639,27000,48600,21600,[21600])),
    asserta(driver_schedule(17673,25200, 61200, 28800, [21600,7200])),
    asserta(driver_schedule(18009,50400, 86400, 28800, [7200,21600])),
    asserta(driver_schedule(18050,54000,90000,28800, [21600,7200])),
    asserta(driver_schedule(18097,57600,79200,21600,[21600])),
    asserta(driver_schedule(18105,57600,79200,21600,[21600])),
    asserta(driver_schedule(18107,57600,79200,21600,[21600])),
    asserta(driver_schedule(18119,59400,81000,21600,[21600])),
    asserta(driver_schedule(18131,66600,88200,21600,[21600])).

%% DRIVER WORKBLOCK TUPLES

load_driver_workblock_tuples():- findall((L,L1),data:driver_schedule(L,_,_,_,L1),ListA),
                                 create_driver_work_tuples(ListA).

create_driver_work_tuples([]):-!.
create_driver_work_tuples([(A,B)|T]):- create_driver_work_tuples1(A,B), create_driver_work_tuples(T).

create_driver_work_tuples1(_,[]):-!.
create_driver_work_tuples1(A,[H|T]):- asserta(driver_work_tuple(A,H)), create_driver_work_tuples1(A,T).