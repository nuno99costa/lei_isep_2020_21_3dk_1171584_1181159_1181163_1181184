:- module(driver_allocation,[load_check/0]).

:- use_module(data).

load_check():-
	findall(L,data:vehicle_duty(_,L),List1),
	flatten(List1,List),
	get_vehicle_duty_total_time(List,0,TotalVehicleDutyTime),
	findall(L,data:driver_schedule(_,_,_,L,_),List2),
	sumlist(List2,TotalDriverDutyTime),
	Diff is (TotalDriverDutyTime - TotalVehicleDutyTime)/3600,
	write("Tempo de Tripulantes em excesso: "), write(Diff), write(" horas"),nl,
	TotalDriverDutyTime >= TotalVehicleDutyTime.

get_vehicle_duty_total_time([],X,X).
get_vehicle_duty_total_time([H|T],A,X) :- get_workblock_total_time(H,Y), A2 is A + Y, get_vehicle_duty_total_time(T,A2,X).

get_workblock_total_time(ID,X) :- data:workblock(ID,_,StartTime,EndTime), X is EndTime - StartTime.