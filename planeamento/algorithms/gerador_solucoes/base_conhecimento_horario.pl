no('Aguiar de Sousa', 'AGUIA', f, f, -8.4464785432391, 41.1293363229325).
no('Baltar', 'BALTR', t, f, -8.38716802227697, 41.1937898023744).
no('Besteiros', 'BESTR', f, f, -8.34043029659082, 41.217018845589).
no('Cete', 'CETE', t, f, -8.35164059584564, 41.183243425797).
no('Cristelo', 'CRIST', t, f, -8.34639896125324, 41.2207801252676).
no('Duas Igrejas', 'DIGRJ', f, f, -8.35481024956726, 41.2278665802794).
no('Estacao (Lordelo)', 'ESTLO', f, t, -8.4227924957086, 41.2521157104055).
no('Estacao (Paredes)', 'ESTPA', f, t, -8.33448520831829, 41.2082119860192).
no('Gandra', 'GAND', f, f, -8.43958765792976, 41.1956579348384).
no('Lordelo', 'LORDL', t, f, -8.42293614720057, 41.2452627470645).
no('Mouriz', 'MOURZ', t, f, -8.36577272258403, 41.1983610215263).
no('Parada de Todeia', 'PARAD', t, f, -8.37023578802149, 41.1765780321068).
no('Paredes', 'PARED', t, f, -8.33566951069481, 41.2062947118362).
no('Recarei', 'RECAR', f, f, -8.42215867462191, 41.1599363478137).
no('Sobrosa', 'SOBRO', t, f, -8.38118071581788, 41.2557331783506).
no('Vandama', 'VANDO', t, f, -8.34160692293342, 41.2328015719913).
no('Vila Cova de Carros', 'VCCAR', t, f, -8.35109395257277, 41.2090666564063).

linha('Paredes_Aguiar', 1, ['AGUIA','RECAR', 'PARAD', 'CETE', 'PARED'], 31, 15700).
linha('Paredes_Aguiar', 3, ['PARED', 'CETE','PARAD', 'RECAR', 'AGUIA'], 31, 15700).
linha('Paredes_Gandra', 5, ['GAND', 'VANDO', 'BALTR', 'MOURZ', 'PARED'], 26, 13000).
linha('Paredes_Gandra', 8, ['PARED', 'MOURZ', 'BALTR', 'VANDO', 'GAND'], 26, 13000).
linha('Paredes_Lordelo',9, ['LORDL','VANDO', 'BALTR', 'MOURZ', 'PARED'], 29, 14300).
linha('Paredes_Lordelo',11, ['PARED','MOURZ', 'BALTR', 'VANDO', 'LORDL'], 29, 14300).
linha('Lordelo_Parada', 24, ['LORDL', 'DIGRJ', 'CRIST', 'VCCAR', 'BALTR', 'PARAD'], 22, 11000).
linha('Lordelo_Parada', 26, ['PARAD', 'BARTR', 'VCCAR', 'CRIST', 'DIGRJ', 'LORDL'], 22, 11000).
%linha('Cristelo_Baltar', nd0, ['CRIST', 'VCCAR', 'BALTR'], 8, 4000).
%linha('Baltar_Cristelo', nd1, ['BARTR', 'VCCAR', 'CRIST'], 8, 4000).
linha('Sobrosa_Cete', 22, ['SOBRO', 'CRIST', 'BESTR', 'VCCAR', 'MOURZ', 'CETE'], 23, 11500).
linha('Sobrosa_Cete', 20, ['CETE', 'MOURZ', 'VCCAR', 'BESTR', 'CRIST', 'SOBRO'], 23, 11500).
linha('Estacao(Lordelo)_Lordelo',34,['ESTLO','LORDL'], 2,1500).
linha('Lordelo_Estacao(Lordelo)',35,['LORDL','ESTLO'], 2,1500).
linha('Estacao(Lordelo)_Sobrosa',36,['ESTLO','SOBRO'], 5,1500).
linha('Sobrosa_Estacao(Lordelo)',37,['SOBRO','ESTLO'], 5,1800).
linha('Estacao(Paredes)_Paredes',38,['ESTPA','PARED'], 2,1500).
linha('Paredes_Estacao(Paredes)',39,['PARED','ESTPA'], 2,1500).

horario(3,[[73800,74280,74580,74880,75420],[72900,73380,73680,73980,74520],[72000,72480,72780,73080,73620],[71100,71580,71880,72180,72720],[70200,70680,70980,71280,71820],[69300,69780,70080,70380,70920],[68400,68880,69180,69480,70020],[67500,67980,68280,68580,69120],[66600,67080,67380,67680,68220],[65700,66180,66480,66780,67320],[63900,64380,64680,64980,65520],[63000,63480,63780,64080,64620],[61200,61680,61980,62280,62820],[59400,59880,60180,60480,61020]]).
horario(1, [[36000, 36540, 36840, 37140, 37620], [37800, 38340, 38640, 38940, 39420], [41400, 41940, 42240, 42540, 43020], [48600, 49140, 49440, 49740, 50220], [55800, 56340, 56640, 56940, 57420]]).
horario(11, [[28320, 28800, 29040, 29280, 30060], [32820, 33300, 33540, 33780, 34560], [47220, 47700, 47940, 48180, 48960], [56220, 56700, 56940, 57180, 57960], [68820, 69300, 69540, 69780, 70560]]).
horario(9, [[29160, 29940, 30180, 30420, 30900], [35460, 36240, 36480, 36720, 37200], [54360, 55140, 55380, 55620, 56100], [65160, 65940, 66180, 66420, 66900], [74160, 74940, 75180,75420, 75900]]).
horario(8, [[36000, 36480, 36720, 36960, 37560], [43200, 43680, 43920, 44160, 44760],[54000, 54480, 54720, 54960, 55560], [72000, 72480, 72720, 72960, 73560]]).
horario(5, [[30360, 30960, 31200, 31440, 31920], [44760, 45360, 45600, 45840, 46320], [59160, 59760, 60000, 60240, 60720], [73560, 74160, 74400, 74640, 75120], [80760, 81360, 81600, 81840, 82320]]).
horario(24, [[26700, 27000, 27240, 27480, 27720, 28020], [39900, 40200, 40440, 40680, 40920, 41220], [49140, 49440, 49680, 49920, 50160, 50460], [59700, 60000, 60240, 60480, 60720, 61020], [70260, 70560, 70800, 71040, 71280, 71580]]).
horario(26, [[29640, 29940, 30180, 30420, 30660, 30960], [45480, 45780, 46020, 46260, 46500, 46800], [57360, 57660, 57900, 58140, 58380, 58680], [70560, 70860, 71100, 71340, 71580,71880]]).
horario(22, [[28800, 29040, 29400, 29640, 29880, 30180], [43200, 43440, 43800, 44040, 44280, 44580], [55200, 55440, 55800, 56040, 56280, 56580], [69600, 69840, 70200, 70440, 70680, 70980]]).
horario(20, [[30180, 30480, 30720, 30960, 31320, 31560], [43380, 43680, 43920, 44160, 44520, 44760], [54180, 54480, 54720, 54960, 55320, 55560], [67380, 67680, 67920, 68160, 68520, 68760]]).
horario(34, [[26580, 26700], [27900, 28020], [29220, 29340]]).
horario(35, [[73200, 73320], [74520, 74640], [75840, 75960]]).
horario(38, [[27300, 27420], [28200, 28320], [28740, 28860], [29100, 29220]]).
horario(39, [[76320, 76440], [89820, 89940], [82320, 82440], [75900, 76020]]).
horario(36, [[27300, 27600], [28500, 28800], [29700, 30000]]).
horario(37, [[77160, 77460], [74760, 75060], [75960, 76260]]).

:-dynamic liga/3.
gera_ligacoes:- 
	retractall(liga(_,_,_)),
	findall(_,((no(_,No1,t,f,_,_);
	no(_,No1,f,t,_,_)),
	(no(_,No2,t,f,_,_);
	no(_,No2,f,t,_,_)),
	No1\==No2,
	linha(_,N,LNos,_,_),
	ordem_membros(No1,No2,LNos),
	assertz(liga(No1,No2,N))),_).

ordem_membros(No1,No2,[No1|L]):- 
	member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- 
	ordem_membros(No1,No2,L).

caminho(Noi,Nof,LCaminho):-caminho(Noi,Nof,[],LCaminho).
caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
caminho(No1,Nof,Lusadas,Lfinal):-
	liga(No1,No2,N),
	\+member((_,_,N),Lusadas),
	\+member((No2,_,_),Lusadas),
	\+member((_,No2,_),Lusadas),
	caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).

:-dynamic melhor_sol_tempo/2.
plan_mud_mot(NoI,NoF,HorarioI,Caminho):-
	get_time(Ti),
	(melhor_caminho(NoI,NoF,HorarioI);true),
	retract(melhor_sol_tempo(Caminho,_)),
	get_time(Tf),
	TSolucao is Tf - Ti,
	write('Tempo de geracao de solucao: '), write(TSolucao),nl.

melhor_caminho(NoI,NoF,HorarioI):-
	asserta(melhor_sol_tempo(_,100000)),
	caminho(NoI,NoF,LCaminho),
	atualiza_melhor(HorarioI,LCaminho),
	fail.

atualiza_melhor(HorarioI, LCaminho):-
	melhor_sol_tempo(_,N),
	calculo_tempo(LCaminho,HorarioI,C),
	C<N, retract(melhor_sol_tempo(_,_)),
	asserta(melhor_sol_tempo(LCaminho,C)).

calculo_tempo([],Res,Res):-!.
calculo_tempo([(NoI,NoF,Linha)|T],HorarioInicial,Res):-
	linha(_,Linha,L,_,_),
	nth1(PosI,L,NoI),
	procurar_horario(Linha,PosI,HorarioInicial,Horario1),
	nth1(PosF,L,NoF),
	nth1(PosF,Horario1,HorarioFinal),
	calculo_tempo(T,HorarioFinal,Res).

%assumimos que o motorista demora 2 minutos a mudar de autocarro numa paragem
procurar_horario(Linha,Pos,HorarioInicial,Res):-
	HorarioInicial1 is HorarioInicial + 120,
	listar_horarios_linha(Linha,[H|T]),
	procurar_horario1(Pos,HorarioInicial1,H,Res).

procurar_horario1(Pos,HorarioInicial,[H|T],Res):-
	nth1(Pos,H,Sec),
	(Sec > HorarioInicial,!,Res = H;procurar_horario1(Pos,HorarioInicial,T,Res)).

listar_horarios_linha(Linha,Res):-
	findall(Hor, horario(Linha, Hor), L),sort(L,Res).