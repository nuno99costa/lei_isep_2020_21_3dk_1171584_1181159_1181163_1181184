no('Aguiar de Sousa', 'AGUIA', f, t, -8.4464785432391, 41.1293363229325).
no('Baltar', 'BALTR', t, f, -8.38716802227697, 41.1937898023744).
no('Besteiros', 'BESTR', f, t, -8.34043029659082, 41.217018845589).
no('Cete', 'CETE', t, f, -8.35164059584564, 41.183243425797).
no('Cristelo', 'CRIST', t, f, -8.34639896125324, 41.2207801252676).
no('Duas Igrejas', 'DIGRJ', t, f, -8.35481024956726, 41.2278665802794).
no('Estação (Lordelo)', 'ESTLO', f, t, -8.4227924957086, 41.2521157104055).
no('Estação (Paredes)', 'ESTPA', f, t, -8.33448520831829, 41.2082119860192).
no('Gandra', 'GAND', f, t, -8.43958765792976, 41.1956579348384).
no('Lordelo', 'LORDL', t, f, -8.42293614720057, 41.2452627470645).
no('Mouriz', 'MOURZ', f, f, -8.36577272258403, 41.1983610215263).
no('Parada de Todeia', 'PARAD', f, t, -8.37023578802149, 41.1765780321068).
no('Paredes', 'PARED', t, f, -8.33566951069481, 41.2062947118362).
no('Recarei', 'RECAR', f, t, -8.42215867462191, 41.1599363478137).
no('Sobrosa', 'SOBRO', t, f, -8.38118071581788, 41.2557331783506).
no('Vandoma', 'VANDO', t, f, -8.34160692293342, 41.2328015719913).
no('Vila Cova de Carros', 'VCCAR', t, f, -8.35109395257277, 41.2090666564063).

no('Paços de Ferreira', 'PFERR', f, t, -8.42215867462191, 41.1599363478137).
no('Freamunde', 'FREA', t, f, -8.38118071581788, 41.2557331783506).
no('Agrela', 'AGRE', f, t, -8.34160692293342, 41.2328015719913).
no('Carvalhosa', 'CARV', t, f, -8.35109395257277, 41.2090666564063).

no('Seroa', 'SERO', t, f, -8.35109395257277, 41.2090666564063).
no('Penamaior', 'PENAM', t, f, -8.35109395257277, 41.2090666564063).
no('Covas', 'COVS', t, f, -8.35109395257277, 41.2090666564063).

linha('Paredes_Aguiar', 1, ['AGUIA','RECAR', 'PARAD', 'CETE', 'PARED'], 31, 15700).
linha('Paredes_Aguiar', 3, ['PARED', 'CETE','PARAD', 'RECAR', 'AGUIA'], 31, 15700).
linha('Paredes_Gandra', 5 , ['GAND', 'VANDO', 'BALTR', 'MOURZ', 'PARED'], 26, 13000).
linha('Paredes_Gandra', 8, ['PARED', 'MOURZ', 'BALTR', 'VANDO', 'GAND'], 26, 13000).
linha('Paredes_Lordelo', 9, ['LORDL','VANDO', 'BALTR', 'MOURZ', 'PARED'], 29, 14300).
linha('Paredes_Lordelo', 11, ['PARED','MOURZ', 'BALTR', 'VANDO', 'LORDL'], 29, 14300).
linha('Lordelo_Parada', 24, ['LORDL', 'DIGRJ', 'CRIST', 'VCCAR', 'BALTR', 'PARAD'], 22, 11000).
linha('Lordelo_Parada', 26, ['PARAD', 'BALTR', 'VCCAR', 'CRIST', 'DIGRJ', 'LORDL'], 22,11000).
%linha('Cristelo_Baltar’, nd0, ['CRIST', 'VCCAR', 'BALTR'], 8, 4000).
%linha('Baltar_Cristelo’, nd1, ['BALTR', 'VCCAR', 'CRIST'], 8, 4000).
linha('Sobrosa_Cete', 22, ['SOBRO', 'CRIST', 'BESTR', 'VCCAR', 'MOURZ', 'CETE'], 23,11500).
linha('Sobrosa_Cete', 20, ['CETE', 'MOURZ', 'VCCAR', 'BESTR', 'CRIST', 'SOBRO'], 23,11500).
linha('Estação(Lordelo)_Lordelo',34,['ESTLO','LORDL'], 2,1500).
linha('Lordelo_Estação(Lordelo)',35,['LORDL','ESTLO'], 2,1500).
linha('Estação(Lordelo)_Sobrosa',36,['ESTLO','SOBRO'], 5,1500).
linha('Sobrosa_Estação(Lordelo)',37,['SOBRO','ESTLO'], 5,1800).
linha('Estação(Paredes)_Paredes',38,['ESTPA','PARED'], 2,1500).
linha('Paredes_Estação(Paredes)',39,['PARED','ESTPA'], 2,1500).

linha('Paços_Agrela', 40, ['PFERR','AGRE'], 31, 15700).
linha('Agrela_Freamunde', 41, ['AGRE', 'PFERR','CARV', 'FREA'], 31, 15700).
linha('Freamunde_Lordelo', 42, ['FREA', 'CARV', 'PFERR', 'ESTLO', 'LORDL'], 26, 13000).
linha('Paços_Lordelo', 43, ['PFERR', 'ESTLO', 'LORDL'], 26, 13000).
linha('Paços_Paredes', 43, ['PFERR', 'ESTLO', 'LORDL', 'ESTPA', 'PARED'], 26, 13000).
linha('Carvalhosa_Recarei', 45, ['CARV', 'SOBRO','RECAR'], 31, 15700).
linha('Agrela_Gandra', 46, ['AGR', 'CETE','PARAD', 'RECAR', 'GAND'], 31, 15700).
linha('Paredes_Agrela', 47, ['AGR', 'PFERR', 'CARV', 'MOURZ', 'PARED'], 26, 13000).
linha('Freamunde_Paços', 48, ['FREA', 'PFERR'], 26, 13000).
linha('Carvalhosa_Cristelo', 49, ['CARV','FREA', 'CRIST'], 29, 14300).

linha('A', 45, ['SERO', 'SOBRO','RECAR'], 31, 15700).
linha('B', 46, ['SERO', 'CETE','PFERR', 'COVS', 'PENAM'], 31, 15700).
linha('C', 47, ['PENAM', 'PFERR', 'COVS'], 26, 13000).
linha('D', 48, ['VCCAR', 'PFERR'], 26, 13000).
linha('D', 48, ['VCCAR', 'COVS'], 26, 13000).
linha('D', 48, ['VCCAR', 'PENAM'], 26, 13000).
linha('D', 48, ['PENAM', 'VCCAR'], 26, 13000).
linha('D', 48, ['COVS', 'VCCAR'], 26, 13000).
linha('I', 48, ['COVS', 'VCCAR'], 26, 13000).
linha('J', 49, ['CARV','COVS', 'VCCAR'], 29, 14300).
linha('K', 47, ['AGR', 'PFERR', 'COVS', 'ESTPA', 'VCCAR'], 26, 13000).
linha('L', 48, ['VCCAR', 'COVS'], 26, 13000).
linha('M', 49, ['VCCAR','FREA', 'PENAM'], 29, 14300).

linha('A', 45, ['SERO', 'PFERR','RECAR'], 31, 15700).
linha('B', 46, ['SERO', 'LORDL','PFERR', 'COVS', 'PENAM'], 31, 15700).
linha('C', 47, ['PENAM', 'LORDL', 'COVS'], 26, 13000).
linha('D', 48, ['LORDL', 'PFERR'], 26, 13000).
linha('D', 48, ['LORDL', 'COVS'], 26, 13000).
linha('D', 48, ['LORDL', 'PENAM'], 26, 13000).
linha('D', 48, ['PENAM', 'PFERR'], 26, 13000).
linha('D', 48, ['COVS', 'PFERR'], 26, 13000).
linha('I', 48, ['COVS', 'ESTPA'], 26, 13000).
linha('J', 49, ['CARV','COVS', 'CRIST'], 29, 14300).
linha('K', 47, ['AGR', 'PFERR', 'COVS', 'ESTPA', 'PARED'], 26, 13000).
linha('L', 48, ['FREA', 'COVS'], 26, 13000).
linha('M', 49, ['CARV','FREA', 'PENAM'], 29, 14300).

:-dynamic liga/3.
gera_ligacoes:- retractall(liga(_,_,_)),
findall(_,
((no(_,No1,t,f,_,_);no(_,No1,f,t,_,_)),
(no(_,No2,t,f,_,_);no(_,No2,f,t,_,_)),
No1\==No2,
linha(_,N,LNos,_,_),
ordem_membros(No1,No2,LNos),
assertz(liga(No1,No2,N))
),_).
ordem_membros(No1,No2,[No1|L]):- member(No2,L),!.
ordem_membros(No1,No2,[_|L]):- ordem_membros(No1,No2,L).

caminho(Noi,Nof,LCaminho):-caminho(Noi,Nof,[],LCaminho).
caminho(No,No,Lusadas,Lfinal):-reverse(Lusadas,Lfinal).
caminho(No1,Nof,Lusadas,Lfinal):-
liga(No1,No2,N),
\+member((_,_,N),Lusadas),
\+member((No2,_,_),Lusadas),
\+member((_,No2,_),Lusadas),
caminho(No2,Nof,[(No1,No2,N)|Lusadas],Lfinal).

menor_ntrocas(Noi,Nof,LCaminho_menostrocas):-
findall(LCaminho,caminho(Noi,Nof,LCaminho),LLCaminho),
menor(LLCaminho,LCaminho_menostrocas).

menor([H],H):-!.
menor([H|T],Hmenor):-menor(T,L1),length(H,C),length(L1,C1),
((C<C1,!,Hmenor=H);Hmenor=L1).
%alterar menos para comparar tempo gasto

plan_mud_mot(Noi,Nof,LCaminho_menostrocas):-
get_time(Ti),
findall(LCaminho,caminho(Noi,Nof,LCaminho),LLCaminho),
menor(LLCaminho,LCaminho_menostrocas),
get_time(Tf),
length(LLCaminho,NSol),
TSol is Tf-Ti,
write('Numero de Solucoes:'),write(NSol),nl,
write('Tempo de geracao da solucao:'),write(TSol),nl.

:- dynamic melhor_sol_ntrocas/2.
plan_mud_mot1(Noi,Nof,LCaminho_menostrocas):-
get_time(Ti),
(melhor_caminho(Noi,Nof);true),
retract(melhor_sol_ntrocas(LCaminho_menostrocas,_)),
get_time(Tf),
TSol is Tf-Ti,
write('Tempo de geracao da solucao:'),write(TSol),nl.

melhor_caminho(Noi,Nof):-
asserta(melhor_sol_ntrocas(_,10000)),
caminho(Noi,Nof,LCaminho),
atualiza_melhor(LCaminho),
fail.

atualiza_melhor(LCaminho):-
melhor_sol_ntrocas(_,N),
length(LCaminho,C),
C<N,retract(melhor_sol_ntrocas(_,_)),
asserta(melhor_sol_ntrocas(LCaminho,C)).