import { browser, by, element, ElementFinder } from 'protractor';

export class LinhaPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }

    getLoginButton(){
        return element(by.id('loginButton'));
    }

    getEmailInput(){
        return element(by.id('emailInput'));
    }

    getPasswordInput(){
        return element(by.id('pwInput'));
    }



    //---------------------------------------------------------------LINHA-------------------------------------------------------------

    //Opção do select
    getOptionCriarLinha() {
        return element(by.id('postLinha'));
    }

    getInputIDLinha() {
        return element(by.id('inLinhaId'));
    }

    getInputCodigoLinha() {
        return element(by.id('inCodigoLinha'));
    }

    getInputNomeLinha() {
        return element(by.id('inNomeLinha'));
    }

    getInputCorLinha() {
        return element(by.id('inCorLinha'));
    }

    getInputIDNoInicialLinha() {
        return element(by.id('inNoInicialIdLinha'));
    }

    getInputIDNoFinalLinha() {
        return element(by.id('inNoFinalIdLinha'));
    }

    getInputTVPermitido() {
        return element(by.id('inIdTVPermitido'));
    }

    getInputTVProibido() {
        return element(by.id('inIdTVProibido'));
    }

    getInputTTPermitido() {
        return element(by.id('inIdTTPermitido'));
    }

    getInputTTProibido() {
        return element(by.id('inIdTTProibido'));
    }

    getSubmitLinhaButton() {
        return element(by.id('submitLinhaButton'));
    }




}