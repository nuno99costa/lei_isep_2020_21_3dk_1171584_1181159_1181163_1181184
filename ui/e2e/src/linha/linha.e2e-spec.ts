/* 
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { LinhaPage } from './linha.po';


describe('LINHA', () => {
    let page: LinhaPage;

    beforeEach(async () => {
        page = new LinhaPage();
        browser.waitForAngularEnabled(false);
        page.navigateTo();
        var loginButton = page.getLoginButton();
        var pw = page.getPasswordInput();
        var email = page.getEmailInput();
        await UtilsTest.waitElements([email, pw, loginButton]);
        email.sendKeys("admin@gmail.com");
        pw.sendKeys("administrador1");
        await loginButton.click();
        await UtilsTest.waitElement(page.getMasterDataRedeButton());

    });

    //---------------------------------------------------------------LINHA---------------------------------------------

    describe('CRIAR LINHA', () => {
        it('should display criar linha option', async () => {
            
            expect(await UtilsTest.testDisplayInputs(page.getOptionCriarLinha(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar linha');
        })

         it('should display input do id da linha', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getInputIDLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input do codigo da linha', async () => {
   
            expect(await UtilsTest.testDisplayInputs(page.getInputCodigoLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input do nome da linha', async () => {
    
            expect(await UtilsTest.testDisplayInputs(page.getInputNomeLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input da cor da linha', async () => {
  
            expect(await UtilsTest.testDisplayInputs(page.getInputCorLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input ID No Inicial da linha', async () => {
     
            expect(await UtilsTest.testDisplayInputs(page.getInputIDNoInicialLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display input ID No Final da linha', async () => {
       
            expect(await UtilsTest.testDisplayInputs(page.getInputIDNoFinalLinha(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display input Tipo de Viatura Permitido', async () => {
    
            expect(await UtilsTest.testDisplayInputs(page.getInputTVPermitido(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display input Tipo de Tripulante Permitido', async () => {
          
            expect(await UtilsTest.testDisplayInputs(page.getInputTTPermitido(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display input Tipo de Viatura Proibido', async () => {
        
            expect(await UtilsTest.testDisplayInputs(page.getInputTVProibido(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display input Tipo de Tripulante Proibido', async () => {
        
            expect(await UtilsTest.testDisplayInputs(page.getInputTTProibido(), [page.getMasterDataRedeButton()], 'input')).toEqual('select');
        })

        it('should display submit linha button', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getSubmitLinhaButton(), [page.getMasterDataRedeButton()], 'text')).toEqual('Submit');
        })

        it('should create a linha and alert of the succes with the linha id', async () => {
            
            page.getMasterDataRedeButton().click();
            const inputId = page.getInputIDLinha();
            const inputNome = page.getInputNomeLinha();
            const inputNoIni = page.getInputIDNoInicialLinha();
            const inputNoFinal = page.getInputIDNoFinalLinha();
            const inputCodigo = page.getInputCodigoLinha();
            const inputCor = page.getInputCorLinha();
  
            await UtilsTest.waitElements([inputId, inputNome, inputNoIni, inputNoFinal, inputCodigo, inputCor]);
            inputId.sendKeys('100');
            inputNome.sendKeys('ac');
            inputNoIni.sendKeys('a');
            inputNoFinal.sendKeys('c');
            inputCodigo.sendKeys('123');
            inputCor.sendKeys('yellow');
            await UtilsTest.sendClick(page.getSubmitLinhaButton());
            await UtilsTest.waitForAlert();
            let alert = browser.switchTo().alert();
            expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
            (await alert).accept();
  
        }) 

    })


}); */