
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { SegmentoPage } from './segmento.po';


describe('SEGMENTO', () => {
  let page: SegmentoPage;

  beforeEach(async () => {
    page = new SegmentoPage();
    browser.waitForAngularEnabled(false);
    page.navigateTo();
    var loginButton = page.getLoginButton();
    var pw = page.getPasswordInput();
    var email = page.getEmailInput();
    await UtilsTest.waitElements([email, pw, loginButton]);
    email.sendKeys("admin@gmail.com");
    pw.sendKeys("administrador1");
    await loginButton.click();
    await UtilsTest.waitElement(page.getMasterDataRedeButton());
  });


  //------------------------------------------------------------CRIAR SEGMENTO------------------------------------------------

  describe('CRIAR SEGMENTO', () => {

    it('should display the criar segmento', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getOptionCriarSegmento(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar segmento');
    })

    it('should display input do id do segmento', async () => {
 
      expect(await UtilsTest.testDisplayInputs(page.getInputSegmentoID(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should display input do nomeSegmento do segmento', async () => {
   
      expect(await UtilsTest.testDisplayInputs(page.getInputNomeSegmento(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should display input do idNoInicial do segmento', async () => {
  
      expect(await UtilsTest.testDisplayInputs(page.getInputIdNoInicial(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display input do idNoFinal do segmento', async () => {
     
      expect(await UtilsTest.testDisplayInputs(page.getInputIdNoFinal(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display input da distanciaNos do segmento', async () => {
    
      expect(await UtilsTest.testDisplayInputs(page.getInputDistanciaEntreNos(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should display input do tempoNos do segmento', async () => {
    
      expect(await UtilsTest.testDisplayInputs(page.getInputTempoEntreNos(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should create a segmento and alert of the succes with the segmento id', async () => {


      page.getMasterDataRedeButton().click();
      const inputId = page.getInputSegmentoID();
      const inputNome = page.getInputNomeSegmento();
      const inputNoIni = page.getInputIdNoInicial();
      const inputNoFinal = page.getInputIdNoFinal();
      const inputTempo = page.getInputTempoEntreNos();
      const inputDist = page.getInputDistanciaEntreNos();

      await UtilsTest.waitElements([inputId, inputNome, inputNoIni, inputNoFinal, inputTempo, inputDist]);
      inputId.sendKeys('100');
      inputNome.sendKeys('ac');
      inputNoIni.sendKeys('a');
      inputNoFinal.sendKeys('c');
      inputTempo.sendKeys(1);
      inputDist.sendKeys(1);
      await UtilsTest.sendClick(page.getSubmitButton());
      await UtilsTest.waitForAlert();
      let alert = browser.switchTo().alert();
      expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
      (await alert).accept();

    })


  })
})
 