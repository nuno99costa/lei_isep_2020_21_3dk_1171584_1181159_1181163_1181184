import { browser, by, element, ElementFinder } from 'protractor';

export class SegmentoPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }

    getLoginButton(){
        return element(by.id('loginButton'));
    }

    getEmailInput(){
        return element(by.id('emailInput'));
    }

    getPasswordInput(){
        return element(by.id('pwInput'));
    }
    
    //------------------------------------------------------------CRIAR SEGMENTO------------------------------------------------

    getOptionCriarSegmento() {
        return element(by.id('postSegmento'));
    }

    getInputSegmentoID() {
        return element(by.id('inputSegmentoId'));
    }

    getInputNomeSegmento() {
        return element(by.id('inputNomeSegmento'));
    }

    getInputIdNoInicial() {
        return element(by.id('inputIdNoInicialSegmento'));
    }

    getInputIdNoFinal() {
        return element(by.id('inputIdNoFinalSegmento'));
    }

    getInputDistanciaEntreNos() {
        return element(by.id('inputDistancia'));
    }

    getInputTempoEntreNos() {
        return element(by.id('inputTempo'));
    }

    getSubmitButton(){
        return element(by.id('submitSegmento'));
    }


}