
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { PercursoPage } from './percurso.po';


describe('PERCURSO', () => {
  let page: PercursoPage;

  beforeEach(async () => {
    page = new PercursoPage();
    browser.waitForAngularEnabled(false);
    page.navigateTo();
    var loginButton = page.getLoginButton();
    var pw = page.getPasswordInput();
    var email = page.getEmailInput();
    await UtilsTest.waitElements([email, pw, loginButton]);
    email.sendKeys("admin@gmail.com");
    pw.sendKeys("administrador1");
    await loginButton.click();
    await UtilsTest.waitElement(page.getMasterDataRedeButton());

  });

  //------------------------------------------------------------CRIAR PERCURSO------------------------------------------------

  describe('CRIAR PERCURSO', () => {
    it('should display criar percurso option', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getOptionCriarPercurso(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar percurso');
    })

    it('should display input do id do percurso', async () => {
 
      expect(await UtilsTest.testDisplayInputs(page.getInputIDPercurso(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should display input do nomePercurso do percurso', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getInputNomePercurso(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
    })

    it('should display input do idLinha do percurso', async () => {
    
      expect(await UtilsTest.testDisplayInputs(page.getInputIDLinhaPercurso(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display a listagem select bar', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getInputOrientacaoPercurso(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display input da listaSegmentos do percurso', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getInputListaSegmentosPercurso(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display input do idNoInicial do percurso', async () => {
 
      expect(await UtilsTest.testDisplayInputs(page.getInputIdNoInicialPercurso(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    it('should display input do idNoFinal do percurso', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getInputIdNoFinalPercurso(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
    })

    /* it('should create a percurso and alert of the succes with the percurso id', async () => {
        page.navigateTo();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
        page.getMasterDataRedeButton().click();
        const inputId = page.getInputIDPercurso();
        const inputNome = page.getInputNomePercurso();
        const inputLinha = page.getInputIDLinhaPercurso();
        const inputNoIni = page.getInputIdNoInicialPercurso();
        const inputNoFinal = page.getInputIdNoFinalPercurso();
        const inputSegmentos = page.getInputListaSegmentosPercurso();
        await UtilsTest.waitElements([inputId, inputNome, inputNoIni, inputNoFinal, inputLinha, inputSegmentos]);
        inputId.sendKeys('100');
        inputNome.sendKeys('1');
        inputNoIni.sendKeys('a');
        inputNoFinal.sendKeys('c');
        page.getInputOrientacaoPercurso().sendKeys('Vazio');
        inputLinha.sendKeys('ac');
        inputSegmentos.sendKeys('ab');
        await UtilsTest.sendClick(page.getAddSegmentoButton());
        inputSegmentos.sendKeys('bc');
        await UtilsTest.sendClick(page.getAddSegmentoButton());
        await browser.sleep(2000);
        await UtilsTest.sendClick(page.getSubmitButton());
        await UtilsTest.waitForAlert();
        let alert = browser.switchTo().alert();
        expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
        (await alert).accept();
  
      }) */
  })



  //------------------------------------------------------------LISTAGENS PERCURSO------------------------------------------------

  describe('LISTAR PERCURSOS DE UMA LISTA', () => {

    it('should display listar percursos option', async () => {

      expect(await UtilsTest.testDisplayInputs(page.getOptionListarPercursos(), [page.getMasterDataRedeButton()], 'text')).toEqual('Listagem dos percursos de uma linha');
    })

  })

}); 