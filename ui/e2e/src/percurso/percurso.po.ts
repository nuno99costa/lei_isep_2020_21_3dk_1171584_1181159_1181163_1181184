import { browser, by, element, ElementFinder } from 'protractor';

export class PercursoPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }

    getPlaneamentoButton(){
        return element(by.id('btnPlaneamento'));
      }

      getLoginButton(){
        return element(by.id('loginButton'));
    }

    getEmailInput(){
        return element(by.id('emailInput'));
    }

    getPasswordInput(){
        return element(by.id('pwInput'));
    }

    //------------------------------------------------------------CRIAR PERCURSO------------------------------------------------

    //Opção do select
    getOptionCriarPercurso() {
        return element(by.id('postPercurso'));
    }

    getInputIDPercurso() {
        return element(by.id('inputPercursoId'));
    }

    getInputNomePercurso() {
        return element(by.id('inputNomePercurso'));
    }

    getInputIDLinhaPercurso() {
        return element(by.id('inputIdLinha'));
    }

    getInputOrientacaoPercurso() {
        return element(by.name('escolherOrientacao'));
    }

    getInputListaSegmentosPercurso() {
        return element(by.id('inputListaSegmentos'));
    }

    getInputIdNoInicialPercurso() {
        return element(by.id('inputIdNoInicial'));
    }

    getInputIdNoFinalPercurso() {
        return element(by.id('inputIdNoFinal'));
    }

    getSubmitButton() {
        return element(by.id('submitPercurso'));
    }

    getAddSegmentoButton() {
        return element(by.id('addSegmentoButton'));
    }

    getIdaOption() {
        return element(by.id('ida'));
    }
    //------------------------------------------------------------LISTAR PERCURSO------------------------------------------------

    //Opção do select
    getOptionListarPercursos() {
        return element(by.id('getPercursos'));
    }

    getInputIdLinhaPercursos() {
        return element(by.id('inputLinhaIdLista'));
    }

    //-------------------------------------------------------MELHOR PERCURSO 2 NOS-----------------------------------

    getInputMelhorPercursoNoInicial(){
        return element(by.id('inputAbvNoInicialSegmento'))
    }

    getInputMelhorPercursoNoFinal(){
        return element(by.id('inputAbvNoFinalSegmento'));
    }

    getLabelResultadoMelhorPercurso(){
        return element(by.id('outputPercursoObtido'));
    }

}