
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { ViagemPage } from './viagem.po';


describe('Viagem', () => {
    let page: ViagemPage;

    beforeEach(async () => {
        page = new ViagemPage();
        browser.waitForAngularEnabled(false);
        page.navigateTo();
        var loginButton = page.getLoginButton();
        var pw = page.getPasswordInput();
        var email = page.getEmailInput();
        await UtilsTest.waitElements([email, pw, loginButton]);
        email.sendKeys("admin@gmail.com");
        pw.sendKeys("administrador1");
        await loginButton.click();
        await UtilsTest.waitElement(page.getMasterDataViagemButton());

    });

/* 
    afterEach(async () => {
        // Assert that there are no errors emitted from the browser
        const logs = await browser.manage().logs().get(logging.Type.BROWSER);
        expect(logs).not.toContain(jasmine.objectContaining({
            level: logging.Level.SEVERE,
        } as logging.Entry));
    });*/

}); 