import { browser, by, element, ElementFinder } from 'protractor';

export class ViagemPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataViagemButton() {
        return element(by.id('btnMDV'));
    }

    getLoginButton() {
        return element(by.id('loginButton'));
    }

    getEmailInput() {
        return element(by.id('emailInput'));
    }

    getPasswordInput() {
        return element(by.id('pwInput'));
    }
    //------------------------------------------------------------CRIAR VIAGEM------------------------------------------------

    getLinhaSelect(){
        return element(by.id('inputListaLinhas'));
    }

    getViagemIdInput(){
        return element(by.id('inputViagemId'));
    }

    getPercursoIdInput(){
        return element(by.id('inputListaPercursos'));
    }

    getTimeInput(){
        return element(by.id('timepickerV'));
    }

    getSubmitBtn(){
        return element(by.id('submitViagem'));
    }
    

}