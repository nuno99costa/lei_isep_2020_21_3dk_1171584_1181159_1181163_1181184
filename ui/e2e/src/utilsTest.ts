import { browser, ElementFinder, protractor } from 'protractor';

export class UtilsTest {
    static async waitElement(element: ElementFinder) {
        const EC = protractor.ExpectedConditions;
        await browser.wait(EC.visibilityOf(element), 20000);
    };

    static async waitElements(elements: ElementFinder[]) {
        const EC = protractor.ExpectedConditions;
        let i = 0;
        for (i = 0; i < elements.length; i++) {
            await browser.wait(EC.visibilityOf(elements[i]), 15000);
        }

    }

    static async sendClick(element: ElementFinder): Promise<boolean> {
        try {
            if (!await element.isDisplayed()) {
                return false;
            }
            await browser.executeScript('arguments[0].click();', await element.getWebElement());
            return true;
        }
        catch (err) {
            return false;
        }
    }

    static async waitForAlert() {
        const EC = protractor.ExpectedConditions;
        await browser.wait(EC.alertIsPresent(), 20000);
    }

    //Método que testa o display de SELECTS e INPUTS
    static async testDisplayInputs(element: ElementFinder, botoesAClicar: ElementFinder[], type: string) {

        let i = 0;
        for (i = 0; i < botoesAClicar.length; i++) {
            await this.sendClick(botoesAClicar[i]);
        }

        await this.waitElement(element);

        switch (type) {
            case 'input':

                return element.getTagName();


            case 'select':
                return element.getTagName();


            case 'text':

                return element.getText();

            default:
                return 'erro';
        }

    }
}