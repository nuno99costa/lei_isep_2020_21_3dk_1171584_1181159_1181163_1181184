import { browser, by, element, ElementFinder } from 'protractor';

export class TipoTripulantePage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }


    getLoginButton(){
        return element(by.id('loginButton'));
    }

    getEmailInput(){
        return element(by.id('emailInput'));
    }

    getPasswordInput(){
        return element(by.id('pwInput'));
    }

    //------------------------------------------------------------CRIAR TIPO DE TRIPULANTE------------------------------------------------
    //Opção do select
    getOptionCriarTipoTripulante() {
        return element(by.id('postTipoTripulante'));
    }

    getInputIdTT() {
        return element(by.id('inputIdTT'));
    }

    getInputCodigoTT() {
        return element(by.id('inputCodigoTT'));
    }

    getInputDescricaoTT() {
        return element(by.id('inputDescricaoTT'));
    }

    getSubmitTTButton() {
        return element(by.id('submitTTButton'));
    }
}