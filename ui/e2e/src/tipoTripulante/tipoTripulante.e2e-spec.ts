 
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { TipoTripulantePage } from './tipoTripulante.po';


describe('TIPO DE TRIPULANTE', () => {
    let page: TipoTripulantePage;

    beforeEach(async () => {
        page = new TipoTripulantePage();
        browser.waitForAngularEnabled(false);
        page.navigateTo();
        var loginButton = page.getLoginButton();
        var pw = page.getPasswordInput();
        var email = page.getEmailInput();
        await UtilsTest.waitElements([email, pw, loginButton]);
        email.sendKeys("admin@gmail.com");
        pw.sendKeys("administrador1");
        await loginButton.click();
        await UtilsTest.waitElement(page.getMasterDataRedeButton());

    });

    //------------------------------------------------------------CRIAR TIPO DE TRIPULANTE------------------------------------------------

    describe('CRIAR TIPO DE TRIPULANTE', () => {
        it('should display criar tipo de tripulante option', async () => {
     
            expect(await UtilsTest.testDisplayInputs(page.getOptionCriarTipoTripulante(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar tipo de tripulante');
        })

        it('should display input ID de tipo de tripulante', async () => {
   
            expect(await UtilsTest.testDisplayInputs(page.getInputIdTT(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input codigo de tipo de tripulante', async () => {
     
            expect(await UtilsTest.testDisplayInputs(page.getInputCodigoTT(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input descricao de tipo de tripulante', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getInputDescricaoTT(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display submit tipo de tripulante button', async () => {
       
            expect(await UtilsTest.testDisplayInputs(page.getSubmitTTButton(), [page.getMasterDataRedeButton()], 'text')).toEqual('Submit');
        })

        it('should create a tipo de tripulante and alert of the succes with the tipo de tripulante id', async () => {
 
            page.getMasterDataRedeButton().click();
            const inputId = page.getInputIdTT();
            const inputCodigo = page.getInputCodigoTT();
            const inputDescricao = page.getInputDescricaoTT();

            await UtilsTest.waitElements([inputId, inputCodigo, inputDescricao]);
            inputId.sendKeys('100');
            inputCodigo.sendKeys('12345678901234567890');
            inputDescricao.sendKeys('ola');

            await UtilsTest.sendClick(page.getSubmitTTButton());
            await UtilsTest.waitForAlert();
            let alert = browser.switchTo().alert();
            expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
            (await alert).accept();

        })

    })


});  