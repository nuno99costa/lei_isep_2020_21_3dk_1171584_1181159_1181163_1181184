  import { NoPage } from './no.po';
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';


describe('NÓ', () => {
    let page: NoPage;

    beforeEach(async () => {
        page = new NoPage();
        browser.waitForAngularEnabled(false);
        page.navigateTo();
        var loginButton = page.getLoginButton();
        var pw = page.getPasswordInput();
        var email = page.getEmailInput();
        await UtilsTest.waitElements([email, pw, loginButton]);
        email.sendKeys("admin@gmail.com");
        pw.sendKeys("administrador1");
        await loginButton.click();
        await UtilsTest.waitElement(page.getMasterDataRedeButton());
    });

    //------------------------------------------------CRIAR NÓ---------------------------------------------------------

    describe('CRIAR NÓ', () => {

        it('should display the criar no option', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getOptionCriarNo(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar nó');
        })

        it('should display input do id do nó', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getNoInputId(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input do nomeCompleto do nó', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getInputNomeCompletoNo(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input da abreviatura do nó', async () => {

            expect(await UtilsTest.testDisplayInputs(page.getInputAbreviaturaNo(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input do isPontoRendicao do nó', async () => {
   
            expect(await UtilsTest.testDisplayInputs(page.getInputPontoRendicao(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input do isEstacaoRecolha do nó', async () => {
   
            expect(await UtilsTest.testDisplayInputs(page.getInputEstacaoRecolha(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input da latitude do nó', async () => {
      
            expect(await UtilsTest.testDisplayInputs(page.getInputLatitude(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input da longitude do nó', async () => {
     
            expect(await UtilsTest.testDisplayInputs(page.getInputLongitude(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display submit no button', async () => {
         
            expect(await UtilsTest.testDisplayInputs(page.getSubmitNoButton(), [page.getMasterDataRedeButton()], 'text')).toEqual('Submit');
        })

        it('should create a no and alert of the succes with the no id', async () => {

            page.getMasterDataRedeButton().click();
            const inputIdNo = page.getNoInputId();
            const inputNome = page.getInputNomeCompletoNo();
            const inputAbv = page.getInputAbreviaturaNo();
            const inputPontoRendicao = page.getInputPontoRendicao();
            const inputER = page.getInputEstacaoRecolha();
            const inputLat = page.getInputLatitude();
            const inputLong = page.getInputLongitude();
            await UtilsTest.waitElements([inputIdNo, inputAbv, inputNome, inputPontoRendicao, inputER, inputLat, inputLong]);
            inputIdNo.sendKeys('100');
            inputNome.sendKeys('teste');
            inputAbv.sendKeys('teste');
            inputLat.sendKeys(1);
            inputLong.sendKeys(1);
            await UtilsTest.sendClick(page.getSubmitNoButton());
            await UtilsTest.waitForAlert();
            let alert = browser.switchTo().alert();
            expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
            (await alert).accept();

        })

    })


});