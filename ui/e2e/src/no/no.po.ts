import { browser, by, element, ElementFinder } from 'protractor';

export class NoPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }

    getLoginButton(){
        return element(by.id('loginButton'));
    }

    getEmailInput(){
        return element(by.id('emailInput'));
    }

    getPasswordInput(){
        return element(by.id('pwInput'));
    }

    //----------------------------------------------------------------------NÓ--------------------------------------------------------

    getOptionCriarNo() {
        return element(by.id('postNo'));
    }

    getNoInputId() {
        return element(by.id('inputNoId'));
    }

    getInputNomeCompletoNo() {
        return element(by.id('inputNomeCompleto'));
    }

    getInputAbreviaturaNo() {
        return element(by.id('inputAbreviatura'));
    }

    getInputPontoRendicao() {
        return element(by.id('pontoRendicaoCheck'));
    }

    getInputEstacaoRecolha() {
        return element(by.id('estacaoRecolhaCheck'));
    }

    getInputLatitude() {
        return element(by.id('inputLatitude'));
    }

    getInputLongitude() {
        return element(by.id('inputLongitude'));
    }

    getSubmitNoButton() {
        return element(by.css('app-dados-no form button'));
    }

}