import { browser, by, element, ElementFinder } from 'protractor';

export class TipoViaturaPage {
    async navigateTo(): Promise<unknown> {
        return browser.get(browser.baseUrl);
    }

    //-----------------------------------------------GERAIS-----------------------------------------
    getMasterDataRedeButton() {
        return element(by.id('btnMDR'));
    }

    getLoginButton(){
      return element(by.id('loginButton'));
  }

  getEmailInput(){
      return element(by.id('emailInput'));
  }

  getPasswordInput(){
      return element(by.id('pwInput'));
  }
    //------------------------------------------------------------CRIAR TIPO DE VIATURA------------------------------------------------

  getOptionCriarTipoViatura() {
    return element(by.id('postTipoViatura'));
  }

  getInputIdTV() {
    return element(by.id('inputIdTv'));
  }

  getInputCodigoTV() {
    return element(by.id('inputCodigoTv'));
  }

  getInputDescricaoTV() {
    return element(by.id('inputDescricaoTv'));
  }

  getInputCombustivelTV() {
    return element(by.name('escolherCombustivel'));
  }

  getInputAutonomiaTV() {
    return element(by.id('inputAutonomiaTv'));
  }

  getInputVelocidadeMediaTV() {
    return element(by.id('inputVelocidadeMediaTv'));
  }

  getInputCustoPorQuilometroTV() {
    return element(by.id('inputCustoPorQuilometroTv'));
  }

  getInputConsumoMedioTV() {
    return element(by.id('inputConsumoMedioTv'));
  }

  getSubmitTVButton() {
    return element(by.id('submitTVButton'));
  }
}