
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from '../utilsTest';
import { TipoViaturaPage } from './tipoViatura.po';



describe('TIPO DE VIATURA', () => {
    let page: TipoViaturaPage;

    beforeEach(async () => {
        page = new TipoViaturaPage();
        browser.waitForAngularEnabled(false);
        page.navigateTo();
        var loginButton = page.getLoginButton();
        var pw = page.getPasswordInput();
        var email = page.getEmailInput();
        await UtilsTest.waitElements([email, pw, loginButton]);
        email.sendKeys("admin@gmail.com");
        pw.sendKeys("administrador1");
        await loginButton.click();
        await UtilsTest.waitElement(page.getMasterDataRedeButton());

    });

    //------------------------------------------------------------CRIAR TIPO DE VIATURA------------------------------------------------

    describe('CRIAR TIPO DE VIATURA', () => {
        it('should display criar tipo de viatura option', async () => {
        
            expect(await UtilsTest.testDisplayInputs(page.getOptionCriarTipoViatura(), [page.getMasterDataRedeButton()], 'text')).toEqual('Criar tipo de viatura');
        })


        it('should display input ID de tipo de viatura', async () => {
 
            expect(await UtilsTest.testDisplayInputs(page.getInputIdTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input código de tipo de viatura', async () => {
    
            expect(await UtilsTest.testDisplayInputs(page.getInputCodigoTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input descrição de tipo de viatura', async () => {
      
            expect(await UtilsTest.testDisplayInputs(page.getInputDescricaoTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input combustível de tipo de viatura', async () => {
    
            expect(await UtilsTest.testDisplayInputs(page.getInputCombustivelTV(), [page.getMasterDataRedeButton()], 'select')).toEqual('select');
        })

        it('should display input autonomia de tipo de viatura', async () => {
          
            expect(await UtilsTest.testDisplayInputs(page.getInputAutonomiaTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input velocidade média de tipo de viatura', async () => {
         
            expect(await UtilsTest.testDisplayInputs(page.getInputVelocidadeMediaTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');
        })

        it('should display input custo por quilómetro de tipo de viatura', async () => {
          
            expect(await UtilsTest.testDisplayInputs(page.getInputCustoPorQuilometroTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should display input consumo médio de tipo de viatura', async () => {
 
            expect(await UtilsTest.testDisplayInputs(page.getInputConsumoMedioTV(), [page.getMasterDataRedeButton()], 'input')).toEqual('input');;
        })

        it('should create a tipo de tripulante and alert of the succes with the tipo de tripulante id', async () => {

            page.getMasterDataRedeButton().click();
            const inputId = page.getInputIdTV();
            const inputCodigo = page.getInputCodigoTV();
            const inputDescricao = page.getInputDescricaoTV();
            const inputCombustivel = page.getInputCombustivelTV();
            const inputAutonomia = page.getInputAutonomiaTV();
            const inputVelocidade = page.getInputVelocidadeMediaTV();
            const inputCusto = page.getInputCustoPorQuilometroTV();
            const inputConsumo = page.getInputConsumoMedioTV();
  
            await UtilsTest.waitElements([inputId, inputCodigo, inputDescricao, inputCombustivel, inputAutonomia, inputVelocidade, inputCusto, inputConsumo]);
            inputId.sendKeys('100');
            inputCodigo.sendKeys('12345678901234567890');
            inputDescricao.sendKeys('ola');
            inputCombustivel.sendKeys('GPL');
            inputAutonomia.sendKeys(1);
            inputVelocidade.sendKeys(1);
            inputCusto.sendKeys(1);
            inputConsumo.sendKeys(1.005);
        
            await UtilsTest.sendClick(page.getSubmitTVButton());
            await UtilsTest.waitForAlert();
            let alert = browser.switchTo().alert();
            expect<any>((await alert).getText()).toEqual('Sucesso! O seu objeto foi criado com id: 100');
            (await alert).accept();
  
        })
    })

}); 