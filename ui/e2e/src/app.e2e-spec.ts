/* import { AppPage } from './app.po';
import { browser, by, ElementFinder, logging, protractor } from 'protractor';
import { UtilsTest } from './utilsTest';


describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    browser.waitForAngularEnabled(false);

  });

  async function waitElement(element: ElementFinder) {
    const EC = protractor.ExpectedConditions;
    await browser.wait(EC.visibilityOf(element), 15000);
  };

  describe('HEADER DA PÁGINA', () => {

    it('should display the title text', async () => {
      page.navigateTo();
      await waitElement(page.getTitleText());
      expect<any>(page.getTitleText().getText()).toEqual('Bem-vindo!');
    })

    it('should display the lead text', async () => {
      page.navigateTo();
      await waitElement(page.getLeadText());
      expect<any>(page.getLeadText().getText()).toEqual('O melhor sistema de gestão de transportes');
    })

    it('should display Master Data Rede button', () => {
      page.navigateTo();
      browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
      expect<any>(page.getMasterDataRedeButton().getText()).toEqual('Master Data Rede');
    })

    it('should display Planeamento button', () => {
      page.navigateTo();
      browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
      expect<any>(page.getPlaneamentoButton().getText()).toEqual('Planeamento');
    })

  })


  //----------------------------------------------------------MDR--------------------------------------------------------

  describe('COMPONENTE MDR', () => {
    it('should display master data rede section', async () => {
      page.navigateTo();

      browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
      page.getMasterDataRedeButton().click();
      await waitElement(page.getMasterDataRedeSectionTitle());
      expect<any>(page.getMasterDataRedeSectionTitle().getText()).toEqual('Master Data Rede');

    })


    it('should display a select bar', async () => {
      page.navigateTo();
      browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
      page.getMasterDataRedeButton().click();
      await waitElement(page.getMasterDataRedeSelectionBar());

      expect<any>(page.getMasterDataRedeSelectionBar().getTagName()).toEqual('SELECT');
    })

    
    //------------------------------------------------------------INSERIR GLX------------------------------------------------

    describe('INSERIR GLX', () => {

      it('should display inserir glx option', async () => {
        page.navigateTo();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
        page.getMasterDataRedeButton().click();
        const element = page.getOptionInserirGLX();
        await waitElement(element);
        expect<any>(element.getText()).toEqual('Inserir GLX');
      })

      it('should display file input', async () => {
        page.navigateTo();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
        page.getMasterDataRedeButton().click();
        const element = page.getInputInserirFicheiroGLX();
        await waitElement(element);
        expect<any>(element.getText()).toEqual('Escolha um ficheiro');
      })

      it('should display submit file button', async () => {
        page.navigateTo();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMasterDataRedeButton()), 10000);
        page.getMasterDataRedeButton().click();
        const element = page.getSubmitGLXButton();
        await waitElement(element);
        expect<any>(element.getText()).toEqual('Submit');
      })


    })

    //-----------------------------------PLANEAMENTO---------------------------------------------------------
    
    describe('COMPONENTE PLANEAMENTO',  () =>{

      it('should display planeamento button', async () => {
        page.navigateTo();
        browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getPlaneamentoButton()), 10000);
        expect<any>(page.getPlaneamentoButton().getText()).toEqual('Planeamento');
      })
      
    })

  })
  describe('COMPONENTE MAPA', () => {

    it('should display map section', async () => {
      page.navigateTo();
      browser.wait(protractor.ExpectedConditions.elementToBeClickable(page.getMapaButton()), 20000);
      page.getMapaButton().click();
      const element = page.getMapaSection();
      await waitElement(element);
      expect<any>(element.getText()).toEqual('Mapa');
    })

     it('should display mapbox map', async () => {
      page.navigateTo();
      await UtilsTest.sendClick(page.getMapaButton());
      const element = page.getMapaMapbox();
      await waitElement(element);
      expect<any>(element.getAttribute('name')).toEqual('mapa');
    }) 

  })


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
 */