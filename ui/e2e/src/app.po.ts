import { browser, by, element, ElementFinder } from 'protractor';

export class AppPage {
  async navigateTo(): Promise<unknown> {
    return browser.get(browser.baseUrl);
  }

  getTitleText() {
    return element(by.id('appTitle'));
  }

  getLeadText() {
    return element(by.css('app-header p'));
  }

  getMasterDataRedeButton() {

    return element(by.id('btnMDR'));
  }

  getMapaButton() {
    return element(by.id('btnMapa'));
  }
  
  getPlaneamentoButton(){
    return element(by.id('btnPlaneamento'));
  }

  getMasterDataRedeSectionTitle() {
    return element(by.id('mdrTitle'));
  }

  getMasterDataRedeSectionLead() {
    return element(by.css('app-master-data-rede p')).getText();
  }

  getMasterDataRedeSelectionBar() {
    return element(by.name("escolherUC"));
  }
  //-----------------------------------------------------COMPONENTE MAPA-------------------------------------------

  getMapaSection() {
    return element(by.id('mapTitle'));
  }

  getMapaMapbox() {
    return element(by.name('mapa'));
  }

  //----------------------------------------------------COMPONENTE PLANEAMENTO------------------------------------------

  

  //------------------------------------------------------------INSERIR GLX------------------------------------------------

  getInputInserirFicheiroGLX() {
    return element(by.id('labelFicheiroGLX'));
  }

  getOptionInserirGLX() {
    return element(by.id('optionInserirGLX'));
  }

  getSubmitGLXButton() {
    return element(by.id('submitGLXButton'));
  }

}
