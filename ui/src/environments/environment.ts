// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  mapBoxKey: 'pk.eyJ1IjoiYW5hc29maWExOCIsImEiOiJja2kzczVuNHk3ZHgyMnRsNml3ejliMzIwIn0.IPiGYUpAYCt0MHsC1Mi6kA',
  mdrURL: 'https://lei-isep-lapr5-g05-mdr.herokuapp.com/api',
  //mdrURL: 'http://localhost:3000/api',
  planeamentoURL: 'https://prolog.nuno99costa.xyz/api',
  mdvURL: 'https://mdv-grupo5-lapr5.azurewebsites.net/api'
  //mdvURL: 'https://localhost:5001/api'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
