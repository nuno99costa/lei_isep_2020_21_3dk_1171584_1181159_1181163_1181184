export const environment = {
  production: true,
  mapBoxKey: 'pk.eyJ1IjoiYW5hc29maWExOCIsImEiOiJja2kzczVuNHk3ZHgyMnRsNml3ejliMzIwIn0.IPiGYUpAYCt0MHsC1Mi6kA',
  mdrURL: 'https://lei-isep-lapr5-g05-mdr.herokuapp.com/api',
  mdvURL: 'https://lei-isep-lapr5-g05-mdv.herokuapp.com/api',
  planeamentoURL: 'http://localhost:5000/api'
};
