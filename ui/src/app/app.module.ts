import { AdminGuard } from './guards/admin.guard';
import { GestorGuard } from './guards/gestor.guard';
import { ClienteGuard } from './guards/cliente.guard';
import { AuthGuard } from './guards/auth.guard';

import { MatCardModule } from '@angular/material/card';
import {
  BrowserModule,
  HAMMER_GESTURE_CONFIG,
  Title
} from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MasterDataRedeComponent } from './master-data-rede/master-data-rede.component';
import { FormBuilder, FormsModule } from '@angular/forms';
import { CriarTipoViaturaComponent } from './master-data-rede/criar-tipo-viatura/criar-tipo-viatura.component';
import { DadosTipoViaturaComponent } from './master-data-rede/criar-tipo-viatura/dados-tipo-viatura/dados-tipo-viatura.component';
import { CriarTipoTripulanteComponent } from './master-data-rede/criar-tipo-tripulante/criar-tipo-tripulante.component';
import { DadosTipoTripulanteComponent } from './master-data-rede/criar-tipo-tripulante/dados-tipo-tripulante/dados-tipo-tripulante.component';
import { CriarLinhaComponent } from './master-data-rede/criar-linha/criar-linha.component';
import { DadosLinhaComponent } from './master-data-rede/criar-linha/dados-linha/dados-linha.component';
import { CriarNoComponent } from './master-data-rede/criar-no/criar-no.component';
import { DadosNoComponent } from './master-data-rede/criar-no/dados-no/dados-no.component';
import { CriarSegmentoComponent } from './master-data-rede/criar-segmento/criar-segmento.component';
import { DadosSegmentoComponent } from './master-data-rede/criar-segmento/dados-segmento/dados-segmento.component';
import { CriarPercursoComponent } from './master-data-rede/criar-percurso/criar-percurso.component';
import { DadosPercursoComponent } from './master-data-rede/criar-percurso/dados-percurso/dados-percurso.component';
import { NoService } from './services/no.service'
import { SegmentoService } from './services/segmento.service';
import { PercursoService } from './services/percurso.service';
import HTTPErrorHandler from './utils/httpErrorHandler';
import { LinhaService } from './services/linha.service';
import { TipoViaturaService } from './services/tipo-viatura.service';
import { TipoTripulanteService } from './services/tipo-tripulante.service';
import { MapComponent } from './map-section/map/map.component';
import { MapSectionComponent } from './map-section/map-section.component';
import { ListaPercursosLinhaComponent } from './master-data-rede/lista-percursos-linha/lista-percursos-linha.component';
import { DadosPercursosComponent } from './master-data-rede/lista-percursos-linha/dados-percursos/dados-percursos.component';
import { InserirGLXComponent } from './master-data-rede/inserir-glx/inserir-glx.component';
import { InserirGLXService } from './services/inserir-glx.service';
import { PlaneamentoComponent } from './planeamento/planeamento.component';
import { PercursoMenorTempoComponent } from './planeamento/percurso-menor-tempo/percurso-menor-tempo.component';
import { DadosPercursoMenorTempoComponent } from './planeamento/percurso-menor-tempo/dados-percurso-menor-tempo/dados-percurso-menor-tempo.component';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MasterDataViagemComponent } from './master-data-viagem/master-data-viagem.component';
import { DefinirViaturaComponent } from './master-data-viagem/definir-viatura/definir-viatura.component';
import { DadosViaturaComponent } from './master-data-viagem/definir-viatura/dados-viatura/dados-viatura.component';
import { CriarVariasViagensComponent } from './master-data-viagem/criar-varias-viagens/criar-varias-viagens.component';
import { DadosVariasViagensComponent } from './master-data-viagem/criar-varias-viagens/dados-varias-viagens/dados-varias-viagens.component';
import { RegistoComponent } from './user/registo/registo.component';
import { LoginComponent } from './user/login/login.component';
import { AuthService } from "./services/auth.service";
import { MatExpansionModule } from '@angular/material/expansion';
import { UserComponent } from './user/user.component';
import { appRoutes } from './routes';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import { CriarViagemComponent } from './master-data-viagem/criar-viagem/criar-viagem.component';
import { DadosViagemComponent } from './master-data-viagem/criar-viagem/dados-viagem/dados-viagem.component';
import { ViagemService } from './services/viagem.service';
import { ListarDadosComponent } from './user/listar-dados/listar-dados.component';
import { UserService } from './services/user.service';
import { UpdateDadosComponent } from './user/update-dados/update-dados.component';
import { CriarServicoViaturaComponent } from './master-data-viagem/criar-servico-viatura/criar-servico-viatura.component';
import { ServicoViaturaService } from './services/servico-viatura.service';
import { DadosServicoViaturaComponent } from './master-data-viagem/criar-servico-viatura/dados-servico-viatura/dados-servico-viatura.component';
import { DatePipe } from '@angular/common';
import { CriarBlocoTrabalhoComponent } from './master-data-viagem/criar-bloco-trabalho/criar-bloco-trabalho.component';
import { DadosBlocoTrabalhoComponent } from './master-data-viagem/criar-bloco-trabalho/dados-bloco-trabalho/dados-bloco-trabalho.component';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { UpdateNoModelo3DComponent } from './master-data-rede/update-no-modelo3-d/update-no-modelo3-d.component';
import { CriarTripulanteComponent } from './master-data-viagem/criar-tripulante/criar-tripulante.component';
import { TripulanteService } from './services/tripulante.service';
import { DadosTripulanteComponent } from './master-data-viagem/criar-tripulante/dados-tripulante/dados-tripulante.component';
import { CriarServicoTripulanteComponent } from './master-data-viagem/criar-servico-tripulante/criar-servico-tripulante.component';
import { DadosServicoTripulanteComponent } from './master-data-viagem/criar-servico-tripulante/dados-servico-tripulante/dados-servico-tripulante.component';
import { ServicoTripulanteService } from './services/servico-tripulante.service';
import { TripulantesBlocosTrabalhoComponent } from './planeamento/tripulantes-blocos-trabalho/tripulantes-blocos-trabalho.component';
import { TripulantesBlocosTrabalhoDadosComponent } from './planeamento/tripulantes-blocos-trabalho/tripulantes-blocos-trabalho-dados/tripulantes-blocos-trabalho-dados.component';
import { ListarLinhasComponent } from './master-data-rede/listar-linhas/listar-linhas.component';
import { DadosLinhasComponent } from './master-data-rede/listar-linhas/dados-linhas/dados-linhas.component';
import { InserirGlxMdvComponent } from './master-data-viagem/inserir-glx-mdv/inserir-glx-mdv.component';
import { AtribuirModeloNoComponent } from './planeamento/atribuir-modelo-no/atribuir-modelo-no.component';
import { AtribuirModelosNoDadosComponent } from './planeamento/atribuir-modelo-no/atribuir-modelos-no-dados/atribuir-modelos-no-dados.component';
import { ListarNosComponent } from './master-data-rede/listar-nos/listar-nos.component';
import { DadosNosComponent } from './master-data-rede/listar-nos/dados-nos/dados-nos.component';
import { MatTabsModule } from '@angular/material/tabs';
import { ListarServicosViaturaComponent } from './master-data-viagem/listar-servicos-viatura/listar-servicos-viatura.component';
import { DadosServicosViaturaComponent } from './master-data-viagem/listar-servicos-viatura/dados-servicos-viatura/dados-servicos-viatura.component';
import { ListarServicosTripulanteComponent } from './master-data-viagem/listar-servicos-tripulante/listar-servicos-tripulante.component';
import { DadosServicosTripulanteComponent } from './master-data-viagem/listar-servicos-tripulante/dados-servicos-tripulante/dados-servicos-tripulante.component'
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatDialogModule } from '@angular/material/dialog';
import {MatSliderModule} from '@angular/material/slider'; 

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MasterDataRedeComponent,
    CriarNoComponent,
    DadosNoComponent,
    CriarSegmentoComponent,
    DadosSegmentoComponent,
    CriarPercursoComponent,
    DadosPercursoComponent,
    CriarTipoViaturaComponent,
    DadosTipoViaturaComponent,
    CriarTipoTripulanteComponent,
    DadosTipoTripulanteComponent,
    CriarLinhaComponent,
    DadosLinhaComponent,
    MapComponent,
    MapSectionComponent,
    CriarTipoTripulanteComponent,
    DadosTipoTripulanteComponent,
    InserirGLXComponent, //mdr
    ListaPercursosLinhaComponent,
    DadosPercursosComponent,
    PlaneamentoComponent,
    PercursoMenorTempoComponent,
    DadosPercursoMenorTempoComponent,
    MasterDataViagemComponent,
    DefinirViaturaComponent,
    DadosViaturaComponent,
    UserComponent,
    LoginComponent,
    RegistoComponent,
    CriarViagemComponent,
    DadosViagemComponent,
    CriarVariasViagensComponent,
    DadosVariasViagensComponent,
    ListarDadosComponent,
    UpdateDadosComponent,
    CriarServicoViaturaComponent,
    DadosServicoViaturaComponent,
    CriarBlocoTrabalhoComponent,
    DadosBlocoTrabalhoComponent,
    UpdateNoModelo3DComponent,
    CriarTripulanteComponent,
    DadosTripulanteComponent,
    CriarServicoTripulanteComponent,
    DadosServicoTripulanteComponent,
    TripulantesBlocosTrabalhoComponent,
    TripulantesBlocosTrabalhoDadosComponent,
    ListarLinhasComponent,
    DadosLinhasComponent,
    InserirGlxMdvComponent,
    AtribuirModeloNoComponent,
    AtribuirModelosNoDadosComponent, //mdv
    ListarNosComponent,
    DadosNosComponent,
    ListarServicosViaturaComponent,
    DadosServicosViaturaComponent,
    ListarServicosTripulanteComponent,
    DadosServicosTripulanteComponent
  ],

  imports: [
    AmazingTimePickerModule,
    HttpClientModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot([
      { path: 'mdr', component: MasterDataRedeComponent },
      { path: 'mapa', component: MapSectionComponent },
      { path: 'planeamento', component: PlaneamentoComponent },
      { path: 'mdv', component: MasterDataViagemComponent }
    ]),
    RouterModule.forRoot(appRoutes),
    AppRoutingModule,
    BrowserAnimationsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatExpansionModule,
    MatCardModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule,
    NgxMaterialTimepickerModule,
    NgbModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatSlideToggleModule, 
    MatDialogModule,
    MatSliderModule
  ],

  providers: [
    HTTPErrorHandler,
    NoService,
    SegmentoService,
    PercursoService,
    TipoViaturaService,
    TripulanteService,
    TipoTripulanteService,
    LinhaService,
    ViagemService,
    InserirGLXService,
    UserService,
    AuthService,
    FormBuilder,
    AuthGuard,
    ClienteGuard,
    GestorGuard,
    AdminGuard,
    AuthService,
    Title,
    ServicoViaturaService,
    DatePipe,
    ServicoTripulanteService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
