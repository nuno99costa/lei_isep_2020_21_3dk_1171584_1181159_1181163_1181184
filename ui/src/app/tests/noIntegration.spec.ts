import { HttpClient } from '@angular/common/http';
import { inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import INoDTO from 'src/app/dto/INoDTO';
import { CriarNoComponent } from 'src/app/master-data-rede/criar-no/criar-no.component';
import { MasterDataRedeComponent } from 'src/app/master-data-rede/master-data-rede.component';
import { NoService } from 'src/app/services/no.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosNoComponent } from '../master-data-rede/criar-no/dados-no/dados-no.component';

describe('NoIntegration', () => {
    let component: DadosNoComponent;
    let fixture: ComponentFixture<DadosNoComponent>;

    //Mock do No
    const mockNo = {
        id: "a",
        nomeCompleto: "a",
        abreviatura: "a",
        isPontoRendicao: true,
        isEstacaoRecolha: false,
        latitude: 12.3,
        longitude: -12.5
    } as INoDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

    //Fazemos o httpClient.post retornar um DTO de um nó
    httpClientSpy.post.and.returnValue(of(mockNo));


    //Setup dos injetaveis (??)
    beforeEach(async () => {

        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarNoComponent, DadosNoComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, NoService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });


    beforeEach(() => {
        fixture = TestBed.createComponent(DadosNoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.nomeCompleto = mockNo.nomeCompleto;
        component.abreviatura = mockNo.abreviatura;
        component.isPontoRendicao = mockNo.isPontoRendicao;
        component.isEstacaoRecolha = mockNo.isEstacaoRecolha;
        component.latitude = mockNo.latitude;
        component.longitude = mockNo.longitude;
    });

    //--------------------------------------------------------------------TESTES-------------------------------
    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should post No using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();

    }); */

});