import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import { CriarTipoTripulanteComponent } from '../master-data-rede/criar-tipo-tripulante/criar-tipo-tripulante.component';
import { DadosTipoTripulanteComponent } from '../master-data-rede/criar-tipo-tripulante/dados-tipo-tripulante/dados-tipo-tripulante.component';
import { MasterDataRedeComponent } from '../master-data-rede/master-data-rede.component';
import { TipoViaturaService } from '../services/tipo-viatura.service';
import HTTPErrorHandler from '../utils/httpErrorHandler';

describe('TipoTripulanteIntegration', () => {
    let component: DadosTipoTripulanteComponent;
    let fixture: ComponentFixture<DadosTipoTripulanteComponent>;

    const mockTipoTripulante = {
        id: '1',
        codigo: '12345678901234567890',
        descricao: 'tp teste'
    } as ITipoTripulanteDTO;

    // Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

    // Fazemos o httpClient.post retornar um DTO de um tipo de viatura
    httpClientSpy.post.and.returnValue(of(mockTipoTripulante));

    // Setup dos injetaveis
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarTipoTripulanteComponent, DadosTipoTripulanteComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, TipoViaturaService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy }], // Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DadosTipoTripulanteComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.codigo = mockTipoTripulante.codigo;
        component.descricao = mockTipoTripulante.descricao;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should post tipo viatura using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();
    }); */
});
