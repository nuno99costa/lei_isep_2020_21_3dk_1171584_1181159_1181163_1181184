import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import ILinhaDTO from "../dto/ILinhaDTO";
import { CriarLinhaComponent } from '../master-data-rede/criar-linha/criar-linha.component';
import { DadosLinhaComponent } from '../master-data-rede/criar-linha/dados-linha/dados-linha.component';
import { MasterDataRedeComponent } from '../master-data-rede/master-data-rede.component';
import { LinhaService } from '../services/linha.service';
import { NoService } from '../services/no.service';
import { TipoTripulanteService } from '../services/tipo-tripulante.service';
import { TipoViaturaService } from '../services/tipo-viatura.service';
import HTTPErrorHandler from '../utils/httpErrorHandler';

describe('LinhaIntegration', () => {
    let component: DadosLinhaComponent;
    let fixture: ComponentFixture<DadosLinhaComponent>;

    const mockLinha = {
        "id": "a",
        "codigo": "A",
        "nome": "Cristelo-Lordelo",
        "cor": "rosa",
        "noInicial": "1",
        "noFinal": "2",
        "viaturasPermitidas": ["5"],
        "viaturasProibidas": ["9"],
        "tripulantesPermitidos": ["7"],
        "tripulantesProibidos": ["6"]
    } as ILinhaDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

    //Fazemos o httpClient.post retornar um DTO de uma linha
    httpClientSpy.post.and.returnValue(of(mockLinha));
    const tipoTripulanteServiceSpy: jasmine.SpyObj<TipoTripulanteService> = jasmine.createSpyObj('TipoTripulanteService', ['listaTiposTripulante']);
    const tipoViaturaServiceSpy: jasmine.SpyObj<TipoViaturaService> = jasmine.createSpyObj('tipoViaturaService', ['listaTiposViatura']);
    const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['listaNos']);

    //Setup dos injetaveis
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarLinhaComponent, DadosLinhaComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, LinhaService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy },{ provide:NoService, useValue: noServiceSpy },{ provide: TipoTripulanteService, useValue: tipoTripulanteServiceSpy },{ provide: TipoViaturaService, useValue: tipoViaturaServiceSpy }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });


    beforeEach(() => {
        fixture = TestBed.createComponent(DadosLinhaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.codigo = mockLinha.codigo;
        component.nome = mockLinha.nome;
        component.cor = mockLinha.cor;
        component.noInicialId = mockLinha.noInicial;
        component.noFinalId = mockLinha.noFinal;
        component.viaturasPermitidas = mockLinha.viaturasPermitidas;
        component.viaturasProibidas = mockLinha.viaturasProibidas;
        component.tripulantesPermitidos = mockLinha.tripulantesPermitidos;
        component.tripulantesProibidos = mockLinha.tripulantesProibidos;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should post linha using http request', () => {
        const testRsult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();
    }); */
});