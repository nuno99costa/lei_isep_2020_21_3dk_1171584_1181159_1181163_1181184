import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import { CriarTipoViaturaComponent } from '../master-data-rede/criar-tipo-viatura/criar-tipo-viatura.component';
import { DadosTipoViaturaComponent } from "../master-data-rede/criar-tipo-viatura/dados-tipo-viatura/dados-tipo-viatura.component";
import { MasterDataRedeComponent } from '../master-data-rede/master-data-rede.component';
import { TipoViaturaService } from '../services/tipo-viatura.service';
import HTTPErrorHandler from '../utils/httpErrorHandler';

describe('TipoViaturaIntegration', () => {
    let component: DadosTipoViaturaComponent;
    let fixture: ComponentFixture<DadosTipoViaturaComponent>;

    const mockTipoViatura = {
        id: "1",
        codigo: "12345678901234567890",
        descricao: "tp teste",
        combustivel: "GPL",
        autonomia: 12,
        velocidadeMedia: 50,
        custoPorQuilometro: 2,
        consumoMedio: 2.444
    } as ITipoViaturaDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

    //Fazemos o httpClient.post retornar um DTO de um tipo de viatura
    httpClientSpy.post.and.returnValue(of(mockTipoViatura));

    //Setup dos injetaveis
    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarTipoViaturaComponent, DadosTipoViaturaComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, TipoViaturaService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DadosTipoViaturaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.codigo = mockTipoViatura.codigo;
        component.descricao = mockTipoViatura.descricao;
        component.escolherCombustivel = mockTipoViatura.combustivel;
        component.autonomia = mockTipoViatura.autonomia;
        component.velocidadeMedia = mockTipoViatura.velocidadeMedia;
        component.custoPorQuilometro = mockTipoViatura.custoPorQuilometro;
        component.consumoMedio = mockTipoViatura.consumoMedio;
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should post tipo viatura using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();
    }); */
});