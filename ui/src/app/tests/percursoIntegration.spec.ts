import { DadosPercursoComponent } from './../master-data-rede/criar-percurso/dados-percurso/dados-percurso.component';
import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { AppComponent } from '../app.component';
import IPercursoDTO from "../dto/IPercursoDTO";
import { CriarPercursoComponent } from '../master-data-rede/criar-percurso/criar-percurso.component';
import { MasterDataRedeComponent } from '../master-data-rede/master-data-rede.component';
import { BrowserModule, Title } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from '../app-routing.module';
import { PercursoService } from '../services/percurso.service';
import HTTPErrorHandler from '../utils/httpErrorHandler';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SegmentoService } from '../services/segmento.service';
import { LinhaService } from '../services/linha.service';
import { NoService } from '../services/no.service';


describe('PercursoIntegration', () => {
    let component: DadosPercursoComponent;
    let fixture: ComponentFixture<DadosPercursoComponent>;
    //Mock do Percurso
    const mockPercurso = {
        id: "1",
        nomePercurso: "percurso1",
        linhaId: "1",
        orientacao: "Ida",
        listaSegmentos: ['1'],
        noInicialId: "1",
        noFinalId: "4"
    } as IPercursoDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

    //Fazemos o httpClient.post retornar um DTO de um nó
    httpClientSpy.post.and.returnValue(of(mockPercurso));

    const segmentoServiceSpy: jasmine.SpyObj<SegmentoService> = jasmine.createSpyObj('segmentoService', ['validarInput', 'criarSegmento', 'listaSegmentos']);
    const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('linhaService', ['validarLinha', 'criarLinha', 'addElementToArray', 'deleteElementOfArray', 'listaLinhas']);
    const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['criarNo', 'validarInput', 'listaNos']);

    //Setup dos injetaveis (??)
    beforeEach(async () => {

        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarPercursoComponent, DadosPercursoComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, { provide: LinhaService, useValue: linhaServiceSpy }, { provide: SegmentoService, useValue: segmentoServiceSpy }, { provide: NoService, useValue: noServiceSpy }, PercursoService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DadosPercursoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.nomePercurso = mockPercurso.nomePercurso;
        component.linhaId = mockPercurso.linhaId;
        component.escolhaOrientacao = mockPercurso.orientacao;
        component.listaSegmentos = mockPercurso.listaSegmentos;
        component.noInicialId = mockPercurso.noInicialId;
        component.noFinalId = mockPercurso.noFinalId;
    })

    //--------------------------------------------------------------------TESTES-------------------------------

    it('should add and delete', () => {
        let x = document.createElement("input");
        x.value = '2';
        component.segmentoIDSelecionado = '2';
        component.addElementToArray();
        expect(component.listaSegmentos).toEqual(['1', '2']);
        component.deleteElementOfArray();
        expect(component.listaSegmentos).toEqual(['1']);
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should post Percurso using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();
    }); */
})