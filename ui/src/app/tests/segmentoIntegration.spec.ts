import { HttpClient } from '@angular/common/http';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from '../app-routing.module';
import { AppComponent } from '../app.component';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import { CriarSegmentoComponent } from '../master-data-rede/criar-segmento/criar-segmento.component';
import { DadosSegmentoComponent } from "../master-data-rede/criar-segmento/dados-segmento/dados-segmento.component";
import { MasterDataRedeComponent } from '../master-data-rede/master-data-rede.component';
import { NoService } from '../services/no.service';
import { SegmentoService } from '../services/segmento.service';
import HTTPErrorHandler from '../utils/httpErrorHandler';

describe('SegmentoIntegration', () => {
    let component: DadosSegmentoComponent;
    let fixture: ComponentFixture<DadosSegmentoComponent>;

    //Mock do segmento:
    const mockSegmento = {
        id: "1",
        nomeSegmento: "seg1",
        idNoInicial: "1",
        idNoFinal: "2",
        distanciaNos: 1000,
        tempoNos: 100
      } as ISegmentoDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
    const noService: jasmine.SpyObj<NoService> = jasmine.createSpyObj('NoService', ['listaNos']);

    //Fazemos o httpClient.post retornar um DTO de um nó
    httpClientSpy.post.and.returnValue(of(mockSegmento));

    //Setup dos injetaveis (??)
    beforeEach(async () => {

        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarSegmentoComponent, DadosSegmentoComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, SegmentoService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy },{ provide: NoService, useValue: noService }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(DadosSegmentoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.nomeSegmento = mockSegmento.nomeSegmento;
        component.idNoInicial = mockSegmento.idNoInicial;
        component.idNoFinal = mockSegmento.idNoFinal;
        component.distanciaNos = mockSegmento.distanciaNos;
        component.tempoNos = mockSegmento.tempoNos;
    });

    //--------------------------------------------------------------------TESTES-------------------------------

    it('should create', () => {
        expect(component).toBeTruthy();
    });

   /*  it('should post Segmento using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.post).toHaveBeenCalled();
    });
     */
})