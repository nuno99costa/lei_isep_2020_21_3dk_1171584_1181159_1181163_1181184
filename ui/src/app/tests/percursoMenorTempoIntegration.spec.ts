import { PercursoMenorTempoService } from './../services/percursoMenorTempoService';
import { DadosPercursoMenorTempoComponent } from './../planeamento/percurso-menor-tempo/dados-percurso-menor-tempo/dados-percurso-menor-tempo.component';
import { HttpClient } from '@angular/common/http';
import { inject, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { AppComponent } from 'src/app/app.component';
import INoDTO from 'src/app/dto/INoDTO';
import { CriarNoComponent } from 'src/app/master-data-rede/criar-no/criar-no.component';
import { MasterDataRedeComponent } from 'src/app/master-data-rede/master-data-rede.component';
import { NoService } from 'src/app/services/no.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import IPercursoDTO from '../dto/IPercursoDTO';

import { DadosNoComponent } from '../master-data-rede/criar-no/dados-no/dados-no.component';
import { AmazingTimePickerService } from 'amazing-time-picker';
import INosPercursoDTO from '../dto/INosPercursoDTO';

describe('PercursoMenorTempoIntegration', () => {
    let component: DadosPercursoMenorTempoComponent;
    let fixture: ComponentFixture<DadosPercursoMenorTempoComponent>;

  //Mock do NosPercurso
  const mockNosPercurso = {
    abvNoInicial: "abv1",   
    abvNoFinal: "abv3",
    tempoSegundos: 3600
  } as INosPercursoDTO;

    //Mock do percurso
    const mockPercurso = {
        id: "1",   
        nomePercurso: "percurso1",
        linhaId: "1",
        orientacao: "Ida",
        listaSegmentos: ["1", "2"],
        noInicialId: "1",
        noFinalId: "3"
      } as IPercursoDTO;

    //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
    const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['get']);

    //Fazemos o httpClient.post retornar um DTO de um nó
    httpClientSpy.get.and.returnValue(of(mockPercurso));

    //Setup dos injetaveis (??)
    beforeEach(async () => {

        await TestBed.configureTestingModule({
            declarations: [AppComponent, MasterDataRedeComponent, CriarNoComponent, DadosNoComponent],
            imports: [BrowserModule, FormsModule, AppRoutingModule],
            providers: [Title, PercursoMenorTempoService, HTTPErrorHandler, { provide: HttpClient, useValue: httpClientSpy }],//Injetaveis do componente
            schemas: [NO_ERRORS_SCHEMA]
        })
            .compileComponents();
    });


    beforeEach(() => {
        fixture = TestBed.createComponent(DadosPercursoMenorTempoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        component.abvNoInicial = mockNosPercurso.abvNoInicial;
        component.abvNoFinal = mockNosPercurso.abvNoFinal;
        component.tempoSegundos = mockNosPercurso.tempoSegundos;
    });

    //--------------------------------------------------------------------TESTES-------------------------------
    it('should create', () => {
        expect(component).toBeTruthy();
    });

    /* it('should get Percurso using http request', () => {
        const testResult = component.submeter();
        expect(httpClientSpy.get).toHaveBeenCalled();

    }); */

});