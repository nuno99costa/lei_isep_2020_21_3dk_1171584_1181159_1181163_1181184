import { IUserDTO } from './../dto/IUserDTO';
import { AuthService } from './../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  showHeader: boolean = false;
  showHeaderN: boolean = true;
  userInfo : IUserDTO;

  constructor(private titleService: Title, private authService : AuthService) {
    this.titleService.setTitle("Home | AIT");
  }

  ngOnInit(): void {
    this.userInfo = this.authService.userInfo;
  }

  ngOnDestroy() : void {
    
  }
 
}
