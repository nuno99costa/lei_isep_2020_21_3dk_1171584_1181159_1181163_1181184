import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-tipo-tripulante',
  templateUrl: './criar-tipo-tripulante.component.html',
  styleUrls: ['./criar-tipo-tripulante.component.css']
})
export class CriarTipoTripulanteComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
