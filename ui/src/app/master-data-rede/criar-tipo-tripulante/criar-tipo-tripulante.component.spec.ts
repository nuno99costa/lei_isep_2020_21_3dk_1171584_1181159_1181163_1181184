import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarTipoTripulanteComponent } from './criar-tipo-tripulante.component';

describe('CriarTipoTripulanteComponent', () => {
  let component: CriarTipoTripulanteComponent;
  let fixture: ComponentFixture<CriarTipoTripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarTipoTripulanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarTipoTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
