import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosTipoTripulanteComponent } from './dados-tipo-tripulante.component';

describe('DadosTipoTripulanteComponent', () => {
  let component: DadosTipoTripulanteComponent;
  let fixture: ComponentFixture<DadosTipoTripulanteComponent>;

  const mockTipoTripulante = {
    id: '1',
    codigo: 'tp',
    descricao: 'tp teste',
  } as ITipoTripulanteDTO;

  // Mock/Spy dos injetáveis: TipoViaturaService e HTTPErrorhandler
  // Fazemos mock do Service que vamos utilizar e dos métodos que vamos testar
  const tipoTripulanteServiceSpy: jasmine.SpyObj<TipoTripulanteService> = jasmine.createSpyObj('TipoTripulanteService', ['criarTipoTripulante', 'validarTipoTripulante']);

  // Chama o método que cria o tipo de viatura e depois retorna o mock que fizemos anteriormente.
  // Como o criar retorna um observable temos de usar o método of()
  tipoTripulanteServiceSpy.criarTipoTripulante.and.returnValue(of(mockTipoTripulante));

  // Em vez de chamarmos o validarTipoViatura (que é void) vamos saltar por cima, chamamos uma fake (que está nos parametros) que é uma função vazia.
  tipoTripulanteServiceSpy.validarTipoTripulante.and.callFake(() => { });

  // Vamos testar esta classe de utils para quando ocorrem erros nos pedidos http
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue('teste');

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DadosTipoTripulanteComponent],
      imports: [BrowserModule],
      providers: [{ provide: TipoTripulanteService, useValue: tipoTripulanteServiceSpy },{provide: Title, useValue: titleServiceSpy}, { provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy }], // Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosTipoTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**Valida se executa os métodos necessários para o submeter(): o que valida os dados e
  o que cria o tipo de tripulante. Como não é esperado que ocorra algum erro, então o expectável
  é que esse método seja chamado zero vezes.**/
  it('should validate data and create a vehicle type', () => {
    component.submeter();
    expect(tipoTripulanteServiceSpy.criarTipoTripulante).toHaveBeenCalled();
    expect(tipoTripulanteServiceSpy.validarTipoTripulante).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
