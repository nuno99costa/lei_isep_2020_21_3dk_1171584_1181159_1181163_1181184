import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';
import { ITipoTripulanteService } from 'src/app/services/IServices/ITipoTripulanteService';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-tipo-tripulante',
  templateUrl: './dados-tipo-tripulante.component.html',
  styleUrls: ['./dados-tipo-tripulante.component.css']
})

export class DadosTipoTripulanteComponent implements OnInit { // Vai ser o nosso controller
  private tipoTripulanteService: ITipoTripulanteService; // Vamos injetar o service
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, tipoTripulanteService: TipoTripulanteService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.tipoTripulanteService = tipoTripulanteService;
    this.httpErrorHandler = httpErrorHandler;
  }

  idTipoTripulante: string;
  codigo: string;
  descricao: string;

  ngOnInit(): void {
  }

  public submeter() {

    const tipoTripulante = {
      id: this.idTipoTripulante,
      codigo: this.codigo,
      descricao: this.descricao
    } as ITipoTripulanteDTO;

    try {
      this.tipoTripulanteService.validarTipoTripulante(tipoTripulante);

      // Resposta do back-end (devolve um Observable). Função subscribe de Observable:
      // Parâmetro 1: Se a resposta for sucesso
      // Parâmetro 2: Se a resposta for de erro
      this.tipoTripulanteService.criarTipoTripulante(tipoTripulante).subscribe(values => {
        alert('Sucesso! O seu objeto foi criado com id: ' + values.id);
      }
        , error => {// Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert('Something happened (not http response error)');
          }

        });

    } catch (err) {
      alert(err);
    }
  }

}
