import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import INoDTO from 'src/app/dto/INoDTO';
import { INoService } from 'src/app/services/IServices/INoService';
import { NoService } from 'src/app/services/no.service';

export interface DialogData {
  noId: string;
  noNomeCompleto: string;
  noAbreviatura: string;
  noIsPontoRendicao: boolean;
  noIsEstacaoRecolha: boolean;
  noLatitude: number;
  noLongitude: number;
  noNomeModelo: string;
}

@Component({
  selector: 'app-dados-nos',
  templateUrl: './dados-nos.component.html',
  styleUrls: ['./dados-nos.component.css']
})

export class DadosNosComponent implements OnInit {

  private noService: INoService;

  constructor(private titleService: Title, noService: NoService, public dialog: MatDialog) {
    this.titleService.setTitle("AIT: MDR");
    this.noService = noService;
  }

  listaNos: INoDTO[] = [];
  no: INoDTO;

  async ngOnInit(): Promise<void> {
    this.listaNos = await this.noService.listaNos();
  }

  openDialog(id: string) {
    this.listaNos.forEach(element => {
      if (element.id == id) this.no = element;
    });

    this.dialog.open(DadosNoDialogComponent, {
      data: {
        noId: this.no.id,
        noNomeCompleto: this.no.nomeCompleto,
        noAbreviatura: this.no.abreviatura,
        noIsPontoRendicao: this.no.isPontoRendicao,
        noIsEstacaoRecolha: this.no.isEstacaoRecolha,
        noLatitude: this.no.latitude,
        noLongitude: this.no.longitude,
        noNomeModelo: this.no.nomeModelo
      }
    });
  }
}

@Component({
  selector: 'dados-no-dialog',
  templateUrl: 'dados-no-dialog.component.html',
})
export class DadosNoDialogComponent {

  noId: string;
  noNomeCompleto: string;
  noAbreviatura: string;
  noIsPontoRendicao: boolean;
  noIsEstacaoRecolha: boolean;
  noLatitude: number;
  noLongitude: number;
  noNomeModelo: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.noId = data.noId;
    this.noNomeCompleto = data.noNomeCompleto;
    this.noAbreviatura = data.noAbreviatura;
    this.noIsPontoRendicao = data.noIsPontoRendicao;
    this.noIsEstacaoRecolha = data.noIsEstacaoRecolha;
    this.noLatitude = data.noLatitude;
    this.noLongitude = data.noLongitude;
    this.noNomeModelo = data.noNomeModelo;
  }

}