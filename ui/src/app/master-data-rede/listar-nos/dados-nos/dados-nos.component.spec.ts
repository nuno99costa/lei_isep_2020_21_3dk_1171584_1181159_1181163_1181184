import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import INoDTO from 'src/app/dto/INoDTO';
import { LinhaService } from 'src/app/services/linha.service';
import { NoService } from 'src/app/services/no.service';

import { DadosNosComponent } from './dados-nos.component';

describe('DadosNosComponent', () => {
  let component: DadosNosComponent;
  let fixture: ComponentFixture<DadosNosComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);
  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('NoService', ['listaNos']);

  noServiceSpy.listaNos.and.returnValue(Promise.resolve([] as INoDTO[]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosNosComponent ],
      imports: [BrowserModule],
      providers: [{provide: Title, useValue: titleServiceSpy},{provide: MatDialog, useValue: matDialogSpy},{provide: NoService, useValue: noServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosNosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
 