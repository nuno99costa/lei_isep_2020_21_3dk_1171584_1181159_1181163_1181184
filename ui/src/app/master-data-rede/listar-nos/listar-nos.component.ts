import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-listar-nos',
  templateUrl: './listar-nos.component.html',
  styleUrls: ['./listar-nos.component.css']
})
export class ListarNosComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
