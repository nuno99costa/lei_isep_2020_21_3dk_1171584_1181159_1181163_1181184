import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-no',
  templateUrl: './criar-no.component.html',
  styleUrls: ['./criar-no.component.css']
})
export class CriarNoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
