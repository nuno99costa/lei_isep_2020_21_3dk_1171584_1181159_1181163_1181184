import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarNoComponent } from './criar-no.component';

describe('CriarNoComponent', () => {
  let component: CriarNoComponent;
  let fixture: ComponentFixture<CriarNoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarNoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
