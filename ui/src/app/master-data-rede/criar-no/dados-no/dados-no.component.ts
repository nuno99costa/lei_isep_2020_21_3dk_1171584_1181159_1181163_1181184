import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import INoDTO from 'src/app/dto/INoDTO';
import { INoService } from 'src/app/services/IServices/INoService';
import { NoService } from 'src/app/services/no.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-no',
  templateUrl: './dados-no.component.html',
  styleUrls: ['./dados-no.component.css']
})

export class DadosNoComponent implements OnInit {

  private noService: INoService;
  private httpErrorHandler: HTTPErrorHandler;

  //ErrorHandler injetado com o service (Está no utils)
  //Para ser injetado tem de estar no AppModules no providers.
  constructor(private titleService: Title, noService: NoService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.noService = noService;
    this.httpErrorHandler = httpErrorHandler;
  }

  ngOnInit(): void {
  }

  //Variáveis para o two way binding com o template
  //Na template tem [(ngModel)]="variavel". Two way binding. Quando se altera na caixa de texto (ou checkbox, etc.) o valor da variavel é alterado
  idNo: string;
  nomeCompleto: string;
  abreviatura: string;
  isPontoRendicao: boolean = false;//Iniciado a unchecked
  isEstacaoRecolha: boolean = false;
  latitude: number;
  longitude: number;

  //Quando clica para submeter
  submeter() {

    //Cria DTO com os valores do input
    const no = {
      id: this.idNo,
      nomeCompleto: this.nomeCompleto,
      abreviatura: this.abreviatura,
      isPontoRendicao: this.isPontoRendicao,
      isEstacaoRecolha: this.isEstacaoRecolha,
      latitude: this.latitude,
      longitude: this.longitude
    } as INoDTO;

    try {

      this.noService.validarInput(no);

      //Resposta do back-end (devolve um Observable). Função subscribe de Observable:
      //Parâmetro 1: Se a resposta for sucesso
      //Parâmetro 2: Se a resposta for de erro
      this.noService.criarNo(no).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })

    } catch (err) {
      alert(err);
    }
  }




}
