import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import INoDTO from 'src/app/dto/INoDTO';
import { NoService } from 'src/app/services/no.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosNoComponent } from './dados-no.component';

describe('DadosNoComponent', () => {
  let component: DadosNoComponent;
  let fixture: ComponentFixture<DadosNoComponent>;

  //Mock do No
  const mockNo = {
    id: "a",
    nomeCompleto: "a",
    abreviatura: "a",
    isPontoRendicao: true, 
    isEstacaoRecolha: false,
    latitude: 12.3,
    longitude: -12.5
  } as INoDTO;

  //Mock/Spy dos injetaveis (Service e HTTPErrorHandler)
  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['criarNo', 'validarInput']);
  noServiceSpy.criarNo.and.returnValue(of(mockNo));//criarNo devolve um Observable<INoDTO>
  noServiceSpy.validarInput.and.callFake(() => {});//Em vez de chamarmos o validarInput (que é void e vamos saltar por cima), chamamos uma fake (que está nos parametros) que é uma função vazia. 

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  //Setup dos injetaveis (??)
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosNoComponent ],
      imports: [BrowserModule],
      providers: [{provide: NoService, useValue: noServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //--------------------------------------------------------------------TESTES-------------------------------
  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate data and create a no', () => {
    component.submeter();
    expect(noServiceSpy.criarNo).toHaveBeenCalled();
    expect(noServiceSpy.validarInput).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
