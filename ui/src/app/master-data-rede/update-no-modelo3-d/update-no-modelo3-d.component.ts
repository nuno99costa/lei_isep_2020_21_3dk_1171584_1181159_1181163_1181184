import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import IModelo3DDTO from 'src/app/dto/IModelo3DDTO';
import INoDTO from 'src/app/dto/INoDTO';
import { INoService } from 'src/app/services/IServices/INoService';
import { NoService } from 'src/app/services/no.service';
import { UploadModeloService } from 'src/app/services/upload-modelo.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-update-no-modelo3-d',
  templateUrl: './update-no-modelo3-d.component.html',
  styleUrls: ['./update-no-modelo3-d.component.css']
})
export class UpdateNoModelo3DComponent implements OnInit {

  private uploadModeloService: UploadModeloService;
  private httpErrorHandler: HTTPErrorHandler;
  file : File;

  constructor(noService: NoService,  httpErrorHandler: HTTPErrorHandler, uploadModeloService : UploadModeloService) { 
    this.uploadModeloService = uploadModeloService;
    this.httpErrorHandler = httpErrorHandler;
  }

  async ngOnInit(): Promise<void> {

  }

  onChange(e: Event){
    console.log(event);

    const element = (e.currentTarget as HTMLInputElement);
    const label = document.getElementById('customFileLabel');
    if(element && label){
      this.file = element.files?.item(0);
      if(this.file){
        label.innerHTML = this.file.name;
      }
    
    }
  }

  async submeter() {

    try{

      this.uploadModeloService.validarInput(this.file);
      
      const result = await this.uploadModeloService.uploadModelo(this.file);

      result.subscribe(values => {
        alert('Sucesso!');
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch(err){
      alert(err);
    }



  }

}
