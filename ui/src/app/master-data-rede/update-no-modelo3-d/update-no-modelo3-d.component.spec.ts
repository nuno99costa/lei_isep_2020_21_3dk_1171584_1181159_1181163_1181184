import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NoService } from 'src/app/services/no.service';
import { UploadModeloService } from 'src/app/services/upload-modelo.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { UpdateNoModelo3DComponent } from './update-no-modelo3-d.component';

describe('UpdateNoModelo3DComponent', () => {
  let component: UpdateNoModelo3DComponent;
  let fixture: ComponentFixture<UpdateNoModelo3DComponent>;

  //Mock/Spy dos injetaveis (Service e HTTPErrorHandler)
  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['criarNo', 'validarInput']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  const uploadModeloServiceSpy: jasmine.SpyObj<UploadModeloService> = jasmine.createSpyObj('uploadModeloService', ['validarInput', 'uploadModelo']);

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateNoModelo3DComponent ],
      imports: [BrowserModule],
      providers: [{provide: NoService, useValue: noServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: UploadModeloService, useValue: uploadModeloServiceSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateNoModelo3DComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
