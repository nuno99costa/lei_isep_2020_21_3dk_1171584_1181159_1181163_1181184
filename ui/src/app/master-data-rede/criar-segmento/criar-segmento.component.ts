import { SegmentoService } from './../../services/segmento.service';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-segmento',
  templateUrl: './criar-segmento.component.html',
  styleUrls: ['./criar-segmento.component.css']
})
export class CriarSegmentoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
