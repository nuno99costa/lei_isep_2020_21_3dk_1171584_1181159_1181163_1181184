import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarSegmentoComponent } from './criar-segmento.component';

describe('CriarSegmentoComponent', () => {
  let component: CriarSegmentoComponent;
  let fixture: ComponentFixture<CriarSegmentoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarSegmentoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarSegmentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
