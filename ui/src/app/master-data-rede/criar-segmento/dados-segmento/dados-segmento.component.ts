import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { SegmentoService } from 'src/app/services/segmento.service';
import { Component, OnInit } from '@angular/core';
import ISegmentoDTO from 'src/app/dto/ISegmentoDTO';
import { HttpErrorResponse } from '@angular/common/http';
import { ISegmentoService } from 'src/app/services/IServices/ISegmentoService';
import INoDTO from 'src/app/dto/INoDTO';
import { NoService } from 'src/app/services/no.service';
import { INoService } from 'src/app/services/IServices/INoService';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dados-segmento',
  templateUrl: './dados-segmento.component.html',
  styleUrls: ['./dados-segmento.component.css']
})
export class DadosSegmentoComponent implements OnInit {

  private segmentoService: ISegmentoService;
  private httpErrorHandler: HTTPErrorHandler;
  private noService: INoService;

  constructor(private titleService: Title, noService: NoService, segmentoService: SegmentoService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.segmentoService = segmentoService;
    this.httpErrorHandler = httpErrorHandler;
    this.noService = noService;
  }

  async ngOnInit(): Promise<void> {
    this.listaNos = await this.noService.listaNos();
  }

  //Listas para mostrar
  listaNos: INoDTO[] = [];

  //Variáveis para criar
  idSegmento: string;
  nomeSegmento: string;
  idNoInicial: string;
  idNoFinal: string;
  distanciaNos: number;
  tempoNos: number;

  //Quando clica para submeter
  submeter() {
    console.log(this.idNoInicial);
    //Cria DTO com os valores do input
    const segmento = {
      id: this.idSegmento,
      nomeSegmento: this.nomeSegmento,
      idNoInicial: this.idNoInicial,
      idNoFinal: this.idNoFinal,
      distanciaNos: this.distanciaNos,
      tempoNos: this.tempoNos
    } as ISegmentoDTO;

    try {
      this.segmentoService.validarInput(segmento);

      this.segmentoService.criarSegmento(segmento).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error");
          }

        })
    } catch (err) {
      alert(err);
    }
  }

}
