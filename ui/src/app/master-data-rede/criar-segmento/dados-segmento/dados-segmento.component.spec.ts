import { BrowserModule, Title } from '@angular/platform-browser';
import  ISegmentoDTO  from 'src/app/dto/ISegmentoDTO';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DadosSegmentoComponent } from './dados-segmento.component';
import { SegmentoService } from 'src/app/services/segmento.service';
import { of } from 'rxjs';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { NO_ERRORS_SCHEMA, ɵComponentFactory } from '@angular/core';
import { ExpectedConditions } from 'protractor';
import { NoService } from 'src/app/services/no.service';
import INoDTO from 'src/app/dto/INoDTO';

describe('DadosSegmentoComponent', () => {
  let component: DadosSegmentoComponent;
  let fixture: ComponentFixture<DadosSegmentoComponent>;

  //Mock do Segmento
  const mockSegmento = {
    id: "1",
    nomeSegmento: "seg1",
    idNoInicial: "1",
    idNoFinal: "2",
    distanciaNos: 1000,
    tempoNos: 100
  } as ISegmentoDTO;

  //Mock/Spy dos injetaveis (Service e HTTPErrorHandler)
  const segmentoServiceSpy: jasmine.SpyObj<SegmentoService> = jasmine.createSpyObj('segmentoService', ['validarInput', 'criarSegmento']);
  segmentoServiceSpy.criarSegmento.and.returnValue(of(mockSegmento));
  segmentoServiceSpy.validarInput.and.callFake(() => {});

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['criarNo', 'validarInput', 'listaNos']);
  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  noServiceSpy.listaNos.and.returnValue(Promise.resolve([] as INoDTO[]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosSegmentoComponent ],
      imports: [BrowserModule],
      providers: [{provide: SegmentoService, useValue: segmentoServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: NoService, useValue: noServiceSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosSegmentoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.submeter();
    expect(segmentoServiceSpy.criarSegmento).toHaveBeenCalled();
    expect(segmentoServiceSpy.validarInput).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
