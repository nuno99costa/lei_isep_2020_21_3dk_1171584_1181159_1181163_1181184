import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import { InserirGLXService } from 'src/app/services/inserir-glx.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { InserirGLXComponent } from './inserir-glx.component';

describe('InserirGLXComponent', () => {
  let component: InserirGLXComponent;
  let fixture: ComponentFixture<InserirGLXComponent>;

  //Mock/Spy dos injetaveis (Service e HTTPErrorHandler)
  const inserirGLXServiceSpy: jasmine.SpyObj<InserirGLXService> = jasmine.createSpyObj('inserirGLXService', ['inserirGLXMdr', 'validarInput']);
  inserirGLXServiceSpy.inserirGLXMdr.and.returnValue(new Promise(function(resolve, reject) {
    resolve(of('teste'));
 }));
  inserirGLXServiceSpy.validarInput.and.callFake(() => {});//Em vez de chamarmos o validarInput (que é void e vamos saltar por cima), chamamos uma fake (que está nos parametros) que é uma função vazia. 

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InserirGLXComponent ],
      imports: [BrowserModule],
      providers: [{provide: InserirGLXService, useValue: inserirGLXServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InserirGLXComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
