import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-tipo-viatura',
  templateUrl: './criar-tipo-viatura.component.html',
  styleUrls: ['./criar-tipo-viatura.component.css']
})
export class CriarTipoViaturaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
