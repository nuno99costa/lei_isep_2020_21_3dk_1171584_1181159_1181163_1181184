import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarTipoViaturaComponent } from './criar-tipo-viatura.component';

describe('CriarTipoViaturaComponent', () => {
  let component: CriarTipoViaturaComponent;
  let fixture: ComponentFixture<CriarTipoViaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarTipoViaturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarTipoViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
