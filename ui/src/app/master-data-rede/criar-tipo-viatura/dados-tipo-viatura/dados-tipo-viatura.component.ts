import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';
import { ITipoViaturaService } from 'src/app/services/IServices/ITipoViaturaService';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-tipo-viatura',
  templateUrl: './dados-tipo-viatura.component.html',
  styleUrls: ['./dados-tipo-viatura.component.css']
})
export class DadosTipoViaturaComponent implements OnInit { //Vai ser o nosso controller

  private tipoViaturaService: ITipoViaturaService; //Vamos injetar o service
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, tipoViaturaService: TipoViaturaService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.tipoViaturaService = tipoViaturaService;
    this.httpErrorHandler = httpErrorHandler;
  }

  ngOnInit(): void {
  }

  idTipoViatura: string;
  codigo: string;
  descricao: string;
  escolherCombustivel: String;
  autonomia: number;
  velocidadeMedia: number;
  custoPorQuilometro: number;
  consumoMedio: number;

  public submeter() {

    const tipoViatura = {
      id: this.idTipoViatura,
      codigo: this.codigo,
      descricao: this.descricao,
      combustivel: this.escolherCombustivel,
      autonomia: this.autonomia,
      velocidadeMedia: this.velocidadeMedia,
      custoPorQuilometro: this.custoPorQuilometro,
      consumoMedio: this.consumoMedio
    } as ITipoViaturaDTO;

    try {
      this.tipoViaturaService.validarTipoViatura(tipoViatura);

      //Resposta do back-end (devolve um Observable). Função subscribe de Observable:
      //Parâmetro 1: Se a resposta for sucesso
      //Parâmetro 2: Se a resposta for de erro
      this.tipoViaturaService.criarTipoViatura(tipoViatura).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })

    } catch (err) {
      alert(err);
    }
  }

}