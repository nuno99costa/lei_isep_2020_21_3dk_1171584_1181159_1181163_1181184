import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosTipoViaturaComponent } from './dados-tipo-viatura.component';

describe('DadosTipoViaturaComponent', () => {
  let component: DadosTipoViaturaComponent;
  let fixture: ComponentFixture<DadosTipoViaturaComponent>;

  const mockTipoViatura = {
    id: "1",
    codigo: "tp",
    descricao: "tp teste",
    combustivel: "GPL",
    autonomia: 12,
    velocidadeMedia: 50,
    custoPorQuilometro: 2,
    consumoMedio: 2.444
  } as ITipoViaturaDTO;

  //Mock/Spy dos injetáveis: TipoViaturaService e HTTPErrorhandler
  //Fazemos mock do Service que vamos utilizar e dos métodos que vamos testar
  const tipoViaturaServiceSpy: jasmine.SpyObj<TipoViaturaService> = jasmine.createSpyObj('tipoViaturaService', ['criarTipoViatura', "validarTipoViatura"]);

  //Chama o método que cria o tipo de viatura e depois retorna o mock que fizemos anteriormente.
  //Como o criar retorna um observable temos de usar o método of()
  tipoViaturaServiceSpy.criarTipoViatura.and.returnValue(of(mockTipoViatura));

  //Em vez de chamarmos o validarTipoViatura (que é void) vamos saltar por cima, chamamos uma fake (que está nos parametros) que é uma função vazia. 
  tipoViaturaServiceSpy.validarTipoViatura.and.callFake(() => { });

  //Vamos testar esta classe de utils para quando ocorrem erros nos pedidos http
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DadosTipoViaturaComponent],
      imports: [BrowserModule],
      providers: [{ provide: TipoViaturaService, useValue: tipoViaturaServiceSpy }, { provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy },{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosTipoViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  /**Valida se executa os métodos necessários para o submeter(): o que valida os dados e 
  o que cria o tipo de viatura. Como não é esperado que ocorra algum erro, então o expectável 
  é que esse método seja chamado zero vezes.**/
  it('should validate data and create a vehicle type', () => {
    component.submeter();
    expect(tipoViaturaServiceSpy.criarTipoViatura).toHaveBeenCalled();
    expect(tipoViaturaServiceSpy.validarTipoViatura).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
