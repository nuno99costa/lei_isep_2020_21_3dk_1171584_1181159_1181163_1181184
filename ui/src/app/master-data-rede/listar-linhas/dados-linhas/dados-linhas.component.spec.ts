import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import { LinhaService } from 'src/app/services/linha.service';

import { DadosLinhasComponent } from './dados-linhas.component';

describe('DadosLinhasComponent', () => {
  let component: DadosLinhasComponent;
  let fixture: ComponentFixture<DadosLinhasComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);
  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('LinhaService', ['listaLinhas']);

  linhaServiceSpy.listaLinhas.and.returnValue(Promise.resolve([] as ILinhaDTO[]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosLinhasComponent ],
      imports: [BrowserModule],
      providers: [{provide: Title, useValue: titleServiceSpy},{provide: MatDialog, useValue: matDialogSpy},{provide: LinhaService, useValue: linhaServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosLinhasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
