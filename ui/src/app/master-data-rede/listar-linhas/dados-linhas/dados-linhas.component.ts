import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import { ILinhaService } from 'src/app/services/IServices/ILinhaService';
import { LinhaService } from 'src/app/services/linha.service';

export interface DialogData {
  linhaId: string;
  linhaCodigo: string;
  linhaNome: string;
  linhaCor: string;
  linhaNoInicial: string;
  linhaNoFinal: string;
  linhaViaturasPermitidas: string[];
  linhaViaturasProibidas: string[];
  linhaTripulantesPermitidos: string[];
  linhaTripulantesProibidos: string[];
}

@Component({
  selector: 'app-dados-linhas',
  templateUrl: './dados-linhas.component.html',
  styleUrls: ['./dados-linhas.component.css']
})
export class DadosLinhasComponent implements OnInit {

  private linhaService: ILinhaService;

  constructor(private titleService: Title, linhaService: LinhaService, public dialog: MatDialog) {
    this.titleService.setTitle("AIT: MDR");
    this.linhaService = linhaService;
  }

  linhasDTO: ILinhaDTO[] = [];
  linha: ILinhaDTO;

  async ngOnInit(): Promise<void> {
    this.linhasDTO = await this.linhaService.listaLinhas();
  }

  openDialog(id: string) {
    this.linhasDTO.forEach(element => {
      if (element.id == id) this.linha = element;
    });

    this.dialog.open(DadosLinhaDialogComponent, {
      data: {
        linhaId: this.linha.id,
        linhaCodigo: this.linha.codigo,
        linhaNome: this.linha.nome,
        linhaCor: this.linha.cor,
        linhaNoInicial: this.linha.noInicial,
        linhaNoFinal: this.linha.noFinal,
        linhaViaturasPermitidas: this.linha.viaturasPermitidas,
        linhaViaturasProibidas: this.linha.viaturasProibidas,
        linhaTripulantesPermitidos: this.linha.tripulantesPermitidos,
        linhaTripulantesProibidos: this.linha.tripulantesProibidos
      }
    });
  }
}

@Component({
  selector: 'dados-linha-dialog',
  templateUrl: 'dados-linha-dialog.component.html',
})
export class DadosLinhaDialogComponent {

  linhaId: string;
  linhaCodigo: string;
  linhaNome: string;
  linhaCor: string;
  linhaNoInicial: string;
  linhaNoFinal: string;
  linhaViaturasPermitidas: string[];
  linhaViaturasProibidas: string[];
  linhaTripulantesPermitidos: string[];
  linhaTripulantesProibidos: string[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.linhaId = data.linhaId;
    this.linhaCodigo = data.linhaCodigo;
    this.linhaNome = data.linhaNome;
    this.linhaCor = data.linhaCor;
    this.linhaNoInicial = data.linhaNoInicial;
    this.linhaNoFinal = data.linhaNoFinal;
    this.linhaViaturasPermitidas = data.linhaViaturasPermitidas;
    this.linhaViaturasProibidas = data.linhaViaturasProibidas;
    this.linhaTripulantesPermitidos = data.linhaTripulantesPermitidos;
    this.linhaTripulantesProibidos = data.linhaTripulantesProibidos;
  }

}
