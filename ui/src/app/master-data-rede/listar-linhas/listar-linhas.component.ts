import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-listar-linhas',
  templateUrl: './listar-linhas.component.html',
  styleUrls: ['./listar-linhas.component.css']
})
export class ListarLinhasComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
