import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-master-data-rede',
  templateUrl: './master-data-rede.component.html',
  styleUrls: ['./master-data-rede.component.css']
})
export class MasterDataRedeComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }


  escolha = "";

  ngOnInit(): void {
  }

  scrollTo(id: string) {
    document.getElementById(id)?.scrollIntoView();
    console.log('Scrolled');
  }

}
