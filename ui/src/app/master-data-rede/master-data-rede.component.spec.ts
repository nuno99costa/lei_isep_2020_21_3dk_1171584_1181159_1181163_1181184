import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataRedeComponent } from './master-data-rede.component';

describe('MasterDataRedeComponent', () => {
  let component: MasterDataRedeComponent;
  let fixture: ComponentFixture<MasterDataRedeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterDataRedeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataRedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
