import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPercursosLinhaComponent } from './lista-percursos-linha.component';

describe('ListaPercursosLinhaComponent', () => {
  let component: ListaPercursosLinhaComponent;
  let fixture: ComponentFixture<ListaPercursosLinhaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaPercursosLinhaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPercursosLinhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
