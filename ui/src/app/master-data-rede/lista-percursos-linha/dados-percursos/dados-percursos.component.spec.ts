import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import { LinhaService } from 'src/app/services/linha.service';
import { PercursoService } from 'src/app/services/percurso.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosPercursosComponent } from './dados-percursos.component';

describe('DadosPercursosComponent', () => {
  let component: DadosPercursosComponent;
  let fixture: ComponentFixture<DadosPercursosComponent>;

  //Mock do id da linha
  const id = "1";

  //Mock da lista de percursos
  const mockPercurso = {
    id: "a",
    nomePercurso: "percurso1",
    linhaId: "1",
    orientacao: "Ida",
    listaSegmentos: ["1","2"],
    noInicialId: "no1",
    noFinalId: "no2"
  } as IPercursoDTO;
  
  const mockListaPercursos = [mockPercurso];


  //Mock/Spy dos injetaveis (Service e HTTPErrorHandler)
  const percursoServiceSpy: jasmine.SpyObj<PercursoService> = jasmine.createSpyObj('percursoService', ['addElementToArray', 'deleteElementOfArray', 'validarInput', 'validarLinha', 'criarPercurso', 'carregarDadosMapa', 'listarPercursoLinha']);
  percursoServiceSpy.validarLinha.and.callFake(()=>{});
  percursoServiceSpy.listarPercursoLinha.and.returnValue(Promise.resolve(mockListaPercursos));

  
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);
  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('LinhaService', ['listaLinhas']);

  linhaServiceSpy.listaLinhas.and.returnValue(Promise.resolve([] as ILinhaDTO[]));

  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosPercursosComponent ],
      imports: [BrowserModule],
      providers: [{provide: MatDialog, useValue: matDialogSpy},{provide: LinhaService, useValue: linhaServiceSpy}, {provide: PercursoService, useValue: percursoServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosPercursosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate linhaId and get percursos', () => {
    component.getListaPercursosLinha();
    expect(percursoServiceSpy.validarLinha).toHaveBeenCalled();
    expect(percursoServiceSpy.listarPercursoLinha).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
