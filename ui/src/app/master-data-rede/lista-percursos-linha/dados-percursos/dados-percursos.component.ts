import { ILinhaService } from './../../../services/IServices/ILinhaService';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { Component, Inject, OnInit } from '@angular/core';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { HttpErrorResponse } from '@angular/common/http';
import { PercursoService } from 'src/app/services/percurso.service';
import { IPercursoService } from 'src/app/services/IServices/IPercursoService';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import { LinhaService } from 'src/app/services/linha.service';
import { Title } from '@angular/platform-browser';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';

export interface DialogData {
  percursoId: string,
  percursoNomePercurso: string,
  percursoLinhaId: string,
  percursoOrientacao: string,
  percursoListaSegmentos: string[],
  percursoNoInicialId: string,
  percursoNoFinalId: string
  percursoDistanciaTotal: number,
  percursoTempoTotal: number
}

@Component({
  selector: 'app-dados-percursos',
  templateUrl: './dados-percursos.component.html',
  styleUrls: ['./dados-percursos.component.css']
})
export class DadosPercursosComponent implements OnInit {

  private percursoService: IPercursoService;
  private linhaService: ILinhaService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, percursoService: PercursoService, linhaService: LinhaService, httpErrorHandler: HTTPErrorHandler, public dialog: MatDialog) {
    this.titleService.setTitle("AIT: MDR");
    this.percursoService = percursoService;
    this.linhaService = linhaService;
    this.httpErrorHandler = httpErrorHandler;
  }

  async ngOnInit(): Promise<void> {
    this.listaLinhas = await this.linhaService.listaLinhas();
  }

  listaLinhas: ILinhaDTO[] = [];
  idLinha: string;
  listaPercursosLinha: IPercursoDTO[] = [];
  percurso: IPercursoDTO;

  async getListaPercursosLinha() {
    try {
      this.percursoService.validarLinha(this.idLinha);
      this.listaPercursosLinha = await this.percursoService.listarPercursoLinha(this.idLinha);
    } catch (err) {
      alert(err);
    }
  }

  openDialog(id: string) {
    this.listaPercursosLinha.forEach(element => {
      if (element.id == id) this.percurso = element;
    });

    this.dialog.open(DadosPercursoDialogComponent, {
      data: {
        percursoId: this.percurso.id,
        percursoNomePercurso: this.percurso.nomePercurso,
        percursoLinhaId: this.percurso.linhaId,
        percursoOrientacao: this.percurso.orientacao,
        percursoListaSegmentos: this.percurso.listaSegmentos,
        percursoNoInicialId: this.percurso.noInicialId,
        percursoNoFinalId: this.percurso.noFinalId,
        percursoDistanciaTotal: this.percurso.distanciaTotal,
        percursoTempoTotal: this.percurso.tempoTotal
      }
    });
  }
}

@Component({
  selector: 'dados-percurso-dialog',
  templateUrl: 'dados-percurso-dialog.component.html',
})
export class DadosPercursoDialogComponent {

  percursoId: string;
  percursoNomePercurso: string;
  percursoLinhaId: string;
  percursoOrientacao: string;
  percursoListaSegmentos: string[];
  percursoNoInicialId: string;
  percursoNoFinalId: string;
  percursoDistanciaTotal: number;
  percursoTempoTotal: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.percursoId = data.percursoId;
    this.percursoNomePercurso = data.percursoNomePercurso;
    this.percursoLinhaId = data.percursoLinhaId;
    this.percursoOrientacao = data.percursoOrientacao;
    this.percursoListaSegmentos = data.percursoListaSegmentos;
    this.percursoNoInicialId = data.percursoNoInicialId;
    this.percursoNoFinalId = data.percursoNoFinalId;
    this.percursoDistanciaTotal = data.percursoDistanciaTotal;
    this.percursoTempoTotal = data.percursoTempoTotal;
  }

}
