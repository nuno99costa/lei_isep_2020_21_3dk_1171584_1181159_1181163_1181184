import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-lista-percursos-linha',
  templateUrl: './lista-percursos-linha.component.html',
  styleUrls: ['./lista-percursos-linha.component.css']
})
export class ListaPercursosLinhaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }
  
  ngOnInit(): void {
  }

}
