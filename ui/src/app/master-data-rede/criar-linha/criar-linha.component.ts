import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-linha',
  templateUrl: './criar-linha.component.html',
  styleUrls: ['./criar-linha.component.css']
})
export class CriarLinhaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDR");
  }

  ngOnInit(): void {
  }

}
