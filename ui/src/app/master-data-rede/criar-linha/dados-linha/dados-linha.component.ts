import { HttpErrorResponse } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import INoDTO from 'src/app/dto/INoDTO';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';
import { ILinhaService } from 'src/app/services/IServices/ILinhaService';
import { INoService } from 'src/app/services/IServices/INoService';
import { ITipoTripulanteService } from 'src/app/services/IServices/ITipoTripulanteService';
import { ITipoViaturaService } from 'src/app/services/IServices/ITipoViaturaService';
import { LinhaService } from 'src/app/services/linha.service';
import { NoService } from 'src/app/services/no.service';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-linha',
  templateUrl: './dados-linha.component.html',
  styleUrls: ['./dados-linha.component.css']
})
export class DadosLinhaComponent implements OnInit {

  private linhaService: ILinhaService;
  private noService : INoService;
  private tvService : ITipoViaturaService;
  private ttService : ITipoTripulanteService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, noService : NoService, tvService : TipoViaturaService, ttService : TipoTripulanteService, linhaService: LinhaService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.linhaService = linhaService;
    this.noService = noService;
    this.tvService = tvService;
    this.ttService = ttService;
    this.httpErrorHandler = httpErrorHandler;
  }

  async ngOnInit(): Promise<void> {
    this.listaNos = await this.noService.listaNos();
    this.listaTV = await this.tvService.listaTiposViatura();
    this.listaTT = await this.ttService.listaTiposTripulante();
  }

  //Listas para mostrar
  listaNos : INoDTO[] = [];
  listaTV : ITipoViaturaDTO[] = [];
  listaTT : ITipoTripulanteDTO[] = [];

  //Variaveis auxiliares
  idTVPermitidoSelecionado : string;
  idTTPermitidoSelecionado : string;
  idTVProibidoSelecionado : string;
  idTTProibidoSelecionado : string;

  //Variaveis para DTO
  idLinha: string;
  codigo: string;
  nome: string;
  cor: string;
  noInicialId: string;
  noFinalId: string;
  viaturasPermitidas: string[] = [];
  viaturasProibidas: string[] = [];
  tripulantesPermitidos: string[] = [];
  tripulantesProibidos: string[] = [];

  /**
   * Métodos que adicionam e eliminam tipos de viaturas ou tipos de tripulantes aos arrays 
   * dos mesmos que a linha possui. Temos que fazer a distinção entre permitidos e proibidos.
   * 
   * @param item id do tipo de viatura ou de tripulante introduzido pelo utilizador na UI
   */
  public addTipoViaturaPermitidaToArray() {
    try {
      this.viaturasPermitidas = this.linhaService.addElementToArray(this.idTVPermitidoSelecionado, this.viaturasPermitidas);
    } catch (error) {
      alert(error);
    }
  }

  public addTipoViaturaProibidaToArray() {
    try {
      this.viaturasProibidas = this.linhaService.addElementToArray(this.idTVProibidoSelecionado, this.viaturasProibidas);
    } catch (error) {
      alert(error);
    }
  }

  public deleteTipoViaturaPermitidaOfArray() {
    try {
      this.viaturasPermitidas = this.linhaService.deleteElementOfArray(this.idTVPermitidoSelecionado, this.viaturasPermitidas);
    } catch (error) {
      alert(error);
    }
  }

  public deleteTipoViaturaProibidaOfArray() {
    try {
      this.viaturasProibidas = this.linhaService.deleteElementOfArray(this.idTVProibidoSelecionado, this.viaturasProibidas);
    } catch (error) {
      alert(error);
    }
  }

  public addTipoTripulantePermitidoToArray() {
    try { 
      this.tripulantesPermitidos = this.linhaService.addElementToArray(this.idTTPermitidoSelecionado, this.tripulantesPermitidos);
    } catch (error) {
      alert(error);
    }
  }

  public addTipoTripulanteProibidoToArray() {
    try {
      this.tripulantesProibidos = this.linhaService.addElementToArray(this.idTTProibidoSelecionado, this.tripulantesProibidos);
    } catch (error) {
      alert(error);
    }
  }

  public deleteTipoTripulantePermitidoOfArray() {
    try {
      this.tripulantesPermitidos = this.linhaService.deleteElementOfArray(this.idTTPermitidoSelecionado, this.tripulantesPermitidos);
    } catch (error) {
      alert(error);
    }
  }

  public deleteTipoTripulanteProibidoOfArray() {
    try {
      this.tripulantesProibidos = this.linhaService.deleteElementOfArray(this.idTTProibidoSelecionado, this.tripulantesProibidos);
    } catch (error) {
      alert(error);
    }
  }

  public submeter() {

    const linha = {
      id: this.idLinha,
      codigo: this.codigo,
      nome: this.nome,
      cor: this.cor,
      noInicial: this.noInicialId,
      noFinal: this.noFinalId,
      viaturasPermitidas: this.viaturasPermitidas,
      viaturasProibidas: this.viaturasProibidas,
      tripulantesPermitidos: this.tripulantesPermitidos,
      tripulantesProibidos: this.tripulantesProibidos
    } as ILinhaDTO;

    try {
      this.linhaService.validarLinha(linha);

      this.linhaService.criarLinha(linha).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch (error) {
      alert(error);
    }
  }
}
