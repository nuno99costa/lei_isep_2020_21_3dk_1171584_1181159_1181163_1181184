import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { of } from 'rxjs';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import INoDTO from 'src/app/dto/INoDTO';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';
import { LinhaService } from 'src/app/services/linha.service';
import { NoService } from 'src/app/services/no.service';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosLinhaComponent } from './dados-linha.component';

describe('DadosLinhaComponent', () => {
  let component: DadosLinhaComponent;
  let fixture: ComponentFixture<DadosLinhaComponent>;

  const mockLinha = {
    "id": "a",
    "codigo": "A",
    "nome": "Cristelo-Lordelo",
    "cor": "rosa",
    "noInicial": "1",
    "noFinal": "2",
    "viaturasPermitidas": [],
    "viaturasProibidas": [],
    "tripulantesPermitidos": [],
    "tripulantesProibidos": []
  } as ILinhaDTO;

  const mockViaturasPermitidas = ["1"];
  const mockViaturasProibidas: string[] = [];

  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('linhaService', ['validarLinha', 'criarLinha', 'addElementToArray', 'deleteElementOfArray']);
  linhaServiceSpy.addElementToArray.and.returnValue(mockViaturasPermitidas);
  linhaServiceSpy.deleteElementOfArray.and.returnValue(mockViaturasProibidas);
  linhaServiceSpy.criarLinha.and.returnValue(of(mockLinha));
  linhaServiceSpy.validarLinha.and.callFake(() => { });

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const tipoTripulanteServiceSpy: jasmine.SpyObj<TipoTripulanteService> = jasmine.createSpyObj('TipoTripulanteService', ['listaTiposTripulante']);
  const tipoViaturaServiceSpy: jasmine.SpyObj<TipoViaturaService> = jasmine.createSpyObj('tipoViaturaService', ['listaTiposViatura']);
  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['listaNos']);
  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);

  noServiceSpy.listaNos.and.returnValue(Promise.resolve([] as INoDTO[]));
  tipoViaturaServiceSpy.listaTiposViatura.and.returnValue(Promise.resolve([] as ITipoViaturaDTO[]));
  tipoTripulanteServiceSpy.listaTiposTripulante.and.returnValue(Promise.resolve([] as ITipoTripulanteDTO[]));
  

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DadosLinhaComponent],
      imports: [BrowserModule],
      providers: [{ provide: LinhaService, useValue: linhaServiceSpy }, { provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy },{ provide:NoService, useValue: noServiceSpy },{ provide: TipoTripulanteService, useValue: tipoTripulanteServiceSpy },{ provide: TipoViaturaService, useValue: tipoViaturaServiceSpy },{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosLinhaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should defined', () => {
    expect(component).toBeTruthy();
  });

  it('should add and delete a tipo viatura permitido to an array', () => {
    //Os métodos recebem items de HTML
    let item = document.createElement("input");
    item.value = '1'; //Tem de ser '1', pq foi definido em cima que a função adiciona este valor

    component.idTVPermitidoSelecionado = "ola";

    //Add tipo de viatura permitido
    component.addTipoViaturaPermitidaToArray();
    expect(linhaServiceSpy.addElementToArray).toHaveBeenCalled;
    expect(component.viaturasPermitidas).toEqual(['1']);

    //Delete tipo de vitura permitido
    component.deleteTipoViaturaPermitidaOfArray();
    expect(linhaServiceSpy.deleteElementOfArray).toHaveBeenCalled;
    expect(component.viaturasPermitidas).toEqual([]);

  });

  it('should add and delete a tipo viatura proibido to an array', () => {
    //Os métodos recebem items de HTML
    let item = document.createElement("input");
    item.value = '1';


    component.idTVProibidoSelecionado = "ola";

    //Add tipo de viatura proibido
    component.addTipoViaturaProibidaToArray();
    expect(linhaServiceSpy.addElementToArray).toHaveBeenCalled;
    expect(component.viaturasProibidas).toEqual(['1']);

    //Delete tipo de vitura proibido
    component.deleteTipoViaturaProibidaOfArray();
    expect(linhaServiceSpy.deleteElementOfArray).toHaveBeenCalled;
    expect(component.viaturasProibidas).toEqual([]);

  });

  it('should add and delete a tipo tripulante permitido to an array', () => {
    //Os métodos recebem items de HTML
    let item = document.createElement("input");
    item.value = '1';

    component.idTTPermitidoSelecionado = "ola";

    //Add tipo de tripulante permitido
    component.addTipoTripulantePermitidoToArray();
    expect(linhaServiceSpy.addElementToArray).toHaveBeenCalled;
    expect(component.tripulantesPermitidos).toEqual(['1']);

    //Delete tipo de tripulante permitido
    component.deleteTipoTripulantePermitidoOfArray();
    expect(linhaServiceSpy.deleteElementOfArray).toHaveBeenCalled;
    expect(component.tripulantesPermitidos).toEqual([]);

  });

  it('should add and delete a tipo tripulante proibido to an array', () => {
    //Os métodos recebem items de HTML
    let item = document.createElement("input");
    item.value = '1';

    component.idTTProibidoSelecionado = "ola";

    //Add tipo de tripulante proibido
    component.addTipoTripulanteProibidoToArray();
    expect(linhaServiceSpy.addElementToArray).toHaveBeenCalled;
    expect(component.tripulantesProibidos).toEqual(['1']);

    //Delete tipo de tripulante proibido
    component.deleteTipoTripulanteProibidoOfArray();
    expect(linhaServiceSpy.deleteElementOfArray).toHaveBeenCalled;
    expect(component.tripulantesProibidos).toEqual([]);

  });

  it('should create and validate a linha', () => {
    component.submeter();
    expect(linhaServiceSpy.criarLinha).toHaveBeenCalled();
    expect(linhaServiceSpy.validarLinha).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });
});
