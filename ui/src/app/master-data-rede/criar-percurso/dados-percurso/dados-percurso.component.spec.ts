import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DadosPercursoComponent } from "./dados-percurso.component";
import { PercursoService } from 'src/app/services/percurso.service';
import { of } from 'rxjs';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { SegmentoService } from 'src/app/services/segmento.service';
import { LinhaService } from 'src/app/services/linha.service';
import { NoService } from 'src/app/services/no.service';
import INoDTO from 'src/app/dto/INoDTO';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import ISegmentoDTO from 'src/app/dto/ISegmentoDTO';

describe('DadosPercursoComponent', () => {
  let component: DadosPercursoComponent;
  let fixture: ComponentFixture<DadosPercursoComponent>;

  //Mock do Percurso
  const mockPercurso= {
      id: "1",   
      nomePercurso: "percurso1",
      linhaId: "1",
      orientacao: "Ida",
      listaSegmentos: [],
      noInicialId: "1",
      noFinalId: "4"
    } as IPercursoDTO;
  //Mock lista de segmentos
  const listaSegmentosAdd = ['1'];
  const listaSegmentosDelete : string[] =  [];  

  //Mock/Spy dos injetáveis (Service e HTTPErrorHandler)
  const percursoServiceSpy: jasmine.SpyObj<PercursoService> = jasmine.createSpyObj('percursoService', ['addElementToArray', 'deleteElementOfArray', 'criarPercurso', 'validarInput'])
  percursoServiceSpy.addElementToArray.and.returnValue(listaSegmentosAdd);
  percursoServiceSpy.deleteElementOfArray.and.returnValue(listaSegmentosDelete);
  percursoServiceSpy.criarPercurso.and.returnValue(of(mockPercurso));
  percursoServiceSpy.validarInput.and.callFake(()=>{});

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const segmentoServiceSpy: jasmine.SpyObj<SegmentoService> = jasmine.createSpyObj('segmentoService', ['validarInput', 'criarSegmento', 'listaSegmentos']);
  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('linhaService', ['validarLinha', 'criarLinha', 'addElementToArray', 'deleteElementOfArray', 'listaLinhas']);
  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['criarNo', 'validarInput', 'listaNos']);
  
  noServiceSpy.listaNos.and.returnValue(Promise.resolve([] as INoDTO[]));
  linhaServiceSpy.listaLinhas.and.returnValue(Promise.resolve([] as ILinhaDTO[]));
  segmentoServiceSpy.listaSegmentos.and.returnValue(Promise.resolve([] as ISegmentoDTO[]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosPercursoComponent ],
      imports: [BrowserModule],
      providers: [{provide: PercursoService, useValue: percursoServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: LinhaService, useValue: linhaServiceSpy},{provide: SegmentoService, useValue: segmentoServiceSpy},{provide: NoService, useValue: noServiceSpy},{provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosPercursoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  //--------------------------------------------------------------------TESTES-------------------------------

  it('should add and delete elemnts of list', () => {
    let x = document.createElement("input");
    x.value='1';
    component.segmentoIDSelecionado = "a";
    component.addElementToArray();
    expect(percursoServiceSpy.addElementToArray).toHaveBeenCalled;
    expect(component.listaSegmentos).toEqual(['1']);
    component.deleteElementOfArray();
    expect(percursoServiceSpy.deleteElementOfArray).toHaveBeenCalled;
    expect(component.listaSegmentos).toEqual([]);
  });

  it('should create and validate', () => {
    component.submeter();
    expect(percursoServiceSpy.criarPercurso).toHaveBeenCalled();
    expect(percursoServiceSpy.validarInput).toHaveBeenCalled();
    expect(httpErrorHandlerSpy.handleError).toHaveBeenCalledTimes(0);
  });

});
