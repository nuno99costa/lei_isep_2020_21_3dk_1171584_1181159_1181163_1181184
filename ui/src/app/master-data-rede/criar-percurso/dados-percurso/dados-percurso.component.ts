import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import INoDTO from 'src/app/dto/INoDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import ISegmentoDTO from 'src/app/dto/ISegmentoDTO';
import { ILinhaService } from 'src/app/services/IServices/ILinhaService';
import { INoService } from 'src/app/services/IServices/INoService';
import { IPercursoService } from 'src/app/services/IServices/IPercursoService';
import { ISegmentoService } from 'src/app/services/IServices/ISegmentoService';
import { LinhaService } from 'src/app/services/linha.service';
import { NoService } from 'src/app/services/no.service';
import { PercursoService } from 'src/app/services/percurso.service';
import { SegmentoService } from 'src/app/services/segmento.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-percurso',
  templateUrl: './dados-percurso.component.html',
  styleUrls: ['./dados-percurso.component.css']
})

export class DadosPercursoComponent implements OnInit {

  segmentoService: ISegmentoService;
  linhaService: ILinhaService;
  noService: INoService;
  percursoService: IPercursoService;
  httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, noService: NoService, segmentoService: SegmentoService, linhaService: LinhaService, percursoService: PercursoService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDR");
    this.linhaService = linhaService;
    this.segmentoService = segmentoService;
    this.percursoService = percursoService;
    this.httpErrorHandler = httpErrorHandler;
    this.noService = noService;
  }

  async ngOnInit(): Promise<void> {
    this.listaLinhas = await this.linhaService.listaLinhas();
    this.listaAllSegmentos = await this.segmentoService.listaSegmentos();
    this.listaNos = await this.noService.listaNos();
  }

  //Listas a mostrar
  listaLinhas: ILinhaDTO[] = [];
  listaAllSegmentos: ISegmentoDTO[] = [];
  listaNos: INoDTO[] = [];

  //Auxiliares
  segmentoIDSelecionado: string;

  //Variaveis para DTO
  idPercurso: string;
  nomePercurso: string;
  linhaId: string;
  escolhaOrientacao: string;
  listaSegmentos: string[] = [];
  noInicialId: string;
  noFinalId: string;


  addElementToArray() {
    this.listaSegmentos = this.percursoService.addElementToArray(this.segmentoIDSelecionado, this.listaSegmentos);
  }

  deleteElementOfArray() {
    this.listaSegmentos = this.percursoService.deleteElementOfArray(this.segmentoIDSelecionado, this.listaSegmentos);

  }

  submeter() {

    const percurso = {
      id: this.idPercurso,
      nomePercurso: this.nomePercurso,
      linhaId: this.linhaId,
      orientacao: this.escolhaOrientacao,
      listaSegmentos: this.listaSegmentos,
      noInicialId: this.noInicialId,
      noFinalId: this.noFinalId
    } as IPercursoDTO;

    try {

      this.percursoService.validarInput(percurso);

      this.percursoService.criarPercurso(percurso).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      },
        error => {
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        });

    } catch (err) {
      alert(err);
    }
  }
}
