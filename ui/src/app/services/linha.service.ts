import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import ILinhaDTO from '../dto/ILinhaDTO';
import { ILinhaService } from './IServices/ILinhaService';

@Injectable({
  providedIn: 'root'
})
export class LinhaService implements ILinhaService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  baseURL: String = environment.mdrURL;

  //Validar os campos da UI (Two way binding: se mudar na UI é automaticamente refletida aqui)
  public validarLinha(linhaDTO: ILinhaDTO) {
    if (!linhaDTO.codigo) throw new Error("Introduza um código válido.");

    if (!linhaDTO.nome) throw new Error("Introduza um nome válido.");

    if (!linhaDTO.cor) throw new Error("Introduza uma cor válida.");

    if (!linhaDTO.noInicial) throw new Error("Introduza um ID de nó inicial válido.");

    if (!linhaDTO.noFinal) throw new Error("Introduza um ID de nó final válido.");
  }

  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

  //Cria a nova linha com o DTO
  public criarLinha(linhaDTO: ILinhaDTO) {
    return this.http.post<ILinhaDTO>(`${this.baseURL}/linha`, linhaDTO, this.getHeaders());
  }

  //Adiciona um elemento(tipo de viatura ou de tripulante) a um array
  public addElementToArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (array.includes(element)) throw new Error("Elemento: " + element + " já existe na lista.");

    array.push(element);
    alert("Element: " + element + " added.");

    return array;
  }

  //Apaga um elemento(tipo de viatura ou de tripulante) do array
  public deleteElementOfArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (!array.includes(element)) throw new Error("Elemento: " + element + " não existe na lista.");

    const index = array.indexOf(element, 0);
    if (index > -1) {
      array.splice(index, 1);
      alert("Element: " + element + " deleted.");
    }

    return array;
  }

  async listaLinhas(): Promise<ILinhaDTO[]> {
    return this.http.get<ILinhaDTO[]>(`${this.baseURL}/linha/all/ordenados/nome`, this.getHeaders()).toPromise();
  }
}