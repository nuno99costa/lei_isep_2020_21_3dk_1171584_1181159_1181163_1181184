import ITripulantesBlocosTrabalhoDTO from 'src/app/dto/ITripulantesBlocosTrabalhoDTO';
import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import ITripulanteDTO from '../dto/ITripulanteDTO';
import { ITripulanteService } from './IServices/ITripulanteService';

@Injectable({
  providedIn: 'root'
})
export class TripulanteService implements ITripulanteService {

  // URL BASE DA ROUTE do Tripulante.
  baseURL: string = environment.mdvURL;
  planeamentoURL: string = environment.planeamentoURL;

  constructor(private http: HttpClient, private authService: AuthService) { }

  validarInput(tripulante: ITripulanteDTO, checkBoxRGPD: boolean): void {

    if (!tripulante.nome) { throw new Error('Introduza um nome.'); }

    if (!tripulante.nMecanografico.match(/^[a-zA-Z0-9]{9,}$/)) { throw new Error('Introduza um número mecanográfico válido.'); }

    if (!tripulante.dataNascimento) { throw new Error('Introduza uma data de nascimento.'); }

    let currentDate = new Date();
    let data = new Date(tripulante.dataNascimento);
    let idade = Math.abs(currentDate.getFullYear() - data.getFullYear());
    if (idade < 18) throw new Error("O tripulante tem de ter pelo menos 18 anos.");

    //if (!tripulante.NCCidadao.match(/^[0-9]{8}[ -]*[0-9][A-Z]{2}[0-9]$/)) { throw new Error('Introduza um número de cartão de cidadão válido.'); }

    if (!tripulante.NCConducao) { throw new Error('Introduza um número de carta de condução.'); }

    //if (! this.validateNIF(tripulante.NIF)) { throw new Error('Introduza um NIF válido.'); }

    if (!tripulante.TipoTripulante) { throw new Error('Introduza um tipo de tripulante.'); }

    if (!tripulante.dataEntradaEmpresa) { throw new Error('Introduza a data de entrada na empresa.'); }

    if (!tripulante.dataLicencaConducao) { throw new Error('Introduza a data da licença de condução.'); }

    if (!checkBoxRGPD) throw new Error('Para efetuar o registo, é necessário explicitar o consentimento do tripulante relativamente ao tratamento de dados.');

  }

  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

  criarTripulante(tripulante: ITripulanteDTO): Observable<ITripulanteDTO> {
    return this.http.post<ITripulanteDTO>(`${this.baseURL}/Tripulantes`, tripulante, this.getHeaders());
  }

  async listaTripulantes(): Promise<ITripulanteDTO[]> {
    return this.http.get<ITripulanteDTO[]>(`${this.baseURL}/Tripulantes`, this.getHeaders()).toPromise();
  }

  // retirado da wikipedia
  validateNIF(value: string) {
    const nif = value;
    const validationSets = {
      one: ['1', '2', '3', '5', '6', '8'],
      two: ['45', '70', '71', '72', '74', '75', '77', '79', '90', '91', '98', '99']
    };

    if (nif.length !== 9) {
      return false;
    }

    if (!validationSets.one.includes(nif.substr(0, 1)) && !validationSets.two.includes(nif.substr(0, 2))) {
      return false;
    }

    const total = parseInt(nif[0], 10) * 9 +
      parseInt(nif[1], 10) * 8 +
      parseInt(nif[2], 10) * 7 +
      parseInt(nif[3], 10) * 6 +
      parseInt(nif[4], 10) * 5 +
      parseInt(nif[5], 10) * 4 +
      parseInt(nif[6], 10) * 3 +
      parseInt(nif[7], 10) * 2;
    const modulo11 = (Number(total) % 11);

    const checkDigit = modulo11 < 2 ? 0 : 11 - modulo11;

    return checkDigit === Number(nif[8]);
  }

  validarInputTripulantesBlocosTrabalho(dados: ITripulantesBlocosTrabalhoDTO) {
    if (!dados.numerosARetirar) { throw new Error("Introduza  os números a retirar") };

    if (!dados.custoC) { throw new Error("Introduza  um custo") };

    if (!dados.tempoRelativo) { throw new Error("Introduza  um tempo relativo") };

    if (!dados.numeroNovasGeracoes) { throw new Error("Introduza  um número de novas Gerações") };

    if (!dados.dimensaoPopulacao) { throw new Error("Introduza a dimensão da população") };

    if (!dados.probabilidadeCruzamento) { throw new Error("Introduza  a probabilidade de Cruzamento") };

    if (!dados.probabilidadeMutacao) { throw new Error("Introduza  a probabilidade de Mutação") };

    if (dados.probabilidadeCruzamento < 0 || dados.probabilidadeCruzamento > 100) { throw new Error("Introduza uma probabilidade de cruzamento válida") };

    if (dados.probabilidadeMutacao < 0 || dados.probabilidadeMutacao > 100) { throw new Error("Introduza uma probabilidade de mutação válida") };

  }

  obterListaTripulantesBlocosTrabalho(dados: ITripulantesBlocosTrabalhoDTO): Observable<string[]> {
    return this.http.get<string[]>(`${this.planeamentoURL}/planeamento/escalonar?nret=` + dados.numerosARetirar + `&vdid=` + dados.servicoViaturaId + `&custo=` + dados.custoC + `&trel=` + dados.tempoRelativo
      + `&nger=` + dados.numeroNovasGeracoes + `&dpop=` + dados.dimensaoPopulacao
      + `&pcruz=` + dados.probabilidadeCruzamento + `&pmut=` + dados.probabilidadeMutacao, this.getHeaders());
  }
}
