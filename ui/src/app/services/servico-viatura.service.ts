import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import IServicoViaturaDTO from '../dto/IServicoViaturaDTO';
import { AuthService } from './auth.service';
import { IServicoViaturaService } from './IServices/IServicoViaturaService';

@Injectable({
  providedIn: 'root'
})
export class ServicoViaturaService implements IServicoViaturaService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  baseURL: String = environment.mdvURL;

  validarServicoViatura(servicoViaturaDTO: IServicoViaturaDTO): void {
    if (!servicoViaturaDTO.id || servicoViaturaDTO.id.length != 10) throw new Error("Introduza um ID válido.");

    if (!servicoViaturaDTO.viagensIds || servicoViaturaDTO.viagensIds.length == 0) throw new Error("Introduza viagens.");
  }

  criarServicoViatura(servicoViaturaDTO: IServicoViaturaDTO): Observable<IServicoViaturaDTO> {
    return this.http.post<IServicoViaturaDTO>(`${this.baseURL}/ServicosViatura`, servicoViaturaDTO, this.getHeaders());
  }

  listaServicosViatura(): Promise<IServicoViaturaDTO[]> {
    return this.http.get<IServicoViaturaDTO[]>(`${this.baseURL}/ServicosViatura`, this.getHeaders()).toPromise();
  }

  addElementToArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (array.includes(element)) throw new Error("Elemento: " + element + " já existe na lista.");

    array.push(element);
    alert("Element: " + element + " added.");

    return array;
  }

  deleteElementOfArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (!array.includes(element)) throw new Error("Elemento: " + element + " não existe na lista.");

    const index = array.indexOf(element, 0);
    if (index > -1) {
      array.splice(index, 1);
      alert("Element: " + element + " deleted.");
    }

    return array;
  }

  getHeaders(): { headers: HttpHeaders; } {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }
}
