import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { AuthService } from './auth.service';

import { InserirGLXService } from './inserir-glx.service';

describe('InserirGLXService', () => {
  let glxservice: InserirGLXService;

  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um nó
  httpClientSpy.post.and.returnValue(of('teste'));


  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InserirGLXService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([InserirGLXService], (service: InserirGLXService) => {
    glxservice = service;
  }));

  it('should be created', () => {
    expect(glxservice).toBeTruthy();
  });
});
