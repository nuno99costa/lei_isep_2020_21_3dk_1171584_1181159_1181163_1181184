import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import HTTPErrorHandler from '../utils/httpErrorHandler';
import { AuthService } from './auth.service';

import { ViagemService } from './viagem.service';

describe('ViagemService', () => {
  let service: ViagemService;

  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViagemService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy},{provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([ViagemService], (serviceParam: ViagemService) => {
    service = serviceParam;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
