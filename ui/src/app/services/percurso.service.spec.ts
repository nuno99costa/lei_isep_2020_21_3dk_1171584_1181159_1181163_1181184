import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import IPercursoDTO from '../dto/IPercursoDTO';
import { AuthService } from './auth.service';

import { PercursoService } from './percurso.service';

describe('PercursoService', () => {

  //--------------------------------------------------SETUP DOS TESTES--------------------------------------
  let percursoService: PercursoService;

  //Mock do Percurso
  const mockPercurso= {
    id: "1",   
    nomePercurso: "percurso1",
    linhaId: "1",
    orientacao: "Ida",
    listaSegmentos: ['1'],
    noInicialId: "1",
    noFinalId: "4"
  } as IPercursoDTO;

  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um nó
  httpClientSpy.post.and.returnValue(of(mockPercurso));

  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PercursoService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([PercursoService], (service: PercursoService) => {
    percursoService = service;
  }))

  //----------------------------------------------Começar os testes-----------------------------------------------
  it('should be defined', () => {
    expect(percursoService).toBeTruthy();
  });

  it('should add and delete elements from list of segmentos', () => {
    const listAdd = percursoService.addElementToArray("2", mockPercurso.listaSegmentos).length;
    const listDelete = percursoService.deleteElementOfArray("2",mockPercurso.listaSegmentos).length;
    expect(listAdd).toBe(2);
    expect(listDelete).toBe(1);
  })

  it('should post Percurso using http request', () => {
    const testResult = percursoService.criarPercurso(mockPercurso);
    expect(httpClientSpy.post).toHaveBeenCalled();
    //expect(testResult).toEqual(of(mockPercurso));  
  });

  it('should validate the inputs', () => {
    //Validar se a função lança erros.
    //Atualizamos os dados do mock para lançar erros diferentes a cada teste
    let mockPercursoInvalido = {
      id: "1",   
      nomePercurso: "",
      linhaId: "1",
      orientacao: "Ida",
      listaSegmentos: ['1'],
      noInicialId: "1",
      noFinalId: "4"
    } as IPercursoDTO;

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Introduza um nome válido");

    mockPercursoInvalido.nomePercurso = "percurso1";
    mockPercursoInvalido.linhaId = "";

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Introduza uma linha de id válida");

    mockPercursoInvalido.linhaId = "1";
    mockPercursoInvalido.orientacao = "";

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Selecione uma orientação");

    mockPercursoInvalido.orientacao = "Ida";
    mockPercursoInvalido.listaSegmentos = [];

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Introduza segmentos");

    mockPercursoInvalido.listaSegmentos = ['1'];
    mockPercursoInvalido.noInicialId = "";

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Introduza um id de nó incial válido");

    
    mockPercursoInvalido.noInicialId = "1";
    mockPercursoInvalido.noFinalId = "";

    expect(function() {percursoService.validarInput(mockPercursoInvalido)}).toThrowError("Introduza um id de nó final válido");

  })
});
