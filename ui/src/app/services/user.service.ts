import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import IRegistoDTO from '../dto/IRegistoDTO';
import { AuthService } from './auth.service';
import { IUserService } from './IServices/IUserService';

@Injectable({
  providedIn: 'root'
})
export class UserService implements IUserService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  baseURL: String = environment.mdrURL;

  validarDataNascimento(data: Date) {
    let currentDate = new Date()
    let idade = Math.abs(currentDate.getFullYear() - data.getFullYear());
    if (idade < 13) throw new Error("O cliente tem de ter pelo menos 13 anos para utilizar a aplicação.");
  }

  validarUpdate(utilizador: IRegistoDTO) {
    if (utilizador.name.length < 6) throw new Error("Introduza um nome com mais de 6 dígitos.");

    if (!utilizador.email.match(/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/)) throw new Error("Introduza um email válido.");

    if (utilizador.password.length < 6) throw new Error("Introduza uma palavra passe com mais de 6 dígitos.");

  }

  //Coloca o token no pedido de modo a ser verificado no back end q o token do utilizador corresponde ao token guardado após login
  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };


    return httpOptions;
  }

  getDadosUser(): Promise<IRegistoDTO> {
    return this.http.get<IRegistoDTO>(`${this.baseURL}/user`, this.getHeaders()).toPromise();
  }

  update(utilizador: IRegistoDTO): Observable<IRegistoDTO> {
    return this.http.put<IRegistoDTO>(`${this.baseURL}/user`, utilizador, this.getHeaders());
  }

  delete(): Observable<IRegistoDTO> {
    return this.http.delete<IRegistoDTO>(`${this.baseURL}/user`, this.getHeaders());
  }
}
