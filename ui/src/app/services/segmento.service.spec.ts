import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import { AuthService } from './auth.service';

import { SegmentoService } from './segmento.service';

describe('SegmentoServiceService', () => {

    //--------------------------------------------------SETUP DOS TESTES--------------------------------------
  let segmentoService: SegmentoService;

  //Mock do Segmento
  const mockSegmento = {
    id: "1",
    nomeSegmento: "seg1",
    idNoInicial: "1",
    idNoFinal: "2",
    distanciaNos: 1000,
    tempoNos: 100
  } as ISegmentoDTO;
  
  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um nó
  httpClientSpy.post.and.returnValue(of(mockSegmento));
  
 //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SegmentoService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([SegmentoService], (service: SegmentoService) => {
    segmentoService = service;
  }));

  //----------------------------------------------Começar os testes-----------------------------------------------
  it('should be defined', () => {
    expect(segmentoService).toBeTruthy();
  });

  it('should post Segmento using http request', () => {
    const testResult = segmentoService.criarSegmento(mockSegmento);
    expect(httpClientSpy.post).toHaveBeenCalled();
    //expect(testResult.toEqual(of(mockSegmento));  PERGUNTAR PORQUE NAO FUNCIONA
  })

  it('should validate the inputs', () => {
    //Validar se a função lança erros.
    //Atualizamos os dados do mock para lançar erros diferentes a cada teste
    //Mock do Segmento
    const mockSegmentoInvalido = {
      id: "1",
      nomeSegmento: "",
      idNoInicial: "1",
      idNoFinal: "2",
      distanciaNos: 1000,
      tempoNos: 100
    } as ISegmentoDTO;

    expect(function() {segmentoService.validarInput(mockSegmentoInvalido)}).toThrowError("Introduza um nome para o segmento");

    mockSegmentoInvalido.nomeSegmento = "a";
    mockSegmentoInvalido.idNoInicial = "";

    expect(function() {segmentoService.validarInput(mockSegmentoInvalido)}).toThrowError("Introduza um id de nó incial");

    mockSegmentoInvalido.idNoInicial = "1";
    mockSegmentoInvalido.idNoFinal = "";

    expect(function() {segmentoService.validarInput(mockSegmentoInvalido)}).toThrowError("Introduza um id de nó final");

    mockSegmentoInvalido.idNoFinal = "2";
    mockSegmentoInvalido.distanciaNos = NaN;

    expect(function() {segmentoService.validarInput(mockSegmentoInvalido)}).toThrowError("Introduza uma distância válida");

    mockSegmentoInvalido.distanciaNos = 1000;
    mockSegmentoInvalido.tempoNos = NaN;

    expect(function() {segmentoService.validarInput(mockSegmentoInvalido)}).toThrowError("Introduza um tempo válido");
  
  });

});
