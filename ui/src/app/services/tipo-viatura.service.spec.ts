import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';
import { AuthService } from './auth.service';

import { TipoViaturaService } from './tipo-viatura.service';

describe('TipoViaturaService', () => {
  let tipoViaturaService: TipoViaturaService;

  const mockTipoViatura = {
    id: "1",
    codigo: "tp",
    descricao: "tp teste",
    combustivel: "GPL",
    autonomia: 12,
    velocidadeMedia: 50,
    custoPorQuilometro: 2,
    consumoMedio: 2.444
  } as ITipoViaturaDTO;

  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um tipo de viatura
  httpClientSpy.post.and.returnValue(of(mockTipoViatura));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoViaturaService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  beforeEach(inject([TipoViaturaService], (service: TipoViaturaService) => {
    tipoViaturaService = service;
  }));

  it('should be defined', () => {
    expect(tipoViaturaService).toBeTruthy();
  });

  it('should post Tipo Viatura using http request', () => {
    //Chamamos o método que cria com o mock do tipo de viatura
    const testResult = tipoViaturaService.criarTipoViatura(mockTipoViatura);
    //É esperado que o método post do httpclient seja chamado
    expect(httpClientSpy.post).toHaveBeenCalled();
    //expect(testResult.toEqual(of(mockNo));
  });

  //Este valida cada if do método de validação dos dados do tipo de viatura
  it('should validate the input data of tipo viatura', () => {
    //Criamos um mock onde vão existir valores errados que não são aceitáveis.
    //Vamos mudar ao longo do método os valores para testar.
    let mockTipoViaturaInvalido = {
      id: "1",
      codigo: "",
      descricao: "tp teste",
      combustivel: "GPL",
      autonomia: 12,
      velocidadeMedia: 50,
      custoPorQuilometro: 2,
      consumoMedio: 2.444
    } as ITipoViaturaDTO;

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza um código válido.");

    mockTipoViaturaInvalido.codigo = "aaaaaaaaaaaaaaaaaaaa";
    mockTipoViaturaInvalido.descricao = "";

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza uma descrição.");

    mockTipoViaturaInvalido.descricao = "tp teste";
    mockTipoViaturaInvalido.combustivel = "";

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza um tipo de combustível.");

    mockTipoViaturaInvalido.combustivel = "Gasoleo";
    mockTipoViaturaInvalido.autonomia = NaN;

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza uma autonomia válida.");

    mockTipoViaturaInvalido.autonomia = 12;
    mockTipoViaturaInvalido.velocidadeMedia = NaN;

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza uma velocidade média válida.");

    mockTipoViaturaInvalido.velocidadeMedia = 50;
    mockTipoViaturaInvalido.custoPorQuilometro = NaN;

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza o custo por quilómetro válido.");

    mockTipoViaturaInvalido.custoPorQuilometro = 2;
    mockTipoViaturaInvalido.consumoMedio = NaN;

    expect(function () { tipoViaturaService.validarTipoViatura(mockTipoViaturaInvalido) }).toThrowError("Introduza um consumo médio válido.");

  });
});
