import { AuthService } from './auth.service';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import IViagemDTO from '../dto/IViagemDTO';
import { INoService } from './IServices/INoService';
import { IViagemService } from './IServices/IViagemService';
import INoDTO from '../dto/INoDTO';
import IPercursoDTO from '../dto/IPercursoDTO';
import HTTPErrorHandler from '../utils/httpErrorHandler';

@Injectable({
  providedIn: 'root'
})
export class ViagemService implements IViagemService {

  private httpErrorHandler: HTTPErrorHandler;

  constructor(private http: HttpClient, private authService: AuthService, httpErrorHandler: HTTPErrorHandler) {
    this.httpErrorHandler = httpErrorHandler;
  }

  baseURL: String = environment.mdvURL;

  public validarViagem(viagemDTO: IViagemDTO) {
    if (!viagemDTO.IdPercurso) throw new Error("Introduza um ID de percurso válido.");

    if (!viagemDTO.IdLinha) throw new Error("Introduza um ID de linha válido.");

    if (viagemDTO.HorarioSaida === null) throw new Error("Introduza o horário de saída.");
  }

  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

  public criarViagem(viagemDTO: IViagemDTO): Observable<IViagemDTO> {
    return this.http.post<IViagemDTO>(`${this.baseURL}/viagens`, viagemDTO, this.getHeaders());
  }

  public listaViagens(): Promise<IViagemDTO[]> {
    return this.http.get<IViagemDTO[]>(`${this.baseURL}/viagens`, this.getHeaders()).toPromise();
  }

  public dateTimeEmSegundos(time: string) {
    const horasString: string[] = time.split(":");
    const horas: number = +horasString[0]; //parse de string para number
    const minutos: number = +horasString[1];

    return (horas * 3600) + minutos * 60; //transformação em segundos
  }

  public async criarVariasViagens(linhaIdSelecionado: string, frequenciaEmSec: number, numeroViagens: number, horarioEmSec: number, percursoIdaDto: IPercursoDTO, percursoVoltaDto: IPercursoDTO, listaNosIda: INoDTO[], listaNosVolta: INoDTO[]): Promise<void> {
    var impar: boolean;
    var index;

    if (!(numeroViagens % 2 == 0)) impar = true; --numeroViagens;

    for (index = 0; index < numeroViagens / 2; index++) {

      const viagemIda = {
        IdPercurso: percursoIdaDto.id,
        IdLinha: linhaIdSelecionado,
        IdNoSaida: listaNosIda[0].id,
        HorarioSaida: horarioEmSec
      } as IViagemDTO;

      const viagemVolta = {
        IdPercurso: percursoVoltaDto.id,
        IdLinha: linhaIdSelecionado,
        IdNoSaida: listaNosVolta[0].id,
        HorarioSaida: horarioEmSec + percursoIdaDto.tempoTotal
      } as IViagemDTO;

      try {
        this.validarViagem(viagemIda);
        this.validarViagem(viagemVolta);

        await this.criarUmaViagem(viagemIda);
        await this.criarUmaViagem(viagemVolta);

        horarioEmSec = horarioEmSec + frequenciaEmSec;

      } catch (err) {
        alert(err);
      }
    }

    if (impar == true) {

      const viagemIda = {
        IdPercurso: percursoIdaDto.id,
        IdLinha: linhaIdSelecionado,
        IdNoSaida: listaNosIda[0].id,
        HorarioSaida: horarioEmSec
      } as IViagemDTO;

      try {
        this.validarViagem(viagemIda);

        await this.criarUmaViagem(viagemIda);

      } catch (err) {
        alert(err);
      }
    }
  }


  public async criarViagensParalelas(duracaoTotal: number, linhaIdSelecionado: string, frequenciaEmSec: number, numeroViagens: number, horarioEmSec: number, percursoIdaDto: IPercursoDTO, percursoVoltaDto: IPercursoDTO, listaNosIda: INoDTO[], listaNosVolta: INoDTO[], viagensParalelas: number): Promise<void> {
    var impar: boolean;
    var index;
    var aux;
    var safeHorarioEmSec = horarioEmSec;

    if (!(numeroViagens % 2 == 0)) impar = true; --numeroViagens;

    for (aux = 0; aux < viagensParalelas; aux++) {

      

      for (index = 0; index < numeroViagens / 2; index++) {

        const viagemIda = {
          IdPercurso: percursoIdaDto.id,
          IdLinha: linhaIdSelecionado,
          IdNoSaida: listaNosIda[0].id,
          HorarioSaida: horarioEmSec
        } as IViagemDTO;

        const viagemVolta = {
          IdPercurso: percursoVoltaDto.id,
          IdLinha: linhaIdSelecionado,
          IdNoSaida: listaNosVolta[0].id,
          HorarioSaida: horarioEmSec + percursoIdaDto.tempoTotal
        } as IViagemDTO;

        try {
          this.validarViagem(viagemIda);
          this.validarViagem(viagemVolta);

          await this.criarUmaViagem(viagemIda);
          await this.criarUmaViagem(viagemVolta);

          horarioEmSec = horarioEmSec + duracaoTotal;

        } catch (err) {
          alert(err);
        }
      }

      if (impar == true) {

        const viagemIda = {
          IdPercurso: percursoIdaDto.id,
          IdLinha: linhaIdSelecionado,
          IdNoSaida: listaNosIda[0].id,
          HorarioSaida: horarioEmSec
        } as IViagemDTO;

        try {
          this.validarViagem(viagemIda);

          await this.criarUmaViagem(viagemIda);

          horarioEmSec = horarioEmSec + frequenciaEmSec;

        } catch (err) {
          alert(err);
        }
      }

      horarioEmSec = safeHorarioEmSec + frequenciaEmSec;
      
    }

  }

  public async criarUmaViagem(viagem: IViagemDTO) {
    try {
      this.criarViagem(viagem).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.viagemId);
      },
        error => {
          if (error instanceof HttpErrorResponse) {
            console.log(error.message);
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }
        });
    } catch (error) {
      alert(error);
    }
  }

  public async procurarPercurso(id : string, lista : IPercursoDTO[]) : Promise<IPercursoDTO>{
    var i = 0;
    for(i = 0; i < lista.length; i++){
      if(lista[i].id === id){
        return lista[i];
      }
    }
    return null;
  }
}
