import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import ILinhaDTO from '../dto/ILinhaDTO';
import { AuthService } from './auth.service';

import { LinhaService } from './linha.service';

describe('LinhaService', () => {
  let linhaService: LinhaService;

  const mockLinha = {
    "id": "a",
    "codigo": "A",
    "nome": "Cristelo-Lordelo",
    "cor": "rosa",
    "noInicial": "1",
    "noFinal": "2",
    "viaturasPermitidas": ["5"],
    "viaturasProibidas": ["9"],
    "tripulantesPermitidos": ["7"],
    "tripulantesProibidos": ["6"]
  } as ILinhaDTO;

  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um tipo de viatura
  httpClientSpy.post.and.returnValue(of(mockLinha));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LinhaService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  beforeEach(inject([LinhaService], (service: LinhaService) => {
    linhaService = service;
  }));

  it('should be defined', () => {
    expect(linhaService).toBeTruthy();
  });

  it('should post Linha using http request', () => {
    const testResult = linhaService.criarLinha(mockLinha);
    expect(httpClientSpy.post).toHaveBeenCalled();
  });

  it('should validate the input of linha', () => {
    const mockLinhaInvalido = {
      "id": "a",
      "codigo": "",
      "nome": "Cristelo-Lordelo",
      "cor": "rosa",
      "noInicial": "1",
      "noFinal": "2",
      "viaturasPermitidas": ["5"],
      "viaturasProibidas": ["9"],
      "tripulantesPermitidos": ["7"],
      "tripulantesProibidos": ["6"]
    } as ILinhaDTO;

    expect(function () { linhaService.validarLinha(mockLinhaInvalido) }).toThrowError("Introduza um código válido.");

    mockLinhaInvalido.codigo = "A";
    mockLinhaInvalido.nome = "";

    expect(function () { linhaService.validarLinha(mockLinhaInvalido) }).toThrowError("Introduza um nome válido.");

    mockLinhaInvalido.nome = "Cristelo-Lordelo";
    mockLinhaInvalido.cor = "";

    expect(function () { linhaService.validarLinha(mockLinhaInvalido) }).toThrowError("Introduza uma cor válida.");

    mockLinhaInvalido.cor = "rosa";
    mockLinhaInvalido.noInicial = "";

    expect(function () { linhaService.validarLinha(mockLinhaInvalido) }).toThrowError("Introduza um ID de nó inicial válido.");

    mockLinhaInvalido.noInicial = "1";
    mockLinhaInvalido.noFinal = "";

    expect(function () { linhaService.validarLinha(mockLinhaInvalido) }).toThrowError("Introduza um ID de nó final válido.");

  });

  it('should add an element to an array', () => {
    var element = "4";
    var array: string[] = [];
    var result: string[] = [];
    result.push(element);

    expect(linhaService.addElementToArray(element, array)).toEqual(result);

    element = "";
    expect(function () { linhaService.addElementToArray(element, array) }).toThrowError("Introduza um ID válido.");

    element = "4";
    expect(function () { linhaService.addElementToArray(element, result) }).toThrowError("Elemento: " + element + " já existe na lista.");

  });

  it('should delete an element of an array', () => {
    var element = "4";
    var result: string[] = [];
    var array: string[] = [];
    array.push(element);

    expect(linhaService.deleteElementOfArray(element, array)).toEqual([]);

    element = "";
    expect(function () { linhaService.deleteElementOfArray(element, array) }).toThrowError("Introduza um ID válido.");

    element = "4";
    expect(function () { linhaService.deleteElementOfArray(element, result) }).toThrowError("Elemento: " + element + " não existe na lista.");

  });

});
