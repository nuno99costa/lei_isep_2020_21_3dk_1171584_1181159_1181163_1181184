import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IBlocoTrabalhoDTO } from '../dto/IBlocoTrabalhoDTO';
import ICreatingBlocosTrabalhoDTO from '../dto/ICreatingBlocosTrabalhoDTO';
import { AuthService } from './auth.service';
import { IBlocoTrabalhoService } from './IServices/IBlocoTrabalhoService';

@Injectable({
  providedIn: 'root'
})
export class BlocoTrabalhoService implements IBlocoTrabalhoService {

  baseURL: String = environment.mdvURL;

  constructor(private http: HttpClient, private authService: AuthService) { }

  validarBlocoTrabalho(blocoTrabalhoDTO: ICreatingBlocosTrabalhoDTO): void {
    if (!blocoTrabalhoDTO.DuracaoMaxima) throw new Error("Introduza uma duração máxima.");

    if (!blocoTrabalhoDTO.ServicoViaturaId) throw new Error("Introduza um ID de serviço de viatura.");

    if (!blocoTrabalhoDTO.numeroMaximoBlocos) throw new Error("Introduza um número máximo de blocos.");
  }

  criarBlocosTrabalho(blocoTrabalhoDTO: ICreatingBlocosTrabalhoDTO): Observable<ICreatingBlocosTrabalhoDTO> {
    return this.http.post<ICreatingBlocosTrabalhoDTO>(`${this.baseURL}/BlocosTrabalho/Multiple`, blocoTrabalhoDTO, this.getHeaders());
  }

  getBlocosServicoViatura(idServicoViatura: string): Promise<IBlocoTrabalhoDTO[]> {
    return this.http.get<IBlocoTrabalhoDTO[]>(`${this.baseURL}/BlocosTrabalho/servicoViatura/${idServicoViatura}`, this.getHeaders()).toPromise();
  }

  public dateTimeEmSegundos(duracao: string) {
    const horasString: string[] = duracao.split(":");
    const horas: number = +horasString[0]; //parse de string para number
    const minutos: number = +horasString[1];

    return (horas * 3600) + minutos * 60; //transformação em segundos
  }

  getHeaders(): { headers: HttpHeaders; } {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }
}
