import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';

import { BlocoTrabalhoService } from './bloco-trabalho.service';

describe('BlocoTrabalhoService', () => {
  let service: BlocoTrabalhoService;

  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BlocoTrabalhoService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([BlocoTrabalhoService], (serviceParam: BlocoTrabalhoService) => {
    service = serviceParam;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
