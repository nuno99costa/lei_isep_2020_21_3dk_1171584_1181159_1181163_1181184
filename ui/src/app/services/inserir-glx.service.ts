import { AuthService } from './auth.service';
import { IInserirGlxService } from './IServices/IInserirGlxService';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InserirGLXService implements IInserirGlxService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  //URL BASE DA ROUTE do Nó.
  baseURLMDR: String = environment.mdrURL;
  baseURLMDV: String = environment.mdvURL;

  validarInput(file: File | null | undefined) {
    if (!file) {
      throw new Error('Ficheiro não inserido');
    }
    if (file && !file.name.endsWith('.glx')) {
      throw new Error('O ficheiro tem de ser do tipo .glx');
    }
  }

  getHeaders() {
    let headers = new HttpHeaders({
      'auth-token': this.authService.userInfo.token,
      'Content-Type': 'application/xml'
    });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

  lerFicheiro = function (file: File) {
    const temporaryFileReader = new FileReader();

    return new Promise((resolve) => {

      temporaryFileReader.onload = () => {
        resolve(temporaryFileReader.result);
      };

      temporaryFileReader.readAsText(file);
    });

  }

  async inserirGLXMdr(file: File): Promise<Observable<string>> {
    const content = await this.lerFicheiro(file);
    console.log(content);
    
    return this.http.post<string>(`${this.baseURLMDR}/inserirGLX`, content, this.getHeaders());
  }

  async inserirGLXMdv(file: File): Promise<Observable<string>> {
    const content = await this.lerFicheiro(file);
    let headers = new HttpHeaders({
      'auth-token': this.authService.userInfo.token,
      'Content-Type': 'text/plain'
    });


    return this.http.post(`${this.baseURLMDV}/Filesystem`, content, {headers, responseType:'text'});
  }

}
