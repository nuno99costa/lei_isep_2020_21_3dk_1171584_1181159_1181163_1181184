import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import IRegistoDTO from 'src/app/dto/IRegistoDTO';

export interface IUserService {
    validarDataNascimento(data: Date): void;
    getDadosUser(): Promise<IRegistoDTO>;
    getHeaders(): { headers: HttpHeaders };
    update(utilizador: IRegistoDTO): Observable<IRegistoDTO>;
    delete(): Observable<IRegistoDTO>;
    validarUpdate(utilizador: IRegistoDTO): void;
}