import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';

export interface ILinhaService {
    validarLinha(linhaDTO : ILinhaDTO) : void;
    criarLinha(linhaDTO : ILinhaDTO) : Observable<ILinhaDTO>;
    addElementToArray(id : string, array : string[]) : string[];
    deleteElementOfArray(id : string, array : string[]) : string[];
    listaLinhas() : Promise<ILinhaDTO[]>;
    getHeaders() : { headers: HttpHeaders };
}