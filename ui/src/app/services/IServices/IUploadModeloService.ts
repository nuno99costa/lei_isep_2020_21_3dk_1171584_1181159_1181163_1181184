import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';

export interface IUploadModeloService {
    validarInput(file : File | null | undefined) : void;
}