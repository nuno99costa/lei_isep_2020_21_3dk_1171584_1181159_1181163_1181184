import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

export interface IInserirGlxService {
    validarInput(file : File | null | undefined) : void;
    lerFicheiro(file : File) : void;
    inserirGLXMdr(file : File) : Promise<Observable<string>>;
    inserirGLXMdv(file : File) : Promise<Observable<string>>;
    getHeaders() : { headers: HttpHeaders };
}