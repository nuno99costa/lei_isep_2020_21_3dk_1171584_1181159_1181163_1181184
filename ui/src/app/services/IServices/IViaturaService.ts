import { HttpHeaders } from '@angular/common/http';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { Observable } from 'rxjs';
import IViaturaDTO from 'src/app/dto/IViaturaDTO';

export interface IViaturaService{
    validarInput(viatura : IViaturaDTO) : void;
    criarViatura(viaturaDTO: IViaturaDTO) : Observable<IViaturaDTO>;
    getHeaders() : { headers: HttpHeaders };
    listaViaturas(): Promise<IViaturaDTO[]>;
}