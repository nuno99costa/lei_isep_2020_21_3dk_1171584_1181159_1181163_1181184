import { HttpHeaders } from '@angular/common/http';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { Observable } from 'rxjs';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';

export interface ITipoViaturaService{
    validarTipoViatura(tipoViatura : ITipoViaturaDTO) : void;
    criarTipoViatura(tipoViaturaDTO: ITipoViaturaDTO) : Observable<ITipoViaturaDTO>;
    contarCasasDecimais(num : number) : number;
    listaTiposViatura() : Promise<ITipoViaturaDTO[]>;
    getHeaders() : { headers: HttpHeaders };
}