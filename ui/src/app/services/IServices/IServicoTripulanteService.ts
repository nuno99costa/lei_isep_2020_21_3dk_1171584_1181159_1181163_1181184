import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import IServicoTripulanteDTO from 'src/app/dto/IServicoTripulanteDTO';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';

export interface IServicoTripulanteService {
    validarServicoTripulante(servicoTripulanteDTO: IServicoTripulanteDTO): void;
    criarServicoTripulante(servicoTripulanteDTO: IServicoTripulanteDTO): Observable<IServicoTripulanteDTO>;
    addElementToArray(element: string, array: string[]): string[];
    deleteElementOfArray(element: string, array: string[]): string[];
    dateTimeEmSegundos(time: string): number;
    listaServicosTripulantes(): Promise<IServicoTripulanteDTO[]>;
    getHeaders(): { headers: HttpHeaders };
}