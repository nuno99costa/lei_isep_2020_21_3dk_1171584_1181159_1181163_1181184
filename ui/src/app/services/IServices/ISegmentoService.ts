import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import ISegmentoDTO from 'src/app/dto/ISegmentoDTO';

export interface ISegmentoService {
    validarInput(segmento : ISegmentoDTO) : void;
    criarSegmento(segmento : ISegmentoDTO) : Observable<ISegmentoDTO>;
    listaSegmentos() : Promise<ISegmentoDTO[]>;
    getHeaders() : { headers: HttpHeaders };
}