import { HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import INoDTO from "src/app/dto/INoDTO";
import IPercursoDTO from "src/app/dto/IPercursoDTO";
import IViagemDTO from "src/app/dto/IViagemDTO";

export interface IViagemService {
    validarViagem(viagemDTO: IViagemDTO): void;
    criarViagem(viagemDTO: IViagemDTO): Observable<IViagemDTO>;
    listaViagens(): Promise<IViagemDTO[]>;
    dateTimeEmSegundos(time: string): number;
    getHeaders(): { headers: HttpHeaders };
    criarVariasViagens(linhaIdSelecionado: string, frequenciaEmSec: number, numeroViagens: number, horarioEmSec: number, percursoIdaDto: IPercursoDTO, percursoVoltaDto: IPercursoDTO, listaNosIda: INoDTO[], listaNosVolta: INoDTO[]): void;
    criarViagensParalelas(duracaoTotal: number, linhaIdSelecionado: string, frequenciaEmSec: number, numeroViagens: number, horarioEmSec: number, percursoIdaDto: IPercursoDTO, percursoVoltaDto: IPercursoDTO, listaNosIda: INoDTO[], listaNosVolta: INoDTO[], viagensParalelas: number): void;
    criarUmaViagem(viagem: IViagemDTO): void;
    procurarPercurso(id : string, lista : IPercursoDTO[]) : Promise<IPercursoDTO>;
}