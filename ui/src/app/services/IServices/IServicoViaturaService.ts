import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';

export interface IServicoViaturaService {
    validarServicoViatura(servicoViaturaDTO: IServicoViaturaDTO): void;
    criarServicoViatura(servicoViaturaDTO: IServicoViaturaDTO): Observable<IServicoViaturaDTO>;
    listaServicosViatura(): Promise<IServicoViaturaDTO[]>;
    addElementToArray(element: string, array: string[]): string[];
    deleteElementOfArray(element: string, array: string[]): string[];
    getHeaders(): { headers: HttpHeaders };
}