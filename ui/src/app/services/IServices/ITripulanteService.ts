import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import ITripulantesBlocosTrabalhoDTO from 'src/app/dto/ITripulantesBlocosTrabalhoDTO';
import ITripulanteDTO from '../../dto/ITripulanteDTO';

export interface ITripulanteService{
    validarInput(tripulante: ITripulanteDTO, checkBoxRGPD : boolean): void;
    criarTripulante(tripulanteDTO: ITripulanteDTO): Observable<ITripulanteDTO>;
    listaTripulantes(): Promise<ITripulanteDTO[]>;
    getHeaders(): { headers: HttpHeaders };
    validateNIF(value: string): boolean;
    validarInputTripulantesBlocosTrabalho(dados: ITripulantesBlocosTrabalhoDTO): void;
    obterListaTripulantesBlocosTrabalho(dados: ITripulantesBlocosTrabalhoDTO): Observable<string[]>;
}
