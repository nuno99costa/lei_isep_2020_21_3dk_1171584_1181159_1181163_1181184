import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBlocoTrabalhoDTO } from 'src/app/dto/IBlocoTrabalhoDTO';
import ICreatingBlocosTrabalhoDTO from 'src/app/dto/ICreatingBlocosTrabalhoDTO';

export interface IBlocoTrabalhoService {
    validarBlocoTrabalho(blocoTrabalhoDTO: ICreatingBlocosTrabalhoDTO): void;
    criarBlocosTrabalho(blocoTrabalhoDTO: ICreatingBlocosTrabalhoDTO): Observable<ICreatingBlocosTrabalhoDTO>;
    getBlocosServicoViatura(idServicoViatura: string): Promise<IBlocoTrabalhoDTO[]>;
    getHeaders(): { headers: HttpHeaders };
    dateTimeEmSegundos(duracao: string): number;
}