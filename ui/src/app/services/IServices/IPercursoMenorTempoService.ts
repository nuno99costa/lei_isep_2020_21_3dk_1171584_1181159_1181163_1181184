import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import INosPercursoDTO from 'src/app/dto/INosPercursoDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';

export interface IPercursoMenorTempoService{
    validarInput(nosPercurso : INosPercursoDTO) : void;
    obterPercursoMenorTempo(nosPercurso : INosPercursoDTO) : Observable<IPercursoDTO>;
    getHeaders() : { headers: HttpHeaders };
}