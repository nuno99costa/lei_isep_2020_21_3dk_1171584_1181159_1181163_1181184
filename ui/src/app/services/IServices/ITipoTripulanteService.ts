import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';

export interface ITipoTripulanteService {
    validarTipoTripulante(tipoTripulante : ITipoTripulanteDTO) : void;
    criarTipoTripulante(tipoTripulanteDTO : ITipoTripulanteDTO) : Observable<ITipoTripulanteDTO>;
    listaTiposTripulante() : Promise<ITipoTripulanteDTO[]>;
    getHeaders() : { headers: HttpHeaders };
}