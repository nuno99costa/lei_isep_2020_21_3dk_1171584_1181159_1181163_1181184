import { HttpHeaders } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import IModelo3DDTO from 'src/app/dto/IModelo3DDTO';
import INoDTO from 'src/app/dto/INoDTO';

export interface INoService{
    criarUpdateModeloDeNo(noId : string, nomeModelo: { nomeModelo: string; }) : Observable<INoDTO>;
    validarInput(no : INoDTO) : void;
    criarNo(no : INoDTO) : Observable<INoDTO>;
    listaNos(): Promise<INoDTO[]>;
    getHeaders() : { headers: HttpHeaders };
    listaModelos(): Promise<IModelo3DDTO[]>;
    procurarNoEmLista(lista : INoDTO[], idNo : string) : Promise<INoDTO>;
    procurarModelo3DEmLista(lista : IModelo3DDTO[], nomeModelo : string) : Promise<IModelo3DDTO>;
}