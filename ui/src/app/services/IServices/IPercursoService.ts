import { HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import IDadosLinhaMapaDTO from 'src/app/dto/IDadosLinhaDTO';
import INoDTO from 'src/app/dto/INoDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';

export interface IPercursoService {
    addElementToArray(id: string, array: string[]): string[];
    deleteElementOfArray(id: string, array: string[]): string[];
    validarInput(percurso: IPercursoDTO): void;
    validarLinha(idLinha: string): void;
    criarPercurso(percurso: IPercursoDTO): Observable<IPercursoDTO>;
    carregarDadosMapa(): Observable<IDadosLinhaMapaDTO[]>;
    listarPercursoLinha(idLinha: string): Promise<IPercursoDTO[]>;
    getNosPercursoId(percursoId: string): Promise<INoDTO[]>;
    getHeaders() : { headers: HttpHeaders };
}
