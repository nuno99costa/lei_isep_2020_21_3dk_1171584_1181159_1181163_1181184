import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;

  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);

  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([AuthService], (serviceParam: AuthService) => {
    service = serviceParam;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
