import { AuthService } from './auth.service';
import { IPercursoService } from './IServices/IPercursoService';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import IPercursoDTO from "src/app/dto/IPercursoDTO";
import { Observable } from 'rxjs';
import IDadosLinhaMapaDTO from '../dto/IDadosLinhaDTO';
import { environment } from 'src/environments/environment';
import INoDTO from '../dto/INoDTO';

@Injectable({
  providedIn: 'root'
})
export class PercursoService implements IPercursoService {

  constructor(private http: HttpClient, private authService : AuthService) { }

  //URL BASE DA ROUTE do Segmento.
  baseURL : String = environment.mdrURL;

  addElementToArray(id: string, array: string[]): string[] {
    if (array.includes(id)) {
      return array;
    }

    if (id == "") {
      return array;
    }

    array.push(id);
    return array;
  }

  deleteElementOfArray(id: string, array: string[]): string[] {
    try {
      if (!id) {
        throw new Error("Introduza um ID válido.");
      }

      if (!array.includes(id)) {
        throw new Error("Elemento: " + id + " não existe na lista.");
      }

      const index = array.indexOf(id, 0);
      if (index > -1) {
        array.splice(index, 1);
      }

      return array;

    } catch (error) {
      alert(error);
    }
    return array;
  }

  //Validar inputs do nó. É void, mas quando um input é invalido lança um erro. Esse erro vai ser catched no controller.
  validarInput(percurso: IPercursoDTO) {
    if (!percurso.nomePercurso) {//Se variável não tem valor
      throw new Error("Introduza um nome válido");
    }

    if (!percurso.linhaId) {
      throw new Error("Introduza uma linha de id válida");
    }

    if (!percurso.orientacao) {
      throw new Error("Selecione uma orientação");
    }

    if (percurso.listaSegmentos.length == 0) {
      throw new Error("Introduza segmentos");
    }

    if (!percurso.noInicialId) {
      throw new Error("Introduza um id de nó incial válido");
    }

    if (!percurso.noFinalId) {
      throw new Error("Introduza um id de nó final válido");
    }
  }

  validarLinha(idLinha: string) {
    if (!idLinha) {
      throw new Error("Introduza um id de uma linha existente");
    }
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  
  criarPercurso(percurso: IPercursoDTO): Observable<IPercursoDTO> {
    return this.http.post<IPercursoDTO>(`${this.baseURL}/percurso`, percurso, this.getHeaders());
  }

  carregarDadosMapa() {
    return this.http.get<IDadosLinhaMapaDTO[]>(`${this.baseURL}/percurso/dadosMapa`, this.getHeaders());
  }

  listarPercursoLinha(idLinha: string): Promise<IPercursoDTO[]> {
    return this.http.get<IPercursoDTO[]>(`${this.baseURL}/percurso/linha/` + idLinha, this.getHeaders()).toPromise();
  }

  getNosPercursoId(percursoId: string): Promise<INoDTO[]> {
    return this.http.get<INoDTO[]>(`${this.baseURL}/percurso/nos/` + percursoId, this.getHeaders()).toPromise();
  }
}

