import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import INoDTO from '../dto/INoDTO';
import { AuthService } from './auth.service';

import { NoService } from './no.service';

describe('NoService', () => {

  //--------------------------------------------------SETUP DOS TESTES--------------------------------------
  let noService: NoService;//Instância a usar nos testes

  const mockNo = {
    id: "a",
    nomeCompleto: "a",
    abreviatura: "a",
    isPontoRendicao: true,
    isEstacaoRecolha: false,
    latitude: 12.3,
    longitude: -12.5
  } as INoDTO;

  //Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  //Fazemos o httpClient.post retornar um DTO de um nó
  httpClientSpy.post.and.returnValue(of(mockNo));

  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([NoService], (service: NoService) => {
    noService = service;
  }));

  //----------------------------------------------Começar os testes-----------------------------------------------
  it('should be defined', () => {
    expect(noService).toBeTruthy();
  });

  it('should post No using http request', () => {
    const testResult = noService.criarNo(mockNo);
    expect(httpClientSpy.post).toHaveBeenCalled();
    //expect(testResult.toEqual(of(mockNo));  PERGUNTAR PORQUE NAO FUNCIONA
  });

  it('should validate the inputs', () => {
    //Validar se a função lança erros.
    //Atualizamos os dados do mock para lançar erros diferentes a cada teste
    let mockNoInvalido = {
      id: "a",
      nomeCompleto: "",
      abreviatura: "a",
      isPontoRendicao: true,
      isEstacaoRecolha: false,
      latitude: 12.3,
      longitude: -12.5
    } as INoDTO;

    expect(function() {noService.validarInput(mockNoInvalido)}).toThrowError("Introduza um nome completo");

    mockNoInvalido.nomeCompleto = "a";
    mockNoInvalido.abreviatura = "";

    expect(function() {noService.validarInput(mockNoInvalido)}).toThrowError("Introduza uma abreviatura");

    mockNoInvalido.abreviatura = "a";
    mockNoInvalido.latitude = NaN;

    expect(function() {noService.validarInput(mockNoInvalido)}).toThrowError("Introduza uma latitude válida");

    mockNoInvalido.latitude = 12.3;
    mockNoInvalido.longitude = NaN;

    expect(function() {noService.validarInput(mockNoInvalido)}).toThrowError("Introduza uma longitude válida");

  });


});
