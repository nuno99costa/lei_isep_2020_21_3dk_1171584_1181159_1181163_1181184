import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import IServicoTripulanteDTO from '../dto/IServicoTripulanteDTO';
import HTTPErrorHandler from '../utils/httpErrorHandler';
import { AuthService } from './auth.service';
import { IServicoTripulanteService } from './IServices/IServicoTripulanteService';

@Injectable({
  providedIn: 'root'
})
export class ServicoTripulanteService implements IServicoTripulanteService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  baseURL: String = environment.mdvURL;

  validarServicoTripulante(servicoTripulanteDTO: IServicoTripulanteDTO): void {
    if (!servicoTripulanteDTO.id || servicoTripulanteDTO.id.length != 10) throw new Error("Introduza ID válido.");

    if (!servicoTripulanteDTO.idsBlocosTrabalho || servicoTripulanteDTO.idsBlocosTrabalho.length == 0) throw new Error("Introduza os blocos de trabalho.");
  }

  criarServicoTripulante(servicoTripulanteDTO: IServicoTripulanteDTO): Observable<IServicoTripulanteDTO> {
    return this.http.post<IServicoTripulanteDTO>(`${this.baseURL}/ServicosTripulante`, servicoTripulanteDTO, this.getHeaders());
  }

  addElementToArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (array.includes(element)) throw new Error("Elemento: " + element + " já existe na lista.");

    array.push(element);
    alert("Element: " + element + " added.");

    return array;
  }

  deleteElementOfArray(element: string, array: string[]): string[] {
    if (!element) throw new Error("Introduza um ID válido.");
    if (!array.includes(element)) throw new Error("Elemento: " + element + " não existe na lista.");

    const index = array.indexOf(element, 0);
    if (index > -1) {
      array.splice(index, 1);
      alert("Element: " + element + " deleted.");
    }

    return array;
  }

  dateTimeEmSegundos(time: string): number {
    const horasString: string[] = time.split(":");
    const horas: number = +horasString[0]; //parse de string para number
    const minutos: number = +horasString[1];

    return (horas * 3600) + minutos * 60; //transformação em segundos
  }

  listaServicosTripulantes(){
    return this.http.get<IServicoTripulanteDTO[]>(`${this.baseURL}/ServicosTripulante`, this.getHeaders()).toPromise();
  }

  getHeaders(): { headers: HttpHeaders; } {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }
}
