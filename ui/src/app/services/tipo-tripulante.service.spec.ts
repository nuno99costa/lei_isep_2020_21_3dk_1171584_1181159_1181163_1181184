import { HttpClient } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';
import { AuthService } from './auth.service';

import { TipoTripulanteService } from './tipo-tripulante.service';

describe('TipoTripulanteService', () => {
  let tipoTripulanteService: TipoTripulanteService;

  const mockTipoTripulante = {
    id: '1',
    codigo: 'tp',
    descricao: 'tp teste'
  } as ITipoTripulanteDTO;

  // Os métodos do HttpClient têm de ser mocked. Por isso, usamo-lo como spy.
  const httpClientSpy: jasmine.SpyObj<HttpClient> = jasmine.createSpyObj('httpClient', ['post']);
  const authSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['login']);
  authSpy.userInfo = {email: 'ola@gmail.com', isAdministrador: true, isCliente: false, isGestor: false, token: 'ola', tokenExp: 30};

  // Fazemos o httpClient.post retornar um DTO de um tipo de viatura
  httpClientSpy.post.and.returnValue(of(mockTipoTripulante));

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TipoTripulanteService,
        {
          provide: HttpClient,
          useValue: httpClientSpy
        }, {provide: AuthService, useValue: authSpy}]
    });
  });

  beforeEach(inject([TipoTripulanteService], (service: TipoTripulanteService) => {
    tipoTripulanteService = service;
  }));

  it('should be defined', () => {
    expect(tipoTripulanteService).toBeTruthy();
  });

  it('should post Tipo Tripulante using http request', () => {
    // Chamamos o método que cria com o mock do tipo de tripulante
    const testResult = tipoTripulanteService.criarTipoTripulante(mockTipoTripulante);
    // É esperado que o método post do httpclient seja chamado
    expect(httpClientSpy.post).toHaveBeenCalled();
    // expect(testResult.toEqual(of(mockNo));
  });

  // Este valida cada if do método de validação dos dados do tipo de tripulante
  it('should validate the input data of tipo tripulante', () => {
    // Criamos um mock onde vão existir valores errados que não são aceitáveis.
    // Vamos mudar ao longo do método os valores para testar.
    const mockTipoTripulanteInvalido = {
      id: '1',
      codigo: '',
      descricao: 'tp teste'
    } as ITipoTripulanteDTO;

    expect(function() { tipoTripulanteService.validarTipoTripulante(mockTipoTripulanteInvalido); }).toThrowError('Introduza um código válido.');

    mockTipoTripulanteInvalido.codigo = 'aaaaaaaaaaaaaaaaaaaa';
    mockTipoTripulanteInvalido.descricao = '';

    expect(function() { tipoTripulanteService.validarTipoTripulante(mockTipoTripulanteInvalido); }).toThrowError('Introduza uma descrição.');

  });
});
