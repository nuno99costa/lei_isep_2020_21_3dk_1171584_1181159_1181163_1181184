import { AuthService } from './auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import IViaturaDTO from '../dto/IViaturaDTO';
import { IViaturaService } from './IServices/IViaturaService';

@Injectable({
  providedIn: 'root'
})
export class ViaturaService implements IViaturaService {

  //URL BASE DA ROUTE da Viatura.
  baseURL: String = environment.mdvURL;
  constructor(private http: HttpClient, private authService: AuthService) { }

  validarInput(viatura: IViaturaDTO): void {

    if (!viatura.matricula) throw new Error("Introduza uma matrícula.");

    if (!viatura.matricula.match(/^(([A-Z]{2}-\d{2}-(\d{2}|[A-Z]{2}))|(\d{2}-(\d{2}-[A-Z]{2}|[A-Z]{2}-\d{2})))$/)) throw new Error("Introduza uma matrícula válida.");

    if (!viatura.vin) throw new Error("Introduza um vin");

    if (!viatura.TipoViatura) throw new Error("Introduza um tipo de viatura");

    if (!viatura.data) throw new Error("Introduza uma data");

  }

  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }

  criarViatura(viatura: IViaturaDTO): Observable<IViaturaDTO> {
    return this.http.post<IViaturaDTO>(`${this.baseURL}/viaturas`, viatura, this.getHeaders());
  }

  listaViaturas(): Promise<IViaturaDTO[]> {
    return this.http.get<IViaturaDTO[]>(`${this.baseURL}/viaturas`, this.getHeaders()).toPromise();
  }
}
