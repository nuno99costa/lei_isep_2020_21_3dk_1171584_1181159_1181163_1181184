import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUploadModeloService } from './IServices/IUploadModeloService';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadModeloService implements IUploadModeloService{

  constructor(private authService : AuthService, private http: HttpClient) { }

  baseURL: String = environment.mdrURL;
  
  validarInput(file: File | null | undefined) {
    if (!file) {
      throw new Error('Ficheiro não inserido');
    }
    if (file && !file.name.endsWith('.zip')) {
      throw new Error('O ficheiro tem de ser do tipo .zip');
    }
  }
  
  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  
  async uploadModelo(file: File) : Promise<Observable<string>>{
    const formData = new FormData();
    formData.append(file.name.replace(/\.[^/.]+$/, ""), file, file.name)
    console.log(formData);
    return this.http.post<string>(`${this.baseURL}/modelo3D`,formData, this.getHeaders());
  }
}
