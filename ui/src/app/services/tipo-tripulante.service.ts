import { AuthService } from './auth.service';
import { ITipoTripulanteService } from './IServices/ITipoTripulanteService';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import ITipoTripulanteDTO from '../dto/ITipoTripulanteDTO';


@Injectable({
  providedIn: 'root'
})
export class TipoTripulanteService implements ITipoTripulanteService{

  // httpClient é injetado
  constructor(private http: HttpClient, private authService : AuthService) { }
  

  baseURL: string = environment.mdrURL;

  // Validar os campos da UI (Two way binding: se mudar na UI é automaticamente refletida aqui)
  public validarTipoTripulante(tipoTripulante: ITipoTripulanteDTO) {
    if (tipoTripulante.codigo.length != 20) { throw new Error('Introduza um código válido.'); }

    if (!tipoTripulante.descricao) { throw new Error('Introduza uma descrição.'); }
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  
  // Cria o novo tipo de viatura com os dados passados por parâmetro através de um DTO
  public criarTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO) {
    return this.http.post<ITipoTripulanteDTO>(`${this.baseURL}/tipoTripulante`, tipoTripulanteDTO, this.getHeaders());
  }

  listaTiposTripulante(): Promise<ITipoTripulanteDTO[]> {
    return this.http.get<ITipoTripulanteDTO[]>(`${this.baseURL}/tipoTripulante`, this.getHeaders()).toPromise();
  }
}
