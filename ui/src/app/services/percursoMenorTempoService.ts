import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import INoDTO from '../dto/INoDTO';
import { environment } from 'src/environments/environment';
import { IPercursoMenorTempoService } from './IServices/IPercursoMenorTempoService';
import INosPercursoDTO from '../dto/INosPercursoDTO';
import IPercursoDTO from '../dto/IPercursoDTO';

@Injectable({
  providedIn: 'root'
})
export class PercursoMenorTempoService implements IPercursoMenorTempoService{

  constructor(private http: HttpClient, private authService : AuthService){ }

  //URL BASE DA ROUTE do Nó.
  baseURL: String = environment.planeamentoURL;

  //Validar inputs do nó. É void, mas quando um input é invalido lança um erro. Esse erro vai ser catched no controller.
  validarInput(nosPercurso : INosPercursoDTO){
    if(!nosPercurso.abvNoInicial){//Se variável não tem valor
      throw new Error("Introduza uma abreviatura de nó inicial");
    }

    if(!nosPercurso.abvNoFinal){//Se variável não tem valor
        throw new Error("Introduza uma abreviatura de nó final");
      }

    if(!nosPercurso.abvNoInicial || isNaN(nosPercurso.tempoSegundos)){ //isNaN retorna true se o valor não é um número
      throw new Error("Introduza uma hora válida");
    }

  }

  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  
  obterPercursoMenorTempo(nosPercurso : INosPercursoDTO): Observable<IPercursoDTO>{
    return this.http.get<IPercursoDTO>(`${this.baseURL}/planeamento/obterPercurso?noI=` + nosPercurso.abvNoInicial + `&noF=` + nosPercurso.abvNoFinal + `&t=` + nosPercurso.tempoSegundos, this.getHeaders());
  }

}