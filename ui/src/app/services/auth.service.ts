
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Subject } from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import ILoginDTO from '../dto/ILoginDTO';
import IRegistoDTO from '../dto/IRegistoDTO';
import { lchmod } from 'fs';
import { IUserDTO } from '../dto/IUserDTO';
import { JwtHelperService } from '@auth0/angular-jwt';



class Token { token: string };

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private authUrlMDR = environment.mdrURL;
  private authUrlMDV = environment.mdvURL;
  public userInfo: IUserDTO;
  authentication: Subject<IUserDTO> = new Subject<IUserDTO>();

  constructor(private http: HttpClient) {
    this.userInfo = localStorage.userInfo;
  }

  async login(utilizador: ILoginDTO): Promise<boolean> {
    const tokenPromise = this.http.post<{ token: string }>(`${this.authUrlMDR}/user/login`, {
      email: utilizador.email,
      password: utilizador.password
    }).toPromise();
    const tokenString = await tokenPromise;
    const token = { token: tokenString.token };
    try {
      if (token) {
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(token.token);
        this.userInfo = {
          token: tokenString.token,
          tokenExp: decodedToken.tokenExp,
          email: decodedToken.email,
          isAdministrador: decodedToken.isAdministrador,
          isCliente: decodedToken.isCliente,
          isGestor: decodedToken.isGestor
        }
        localStorage.userInfo = this.userInfo.token;
        this.authentication.next(this.userInfo);
        return true;
      }
      this.authentication.next(this.userInfo);
      return false;
    } catch (erro) {
      return false;
    }

  }

  logout() {
    this.userInfo = null;
    localStorage.removeItem('userInfo');
    this.authentication.next(this.userInfo);
  }



  registo(utilizador: IRegistoDTO) {
    return this.http.post(`${this.authUrlMDR}/user/register`, utilizador);
  }

  validarDataNascimento(data: Date) {
    let currentDate = new Date()
    let idade = Math.abs(currentDate.getFullYear() - data.getFullYear());
    if (idade < 13) throw new Error("O cliente tem de ter pelo menos 13 anos para utilizar a aplicação.");
  }

  validarRegisto(utilizador: IRegistoDTO, checkBoxRGPD: boolean) {
    if (!utilizador.name) throw new Error("Introduza um nome.");

    if (utilizador.name.length < 6) throw new Error("Introduza um nome com mais de 6 dígitos.");

    if (!utilizador.email) throw new Error("Introduza um email.");

    if (!utilizador.email.match(/[^@ \t\r\n]+@[^@ \t\r\n]+\.[^@ \t\r\n]+/)) throw new Error("Introduza um email válido.");

    if (!utilizador.password) throw new Error("Introduza palavra-passe.");

    if (utilizador.password.length < 6) throw new Error("Introduza uma palavra passe com mais de 6 dígitos.");

    if (!checkBoxRGPD) throw new Error("Para efetuar o registo, é necessário o seu consentimento relativamente ao tratamento de dados.");
  }

}
