import { AuthService } from './auth.service';
import { ITipoViaturaService } from './IServices/ITipoViaturaService';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import ITipoViaturaDTO from '../dto/ITipoViaturaDTO';


@Injectable({
  providedIn: 'root'
})
export class TipoViaturaService implements ITipoViaturaService{

  //httpClient é injetado
  constructor(private http: HttpClient, private authService : AuthService) { }

  

  baseURL: String = environment.mdrURL;

  //Validar os campos da UI (Two way binding: se mudar na UI é automaticamente refletida aqui)
  public validarTipoViatura(tipoViatura: ITipoViaturaDTO) {
    if (tipoViatura.codigo.length != 20) throw new Error("Introduza um código válido.");

    if (!tipoViatura.descricao) throw new Error("Introduza uma descrição.");

    if (!tipoViatura.combustivel) throw new Error("Introduza um tipo de combustível.");

    if (!tipoViatura.autonomia || isNaN(tipoViatura.autonomia)) throw new Error("Introduza uma autonomia válida.");

    if (!tipoViatura.custoPorQuilometro || isNaN(tipoViatura.custoPorQuilometro)) throw new Error("Introduza o custo por quilómetro válido.");

    if (!tipoViatura.consumoMedio || isNaN(tipoViatura.consumoMedio) || this.contarCasasDecimais(tipoViatura.consumoMedio) != 3) throw new Error("Introduza um consumo médio válido.");

    if (!tipoViatura.velocidadeMedia || isNaN(tipoViatura.velocidadeMedia)) throw new Error("Introduza uma velocidade média válida.");
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  
  //Cria o novo tipo de viatura com os dados passados por parâmetro através de um DTO
  public criarTipoViatura(tipoViaturaDTO: ITipoViaturaDTO) {
    return this.http.post<ITipoViaturaDTO>(`${this.baseURL}/tipoViatura`, tipoViaturaDTO, this.getHeaders());
  }

  //Conta o número de casas decimais de um número
  public contarCasasDecimais(num: number) {
    if (Math.floor(num) !== num) return num.toString().split(".")[1].length || 0;

    return 0;
  }

  listaTiposViatura(): Promise<ITipoViaturaDTO[]> {
    return this.http.get<ITipoViaturaDTO[]>(`${this.baseURL}/tipoViatura`, this.getHeaders()).toPromise();
  }
}
