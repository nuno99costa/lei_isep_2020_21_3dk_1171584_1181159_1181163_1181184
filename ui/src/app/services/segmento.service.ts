import { AuthService } from './auth.service';
import { ISegmentoService } from './IServices/ISegmentoService';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import ISegmentoDTO from '../dto/ISegmentoDTO';
import INoDTO from '../dto/INoDTO';

@Injectable({
  providedIn: 'root'
})
export class SegmentoService implements ISegmentoService{

  constructor(private http: HttpClient, private authService : AuthService) { }

  baseURL: String = environment.mdrURL;

  //Validar inputs do segmento. É void, mas quando um input é invalido lança um erro. Esse erro vai ser catched no controller.

  validarInput(segmento: ISegmentoDTO){
    if(!segmento.nomeSegmento){//Se variável não tem valor
      throw new Error("Introduza um nome para o segmento");
    }

    if(!segmento.idNoInicial){
      throw new Error("Introduza um id de nó incial");
    }

    if(!segmento.idNoFinal){
      throw new Error("Introduza um id de nó final");
    }

    if(!segmento.distanciaNos || isNaN(segmento.distanciaNos)){
      throw new Error("Introduza uma distância válida");
    }

    if(!segmento.tempoNos || isNaN(segmento.tempoNos)){
      throw new Error("Introduza um tempo válido");
    }
  }

  getHeaders(){
    let headers = new HttpHeaders({'auth-token': this.authService.userInfo.token});

    let httpOptions = {
      headers : headers
    };
    
    return httpOptions;
  }
  //Recebe o dto do segmento
  //Faz pedido post ao servidor e manda o DTO do segmento no body do pedido
  criarSegmento(segmento: ISegmentoDTO): Observable<ISegmentoDTO>{
    return this.http.post<ISegmentoDTO>(`${this.baseURL}/segmento`, segmento, this.getHeaders());
  }

  listaSegmentos(): Promise<ISegmentoDTO[]>{
    return this.http.get<ISegmentoDTO[]>(`${this.baseURL}/segmento`, this.getHeaders()).toPromise();
  }

  
}
