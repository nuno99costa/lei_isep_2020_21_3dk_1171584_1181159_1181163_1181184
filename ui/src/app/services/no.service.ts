import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import INoDTO from '../dto/INoDTO';
import { environment } from 'src/environments/environment';
import { INoService } from './IServices/INoService';
import IModelo3DDTO from '../dto/IModelo3DDTO';

@Injectable({
  providedIn: 'root'
})
export class NoService implements INoService {

  constructor(private http: HttpClient, private authService: AuthService) { }



  //URL BASE DA ROUTE do Nó.
  baseURL: String = environment.mdrURL;

  //Validar inputs do nó. É void, mas quando um input é invalido lança um erro. Esse erro vai ser catched no controller.
  validarInput(no: INoDTO) {
    if (!no.nomeCompleto) {//Se variável não tem valor
      throw new Error("Introduza um nome completo");
    }

    if (!no.abreviatura) {
      throw new Error("Introduza uma abreviatura");
    }

    if (!no.latitude || isNaN(no.latitude)) { //isNaN retorna true se o valor não é um número
      throw new Error("Introduza uma latitude válida");
    }

    if (!no.longitude || isNaN(no.longitude)) { //isNaN retorna true se o valor não é um número
      throw new Error("Introduza uma longitude válida");
    }

  }

  getHeaders() {
    let headers = new HttpHeaders({ 'auth-token': this.authService.userInfo.token });

    let httpOptions = {
      headers: headers
    };

    return httpOptions;
  }
  //Recebe o dto do nó
  //Faz pedido post ao servidor e manda o DTO do nó no body do pedido. 
  criarNo(no: INoDTO): Observable<INoDTO> {
    return this.http.post<INoDTO>(`${this.baseURL}/no`, no, this.getHeaders());
  }

  async listaNos(): Promise<INoDTO[]> {
    return this.http.get<INoDTO[]>(`${this.baseURL}/no/all/ordenados/nome`, this.getHeaders()).toPromise();
  }

  async listaModelos(): Promise<IModelo3DDTO[]> {
    return this.http.get<IModelo3DDTO[]>(`${this.baseURL}/modelo3D`, this.getHeaders()).toPromise();
  }

  criarUpdateModeloDeNo(noId: string, nomeModelo: { nomeModelo: string }): Observable<INoDTO> {
    return this.http.put<INoDTO>(`${this.baseURL}/no/modelo3d/${noId}`, nomeModelo, this.getHeaders());
  }

  async procurarNoEmLista(lista: INoDTO[], idNo : string): Promise<INoDTO> {
    let i = 0;
    for(i = 0; i < lista.length; i++){
      if(lista[i].id === idNo) return lista[i];
    }
    return null;
  }

  async procurarModelo3DEmLista(lista: IModelo3DDTO[], nomeModelo : string): Promise<IModelo3DDTO> {
    let i = 0;
    for(i = 0; i < lista.length; i++){
      if(lista[i].nome === nomeModelo) return lista[i];
    }
    return null;
  }
}
