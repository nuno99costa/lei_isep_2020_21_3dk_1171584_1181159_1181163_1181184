import { PercursoMenorTempoService } from './../../../services/percursoMenorTempoService';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import INosPercursoDTO from 'src/app/dto/INosPercursoDTO';

import { DadosPercursoMenorTempoComponent } from './dados-percurso-menor-tempo.component';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import { of } from 'rxjs';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';

describe('DadosPercursoMenorTempoComponent', () => {
  let component: DadosPercursoMenorTempoComponent;
  let fixture: ComponentFixture<DadosPercursoMenorTempoComponent>;
  
  //Mock do percurso
  const mockPercurso = {
    id: "1",   
    nomePercurso: "percurso1",
    linhaId: "1",
    orientacao: "Ida",
    listaSegmentos: ["1", "2"],
    noInicialId: "1",
    noFinalId: "3"
  } as IPercursoDTO;

  const percursoMenorTempoServiceSpy: jasmine.SpyObj<PercursoMenorTempoService> = jasmine.createSpyObj('percursoMenorTempoService', ['validarInput','obterPercursoMenorTempo']);
  percursoMenorTempoServiceSpy.validarInput.and.callFake(() => {});
  percursoMenorTempoServiceSpy.obterPercursoMenorTempo.and.returnValue(of(mockPercurso));

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosPercursoMenorTempoComponent ],
      imports: [BrowserModule],
      providers: [{provide: PercursoMenorTempoService, useValue: percursoMenorTempoServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosPercursoMenorTempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//--------------------------------------------------------------------TESTES-------------------------------

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
