import { PercursoMenorTempoService } from './../../../services/percursoMenorTempoService';
import { Component, OnInit } from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import INosPercursoPreferivel from 'src/app/dto/INosPercursoDTO';
import { IPercursoMenorTempoService } from 'src/app/services/IServices/IPercursoMenorTempoService';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import { HttpErrorResponse } from '@angular/common/http';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dados-percurso-menor-tempo',
  templateUrl: './dados-percurso-menor-tempo.component.html',
  styleUrls: ['./dados-percurso-menor-tempo.component.css']
})
export class DadosPercursoMenorTempoComponent implements OnInit {

  private percursoMenorTempoService: IPercursoMenorTempoService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, percursoMenorTempoService: PercursoMenorTempoService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: Planeamento");
    this.percursoMenorTempoService = percursoMenorTempoService;
    this.httpErrorHandler = httpErrorHandler;
  }

  abvNoInicial: string;
  abvNoFinal: string;
  horaInicio: string;

  tempoSegundos: number;

  percursoObtido: string;
  //Para abrir/fechar o relógio e guardar o tempo em string

  ngOnInit(): void {
  }

  submeter() {

    //Horas + minutos para segundos
    this.tempoSegundos = this.dateTimeEmSegundos(this.horaInicio);
    console.log(this.tempoSegundos);

    const nosPercurso = {
      abvNoInicial: this.abvNoInicial,
      abvNoFinal: this.abvNoFinal,
      tempoSegundos: this.tempoSegundos
    } as INosPercursoPreferivel;

    try {
      this.percursoMenorTempoService.validarInput(nosPercurso);


      this.percursoMenorTempoService.obterPercursoMenorTempo(nosPercurso).subscribe(values => {
        alert("Nome do percurso obtido: " + values.nomePercurso);
      }
        , error => {
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })

    } catch (err) {
      alert(err);
    }

  }

  private dateTimeEmSegundos(horaInicio: string): number {
    const horasString: string[] = horaInicio.split(":");
    const horas: number = +horasString[0]; //parse de string para number
    const minutos: number = +horasString[1];

    return (horas * 3600) + minutos * 60; //transformação em segundos
  }

}
