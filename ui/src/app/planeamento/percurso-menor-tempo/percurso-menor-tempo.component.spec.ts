import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PercursoMenorTempoComponent } from './percurso-menor-tempo.component';

describe('PercursoMenorTempoComponent', () => {
  let component: PercursoMenorTempoComponent;
  let fixture: ComponentFixture<PercursoMenorTempoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PercursoMenorTempoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PercursoMenorTempoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
