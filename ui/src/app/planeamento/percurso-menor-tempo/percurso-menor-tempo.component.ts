import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-percurso-menor-tempo',
  templateUrl: './percurso-menor-tempo.component.html',
  styleUrls: ['./percurso-menor-tempo.component.css']
})
export class PercursoMenorTempoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: Planeamento");
  }
  ngOnInit(): void {
  }

}
