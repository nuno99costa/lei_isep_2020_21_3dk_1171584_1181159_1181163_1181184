import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { NoService } from 'src/app/services/no.service';

import { AtribuirModelosNoDadosComponent } from './atribuir-modelos-no-dados.component';

describe('AtribuirModelosNoDadosComponent', () => {
  let component: AtribuirModelosNoDadosComponent;
  let fixture: ComponentFixture<AtribuirModelosNoDadosComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['listaNos']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtribuirModelosNoDadosComponent ],
      imports: [BrowserModule],
      providers: [{provide: NoService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtribuirModelosNoDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
