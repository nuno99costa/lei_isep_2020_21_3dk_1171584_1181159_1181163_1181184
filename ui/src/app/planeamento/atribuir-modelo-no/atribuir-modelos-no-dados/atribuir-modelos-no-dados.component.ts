import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import IModelo3DDTO from 'src/app/dto/IModelo3DDTO';
import INoDTO from 'src/app/dto/INoDTO';
import { INoService } from 'src/app/services/IServices/INoService';
import { NoService } from 'src/app/services/no.service';

@Component({
  selector: 'app-atribuir-modelos-no-dados',
  templateUrl: './atribuir-modelos-no-dados.component.html',
  styleUrls: ['./atribuir-modelos-no-dados.component.css']
})
export class AtribuirModelosNoDadosComponent implements OnInit {

  private noService: INoService;

  constructor(noService: NoService, private titleService: Title) {
    this.titleService.setTitle("AIT: Planeamento");
    this.noService = noService;
  }

  idNo: string;
  nomeModelo: string;
  listaNos: INoDTO[] = [];
  listaModelos: IModelo3DDTO[] = [];

  async ngOnInit(): Promise<void> {
    this.listaNos = await this.noService.listaNos();
    this.listaModelos = await this.noService.listaModelos();
  }

  async colocarNo() {
    const no: INoDTO = await this.noService.procurarNoEmLista(this.listaNos, this.idNo);
    const modelo: IModelo3DDTO = await this.noService.procurarModelo3DEmLista(this.listaModelos, this.nomeModelo);

    if (no === null || modelo === null) {
      alert("No ou Modelo Invalido");
      
    }
    await this.noService.criarUpdateModeloDeNo(no.id, { nomeModelo: modelo.nome}).subscribe((values) => {
      alert('Sucesso');
    })
  }

}
