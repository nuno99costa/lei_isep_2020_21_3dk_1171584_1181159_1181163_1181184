import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtribuirModeloNoComponent } from './atribuir-modelo-no.component';

describe('AtribuirModeloNoComponent', () => {
  let component: AtribuirModeloNoComponent;
  let fixture: ComponentFixture<AtribuirModeloNoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtribuirModeloNoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtribuirModeloNoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
