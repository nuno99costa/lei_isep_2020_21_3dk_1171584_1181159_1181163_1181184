import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-planeamento',
  templateUrl: './planeamento.component.html',
  styleUrls: ['./planeamento.component.css']
})
export class PlaneamentoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: Planeamento");
  }

  ngOnInit(): void {
  }

  escolha = "";

  scrollTo(id: string) {
    document.getElementById(id)?.scrollIntoView();
    console.log('Scrolled');
  }
}
