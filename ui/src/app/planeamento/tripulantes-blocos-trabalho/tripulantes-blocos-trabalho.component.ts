import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-tripulantes-blocos-trabalho',
  templateUrl: './tripulantes-blocos-trabalho.component.html',
  styleUrls: ['./tripulantes-blocos-trabalho.component.css']
})
export class TripulantesBlocosTrabalhoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: Planeamento");
   }

  ngOnInit(): void {
  }

}
