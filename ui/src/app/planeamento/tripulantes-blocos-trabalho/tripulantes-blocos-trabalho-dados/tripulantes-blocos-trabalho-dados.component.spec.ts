import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { TripulanteService } from 'src/app/services/tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { TripulantesBlocosTrabalhoDadosComponent } from './tripulantes-blocos-trabalho-dados.component';

describe('TripulantesBlocosTrabalhoDadosComponent', () => {
  let component: TripulantesBlocosTrabalhoDadosComponent;
  let fixture: ComponentFixture<TripulantesBlocosTrabalhoDadosComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<TripulanteService> = jasmine.createSpyObj('noService', ['listaNos']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripulantesBlocosTrabalhoDadosComponent ],
      imports: [BrowserModule],
      providers: [{provide: TripulanteService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripulantesBlocosTrabalhoDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
