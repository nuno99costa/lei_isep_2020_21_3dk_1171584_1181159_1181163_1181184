
import { Component, OnInit } from '@angular/core';
import { TripulanteService } from 'src/app/services/tripulante.service';
import ITripulantesBlocosTrabalhoDTO from 'src/app/dto/ITripulantesBlocosTrabalhoDTO';
import { HttpErrorResponse } from '@angular/common/http';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import { Title } from '@angular/platform-browser';
import { ITripulanteService } from 'src/app/services/IServices/ITripulanteService';
import { IServicoViaturaService } from 'src/app/services/IServices/IServicoViaturaService';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';

@Component({
  selector: 'app-tripulantes-blocos-trabalho-dados',
  templateUrl: './tripulantes-blocos-trabalho-dados.component.html',
  styleUrls: ['./tripulantes-blocos-trabalho-dados.component.css']
})
export class TripulantesBlocosTrabalhoDadosComponent implements OnInit {

  private httpErrorHandler: HTTPErrorHandler;
  private tripulanteService: ITripulanteService;
  private servicoViaturaService: IServicoViaturaService;

  constructor(servicoViaturaService: ServicoViaturaService, private titleService: Title, tripulanteService: TripulanteService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: Planeamento");
    this.tripulanteService = tripulanteService;
    this.httpErrorHandler = httpErrorHandler;
    this.servicoViaturaService = servicoViaturaService;
  }

  numerosARetirar: number;
  custoC: number;
  tempoRelativo: number;
  numeroNovasGeracoes: number;
  dimensaoPopulacao: number;
  probabilidadeCruzamento: number;
  probabilidadeMutacao: number;

  listaTripulantesBlocosTrabalho: string[] = [];
  listaAllServicosViatura: IServicoViaturaDTO[] = [];

  servicoViaturaId: string;

  async ngOnInit(): Promise<void> {
    this.listaAllServicosViatura = await this.servicoViaturaService.listaServicosViatura();
  }

  getListaTripulantesBlocosTrabalho() {

    try {

      var dadosTripulantesBlocosTrabalho = {
        servicoViaturaId: this.servicoViaturaId,
        numerosARetirar: this.numerosARetirar,
        custoC: this.custoC,
        tempoRelativo: this.tempoRelativo,
        numeroNovasGeracoes: this.numeroNovasGeracoes,
        dimensaoPopulacao: this.dimensaoPopulacao,
        probabilidadeCruzamento: this.probabilidadeCruzamento,
        probabilidadeMutacao: this.probabilidadeMutacao
      } as ITripulantesBlocosTrabalhoDTO;

      this.tripulanteService.validarInputTripulantesBlocosTrabalho(dadosTripulantesBlocosTrabalho);

      this.tripulanteService.obterListaTripulantesBlocosTrabalho(dadosTripulantesBlocosTrabalho).subscribe(values => {
        var i = 0;
        for (i = 0; i < values.length; i++) {
          this.listaTripulantesBlocosTrabalho.push(values[i]);
        }
        alert("Sucesso! A sua lista foi obtida!");
      }
        , error => {
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch (err) {
      alert(err);
    }
  }

}
