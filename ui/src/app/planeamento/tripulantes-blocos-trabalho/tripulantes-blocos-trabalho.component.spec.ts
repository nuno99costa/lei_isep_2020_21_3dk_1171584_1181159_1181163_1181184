import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TripulantesBlocosTrabalhoComponent } from './tripulantes-blocos-trabalho.component';

describe('TripulantesBlocosTrabalhoComponent', () => {
  let component: TripulantesBlocosTrabalhoComponent;
  let fixture: ComponentFixture<TripulantesBlocosTrabalhoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TripulantesBlocosTrabalhoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TripulantesBlocosTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
