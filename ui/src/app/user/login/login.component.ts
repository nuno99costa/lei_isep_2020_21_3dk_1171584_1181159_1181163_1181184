import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import ILoginDTO from 'src/app/dto/ILoginDTO';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Title } from '@angular/platform-browser';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements ErrorStateMatcher, OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private titleService: Title, private authService: AuthService, private router: Router, private route: ActivatedRoute) {
    this.titleService.setTitle("Login | AIT");
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(15),
  ]);


  matcher = new ErrorStateMatcher;

  ngOnInit(): void {
    this.authService.logout();

    this.route.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'Este utilizador não pode aceder à aplicação.'
      }
    })
  }

  email: string;
  password: string;

  async login() {

    const utilizador = {
      email: this.email,
      password: this.password
    } as ILoginDTO;

    try {
      this.loading = true;
      const loggedIn = await this.authService.login(utilizador);
      this.loading = false;
      if (loggedIn === true) {
        this.router.navigate(['/header'], { relativeTo: this.route });
      } else {
        this.error = 'Email ou password incorretas';

      }
    } catch (err) {
      alert('Email ou password incorretas');
      this.loading = false;
    }

  }

  registo() {
    this.router.navigate(['/registo'], { relativeTo: this.route });
  }
}

