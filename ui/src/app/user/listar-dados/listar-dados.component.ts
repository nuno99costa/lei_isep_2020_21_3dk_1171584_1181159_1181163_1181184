import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IUserService } from 'src/app/services/IServices/IUserService';
import { UserService } from 'src/app/services/user.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-listar-dados',
  templateUrl: './listar-dados.component.html',
  styleUrls: ['./listar-dados.component.css']
})
export class ListarDadosComponent implements OnInit {

  private userService: IUserService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, httpErrorHandler: HTTPErrorHandler, userService: UserService) {
    this.titleService.setTitle("Consulta de dados | AIT: Cliente");
    this.userService = userService;
    this.httpErrorHandler = httpErrorHandler;
  }

  valueEmail: string;
  valueNome: string;
  valueDataNascimento: string;

  async ngOnInit(): Promise<void> {
    const userDTO = await this.userService.getDadosUser();

    this.valueEmail = userDTO.email;
    this.valueNome = userDTO.name;
    this.valueDataNascimento = userDTO.dataNascimento;
  }

}
