import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import IRegistoDTO from 'src/app/dto/IRegistoDTO';
import { IUserDTO } from 'src/app/dto/IUserDTO';
import { UserService } from 'src/app/services/user.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { ListarDadosComponent } from './listar-dados.component';

describe('ListarDadosComponent', () => {
  let component: ListarDadosComponent;
  let fixture: ComponentFixture<ListarDadosComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<UserService> = jasmine.createSpyObj('userService', ['getDadosUser']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);

  svServiceSpy.getDadosUser.and.returnValue(Promise.resolve({email: 'ola@gmail.com', name: 'Nuno', dataNascimento: '03/05/2000'} as IRegistoDTO));


  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarDadosComponent ],
      imports: [BrowserModule],
      providers: [{provide: UserService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
