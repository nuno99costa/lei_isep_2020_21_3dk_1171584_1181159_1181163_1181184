import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import IRegistoDTO from 'src/app/dto/IRegistoDTO';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { UpdateDadosComponent } from './update-dados.component';

describe('UpdateDadosComponent', () => {
  let component: UpdateDadosComponent;
  let fixture: ComponentFixture<UpdateDadosComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<UserService> = jasmine.createSpyObj('userService', ['update', 'getDadosUser']);
  const routerSpy: jasmine.SpyObj<Router> = jasmine.createSpyObj('router', ['navigate']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  svServiceSpy.getDadosUser.and.returnValue(Promise.resolve({email: 'ola@gmail.com', name: 'Nuno', dataNascimento: '03/05/2000'} as IRegistoDTO));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateDadosComponent],
      imports: [BrowserModule],
      providers: [{provide: UserService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy},{provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: Router, useValue: routerSpy}, {
        provide: ActivatedRoute, useValue: {
          params: of({ id: 'test' })
        }
      }],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent( UpdateDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
