import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import "@angular/compiler";
import IRegistoDTO from 'src/app/dto/IRegistoDTO';
import { HttpErrorResponse } from '@angular/common/http';
import { IUserService } from 'src/app/services/IServices/IUserService';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-update-dados',
  templateUrl: './update-dados.component.html',
  styleUrls: ['./update-dados.component.css']
})
export class UpdateDadosComponent implements OnInit {

  private userService: IUserService;
  model: any = {};
  loading = false;
  error = '';

  constructor(private titleService: Title, private router: Router, private route: ActivatedRoute, private httpErrorHandler: HTTPErrorHandler, userService: UserService) {
    this.titleService.setTitle("Alteração de dados | AIT");
    this.userService = userService;
  }

  valueNome: string;
  valueEmail: string;
  valuePassword: string;
  valueDataNascimento: string;

  name: string;
  email: string;
  password: string;
  dataInput: Date;

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

  emailFormControl = new FormControl('', [
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.minLength(6),
    Validators.maxLength(15),
  ]);

  matcher = new ErrorStateMatcher;

  async ngOnInit(): Promise<void> {
    const userInfo = await this.userService.getDadosUser();
    this.valueNome = userInfo.name;
    this.valueEmail = userInfo.email;
    this.valuePassword = userInfo.password;
    this.valueDataNascimento = userInfo.dataNascimento;

    this.route.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'Este utilizador não pode aceder à aplicação.'
      }
    })
  }

  update() {

    try {

      if(this.dataInput) this.userService.validarDataNascimento(this.dataInput);

      if (!this.name) this.name = this.valueNome;
      if (!this.email) this.email = this.valueEmail;
      if (!this.password) this.password = this.valuePassword;
      if (!this.dataInput) this.dataInput = new Date(this.valueDataNascimento);

      const utilizador = {
        name: this.name,
        email: this.email,
        password: this.password,
        dataNascimento: this.dataInput.toDateString(),
        role: 'Cliente'
      } as IRegistoDTO;

      this.loading = true;
      this.userService.validarUpdate(utilizador);
      this.userService.update(utilizador).subscribe(values => {
        alert("Alterações efetuadas com sucesso!");
        this.router.navigate(['/login'], { relativeTo: this.route });
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
            this.router.navigate(['/header'], { relativeTo: this.route });
          }

        })

    } catch (err) {
      alert(err);
      this.router.navigate(['/header'], { relativeTo: this.route });
    }
  }

  delete() {
    try {
      this.loading = true;
      this.userService.delete().subscribe(values => {
        alert("Utilizador eliminado com sucesso!");
        this.router.navigate(['/login'], { relativeTo: this.route });
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
            this.router.navigate(['/header'], { relativeTo: this.route });
          }

        })

    } catch (err) {
      alert(err);
      this.router.navigate(['/header'], { relativeTo: this.route });
    }
  }

  cancelar() {
    this.router.navigate(['/header'], { relativeTo: this.route });
  }

}
