import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { AuthService } from 'src/app/services/auth.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { RegistoComponent } from './registo.component';

describe('RegistoComponent', () => {
  let component: RegistoComponent;
  let fixture: ComponentFixture<RegistoComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<AuthService> = jasmine.createSpyObj('authService', ['logout']);
  const routerSpy: jasmine.SpyObj<Router> = jasmine.createSpyObj('router', ['navigate']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [RegistoComponent],
      imports: [BrowserModule],
      providers: [{provide: MatDialog, useValue: matDialogSpy},{provide: AuthService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy},{provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy},{provide: Router, useValue: routerSpy}, {
          provide: ActivatedRoute, useValue: {
            params: of({ id: 'test' })
          }
        }],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });



  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
