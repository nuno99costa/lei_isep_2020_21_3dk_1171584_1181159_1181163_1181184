import { HttpErrorResponse } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute } from '@angular/router';
import IRegistoDTO from 'src/app/dto/IRegistoDTO';
import { AuthService } from 'src/app/services/auth.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';


@Component({
  selector: 'app-registo',
  templateUrl: './registo.component.html',
  styleUrls: ['./registo.component.css']
})

export class RegistoComponent implements OnInit {

  model: any = {};
  loading = false;
  error = '';

  constructor(private titleService: Title, private authService: AuthService, private router: Router, private route: ActivatedRoute, private httpErrorHandler: HTTPErrorHandler, public dialog: MatDialog) {
    this.titleService.setTitle("Registo | AIT");
  }

  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }

  nameFormControl = new FormControl('', [
    Validators.required
  ]);

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    Validators.maxLength(15),
  ]);

  dataFormControl = new FormControl('', [
    Validators.required,
  ])

  matcher = new ErrorStateMatcher;

  ngOnInit(): void {
    this.authService.logout();

    this.route.params.subscribe(params => {
      if (params['u'] !== undefined) {
        ;
        this.error = 'Este utilizador não pode aceder à aplicação.'
      }
    })
  }

  name: string;
  email: string;
  password: string;
  checkBoxRGPD: boolean = false;
  dataInput: Date;

  registo() {

    try {
      this.authService.validarDataNascimento(this.dataInput);

      const utilizador = {
        name: this.name,
        email: this.email,
        password: this.password,
        dataNascimento: this.dataInput.toDateString(),
        role: 'Cliente'
      } as IRegistoDTO;


      this.loading = true;
      this.authService.validarRegisto(utilizador, this.checkBoxRGPD);
      this.authService.registo(utilizador).subscribe(values => {
        alert("Registado com sucesso!");
        this.router.navigate(['/login'], { relativeTo: this.route });
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
            this.router.navigate(['/registo'], { relativeTo: this.route });
          }

        })

    } catch (err) {
      alert(err);
      this.router.navigate(['/registo'], { relativeTo: this.route });
    }
  }

  cancelar() {
    this.router.navigate(['/login'], { relativeTo: this.route });
  }

  openDialog() {
    this.dialog.open(RegistoDialogComponent, {
      height: '400px',
      width: '650px',
    });
  }
}

@Component({
  selector: 'registo-dialog',
  templateUrl: 'registo-dialog.component.html',
})
export class RegistoDialogComponent {}