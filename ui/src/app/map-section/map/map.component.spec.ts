import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { of } from 'rxjs';
import { NoService } from 'src/app/services/no.service';
import { PercursoService } from 'src/app/services/percurso.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { MapComponent } from './map.component';

describe('MapComponent', () => {
  let component: MapComponent;
  let fixture: ComponentFixture<MapComponent>;

  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  httpErrorHandlerSpy.handleError.and.returnValue("teste");

  const percursoServiceSpy: jasmine.SpyObj<PercursoService> = jasmine.createSpyObj('percursoService', ['carregarDadosMapa']);
  percursoServiceSpy.carregarDadosMapa.and.returnValue(of());

  const noServiceSpy: jasmine.SpyObj<NoService> = jasmine.createSpyObj('noService', ['listaNos', 'listaModelos']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MapComponent],
      imports: [BrowserModule],
      providers: [{ provide: PercursoService, useValue: percursoServiceSpy }, { provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy }, {provide: NoService, useValue: noServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
});

beforeEach(() => {
  fixture = TestBed.createComponent(MapComponent);
  component = fixture.componentInstance;
  fixture.detectChanges();
});

it('should create', () => {
  expect(component).toBeTruthy();
});
});
