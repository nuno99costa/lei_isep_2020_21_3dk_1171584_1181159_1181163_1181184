import { element } from 'protractor';
import { map } from 'rxjs/operators';
import { features } from 'process';
import { IUserDTO } from './../../dto/IUserDTO';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import * as GeoJSON from "geojson";
import { environment } from 'src/environments/environment';
import * as Mapboxgl from 'mapbox-gl';
import { PercursoService } from 'src/app/services/percurso.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';
import IDadosLinhaMapaDTO from 'src/app/dto/IDadosLinhaDTO';
import { HttpErrorResponse } from '@angular/common/http';
import { NoService } from 'src/app/services/no.service';
import INoDTO from 'src/app/dto/INoDTO';
import * as mapboxgl from 'mapbox-gl';
import * as THREE from 'three';
import { GLTF, GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { Loader } from 'three';
import utils from '../../utils/utils';
import IModelo3DDTO from 'src/app/dto/IModelo3DDTO';
const Threebox = require('threebox-plugin').Threebox;
import * as tb from 'threebox-plugin';
import { animate } from '@angular/animations';
import UtilsMap from '../../utils/utils';
declare global {
  interface Window {
    tb: typeof tb;
  }
}
window.tb = tb;
const VELOCIDADE_AUTOCARRO = 0.000090;
const RAIO_COLISAO = 0.0006;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent {

  /** */
  private percursoService: PercursoService;
  private noService: NoService;
  private httpErrorHandler: HTTPErrorHandler;

  map: Mapboxgl.Map;
  listaNos: INoDTO[] = [];
  listaModelos: IModelo3DDTO[] = [];
  listaLinhas: IDadosLinhaMapaDTO[] = [];
  idNo: string;
  nomeModelo: string;
  userInfo: IUserDTO;


  horaEscolhida: number;
  hora: string;
  date: Date = new Date(2021, 7, 14, 15, 3);
  styles = {
    day: 'streets-v11',
    night: 'dark-v10'
  }
  selectedStyle: string;
  origin: [number, number] = [-8.348264, 41.2061805];
  horaOutput: string = this.date.getHours.toString();
  on3d: boolean;
  atualiza: boolean;

  coordenadasAutocarro: [number, number, number] = [-8.348264, 41.2061805, -5];
  modelAutocarro: any;
  pressW: boolean = false;
  pressS: boolean = false;
  pressA: boolean = false;
  pressD: boolean = false;
  currentZ: number;
  currentBearing: number = 0;


  public constructor(percursoService: PercursoService, httpErrorHandler: HTTPErrorHandler, noService: NoService) {
    this.percursoService = percursoService;
    this.noService = noService;
    this.httpErrorHandler = httpErrorHandler;
  }


  public async ngOnInit() {
    this.selectedStyle = this.styles.day;
    this.on3d = false;
    this.map = new mapboxgl.Map({
      accessToken: environment.mapBoxKey,
      container: 'mapa-mapbox', // container id
      style: 'mapbox://styles/mapbox/' + this.selectedStyle,
      center: [-8.348264, 41.2061805], // LGN, LAT
      zoom: 15 // starting zoom
    });

    window.tb = new Threebox(
      this.map,
      this.map.getCanvas().getContext('webgl'),
      {
        realSunlight: true,
        enableSelectingObjects: true, //enable 3D models over/selection
        enableTooltips: true // enable default tooltips on fill-extrusion and 3D models 
      }
    );
    this.map.touchPitch.disable();
    this.map.doubleClickZoom.disable();
    this.map.keyboard.disable();
    this.map.addControl(new Mapboxgl.NavigationControl());
    this.map.scrollZoom.disable();
    this.map.setMaxPitch(0);



    this.listaLinhas = await this.percursoService.carregarDadosMapa().toPromise();
    this.listaNos = await this.noService.listaNos();
    this.listaModelos = await this.noService.listaModelos();

    this.map.on('render', () => {
      window.tb.setSunlight(this.date);
      /* if(this.on3d ){
       this.carregarNos3d();
      }else if(!this.on3d ){
        this.carregarNos2d();
      } */
      this.changeStyleWithDaylight(this.date, this.origin);
    })

    let loaded = false;
    this.map.on('idle', () => {
      if (!loaded) {
        this.map.flyTo({
          center: this.origin,
          zoom: 15
        });
        loaded = true;
      }
    })

    //2d nos models
    this.carregarNos2d();

    //3d buildings
    this.add3Dbuildings();

  }

  private carregarNos2d() {
    this.date

    var _this = this;
    this.map.addLayer({
      id: 'custom_layer_nos2d',
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, mbxContext) {

        _this.listaNos.forEach((element) => {
          //center of where the objects are
          var coords = [element.longitude, element.latitude, 2];

          //cylinder as "base" for each one of the 3d Models 
          //in here i cant do the Tooltip for the object**
          const geometry = new THREE.CylinderGeometry(1.2, 1.6, 0.30, 32);
          const material = new THREE.MeshLambertMaterial({ color: 0x5B5B5B });
          const cylinder = new THREE.Mesh(geometry, material);

          var baseOptions = {
            obj: cylinder,
            anchor: 'center',
            adjustment: { x: 0, y: 0, z: -0.4 }
          };

          let base = window.tb.Object3D(baseOptions);
          base.setCoords(coords);
          base.setRotation({ x: 90, y: 0, z: 0 });
          //The text is just for the test
          base.addTooltip('<strong>' + element.abreviatura + '</strong>'
            + '<p>Id:' + element.id + '</p>'
            + '<p>Estação de recolha:' + element.isEstacaoRecolha + '</p>'
            + '<p>Ponto de rendição:' + element.isPontoRendicao + '</p>', true);
          base.castShadow = true;

          window.tb.add(base);
        });

      },

      render: function (gl, matrix) {
        window.tb.setSunlight(_this.date, [-8.348264, 41.2061805]);
        //_this.changeStyleWithDaylight(_this.date, _this.origin);
        window.tb.update();
      }
    });
  }

  public async buttonClickAdicionarLinhas() {
    var _this = this;
    this.animate();
    this.map.addLayer({
      id: 'custom_layer_linhas',
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, mbxContext) {
        _this.listaLinhas.forEach((element) => {
          for (var index = 0; index < element.matrizCoordenadas.length; index++) {
            for (var index2 = 0; index2 < element.matrizCoordenadas[index].length; index2++) {

              if (index2 != element.matrizCoordenadas[index].length - 1) {
                var cor = element.cor;

                var tubeOptions;
                console.log("2");

                if (cor.startsWith('RGB')) {
                  tubeOptions = {
                    geometry: [element.matrizCoordenadas[index][index2], element.matrizCoordenadas[index][index2 + 1]],
                    radius: 0.1,
                    sides: 8,
                    material: 'MeshPhysicalMaterial',
                    color: UtilsMap.toHexadecimal(cor),
                    anchor: 'center'
                  }
                } else {
                  tubeOptions = {
                    geometry: [element.matrizCoordenadas[index][index2], element.matrizCoordenadas[index][index2 + 1]],
                    radius: 0.08,
                    sides: 8,
                    material: 'MeshPhysicalMaterial',
                    color: element.cor,
                    anchor: 'center'
                  }
                }
                let tube = window.tb.tube(tubeOptions);
                var a = ((element.matrizCoordenadas[index][index2][0] + element.matrizCoordenadas[index][index2 + 1][0]) / 2);
                var b = ((element.matrizCoordenadas[index][index2][1] + element.matrizCoordenadas[index][index2 + 1][1]) / 2);
                var mid = [a, b, 0];
                tube.setCoords(mid);
                tube.addTooltip('<strong> Nome Linha: ' + element.nomeLinha + '</strong>'
                  + '<p> Cor Linha: ' + element.cor + '</p>'
                  + '<p> Nome Percurso:' + element.nomesPercursos[index] + '</p>', true);
                // tube.material.wireframe = true
                window.tb.add(tube);
              }
            }
          }
        });

      },

      render: function (gl, matrix) {
        window.tb.setSunlight(_this.date, [-8.348264, 41.2061805]);
        //_this.changeStyleWithDaylight(_this.date, _this.origin);
        window.tb.update();
      }

    });
  }


  public carregarNos3d() {
    // stats
    // stats = new Stats();
    // map.getContainer().appendChild(stats.dom);
    this.animate();
    var _this = this;
    this.map.addLayer({

      id: 'custom_layer_nos3d',
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, mbxContext) {
        _this.listaNos.forEach(async (element) => {



          var modelos = await _this.noService.listaModelos();
          let options;
          var url;


          if (element.nomeModelo) {
            url = (await _this.noService.procurarModelo3DEmLista(modelos, element.nomeModelo)).path;
          } else {
            url = "https://lei-isep-lapr5-g05-mdr.herokuapp.com/modelos/BusStop1/scene.gltf";
          }
          //For each one of a list that i fill first
          options = {
            type: 'gltf', //'gltf'/'mtl'
            obj: url, //model url
            bin: '', //replace by mtl attribute
            units: 'meters', //units in the default values are always in meters
            scale: 1,
            rotation: { x: 90, y: 180, z: 0 }, //default rotation
            anchor: 'center'
          }
          //center of where the objects are
          var coords = [element.longitude, element.latitude, 2.3];
          //next i check what type of element it is 
          //it can only be one at the same time, so i use different models for each type
          window.tb.loadObj(options, function (model: any) {
            model.setCoords(coords);
            model.addTooltip('<strong>' + element.abreviatura + '</strong>'
              + '<p>Id:' + element.id + '</p>'
              + '<p>Estação de recolha:' + element.isEstacaoRecolha + '</p>'
              + '<p>Ponto de rendição:' + element.isPontoRendicao + '</p>', true);
            model.setRotation({ x: 0, y: 0, z: Math.floor(Math.random() * 100) })
            model.castShadow = true;
            window.tb.add(model);
          });

          console.log(element.abreviatura)


        });

      },

      render: function (gl, matrix) {
        window.tb.setSunlight(_this.date, [-8.348264, 41.2061805]);
        //_this.changeStyleWithDaylight(_this.date, _this.origin);
        window.tb.update();
      },

    });
  }

  public carregarAutocarro3d() {
    window.tb.options.passiveRendering = true;
    window.tb.options.enableTooltips = false;
    this.animate();
    var _this = this;
    this.map.addLayer({

      id: 'custom_layer_autocarro3d',
      type: 'custom',
      renderingMode: '3d',
      onAdd: function (map, mbxContext) {
        //var modelos = await _this.noService.listaModelos();
        //var url = (await _this.noService.procurarModelo3DEmLista(modelos, element.nomeModelo)).path;
        let options = {
          type: 'gltf', //'gltf'/'mtl'
          obj: 'https://lei-isep-lapr5-g05-mdr.herokuapp.com/modelos/bus/scene.gltf', //model url
          bin: '', //replace by mtl attribute
          units: 'meters', //units in the default values are always in meters
          scale: 1,
          rotation: { x: 90, y: 180, z: 0 }, //default rotation
          anchor: 'center'
        }
        //For each one of a list that i fill first

        //center of where the objects are
        var coords = _this.coordenadasAutocarro;
        //next i check what type of element it is 
        //it can only be one at the same time, so i use different models for each type
        window.tb.loadObj(options, function (model: any) {
          model.setCoords(coords);
          model.setRotation({ x: 0, y: 0, z: 90 })
          model.castShadow = true;
          window.tb.add(model);
          _this.modelAutocarro = model;
          _this.currentZ = 90;
        });


      },

      render: function (gl, matrix) {
        if (_this.atualiza) {
          if (_this.pressW) {
            var novasCoordenadas = _this.newBusCoordinates(_this.currentZ, true, _this.coordenadasAutocarro);
            if (_this.podeAvancar(novasCoordenadas)) {
              _this.coordenadasAutocarro = novasCoordenadas;
              _this.modelAutocarro.setCoords(_this.coordenadasAutocarro);
              _this.map.jumpTo({
                center: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]],
                zoom: 17,
                bearing: _this.currentBearing,
                pitch: 3,
                around: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]]
              });
            }

          }

          if (_this.pressS) {
            var novasCoordenadas = _this.newBusCoordinates(_this.currentZ, false, _this.coordenadasAutocarro);;
            if (_this.podeAvancar(novasCoordenadas)) {
              _this.coordenadasAutocarro = novasCoordenadas;
              _this.modelAutocarro.setCoords(_this.coordenadasAutocarro);
              _this.map.jumpTo({
                center: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]],
                zoom: 17,
                bearing: _this.currentBearing,
                pitch: 3,
                around: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]]
              });
            }
          }

          if (_this.pressA) {
            let newRotation = _this.currentZ + 90;
            if (newRotation > 360) {
              newRotation = 90;
            }
            _this.currentZ = newRotation;
            _this.modelAutocarro.setRotation({ x: 0, y: 0, z: _this.currentZ });
            _this.currentBearing = _this.currentBearing - 90;
            _this.map.jumpTo({
              center: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]],
              zoom: 17,
              bearing: _this.currentBearing,
              pitch: 3,
              around: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]]
            });
          }

          if (_this.pressD) {
            let newRotation = _this.currentZ - 90;
            if (newRotation < 90) {
              newRotation = 360;
            }

            _this.currentBearing = _this.currentBearing + 90;
            _this.currentZ = newRotation;
            _this.modelAutocarro.setRotation({ x: 0, y: 0, z: _this.currentZ });
            _this.map.jumpTo({
              center: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]],
              zoom: 17,
              bearing: _this.currentBearing,
              pitch: 3,
              around: [_this.coordenadasAutocarro[0], _this.coordenadasAutocarro[1]]
            });
          }
          _this.atualiza = false;
          console.log("Bearing" + _this.currentBearing);
          console.log("Rotation" + _this.currentZ);


        }
        window.tb.setSunlight(_this.date, [-8.348264, 41.2061805]);
        _this.changeStyleWithDaylight(_this.date, _this.origin);
        window.tb.update();


      }

    });
  }

  podeAvancar(novasCoordenadas: [number, number, number]): boolean {
    let i = 0;
    for (i = 0; i < this.listaNos.length; i++) {
      var dist = Math.sqrt(Math.pow((novasCoordenadas[1] - this.listaNos[i].latitude), 2) + Math.pow((novasCoordenadas[0] - this.listaNos[i].longitude), 2));
      if (dist <= RAIO_COLISAO) {
        console.log("Demasiado perto! Distancia = " + dist)
        return false;
      }
    }
    return true;
  }

  add3Dbuildings() {
    var map = this.map;
    this.map.on('load', function () {

      var layers = map.getStyle().layers;

      var labelLayerId;
      for (var index = 0; index < layers.length; index++) {
        if (layers[index].type === 'symbol' && layers[index].layout === 'text-field') {
          labelLayerId = layers[index].id;
          break;
        }
      }

      map.addLayer(
        {
          'id': '3d-buildings',
          'source': 'composite',
          'source-layer': 'building',
          'filter': ['==', 'extrude', 'true'],
          'type': 'fill-extrusion',
          'minzoom': 15,
          'paint': {
            'fill-extrusion-color': '#aaa',
            'fill-extrusion-height': [
              'interpolate',
              ['linear'],
              ['zoom'],
              15,
              0,
              15.05,
              ['get', 'height']
            ],
            'fill-extrusion-base': [
              'interpolate',
              ['linear'],
              ['zoom'],
              15,
              0,
              15.05,
              ['get', 'min_height']
            ],
            'fill-extrusion-opacity': 0.6
          }
        },
        labelLayerId
      );
    });
  }


  onKeypressEvent() {
    window.addEventListener('keydown', key => {
      this.atualiza = true;
      var pressedKey = key.key;
      if (pressedKey == "w") {
        this.pressW = true;

        console.log(pressedKey + "keydown");
      }
      if (pressedKey == "s") {
        this.pressS = true;
        console.log(pressedKey + "keydown");
      }
      if (pressedKey == "a") {
        this.pressA = true;
        console.log(pressedKey + "keydown");
      }
      if (pressedKey == "d") {
        this.pressD = true;
        console.log(pressedKey + "keydown");
      }
    })

    window.addEventListener('keyup', key => {
      var pressedKey = key.key;
      if (pressedKey == "w") {
        this.pressW = false;
        console.log(pressedKey + "keyup");
      }
      if (pressedKey == "s") {
        this.pressS = false;
        console.log(pressedKey + "keyup");
      }
      if (pressedKey == "a") {
        this.pressA = false;
        console.log(pressedKey + "keyup");
      }
      if (pressedKey == "d") {
        this.pressD = false;
        console.log(pressedKey + "keyup");
      }
    })
  }

  public async changeTo3d() {
    var centerCoords: mapboxgl.LngLatLike = this.origin;
    this.map.keyboard.disable();
    this.map.dragPan.enable();
    this.map.dragRotate.enable();
    this.on3d = true;
    this.map.setMaxPitch(60);
    this.map.setMinPitch(59.9);
    this.map.setPitch(0);

    /*     if(this.map.getLayer('custom_layer_nos3d')){
          this.map.removeLayer('custom_layer_nos3d');
        }
        this.map.triggerRepaint(); */
    this.carregarNos3d();
  }

  public async changeTo2d() {
    var centerCoords: mapboxgl.LngLatLike = this.origin;

    this.map.setMinPitch(0);
    this.map.setPitch(0);
    this.map.setMaxPitch(0.1);
    this.on3d = false;
  }

  public chanteTo1stPerson() {
    var centerCoords: mapboxgl.LngLatLike = this.origin;
    var jumpToOptions = {
      center: [this.coordenadasAutocarro[0], this.coordenadasAutocarro[1]],
      zoom: 19,
      bearing: 0,
      pitch: 60,
      around: [this.coordenadasAutocarro[0], this.coordenadasAutocarro[1]],
      duration: 3000,
      animate: true,
      essential: true,
    }
    this.map.setMaxPitch(60);
    this.map.setMinPitch(59.9);
    this.map.setPitch(59.9);
    this.map.zoomIn(jumpToOptions)
    //this.map.jumpTo(jumpToOptions);
    this.map.keyboard.enable();
    this.map.dragPan.disable();

    /* 
        if (!this.map.getLayer('custom_layer_nos3d')) {
          this.carregarNos3d();
        } */

    this.carregarAutocarro3d();
  }
  public animate() {
    requestAnimationFrame(animate);
    //stats.update();
  }

  public alteracaoHora() {
    this.date.setHours(Math.floor(this.horaEscolhida / 60));
    this.date.setMinutes(Math.floor(this.horaEscolhida % 60));
    this.date.setSeconds(0);
    this.map.triggerRepaint();
    if (Math.floor(this.horaEscolhida % 60) == 0) {
      this.horaOutput = Math.floor(this.horaEscolhida / 60) + ':00'
    } else if (Math.floor(this.horaEscolhida % 60) == 60) {
      this.horaOutput = (Math.floor(this.horaEscolhida / 60) + 1) + ':00'
    } else {
      this.horaOutput = Math.floor(this.horaEscolhida / 60) + ':' + Math.floor(this.horaEscolhida % 60)
    }
  }

  public changeStyleWithDaylight(date: Date, origin: [number, number]) {
    let sunTimes = window.tb.getSunTimes(date, origin);
    if (date >= sunTimes.sunriseEnd && date <= sunTimes.sunsetStart) {
      if (this.selectedStyle != this.styles.day) {
        console.log("it's day");
        window.tb.setStyle("mapbox://styles/mapbox/" + this.styles.day);
        this.selectedStyle = this.styles.day;
        this.map.triggerRepaint();
      }
    } else {
      if (this.selectedStyle != this.styles.night) {
        console.log("it's night");
        window.tb.setStyle("mapbox://styles/mapbox/" + this.styles.night);
        this.selectedStyle = this.styles.night;
        this.map.triggerRepaint();
      }
    }
  }

  public newBusCoordinates(currentRotation: number, isParaFrente: boolean, currentCoordinates: [number, number, number]): [number, number, number] {

    if (currentRotation == 90) {
      if (isParaFrente) {
        return [currentCoordinates[0], currentCoordinates[1] + VELOCIDADE_AUTOCARRO, currentCoordinates[2]];
      } else {
        return [currentCoordinates[0], currentCoordinates[1] - VELOCIDADE_AUTOCARRO, currentCoordinates[2]];
      }
    }

    if (currentRotation == 270) {
      if (isParaFrente) {
        return [currentCoordinates[0], currentCoordinates[1] - VELOCIDADE_AUTOCARRO, currentCoordinates[2]];
      } else {
        return [currentCoordinates[0], currentCoordinates[1] + VELOCIDADE_AUTOCARRO, currentCoordinates[2]];
      }
    }

    if (currentRotation == 180) {
      if (isParaFrente) {
        return [currentCoordinates[0] - VELOCIDADE_AUTOCARRO, currentCoordinates[1], currentCoordinates[2]];
      } else {
        return [currentCoordinates[0] + VELOCIDADE_AUTOCARRO, currentCoordinates[1], currentCoordinates[2]];
      }
    }

    if (currentRotation == 360) {
      if (isParaFrente) {
        return [currentCoordinates[0] + VELOCIDADE_AUTOCARRO, currentCoordinates[1], currentCoordinates[2]];
      } else {
        return [currentCoordinates[0] - VELOCIDADE_AUTOCARRO, currentCoordinates[1], currentCoordinates[2]];
      }
    }

    console.log("-------------------DEU NULL AS NOVAS COORDENADAS------------------------")
    return null;

  }

}



