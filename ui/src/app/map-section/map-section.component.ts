import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-map-section',
  templateUrl: './map-section.component.html',
  styleUrls: ['./map-section.component.css']
})
export class MapSectionComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("Mapa | AIT");
  }

  ngOnInit(): void {
  }

}
