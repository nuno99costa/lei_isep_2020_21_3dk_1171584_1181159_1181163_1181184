import { LoginComponent } from './user/login/login.component';
import { RegistoComponent } from './user/registo/registo.component';
import { HeaderComponent } from './header/header.component';
import { Routes } from "@angular/router";
import { ListarDadosComponent } from './user/listar-dados/listar-dados.component';
import { UpdateDadosComponent } from './user/update-dados/update-dados.component';

export const appRoutes: Routes = [
    { 
        path : 'header', component: HeaderComponent
    },

    {
        path: 'registo', component: RegistoComponent,
        children: [{path:'', component: LoginComponent}]
    },

    {
        path: 'login', component: LoginComponent,
        children: [{ path: '', component: RegistoComponent}]
    },

    {
        path: 'listarDados', component: ListarDadosComponent,
        children: [{ path: '', component: ListarDadosComponent}]
    },

    {
        path: 'alteracaoDados', component: UpdateDadosComponent,
        children: [{ path: '', component: UpdateDadosComponent}]
    },

    {
        path: '', redirectTo:'/login', pathMatch:'full'
    }
];