import { IUserDTO } from './dto/IUserDTO';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router'; 
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'LAPR5';
  subscriptionAuth: Subscription;
  userInfo: IUserDTO;

  constructor(private titleService: Title, private authService: AuthService, private cdr: ChangeDetectorRef, private router: Router, private route: ActivatedRoute) { 
    this.titleService.setTitle("AIT");
  }

  loggedin: boolean = true;

  ngOnInit(): void {
    this.userInfo = this.authService.userInfo;
    this.subscriptionAuth = this.authService.authentication.subscribe((userInfo) => {
      this.userInfo = userInfo;
      this.cdr.detectChanges();
    })
  }

  ngOnDestroy() {
    this.subscriptionAuth.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login'], { relativeTo: this.route })
  }
}

