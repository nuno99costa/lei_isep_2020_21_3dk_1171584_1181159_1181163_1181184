import { CanActivate, Router } from '@angular/router';
import { Injectable } from "@angular/core";
import { AuthService } from '../services/auth.service';

@Injectable()
export class GestorGuard implements CanActivate {
    constructor(private router : Router, private authService: AuthService) { }

    canActivate() {
        if(this.authService.userInfo)
            if(this.authService.userInfo.isGestor)
            return true;
        
        this.router.navigate(['/login', {u : 'no'}]);
        return false;
    }
}