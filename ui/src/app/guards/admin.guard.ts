import { AuthService } from './../services/auth.service';
import { Router,CanActivate } from '@angular/router';
import {Injectable } from '@angular/core';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor( private router : Router, private authService: AuthService) {}

    canActivate() {
        if(this.authService.userInfo)
            if(this.authService.userInfo.isAdministrador)
                return true;

        this.router.navigate(['/login', {u : 'no'}]);
        return false;
    }    
}