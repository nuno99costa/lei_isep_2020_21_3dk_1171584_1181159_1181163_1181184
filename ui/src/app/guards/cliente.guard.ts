import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';

@Injectable()
export class ClienteGuard implements CanActivate {
    constructor(private router : Router, private authService: AuthService) { }

    canActivate() {
        if(this.authService.userInfo)
            if(this.authService.userInfo.isCliente)
            return true;
        
        this.router.navigate(['/login', {u : 'no'}]);
        return false;
    }
}