import { MasterDataViagemComponent } from './master-data-viagem/master-data-viagem.component';
import { MasterDataRedeComponent } from './master-data-rede/master-data-rede.component';
import { AuthGuard } from './guards/auth.guard';
import { AdminGuard } from './guards/admin.guard';
import { GestorGuard } from './guards/gestor.guard';
import { ClienteGuard } from './guards/cliente.guard';
import { HeaderComponent } from './header/header.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './user/login/login.component';
import { MapSectionComponent } from './map-section/map-section.component';
import { PlaneamentoComponent } from './planeamento/planeamento.component';

const routes: Routes = 
[
  { path: 'login', component: LoginComponent},

  {path: 'mapa', component: MapSectionComponent, 
   canActivate: [/*AuthGuard,*/ ClienteGuard]},

  {path: 'mdr', component: MasterDataRedeComponent,
   canActivate: [/*AuthGuard,*/  AdminGuard]},
   
  {path: 'mdv', component: MasterDataViagemComponent,
   canActivate: [/*AuthGuard,*/  AdminGuard]},

  {path: 'planeamento', component: PlaneamentoComponent,
   canActivate: [/*AuthGuard,*/  GestorGuard]}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
