export default interface ITipoTripulanteDTO {
    id: string;
    codigo: string;
    descricao: string;
}
