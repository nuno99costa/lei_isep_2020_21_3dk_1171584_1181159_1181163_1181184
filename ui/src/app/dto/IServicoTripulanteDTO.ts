export default interface IServicoTripulanteDTO {
    id: string;
    idsBlocosTrabalho: string[];
    duracao: number;
}