export default interface INosPercursoDTO{
    abvNoInicial : string;
    abvNoFinal : string;
    tempoSegundos : number;
}