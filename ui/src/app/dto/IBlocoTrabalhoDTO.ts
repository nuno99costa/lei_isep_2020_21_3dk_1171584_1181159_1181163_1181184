export interface IBlocoTrabalhoDTO {
    id: string;
    Inicio: number;
    Fim: number;
    ServicoViaturaId: string;
}