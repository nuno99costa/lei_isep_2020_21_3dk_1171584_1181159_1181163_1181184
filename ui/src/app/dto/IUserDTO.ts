export class IUserDTO  {
    token: string;
    tokenExp: number;
    email: string;
    isAdministrador: boolean;
    isCliente: boolean;
    isGestor: boolean;
}