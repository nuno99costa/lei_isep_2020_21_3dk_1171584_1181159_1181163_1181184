export default interface IDadosLinhaMapaDTO {
    cor: string;
    matrizCoordenadas: [number, number][][];
    nomeLinha: string;
    nomesPercursos: string[];
}