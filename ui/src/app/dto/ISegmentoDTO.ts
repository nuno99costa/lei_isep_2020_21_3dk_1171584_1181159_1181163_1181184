export default interface ISegmentoDTO{
    
    id: string;
    nomeSegmento: string;
    idNoInicial: string;
    idNoFinal: string;
    distanciaNos: number;
    tempoNos: number;

}