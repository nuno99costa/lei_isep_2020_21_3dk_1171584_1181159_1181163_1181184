export default interface ICreatingBlocosTrabalhoDTO {
    DuracaoMaxima: number;
    numeroMaximoBlocos: number;
    ServicoViaturaId: string;
}