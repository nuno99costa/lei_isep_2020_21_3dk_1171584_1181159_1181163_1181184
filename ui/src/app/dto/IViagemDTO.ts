export default interface IViagemDTO {
    viagemId: string;
    IdPercurso: string;
    IdLinha: string;
    IdNoSaida: string;
    HorarioSaida: number;
    HoraSaida: number;
}