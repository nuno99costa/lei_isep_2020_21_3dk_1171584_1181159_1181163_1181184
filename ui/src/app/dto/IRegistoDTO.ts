export default interface IRegistoDTO {
    name: string;
    email: string;
    password: string;
    dataNascimento: string;
    role: string;
}