export default interface IViaturaDTO {
    vin: string;
    matricula: string;
    TipoViatura: string;
    data: Date;
}