export default interface ITripulanteDTO {
    nMecanografico: string;
    nome: string;
    dataNascimento: string;
    NCCidadao: string;
    NCConducao: string;
    NIF: string;
    TipoTripulante: string;
    dataEntradaEmpresa: string;
    dataSaidaEmpresa: string;
    dataLicencaConducao: string;
}
