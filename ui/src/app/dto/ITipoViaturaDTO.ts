export default interface ITipoViaturaDTO {
    id: string;
    codigo: string;
    descricao: string;
    combustivel: string;
    autonomia: number;
    velocidadeMedia: number;
    custoPorQuilometro: number;
    consumoMedio: number;
}