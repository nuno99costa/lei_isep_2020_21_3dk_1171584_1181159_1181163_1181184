export default interface IServicoViaturaDTO {
    id: string;
    duracaoSegundos: number;
    viagensIds: string[];
}
