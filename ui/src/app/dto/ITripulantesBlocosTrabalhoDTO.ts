export default interface ITripulantesBlocosTrabalhoDTO {
    servicoViaturaId: string;
    numerosARetirar: number;
    custoC: number;
    tempoRelativo: number;
    numeroNovasGeracoes: number;
    dimensaoPopulacao: number;
    probabilidadeCruzamento: number;
    probabilidadeMutacao: number;
}
