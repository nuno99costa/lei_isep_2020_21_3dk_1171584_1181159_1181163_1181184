
export default interface IPercursoDTO {
  id: string,
  nomePercurso: string,
  linhaId: string,
  orientacao: string,
  listaSegmentos: string[],
  noInicialId: string,
  noFinalId: string
  distanciaTotal: number,
  tempoTotal: number
}