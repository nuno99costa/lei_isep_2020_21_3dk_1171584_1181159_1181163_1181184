export default class UtilsMap {
  
   public static toHexadecimal(rgb : string ) {
        var a = rgb.split("(")[1].split(")")[0];
        var c : string[] = a.split(",");
        var index = 0;

        return "#" + this.componentToHex(parseInt(c[0])) + this.componentToHex(parseInt(c[1])) + this.componentToHex(parseInt(c[2]));
    }

    public static componentToHex(c : number) {
        var hex = c.toString(16);
        return hex.length == 1 ? "0" + hex : hex;
    }

    public static delay(ms: number) {​​​​​
      return new Promise(resolve => setTimeout(resolve, ms));
    }
}