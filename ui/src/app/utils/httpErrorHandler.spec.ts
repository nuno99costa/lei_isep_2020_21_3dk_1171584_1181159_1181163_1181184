import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import INoDTO from '../dto/INoDTO';
import HTTPErrorHandler from './httpErrorHandler';


describe('HTTPErrorHandler', () => {

  //--------------------------------------------------SETUP DOS TESTES--------------------------------------

  let httpErrorHandler : HTTPErrorHandler;
  //Antes de cada teste
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HTTPErrorHandler,]
    });
  });

  //Perguntar o que faz ao professor
  beforeEach(inject([HTTPErrorHandler], (errorHandler: HTTPErrorHandler) => {
    httpErrorHandler = errorHandler;
  }));

  //----------------------------------------------Começar os testes-----------------------------------------------
  it('should be defined', () => {
    expect(httpErrorHandler).toBeTruthy();
  });

  it('should return "Error Event"', () => {
    
    let error : HttpErrorResponse = new HttpErrorResponse({status: 404});
    const testResult = httpErrorHandler.handleError(error);
    expect(testResult).toEqual("Status: " + 404 +  "\nMessage: Not Found");

  });


  

});