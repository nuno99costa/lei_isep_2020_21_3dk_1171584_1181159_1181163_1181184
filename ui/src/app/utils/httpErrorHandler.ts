import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler } from '@angular/core';


export default class HTTPErrorHandler implements ErrorHandler{

    handleError(error: HttpErrorResponse): string {
        if(error.error instanceof ErrorEvent){
            return "Error Event";
        }else{
            switch(error.status){
                case 404:
                    return "Status: " + error.status +  "\nMessage: Not Found"
                break;

                default:
                    return "Status: " + error.status + "\nMessage: " + error.message
                break;
            }
            
        }

        
    }

}