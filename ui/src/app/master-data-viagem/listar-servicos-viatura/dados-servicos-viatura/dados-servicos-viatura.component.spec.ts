import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';

import { DadosServicosViaturaComponent } from './dados-servicos-viatura.component';

describe('DadosServicosViaturaComponent', () => {
  let component: DadosServicosViaturaComponent;
  let fixture: ComponentFixture<DadosServicosViaturaComponent>;

  const svServiceSpy: jasmine.SpyObj<ServicoViaturaService> = jasmine.createSpyObj('servicoTripulanteService', ['listaServicosViatura']);
  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);

  svServiceSpy.listaServicosViatura.and.returnValue(Promise.resolve([]));
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosServicosViaturaComponent ],
      imports: [BrowserModule],
      providers: [{provide: Title, useValue: titleServiceSpy}, {provide: ServicoViaturaService, useValue: svServiceSpy},{provide: MatDialog, useValue: matDialogSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosServicosViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
