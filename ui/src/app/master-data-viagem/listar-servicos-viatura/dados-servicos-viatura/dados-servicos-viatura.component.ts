import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';
import { IServicoViaturaService } from 'src/app/services/IServices/IServicoViaturaService';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';

export interface DialogData {
  svId: string;
  svDuracaoSegundos: number;
  svViagensIds: string[];
}

@Component({
  selector: 'app-dados-servicos-viatura',
  templateUrl: './dados-servicos-viatura.component.html',
  styleUrls: ['./dados-servicos-viatura.component.css']
})
export class DadosServicosViaturaComponent implements OnInit {

  private servicoViaturaService: IServicoViaturaService;

  constructor(private titleService: Title, servicoViaturaService: ServicoViaturaService, public dialog: MatDialog) {
    this.titleService.setTitle("AIT: MDV");
    this.servicoViaturaService = servicoViaturaService;

  }

  listaServicosViatura: IServicoViaturaDTO[] = [];
  servicoViatura: IServicoViaturaDTO;

  async ngOnInit(): Promise<void> {
    this.listaServicosViatura = await this.servicoViaturaService.listaServicosViatura();
  }

  openDialog(id: string) {
    let index;
    for(index = 0; index < this.listaServicosViatura.length; index++){
      if (this.listaServicosViatura[index].id == id) this.servicoViatura = this.listaServicosViatura[index]; 
    }

    this.dialog.open(DadosServicoViaturaDialogComponent, {
      data: {
        svId: this.servicoViatura.id,
        svDuracaoSegundos: this.servicoViatura.duracaoSegundos,
        svViagensIds: this.servicoViatura.viagensIds
      }
    });
  }
}

@Component({
  selector: 'dados-servico-viatura-dialog',
  templateUrl: 'dados-servico-viatura-dialog.component.html',
})
export class DadosServicoViaturaDialogComponent {

  svId: string;
  svDuracaoSegundos: number;
  svViagensIds: string[];

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.svId = data.svId;
    this.svDuracaoSegundos = data.svDuracaoSegundos;
    this.svViagensIds = data.svViagensIds;
  }

}
