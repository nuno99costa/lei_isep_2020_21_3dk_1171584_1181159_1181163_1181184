import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-listar-servicos-viatura',
  templateUrl: './listar-servicos-viatura.component.html',
  styleUrls: ['./listar-servicos-viatura.component.css']
})
export class ListarServicosViaturaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
