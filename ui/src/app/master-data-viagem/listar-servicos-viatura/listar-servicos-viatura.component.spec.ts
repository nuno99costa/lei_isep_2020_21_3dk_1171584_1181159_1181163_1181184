import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarServicosViaturaComponent } from './listar-servicos-viatura.component';

describe('ListarServicosViaturaComponent', () => {
  let component: ListarServicosViaturaComponent;
  let fixture: ComponentFixture<ListarServicosViaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarServicosViaturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarServicosViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
