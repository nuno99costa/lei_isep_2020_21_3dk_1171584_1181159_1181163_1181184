import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-tripulante',
  templateUrl: './criar-tripulante.component.html',
  styleUrls: ['./criar-tripulante.component.css']
})
export class CriarTripulanteComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
