import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import ITipoTripulanteDTO from 'src/app/dto/ITipoTripulanteDTO';
import ITripulanteDTO from 'src/app/dto/ITripulanteDTO';
import { ITipoTripulanteService } from 'src/app/services/IServices/ITipoTripulanteService';
import { ITripulanteService } from 'src/app/services/IServices/ITripulanteService';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import { TripulanteService } from 'src/app/services/tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-tripulante',
  templateUrl: './dados-tripulante.component.html',
  styleUrls: ['./dados-tripulante.component.css']
})
export class DadosTripulanteComponent implements OnInit {

  private tripulanteService: ITripulanteService;
  private tipoTripulanteService: ITipoTripulanteService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private datePipe: DatePipe, private titleService: Title, tripulanteService: TripulanteService, tipoTripulanteService: TipoTripulanteService, httpErrorHandler: HTTPErrorHandler, public dialog: MatDialog) {
    this.titleService.setTitle('AIT: MDV');
    this.tripulanteService = tripulanteService;
    this.tipoTripulanteService = tipoTripulanteService;
    this.httpErrorHandler = httpErrorHandler;
  }

  NMecanograficoTripulante: string;
  nomeTripulante: string;
  dataNascimentoTripulante: Date;
  NCCidadaoTripulante: string;
  NCConducaoTripulante: string;
  NIFTripulante: string;
  tipoTripulante: string;
  listaTipoTripulante: ITipoTripulanteDTO[] = [];
  dataEntradaEmpresaTripulante: Date;
  dataSaidaEmpresaTripulante: Date;
  dataLicencaConducaoTripulante: Date;
  checkBoxRGPD: boolean = false;

  async ngOnInit(): Promise<void> {
    this.listaTipoTripulante = await this.tipoTripulanteService.listaTiposTripulante();
  }

  submeter() {

    // Cria DTO com os valores do input
    const tripulante = {
      nMecanografico: this.NMecanograficoTripulante,
      nome: this.nomeTripulante,
      dataNascimento: this.datePipe.transform(this.dataNascimentoTripulante, 'dd/MM/yyyy'),
      NCCidadao: this.NCCidadaoTripulante,
      NCConducao: this.NCConducaoTripulante,
      NIF: this.NIFTripulante,
      TipoTripulante: this.tipoTripulante,
      dataEntradaEmpresa: this.datePipe.transform(this.dataEntradaEmpresaTripulante, 'dd/MM/yyyy'),
      dataSaidaEmpresa: this.datePipe.transform(this.dataSaidaEmpresaTripulante, 'dd/MM/yyyy'),
      dataLicencaConducao: this.datePipe.transform(this.dataLicencaConducaoTripulante, 'dd/MM/yyyy')
    } as ITripulanteDTO;

    console.log(tripulante);
    try {

      this.tripulanteService.validarInput(tripulante, this.checkBoxRGPD);

      // Resposta do back-end (devolve um Observable). Função subscribe de Observable:
      // Parâmetro 1: Se a resposta for sucesso
      // Parâmetro 2: Se a resposta for de erro
      this.tripulanteService.criarTripulante(tripulante).subscribe(values => {
        alert('Sucesso! O seu objeto foi criado com o número mecanográfico: ' + values.nMecanografico);
      }
        , error => {// Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert('Something happened (not http response error)');
          }

        });

    } catch (err) {
      alert(err);
    }
  }

  openDialog() {
    this.dialog.open(TripulanteDialogComponent, {
      height: '400px',
      width: '650px',
    });
  }
}

@Component({
  selector: 'tripulante-dialog',
  templateUrl: 'tripulante-dialog.component.html',
})
export class TripulanteDialogComponent {}
