import { DatePipe } from '@angular/common';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import { TipoTripulanteService } from 'src/app/services/tipo-tripulante.service';
import { TripulanteService } from 'src/app/services/tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosTripulanteComponent } from './dados-tripulante.component';

describe('DadosTripulanteComponent', () => {
  let component: DadosTripulanteComponent;
  let fixture: ComponentFixture<DadosTripulanteComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const datePipeSpy: jasmine.SpyObj<DatePipe> = jasmine.createSpyObj('datePipe', ['transform']);
  const tServiceSpy: jasmine.SpyObj<TripulanteService> = jasmine.createSpyObj('tripulanteService', ['validarInput']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);
  const tipoTripulanteServiceSpy: jasmine.SpyObj<TipoTripulanteService> = jasmine.createSpyObj('TipoTripulanteService', ['criarTipoTripulante', 'validarTipoTripulante', 'listaTiposTripulante']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosTripulanteComponent ],
      imports: [BrowserModule],
      providers: [{provide: DatePipe, useValue: datePipeSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: TripulanteService, useValue: tServiceSpy},{provide: MatDialog, useValue: matDialogSpy},{provide: TipoTripulanteService, useValue: tipoTripulanteServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
