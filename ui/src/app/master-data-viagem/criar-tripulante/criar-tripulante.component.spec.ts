import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarTripulanteComponent } from './criar-tripulante.component';

describe('CriarTripulanteComponent', () => {
  let component: CriarTripulanteComponent;
  let fixture: ComponentFixture<CriarTripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarTripulanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
