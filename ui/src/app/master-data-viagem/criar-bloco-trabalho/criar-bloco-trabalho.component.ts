import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-bloco-trabalho',
  templateUrl: './criar-bloco-trabalho.component.html',
  styleUrls: ['./criar-bloco-trabalho.component.css']
})
export class CriarBlocoTrabalhoComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
