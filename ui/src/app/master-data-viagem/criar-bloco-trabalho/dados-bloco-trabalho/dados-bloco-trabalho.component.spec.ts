import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BlocoTrabalhoService } from 'src/app/services/bloco-trabalho.service';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosBlocoTrabalhoComponent } from './dados-bloco-trabalho.component';

describe('DadosBlocoTrabalhoComponent', () => {
  let component: DadosBlocoTrabalhoComponent;
  let fixture: ComponentFixture<DadosBlocoTrabalhoComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<ServicoViaturaService> = jasmine.createSpyObj('servicoViaturaService', ['listaServicosViatura']);
  const btServiceSpy: jasmine.SpyObj<BlocoTrabalhoService> = jasmine.createSpyObj('blocoTrabalhoService', ['dateTimeEmSegundos', 'validarBlocoTrabalho', 'criarBlocosTrabalho']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);

  svServiceSpy.listaServicosViatura.and.returnValue(Promise.resolve([]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosBlocoTrabalhoComponent ],
      imports: [BrowserModule],
      providers: [{provide: ServicoViaturaService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: BlocoTrabalhoService, useValue: btServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosBlocoTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
