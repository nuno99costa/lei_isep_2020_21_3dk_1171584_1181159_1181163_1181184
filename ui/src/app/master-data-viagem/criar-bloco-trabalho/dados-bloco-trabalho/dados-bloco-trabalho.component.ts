import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import ICreatingBlocosTrabalhoDTO from 'src/app/dto/ICreatingBlocosTrabalhoDTO';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';
import { BlocoTrabalhoService } from 'src/app/services/bloco-trabalho.service';
import { IBlocoTrabalhoService } from 'src/app/services/IServices/IBlocoTrabalhoService';
import { IServicoViaturaService } from 'src/app/services/IServices/IServicoViaturaService';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-bloco-trabalho',
  templateUrl: './dados-bloco-trabalho.component.html',
  styleUrls: ['./dados-bloco-trabalho.component.css']
})
export class DadosBlocoTrabalhoComponent implements OnInit {

  private httpErrorHandler: HTTPErrorHandler;
  private blocoTrabalhoService: IBlocoTrabalhoService;
  private servicoViaturaService: IServicoViaturaService;

  constructor(private titleService: Title, servicoViaturaService: ServicoViaturaService, httpErrorHandler: HTTPErrorHandler, blocoTrabalhoService: BlocoTrabalhoService) {
    this.titleService.setTitle("AIT: MDV");
    this.blocoTrabalhoService = blocoTrabalhoService;
    this.httpErrorHandler = httpErrorHandler;
    this.servicoViaturaService = servicoViaturaService;
  }

  duracao: string;
  numeroMaximoBlocos: number;
  ServicoViaturaId: string;
  listaAllServicosViatura: IServicoViaturaDTO[] = [];

  async ngOnInit(): Promise<void> {
    this.listaAllServicosViatura = await this.servicoViaturaService.listaServicosViatura();
  }

  submeter() {
    const duracaoEmSec = this.blocoTrabalhoService.dateTimeEmSegundos(this.duracao);

    const blocosTrabalho = {
      DuracaoMaxima: duracaoEmSec,
      numeroMaximoBlocos: this.numeroMaximoBlocos,
      ServicoViaturaId: this.ServicoViaturaId
    } as ICreatingBlocosTrabalhoDTO;

    try {

      this.blocoTrabalhoService.validarBlocoTrabalho(blocosTrabalho);
      this.blocoTrabalhoService.criarBlocosTrabalho(blocosTrabalho).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado.");
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch (error) {
      alert(error);
    }
  }

}
