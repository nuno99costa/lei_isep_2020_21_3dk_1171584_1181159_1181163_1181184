import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarBlocoTrabalhoComponent } from './criar-bloco-trabalho.component';

describe('CriarBlocoTrabalhoComponent', () => {
  let component: CriarBlocoTrabalhoComponent;
  let fixture: ComponentFixture<CriarBlocoTrabalhoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarBlocoTrabalhoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarBlocoTrabalhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
