import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarServicosTripulanteComponent } from './listar-servicos-tripulante.component';

describe('ListarServicosTripulanteComponent', () => {
  let component: ListarServicosTripulanteComponent;
  let fixture: ComponentFixture<ListarServicosTripulanteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarServicosTripulanteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarServicosTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
