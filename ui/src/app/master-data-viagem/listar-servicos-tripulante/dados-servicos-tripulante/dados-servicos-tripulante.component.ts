import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Title } from '@angular/platform-browser';
import IServicoTripulanteDTO from 'src/app/dto/IServicoTripulanteDTO';
import { IServicoTripulanteService } from 'src/app/services/IServices/IServicoTripulanteService';
import { ServicoTripulanteService } from 'src/app/services/servico-tripulante.service';

export interface DialogData {
  stId: string;
  stIdsBlocosTrabalho: string[];
  stDuracao: number;
}

@Component({
  selector: 'app-dados-servicos-tripulante',
  templateUrl: './dados-servicos-tripulante.component.html',
  styleUrls: ['./dados-servicos-tripulante.component.css']
})
export class DadosServicosTripulanteComponent implements OnInit {

  private servicoTripulanteService: IServicoTripulanteService;

  constructor(private titleService: Title, servicoTripulanteService: ServicoTripulanteService, public dialog: MatDialog) {
    this.titleService.setTitle("AIT: MDV");
    this.servicoTripulanteService = servicoTripulanteService;
  }

  listaServicosTripulante: IServicoTripulanteDTO[] = [];
  servicoTripulante: IServicoTripulanteDTO;

  async ngOnInit(): Promise<void> {
    this.listaServicosTripulante = await this.servicoTripulanteService.listaServicosTripulantes();
  }

  openDialog(id: string) {
    let index;
    for (index = 0; index < this.listaServicosTripulante.length; index++) {
      if (this.listaServicosTripulante[index].id == id) this.servicoTripulante = this.listaServicosTripulante[index];
    }

    this.dialog.open(DadosServicosTripulanteDialogComponent, {
      data: {
        stId: this.servicoTripulante.id,
        stIdsBlocosTrabalho: this.servicoTripulante.idsBlocosTrabalho,
        stDuracao: this.servicoTripulante.duracao
      }
    });
  }
}

@Component({
  selector: 'dados-servicos-tripulante-dialog',
  templateUrl: 'dados-servicos-tripulante-dialog.component.html',
})
export class DadosServicosTripulanteDialogComponent {

  stId: string;
  stIdsBlocosTrabalho: string[];
  stDuracao: number;

  constructor(@Inject(MAT_DIALOG_DATA) public data: DialogData) {
    this.stId = data.stId;
    this.stIdsBlocosTrabalho = data.stIdsBlocosTrabalho;
    this.stDuracao = data.stDuracao;
  }

}

