import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ServicoTripulanteService } from 'src/app/services/servico-tripulante.service';

import { DadosServicosTripulanteComponent } from './dados-servicos-tripulante.component';

describe('DadosServicosTripulanteComponent', () => {
  let component: DadosServicosTripulanteComponent;
  let fixture: ComponentFixture<DadosServicosTripulanteComponent>;

  const stServiceSpy: jasmine.SpyObj<ServicoTripulanteService> = jasmine.createSpyObj('servicoTripulanteService', ['listaServicosTripulantes']);
  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  
  const matDialogSpy: jasmine.SpyObj<MatDialog> = jasmine.createSpyObj('dialog', ['open']);

  stServiceSpy.listaServicosTripulantes.and.returnValue(Promise.resolve([]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosServicosTripulanteComponent ],
      imports: [BrowserModule],
      providers: [{provide: Title, useValue: titleServiceSpy}, {provide: ServicoTripulanteService, useValue: stServiceSpy},{provide: MatDialog, useValue: matDialogSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosServicosTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
