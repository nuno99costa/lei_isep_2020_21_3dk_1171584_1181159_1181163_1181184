import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-listar-servicos-tripulante',
  templateUrl: './listar-servicos-tripulante.component.html',
  styleUrls: ['./listar-servicos-tripulante.component.css']
})
export class ListarServicosTripulanteComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
