import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { LinhaService } from 'src/app/services/linha.service';
import { PercursoService } from 'src/app/services/percurso.service';
import { ViagemService } from 'src/app/services/viagem.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosViagemComponent } from './dados-viagem.component';

describe('DadosViagemComponent', () => {
  let component: DadosViagemComponent;
  let fixture: ComponentFixture<DadosViagemComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const vServiceSpy: jasmine.SpyObj<ViagemService> = jasmine.createSpyObj('viagemService', ['listaViagens']);
  const percursoServiceSpy: jasmine.SpyObj<PercursoService> = jasmine.createSpyObj('percursoService', ['addElementToArray', 'deleteElementOfArray', 'validarInput', 'validarLinha', 'criarPercurso', 'carregarDadosMapa', 'listarPercursoLinha']);
  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('linhaService', ['validarLinha', 'criarLinha', 'addElementToArray', 'deleteElementOfArray', 'listaLinhas']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);

  linhaServiceSpy.listaLinhas.and.returnValue(Promise.resolve([]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosViagemComponent ],
      imports: [BrowserModule],
      providers: [{provide: ViagemService, useValue: vServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: PercursoService, useValue: percursoServiceSpy}, {provide: LinhaService, useValue: linhaServiceSpy},  {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosViagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
