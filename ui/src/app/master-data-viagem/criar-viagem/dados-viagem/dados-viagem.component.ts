import { Time } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AmazingTimePickerService } from 'amazing-time-picker';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import IViagemDTO from 'src/app/dto/IViagemDTO';
import { ILinhaService } from 'src/app/services/IServices/ILinhaService';
import { IPercursoService } from 'src/app/services/IServices/IPercursoService';
import { IViagemService } from 'src/app/services/IServices/IViagemService';
import { LinhaService } from 'src/app/services/linha.service';
import { PercursoService } from 'src/app/services/percurso.service';
import { ViagemService } from 'src/app/services/viagem.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-viagem',
  templateUrl: './dados-viagem.component.html',
  styleUrls: ['./dados-viagem.component.css']
})
export class DadosViagemComponent implements OnInit {

  private viagemService: IViagemService;
  private linhaService: ILinhaService;
  private percursoService: IPercursoService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, viagemService: ViagemService, linhaService: LinhaService, percursoService: PercursoService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDV");
    this.viagemService = viagemService;
    this.linhaService = linhaService;
    this.percursoService = percursoService;
    this.httpErrorHandler = httpErrorHandler;
  }

  viagemId: string;
  linhaIdSelecionado: string;
  percursoIdSelecionado: string;
  listaAllLinhas: ILinhaDTO[];
  listaAllPercursos: IPercursoDTO[];
  time: string;

  async ngOnInit(): Promise<void> {
    this.listaAllLinhas = await this.linhaService.listaLinhas();
  }

  async mostrarPercursos() {
    this.listaAllPercursos = await this.percursoService.listarPercursoLinha(this.linhaIdSelecionado);
  }

  async submeter() {

    const horarioEmSec = this.viagemService.dateTimeEmSegundos(this.time);

    const listaNos = await this.percursoService.getNosPercursoId(this.percursoIdSelecionado);

    const viagem = {
      viagemId: this.viagemId,
      IdPercurso: this.percursoIdSelecionado,
      IdLinha: this.linhaIdSelecionado,
      IdNoSaida: listaNos[0].id,
      HorarioSaida: horarioEmSec
    } as IViagemDTO;

    try {
      this.viagemService.validarViagem(viagem);

      this.viagemService.criarViagem(viagem).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.viagemId);
      },
        error => {
          if (error instanceof HttpErrorResponse) {
            console.log("ups");
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }
        });

    } catch (err) {
      alert(err);
    }
  }
}
