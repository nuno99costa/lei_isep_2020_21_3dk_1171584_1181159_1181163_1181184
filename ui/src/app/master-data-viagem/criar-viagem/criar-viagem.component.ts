import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-viagem',
  templateUrl: './criar-viagem.component.html',
  styleUrls: ['./criar-viagem.component.css']
})
export class CriarViagemComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
