import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarViagemComponent } from './criar-viagem.component';

describe('CriarViagemComponent', () => {
  let component: CriarViagemComponent;
  let fixture: ComponentFixture<CriarViagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarViagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarViagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
