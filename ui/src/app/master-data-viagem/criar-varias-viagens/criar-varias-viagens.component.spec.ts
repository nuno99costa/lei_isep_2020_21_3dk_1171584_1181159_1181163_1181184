import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarVariasViagensComponent } from './criar-varias-viagens.component';

describe('CriarVariasViagensComponent', () => {
  let component: CriarVariasViagensComponent;
  let fixture: ComponentFixture<CriarVariasViagensComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarVariasViagensComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarVariasViagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
