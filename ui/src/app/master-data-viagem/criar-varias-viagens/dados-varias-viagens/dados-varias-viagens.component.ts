import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AmazingTimePickerService } from 'amazing-time-picker';
import ILinhaDTO from 'src/app/dto/ILinhaDTO';
import INoDTO from 'src/app/dto/INoDTO';
import IPercursoDTO from 'src/app/dto/IPercursoDTO';
import IViagemDTO from 'src/app/dto/IViagemDTO';
import { ILinhaService } from 'src/app/services/IServices/ILinhaService';
import { IPercursoService } from 'src/app/services/IServices/IPercursoService';
import { IViagemService } from 'src/app/services/IServices/IViagemService';
import { LinhaService } from 'src/app/services/linha.service';
import { PercursoService } from 'src/app/services/percurso.service';
import { ViagemService } from 'src/app/services/viagem.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-varias-viagens',
  templateUrl: './dados-varias-viagens.component.html',
  styleUrls: ['./dados-varias-viagens.component.css']
})
export class DadosVariasViagensComponent implements OnInit {

  private viagemService: IViagemService;
  private linhaService: ILinhaService;
  private percursoService: IPercursoService;

  constructor(private titleService: Title, viagemService: ViagemService, linhaService: LinhaService, percursoService: PercursoService) {
    this.titleService.setTitle("AIT: MDV");
    this.viagemService = viagemService;
    this.linhaService = linhaService;
    this.percursoService = percursoService;
  }

  numeroViagens: number;
  linhaIdSelecionado: string;
  percursoIdaId: string;
  percursoVoltaId: string;
  listaAllLinhas: ILinhaDTO[];
  listaAllPercursos: IPercursoDTO[];
  frequencia: string;
  horaInicio: string;

  horarioEmSec: number;
  frequenciaEmSec: number;

  async ngOnInit(): Promise<void> {
    this.listaAllLinhas = await this.linhaService.listaLinhas();
  }

  async mostrarPercursos() {
    this.listaAllPercursos = await this.percursoService.listarPercursoLinha(this.linhaIdSelecionado);
  }

  async submeter() {
    var viagensParalelas: number;

    this.horarioEmSec = this.viagemService.dateTimeEmSegundos(this.horaInicio);
    this.frequenciaEmSec = this.viagemService.dateTimeEmSegundos(this.frequencia);

    var ida: IPercursoDTO = await this.viagemService.procurarPercurso(this.percursoIdaId, this.listaAllPercursos);
    var volta: IPercursoDTO = await this.viagemService.procurarPercurso(this.percursoVoltaId, this.listaAllPercursos);

    const duracaoTotal = volta.tempoTotal + ida.tempoTotal;

    console.log("Duracao total: " + duracaoTotal);

    const listaNosIda = await this.percursoService.getNosPercursoId(this.percursoIdaId);
    const listaNosVolta = await this.percursoService.getNosPercursoId(this.percursoVoltaId);

    if (duracaoTotal > this.frequenciaEmSec && this.numeroViagens > 2) {
      viagensParalelas = parseInt(window.prompt("A frequência é inferior ao tempo total de viagem de ida e volta!", "Nº viagens paralelas"));
      this.viagemService.criarViagensParalelas(duracaoTotal, this.linhaIdSelecionado, this.frequenciaEmSec, this.numeroViagens, this.horarioEmSec, ida, volta, listaNosIda, listaNosVolta, viagensParalelas)
    } else {
      console.log("Horario: " + this.horarioEmSec);
      this.viagemService.criarVariasViagens(this.linhaIdSelecionado, this.frequenciaEmSec, this.numeroViagens, this.horarioEmSec, ida, volta, listaNosIda, listaNosVolta);
    }

  }

}
