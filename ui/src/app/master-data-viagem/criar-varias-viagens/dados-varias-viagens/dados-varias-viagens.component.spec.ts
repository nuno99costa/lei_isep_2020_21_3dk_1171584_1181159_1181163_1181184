import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { LinhaService } from 'src/app/services/linha.service';
import { PercursoService } from 'src/app/services/percurso.service';
import { ViagemService } from 'src/app/services/viagem.service';

import { DadosVariasViagensComponent } from './dados-varias-viagens.component';

describe('DadosVariasViagensComponent', () => {
  let component: DadosVariasViagensComponent;
  let fixture: ComponentFixture<DadosVariasViagensComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const vServiceSpy: jasmine.SpyObj<ViagemService> = jasmine.createSpyObj('viagemService', ['listaViagens']);
  const percursoServiceSpy: jasmine.SpyObj<PercursoService> = jasmine.createSpyObj('percursoService', ['addElementToArray', 'deleteElementOfArray', 'validarInput', 'validarLinha', 'criarPercurso', 'carregarDadosMapa', 'listarPercursoLinha']);
  const linhaServiceSpy: jasmine.SpyObj<LinhaService> = jasmine.createSpyObj('linhaService', ['validarLinha', 'criarLinha', 'addElementToArray', 'deleteElementOfArray', 'listaLinhas']);


  linhaServiceSpy.listaLinhas.and.returnValue(Promise.resolve([]));
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosVariasViagensComponent ],
      imports: [BrowserModule],
      providers: [{provide: ViagemService, useValue: vServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: PercursoService, useValue: percursoServiceSpy}, {provide: LinhaService, useValue: linhaServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosVariasViagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
