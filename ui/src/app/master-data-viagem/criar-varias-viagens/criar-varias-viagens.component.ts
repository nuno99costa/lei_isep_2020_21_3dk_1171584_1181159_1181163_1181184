import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-varias-viagens',
  templateUrl: './criar-varias-viagens.component.html',
  styleUrls: ['./criar-varias-viagens.component.css']
})
export class CriarVariasViagensComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
