import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MasterDataViagemComponent } from './master-data-viagem.component';

describe('MasterDataViagemComponent', () => {
  let component: MasterDataViagemComponent;
  let fixture: ComponentFixture<MasterDataViagemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MasterDataViagemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MasterDataViagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
