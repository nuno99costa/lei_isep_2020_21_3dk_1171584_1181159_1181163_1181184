import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InserirGLXService } from 'src/app/services/inserir-glx.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-inserir-glx-mdv',
  templateUrl: './inserir-glx-mdv.component.html',
  styleUrls: ['./inserir-glx-mdv.component.css']
})
export class InserirGlxMdvComponent implements OnInit {

  inserirGLXService: InserirGLXService;
  httpErrorHandler: HTTPErrorHandler
  file: File | null | undefined;

  constructor(private titleService: Title, inserirGLXService: InserirGLXService, httpErrorHandler: HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDV");
    this.inserirGLXService = inserirGLXService;
    this.httpErrorHandler = httpErrorHandler;
  }

  ngOnInit(): void {
  }

  fileChangeEvent(e: Event) {
    const element = (e.currentTarget as HTMLInputElement);
    const label = document.getElementById('labelFicheiroGLX');
    if (element && label) {
      this.file = element.files?.item(0);
      if (this.file) {
        label.innerHTML = this.file.name;
      }
    }
  }

  async submeter() {
    try {
      this.inserirGLXService.validarInput(this.file);

      const result = await this.inserirGLXService.inserirGLXMdv(this.file as File);
      result.subscribe(values => {
        alert(values);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }
        })
    } catch (err) {
      alert(err);
    }
  }

}
