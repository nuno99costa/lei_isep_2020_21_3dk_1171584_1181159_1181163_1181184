import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-master-data-viagem',
  templateUrl: './master-data-viagem.component.html',
  styleUrls: ['./master-data-viagem.component.css']
})
export class MasterDataViagemComponent implements OnInit {
  
  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }
  
  escolha="";

  ngOnInit(): void {
  }

  scrollTo(id : string){
    document.getElementById(id)?.scrollIntoView();
    console.log('Scrolled');
  }
}
