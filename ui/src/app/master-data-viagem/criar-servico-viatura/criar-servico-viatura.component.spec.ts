import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CriarServicoViaturaComponent } from './criar-servico-viatura.component';

describe('CriarServicoViaturaComponent', () => {
  let component: CriarServicoViaturaComponent;
  let fixture: ComponentFixture<CriarServicoViaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CriarServicoViaturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CriarServicoViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
