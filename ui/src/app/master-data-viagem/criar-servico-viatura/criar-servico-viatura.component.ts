import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-servico-viatura',
  templateUrl: './criar-servico-viatura.component.html',
  styleUrls: ['./criar-servico-viatura.component.css']
})
export class CriarServicoViaturaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
