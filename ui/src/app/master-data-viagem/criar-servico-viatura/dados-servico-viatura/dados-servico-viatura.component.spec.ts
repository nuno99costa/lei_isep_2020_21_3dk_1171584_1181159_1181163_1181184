import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import { ViagemService } from 'src/app/services/viagem.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosServicoViaturaComponent } from './dados-servico-viatura.component';

describe('DadosServicoViaturaComponent', () => {
  let component: DadosServicoViaturaComponent;
  let fixture: ComponentFixture<DadosServicoViaturaComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<ServicoViaturaService> = jasmine.createSpyObj('servicoViaturaService', ['listaServicosViatura']);
  const vServiceSpy: jasmine.SpyObj<ViagemService> = jasmine.createSpyObj('viagemService', ['listaViagens']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);


  vServiceSpy.listaViagens.and.returnValue(Promise.resolve([]));
  
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosServicoViaturaComponent ],
      imports: [BrowserModule],
      providers: [{provide: ServicoViaturaService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: ViagemService, useValue: vServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosServicoViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
