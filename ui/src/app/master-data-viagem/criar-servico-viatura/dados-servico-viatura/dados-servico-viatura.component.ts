import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AmazingTimePickerService } from 'amazing-time-picker';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';
import IViagemDTO from 'src/app/dto/IViagemDTO';
import IViaturaDTO from 'src/app/dto/IViaturaDTO';
import { IServicoViaturaService } from 'src/app/services/IServices/IServicoViaturaService';
import { IViagemService } from 'src/app/services/IServices/IViagemService';
import { IViaturaService } from 'src/app/services/IServices/IViaturaService';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import { ViagemService } from 'src/app/services/viagem.service';
import { ViaturaService } from 'src/app/services/viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-servico-viatura',
  templateUrl: './dados-servico-viatura.component.html',
  styleUrls: ['./dados-servico-viatura.component.css'],
})
export class DadosServicoViaturaComponent implements OnInit {

  private httpErrorHandler: HTTPErrorHandler;
  private servicoViaturaService: IServicoViaturaService;
  private viagemService: IViagemService;

  constructor(private titleService: Title, httpErrorHandler: HTTPErrorHandler, servicoViaturaService: ServicoViaturaService, viagemService: ViagemService) {
    this.titleService.setTitle("AIT: MDV");
    this.httpErrorHandler = httpErrorHandler;
    this.servicoViaturaService = servicoViaturaService;
    this.viagemService = viagemService;
  }

  servicoViaturaId: string;
  listaAllViagens: IViagemDTO[];
  idViagem: string;

  listaViagensId: string[] = [];
  duracao: number = 0;

  async ngOnInit(): Promise<void> {
    this.listaAllViagens = await this.viagemService.listaViagens();
  }

  addElementToArray() {
    try {
      this.listaViagensId = this.servicoViaturaService.addElementToArray(this.idViagem, this.listaViagensId);
    } catch (error) {
      alert(error);
    }
  }

  deleteElementOfArray() {
    try {
      this.listaViagensId = this.servicoViaturaService.deleteElementOfArray(this.idViagem, this.listaViagensId);
    } catch (error) {
      alert(error);
    }
  }

  submeter() {
    const servicoViatura = {
      id: this.servicoViaturaId,
      duracaoSegundos: this.duracao,
      viagensIds: this.listaViagensId
    } as IServicoViaturaDTO;

    try {
      this.servicoViaturaService.validarServicoViatura(servicoViatura);
      this.servicoViaturaService.criarServicoViatura(servicoViatura).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch (error) {
      alert(error);
    }
  }

}
