import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-definir-viatura',
  templateUrl: './definir-viatura.component.html',
  styleUrls: ['./definir-viatura.component.css']
})
export class DefinirViaturaComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
