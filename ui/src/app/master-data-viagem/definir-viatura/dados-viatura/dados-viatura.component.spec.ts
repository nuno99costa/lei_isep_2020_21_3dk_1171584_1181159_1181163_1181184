import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import { ViaturaService } from 'src/app/services/viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosViaturaComponent } from './dados-viatura.component';

describe('DadosViaturaComponent', () => {
  let component: DadosViaturaComponent;
  let fixture: ComponentFixture<DadosViaturaComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const vServiceSpy: jasmine.SpyObj<ViaturaService> = jasmine.createSpyObj('viaturaService', ['criarViatura']);
  const tipoViaturaServiceSpy: jasmine.SpyObj<TipoViaturaService> = jasmine.createSpyObj('tipoViaturaService', ['listaTiposViatura']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosViaturaComponent ],
      imports: [BrowserModule],
      providers: [{provide: ViaturaService, useValue: vServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: TipoViaturaService, useValue: tipoViaturaServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
