import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import ITipoViaturaDTO from 'src/app/dto/ITipoViaturaDTO';
import IViaturaDTO from 'src/app/dto/IViaturaDTO';
import { ITipoViaturaService } from 'src/app/services/IServices/ITipoViaturaService';
import { IViaturaService } from 'src/app/services/IServices/IViaturaService';
import { TipoViaturaService } from 'src/app/services/tipo-viatura.service';
import { ViaturaService } from 'src/app/services/viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-viatura',
  templateUrl: './dados-viatura.component.html',
  styleUrls: ['./dados-viatura.component.css']
})
export class DadosViaturaComponent implements OnInit {

  private viaturaService: IViaturaService;
  private tipoViaturaService: ITipoViaturaService;
  private httpErrorHandler: HTTPErrorHandler;

  constructor(private titleService: Title, viaturaService : ViaturaService, tipoViaturaService : TipoViaturaService, httpErrorHandler : HTTPErrorHandler) {
    this.titleService.setTitle("AIT: MDV");
    this.viaturaService = viaturaService;
    this.tipoViaturaService = tipoViaturaService;
    this.httpErrorHandler = httpErrorHandler;
  }

  matriculaViatura : string;
  vinViatura: string;
  tipoViaturaId: string;
  dataInput: Date;

  listaTipoViaturas : ITipoViaturaDTO[] = [];

  async ngOnInit(): Promise<void> {
    this.listaTipoViaturas = await this.tipoViaturaService.listaTiposViatura();
  }

  submeter() {
        //Cria DTO com os valores do input
        console.log(this.tipoViaturaId);
        const viatura = {
          vin: this.vinViatura,
          matricula: this.matriculaViatura.toUpperCase(),
          TipoViatura: this.tipoViaturaId,
          data: this.dataInput
        } as IViaturaDTO;

        console.log(viatura.data);
        try {

          this.viaturaService.validarInput(viatura);

          //Resposta do back-end (devolve um Observable). Função subscribe de Observable:
          //Parâmetro 1: Se a resposta for sucesso
          //Parâmetro 2: Se a resposta for de erro
          this.viaturaService.criarViatura(viatura).subscribe(values => {
            alert("Sucesso! O seu objeto foi criado com o vin: " + values.vin);
          }
            , error => {//Parâmetro 2: Se a resposta for de erro
              if (error instanceof HttpErrorResponse) {
                alert(this.httpErrorHandler.handleError(error));
              } else {
                alert("Something happened (not http response error)");
              }

            })

        } catch (err) {
          alert(err);
        }
  }
}
