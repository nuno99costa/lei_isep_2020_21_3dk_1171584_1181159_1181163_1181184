import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DefinirViaturaComponent } from './definir-viatura.component';

describe('DefinirViaturaComponent', () => {
  let component: DefinirViaturaComponent;
  let fixture: ComponentFixture<DefinirViaturaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DefinirViaturaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DefinirViaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
