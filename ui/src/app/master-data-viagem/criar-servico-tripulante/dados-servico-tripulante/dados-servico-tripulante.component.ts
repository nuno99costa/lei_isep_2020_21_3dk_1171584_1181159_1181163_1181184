import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { IBlocoTrabalhoDTO } from 'src/app/dto/IBlocoTrabalhoDTO';
import IServicoTripulanteDTO from 'src/app/dto/IServicoTripulanteDTO';
import IServicoViaturaDTO from 'src/app/dto/IServicoViaturaDTO';
import ITripulanteDTO from 'src/app/dto/ITripulanteDTO';
import { BlocoTrabalhoService } from 'src/app/services/bloco-trabalho.service';
import { IBlocoTrabalhoService } from 'src/app/services/IServices/IBlocoTrabalhoService';
import { IServicoTripulanteService } from 'src/app/services/IServices/IServicoTripulanteService';
import { IServicoViaturaService } from 'src/app/services/IServices/IServicoViaturaService';
import { ITripulanteService } from 'src/app/services/IServices/ITripulanteService';
import { ServicoTripulanteService } from 'src/app/services/servico-tripulante.service';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import { TripulanteService } from 'src/app/services/tripulante.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

@Component({
  selector: 'app-dados-servico-tripulante',
  templateUrl: './dados-servico-tripulante.component.html',
  styleUrls: ['./dados-servico-tripulante.component.css']
})
export class DadosServicoTripulanteComponent implements OnInit {

  private httpErrorHandler: HTTPErrorHandler;
  private servicoTripulanteService: IServicoTripulanteService;
  private servicoViaturaService: IServicoViaturaService;
  private blocoTrabalhoService: IBlocoTrabalhoService;

  constructor(blocoTrabalhoService: BlocoTrabalhoService, servicoViaturaService: ServicoViaturaService, private titleService: Title, httpErrorHandler: HTTPErrorHandler, servicoTripulanteService: ServicoTripulanteService) {
    this.blocoTrabalhoService = blocoTrabalhoService;
    this.servicoViaturaService = servicoViaturaService;
    this.titleService.setTitle("AIT: MDV");
    this.httpErrorHandler = httpErrorHandler;
    this.servicoTripulanteService = servicoTripulanteService;
  }

  servicoTripulanteId: string;
  idBlocoTrabalho: string;

  listaAllBlocosTrabalho: IBlocoTrabalhoDTO[] = [];
  listaBlocosTrabalho: string[] = [];
  listaAllServicosViatura: IServicoViaturaDTO[] = [];

  servicoViaturaId: string;

  async ngOnInit(): Promise<void> {
    this.listaAllServicosViatura = await this.servicoViaturaService.listaServicosViatura();
  }

  async changeServico(){
    console.log(this.servicoViaturaId);
    this.listaAllBlocosTrabalho = await this.blocoTrabalhoService.getBlocosServicoViatura(this.servicoViaturaId);
  }

  addElementToArray() {
    try {
      this.listaBlocosTrabalho = this.servicoTripulanteService.addElementToArray(this.idBlocoTrabalho, this.listaBlocosTrabalho);
    } catch (error) {
      alert(error);
    }
  }

  deleteElementOfArray() {
    try {
      this.listaBlocosTrabalho = this.servicoTripulanteService.deleteElementOfArray(this.idBlocoTrabalho, this.listaBlocosTrabalho);
    } catch (error) {
      alert(error);
    }
  }

  submeter() {

    const servicoTripulante = {
      id: this.servicoTripulanteId,
      idsBlocosTrabalho: this.listaBlocosTrabalho,
      duracao: 0
    } as IServicoTripulanteDTO;

    console.log(servicoTripulante);

    try {
      this.servicoTripulanteService.validarServicoTripulante(servicoTripulante);
      this.servicoTripulanteService.criarServicoTripulante(servicoTripulante).subscribe(values => {
        alert("Sucesso! O seu objeto foi criado com id: " + values.id);
      }
        , error => {//Parâmetro 2: Se a resposta for de erro
          if (error instanceof HttpErrorResponse) {
            alert(this.httpErrorHandler.handleError(error));
          } else {
            alert("Something happened (not http response error)");
          }

        })
    } catch (error) {
      alert(error);
    }
  }

}
