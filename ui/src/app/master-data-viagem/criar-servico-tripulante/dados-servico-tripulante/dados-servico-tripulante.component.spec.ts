import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BlocoTrabalhoService } from 'src/app/services/bloco-trabalho.service';
import { ServicoTripulanteService } from 'src/app/services/servico-tripulante.service';
import { ServicoViaturaService } from 'src/app/services/servico-viatura.service';
import HTTPErrorHandler from 'src/app/utils/httpErrorHandler';

import { DadosServicoTripulanteComponent } from './dados-servico-tripulante.component';

describe('DadosServicoTripulanteComponent', () => {
  let component: DadosServicoTripulanteComponent;
  let fixture: ComponentFixture<DadosServicoTripulanteComponent>;

  const titleServiceSpy: jasmine.SpyObj<Title> = jasmine.createSpyObj('titleService', ['setTitle']);
  const svServiceSpy: jasmine.SpyObj<ServicoViaturaService> = jasmine.createSpyObj('servicoViaturaService', ['listaServicosViatura']);
  const btServiceSpy: jasmine.SpyObj<BlocoTrabalhoService> = jasmine.createSpyObj('blocoTrabalhoService', ['dateTimeEmSegundos', 'validarBlocoTrabalho', 'criarBlocosTrabalho']);
  const httpErrorHandlerSpy: jasmine.SpyObj<HTTPErrorHandler> = jasmine.createSpyObj('httpErrorHandler', ['handleError']);
  const stServiceSpy: jasmine.SpyObj<ServicoTripulanteService> = jasmine.createSpyObj('servicoTripulanteService', ['criarServicoTripulante']);

  svServiceSpy.listaServicosViatura.and.returnValue(Promise.resolve([]));

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DadosServicoTripulanteComponent ],
      imports: [BrowserModule],
      providers: [{provide: ServicoViaturaService, useValue: svServiceSpy}, {provide: Title, useValue: titleServiceSpy}, {provide: HTTPErrorHandler, useValue: httpErrorHandlerSpy}, {provide: BlocoTrabalhoService, useValue: btServiceSpy}, {provide: ServicoTripulanteService, useValue: stServiceSpy}],//Injetaveis do componente
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DadosServicoTripulanteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
