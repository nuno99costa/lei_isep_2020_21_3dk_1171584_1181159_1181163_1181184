import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-criar-servico-tripulante',
  templateUrl: './criar-servico-tripulante.component.html',
  styleUrls: ['./criar-servico-tripulante.component.css']
})
export class CriarServicoTripulanteComponent implements OnInit {

  constructor(private titleService: Title) {
    this.titleService.setTitle("AIT: MDV");
  }

  ngOnInit(): void {
  }

}
