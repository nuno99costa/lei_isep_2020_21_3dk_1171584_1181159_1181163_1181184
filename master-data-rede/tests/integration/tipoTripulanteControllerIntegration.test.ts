const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../config';
import { Result } from '../../src/core/logic/Result';
import TipoTripulanteController from '../../src/controllers/tipoTripulanteController';
import ITipoTripulanteDTO from '../../src/dto/ITipoTripulanteDTO';
import ITipoTripulanteService from '../../src/services/IServices/ITipoTripulanteService';
import { ITipoTripulanteRepo } from '../../src/services/IRepos/ITipoTripulanteRepo';
import TipoTripulante from '../../src/domain/tipoTripulante';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import TipoTripulanteService from '../../src/services/tipoTripulanteService';

describe('TipoTripulanteController', function () {
    let body = {
        "codigo": "abc",
        "descricao": "Gosta de conduzir"
    };

    //Carregar para o container o repositorio
    let tipoTripulanteRepoClass = require('../../src/repos/tipoTripulanteRepo').default;
    let tipoTripulanteRepoInstance: ITipoTripulanteRepo = Container.get(tipoTripulanteRepoClass);
    Container.set(config.repos.tipoTripulante.name, tipoTripulanteRepoInstance);

    //Buscar o repositorio ao container carregado atrás
    tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);

    const tipoTripulanteServiceInstance = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);
    const ctrl = new TipoTripulanteController(tipoTripulanteServiceInstance);

    describe('criarTipoTripulante', function () {
        it('criarTipoTripulante: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;

            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Stub do DTO do nó que vamos "criar".
            const stubValue = {
                id: "aaa",
                codigo: req.body.codigo,
                descricao: req.body.descricao
            } as ITipoTripulanteDTO;

            //No criado com o stub do DTO.
            const stubTipoTripulante = TipoTripulante.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(tipoTripulanteRepoInstance, "save").returns(stubTipoTripulante);

            await ctrl.criarTipoTripulante(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            }));
            sinon.restore();
        });
    });

    describe("getTipoTripulante", function () {
        it("deve retornar todos os tipos de tripulante guardados", async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;

            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            const stubValue = {
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            } as ITipoTripulanteDTO;

            //Tipo Tripulante criado com o stub do DTO.
            const stubTipoTripulante = TipoTripulante.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(tipoTripulanteRepoInstance, "findAll").returns([stubTipoTripulante]);

            await ctrl.getTipoTripulante(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            }]));

            sinon.restore();
        });
    });
});