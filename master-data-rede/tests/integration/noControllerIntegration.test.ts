const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../config';
import { Result } from '../../src/core/logic/Result';
import NoController from '../../src/controllers/noController';
import INoService from '../../src/services/IServices/INoService'
import INoDTO from '../../src/dto/INoDTO';
import { INoRepo } from '../../src/services/IRepos/INoRepo';
import No from '../../src/domain/no';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import NoService from '../../src/services/noService';

describe('NoControllerIntegration', function () {
    let body = {
        "nomeCompleto": 'Espinho',
        "abreviatura": 'ESP',
        "isPontoRendicao": true,
        "isEstacaoRecolha": false,
        "latitude": 12.3,
        "longitude": -8.4,
        "nomeModelo": "busstop"
    };

    //Stub do DTO do nó que vamos "criar". Baseado no req.body
    const stubValue = {
        id: "aaa",
        nomeCompleto: body.nomeCompleto,
        abreviatura: body.abreviatura,
        isPontoRendicao: body.isPontoRendicao,
        isEstacaoRecolha: body.isEstacaoRecolha,
        latitude: body.latitude,
        longitude: body.longitude,
        nomeModelo: body.nomeModelo
    } as INoDTO;

    //No criado com o stub do DTO.
    const stubNo = No.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    describe('criarNo', function () {
        it('criarNo: returns json with values', async function () {

            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Stub do DTO do nó que vamos "criar". Baseado no req.body
            const stubValue = {
                id: "aaa",
                nomeCompleto: req.body.nomeCompleto,
                abreviatura: req.body.abreviatura,
                isPontoRendicao: req.body.isPontoRendicao,
                isEstacaoRecolha: req.body.isEstacaoRecolha,
                latitude: req.body.latitude,
                longitude: req.body.longitude,
                nomeModelo: req.body.nomeModelo
            } as INoDTO;

            //No criado com o stub do DTO.
            const stubNo = No.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //Carregar para o container o repositorio
            let noRepoClass = require('../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(noRepoInstance, "save").returns(stubNo);

            const service = new NoService(noRepoInstance as INoRepo);
            const ctrl = new NoController(service);

            await ctrl.criarNo(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "nomeCompleto": req.body.nomeCompleto,
                "abreviatura": req.body.abreviatura,
                "isPontoRendicao": req.body.isPontoRendicao,
                "isEstacaoRecolha": req.body.isEstacaoRecolha,
                "latitude": req.body.latitude,
                "longitude": req.body.longitude,
                "nomeModelo": req.body.nomeModelo
            }));

            sinon.restore();
        });
    });

    describe('nosOrdenadosPor', function () {
        it('should list nos ordenados por x', async function () {

            let req: Partial<Request> = {};
            req.params = { tipo: 'nome' };

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };



            //Carregar para o container o repositorio
            let noRepoClass = require('../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(noRepoInstance, "findAll").returns([stubNo]);

            const service = new NoService(noRepoInstance as INoRepo);
            const ctrl = new NoController(service);

            await ctrl.nosOrdenadosPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude,
                "nomeModelo": body.nomeModelo
            }]));

            sinon.restore();
        });
    })

    describe('nosOrdenadosPor', function () {
        it('should list nos ordenados por x', async function () {

            let req: Partial<Request> = {};
            req.params = {
                tipo: "nome",
                iniciais: "E"
            };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Carregar para o container o repositorio
            let noRepoClass = require('../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //stubs (necessarios)
            sinon.stub(noRepoInstance, "findNoComNomeComecadoPor").returns([stubNo]);
            sinon.stub(noRepoInstance, "findNoComCodigoComecadoPor").returns([stubNo]);

            const service = new NoService(noRepoInstance as INoRepo);
            const ctrl = new NoController(service);

            await ctrl.nosComecadosPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude,
                "nomeModelo": body.nomeModelo
            }]));

            sinon.restore();

        });
    });

    describe("getNoById", function () {
        it("deve retornar um nó com o id correspondente passado por parametro", async function () {
            let req: Partial<Request> = {};
            req.params = {
                id: "aaa"
            };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Carregar para o container o repositorio
            let noRepoClass = require('../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //stubs (necessarios)
            sinon.stub(noRepoInstance, "findByDomainId").returns(stubNo);

            const service = new NoService(noRepoInstance as INoRepo);
            const ctrl = new NoController(service);

            await ctrl.getNoById(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude,
                "nomeModelo": body.nomeModelo
            }));

            sinon.restore();
        });
    });

});