
import { Response, Request, NextFunction } from 'express';
import config from "../../config";
import Container from 'typedi';
import ISegmentoDTO from "../../src/dto/ISegmentoDTO";
import { ISegmentoRepo } from '../../src/services/IRepos/ISegmentoRepo';
import SegmentoService from '../../src/services/segmentoService';
import { INoRepo } from '../../src/services/IRepos/INoRepo';
import SegmentoController from '../../src/controllers/segmentoController';
import ISegmentoController from '../../src/controllers/IControllers/ISegmentoController';
import ISegmentoService from '../../src/services/IServices/ISegmentoService';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import Segmento from '../../src/domain/segmento';

const sinon = require('sinon');

describe("SegmentoControllerIntegration", function () {

    let body = {
        "nomeSegmento": 'SaoJoaoMadeira_SantaMariaFeira',
        "idNoInicial": "111",
        "idNoFinal": "222",
        "distanciaNos": 2500,
        "tempoNos": 300
    };

    //Carregar para o container o repositorio do segmento e do no
    let segmentoRepoClass = require('../../src/repos/segmentoRepo').default;
    let segmentoRepoInstance: ISegmentoRepo = Container.get(segmentoRepoClass);
    Container.set(config.repos.segmento.name, segmentoRepoInstance);
    //Buscar o repositorio aos container carregado acima
    segmentoRepoInstance = Container.get(config.repos.segmento.name);

    let noRepoClass = require('../../src/repos/noRepo').default;
    let noRepoInstance: INoRepo = Container.get(noRepoClass);
    Container.set(config.repos.no.name, noRepoInstance);
    //Buscar o repositorio aos container carregado acima
    noRepoInstance = Container.get(config.repos.no.name);

    const segmentoService = new SegmentoService(segmentoRepoInstance as ISegmentoRepo, noRepoInstance as INoRepo);
    const segmentoController = new SegmentoController(segmentoService);

    it('criarSegmento returns json with values', async function () {
        let req: Partial<Request> = {};
        req.body = body;

        let res, status, json;
        status = sinon.stub();
        json = sinon.spy();
        res = { status, json };
        status.returns(res);

        let next: Partial<NextFunction> = () => { };

        //Stub do DTO do percurso que vamos "criar". Baseado no req.body
        const stubValue = {
            id: "aaa",
            nomeSegmento: req.body.nomeSegmento,
            idNoInicial: req.body.idNoInicial,
            idNoFinal: req.body.idNoFinal,
            distanciaNos: req.body.distanciaNos,
            tempoNos: req.body.tempoNos,
        } as ISegmentoDTO;

        //Segmento criado  com o stub do DTO.
        const stubSegmento = Segmento.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

        //"Mock"/"Stub" do metodo save do repositorio do segmento e exist do repositorio do no. Fazemo-lo retornar um segmento criado por nós em cima e um boolean respetivamente.
        sinon.stub(segmentoRepoInstance, "save").returns(stubSegmento);
        sinon.stub(noRepoInstance, "exists").returns(true);

        await segmentoController.criarSegmento(<Request>req, res, <NextFunction>next);

        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match({
            "id": "aaa",
            "nomeSegmento": req.body.nomeSegmento,
            "idNoInicial": req.body.idNoInicial,
            "idNoFinal": req.body.idNoFinal,
            "distanciaNos": req.body.distanciaNos,
            "tempoNos": req.body.tempoNos
        }));

        sinon.restore();
    });

    describe('getSegmentos', function () {
        it('deve retornar todos os segmentos guardados na base de dados', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Stub do DTO do percurso que vamos "criar". Baseado no req.body
            const stubValue = {
                id: "aaa",
                nomeSegmento: req.body.nomeSegmento,
                idNoInicial: req.body.idNoInicial,
                idNoFinal: req.body.idNoFinal,
                distanciaNos: req.body.distanciaNos,
                tempoNos: req.body.tempoNos,
            } as ISegmentoDTO;

            //Segmento criado  com o stub do DTO.
            const stubSegmento = Segmento.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //"Mock"/"Stub" do metodo save do repositorio do segmento e exist do repositorio do no. Fazemo-lo retornar um segmento criado por nós em cima e um boolean respetivamente.
            sinon.stub(segmentoRepoInstance, "findAll").returns([stubSegmento]);

            await segmentoController.getSegmentos(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeSegmento": req.body.nomeSegmento,
                "idNoInicial": req.body.idNoInicial,
                "idNoFinal": req.body.idNoFinal,
                "distanciaNos": req.body.distanciaNos,
                "tempoNos": req.body.tempoNos
            }]));

            sinon.restore();
        });
    });
});
