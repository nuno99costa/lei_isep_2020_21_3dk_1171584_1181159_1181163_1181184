const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../config';
import LinhaController from '../../src/controllers/linhaController';
import ILinhaDTO from '../../src/dto/ILinhaDTO';
import { ILinhaRepo } from '../../src/services/IRepos/ILinhaRepo';
import { INoRepo } from '../../src/services/IRepos/INoRepo';
import { ITipoTripulanteRepo } from '../../src/services/IRepos/ITipoTripulanteRepo';
import { ITipoViaturaRepo } from '../../src/services/IRepos/ITipoViaturaRepo';
import LinhaService from '../../src/services/linhaService';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import Linha from '../../src/domain/linha';
import { NoId } from '../../src/domain/noId';
import { TipoTripulanteId } from '../../src/domain/tipoTripulanteId';
import { TipoViaturaId } from '../../src/domain/tipoViaturaId';

describe('LinhaController', function () {
    let body = {
        "codigo": "A",
        "nome": "Cristelo-Lordelo",
        "cor": "rosa",
        "noInicial": "1",
        "noFinal": "2",
        "viaturasPermitidas": ["5"],
        "viaturasProibidas": ["9"],
        "tripulantesPermitidos": ["7"],
        "tripulantesProibidos": ["6"]
    };

    //Stub do DTO da linha que vamos "criar".
    const stubValue = {
        "id": "a",
        codigo: body.codigo,
        nome: body.nome,
        cor: body.cor,
        noInicial: body.noInicial,
        noFinal: body.noFinal,
        viaturasPermitidas: body.viaturasPermitidas,
        viaturasProibidas: body.viaturasProibidas,
        tripulantesPermitidos: body.tripulantesPermitidos,
        tripulantesProibidos: body.tripulantesProibidos
    } as ILinhaDTO;

    //----------------------------LinhaRepo------------------------------
    let linhaRepoClass = require('../../src/repos/linhaRepo').default;
    let linhaRepoInstance: ILinhaRepo = Container.get(linhaRepoClass);
    Container.set(config.repos.linha.name, linhaRepoInstance);
    linhaRepoInstance = Container.get(config.repos.linha.name);

    //----------------------------TipoTripulanteRepo---------------------------
    let tipoTripulanteRepoClass = require('../../src/repos/tipoTripulanteRepo').default;
    let tipoTripulanteRepoInstance: ITipoTripulanteRepo = Container.get(tipoTripulanteRepoClass);
    Container.set(config.repos.tipoTripulante.name, tipoTripulanteRepoInstance);
    tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);

    //----------------------------TipoViaturaRepo---------------------------
    let tipoViaturaRepoClass = require('../../src/repos/tipoViaturaRepo').default;
    let tipoViaturaRepoInstance: ITipoViaturaRepo = Container.get(tipoViaturaRepoClass);
    Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);
    tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);

    //---------------------------------NoRepo---------------------------------------
    let noRepoClass = require('../../src/repos/noRepo').default;
    let noRepoInstance: INoRepo = Container.get(noRepoClass);
    Container.set(config.repos.no.name, noRepoInstance);
    noRepoInstance = Container.get(config.repos.no.name);

    /**
     * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
     */
    //NoId:
    const noInicial = NoId.create(new UniqueEntityID(stubValue.noInicial));
    const noFinal = NoId.create(new UniqueEntityID(stubValue.noFinal));

    let index = 0;
    //Tipo de viaturas permitidas
    let viaturasPermitidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasPermitidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasPermitidas[index]));
        viaturasPermitidas.push(viaturaId);
    }

    //Tipo de viaturas proibidas
    let viaturasProibidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasProibidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasProibidas[index]));
        viaturasProibidas.push(viaturaId);
    }

    //Tipo de tripulantes permitidos
    let tripulantesPermitidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesPermitidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesPermitidos[index]));
        tripulantesPermitidos.push(tripulanteId);
    }

    //Tipo de tripulantes proibidos
    let tripulantesProibidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesProibidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesProibidos[index]));
        tripulantesProibidos.push(tripulanteId);
    }

    //Linha criada com o stub do DTO.
    const stubLinha = Linha.create(stubValue, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue.id)).getValue();

    describe('criarLinha', function () {
        it('criarLinha: returns json with values', async function () {
            this.timeout(10000);

            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //----------------------------LinhaRepo------------------------------
            sinon.stub(linhaRepoInstance, "save").returns(stubLinha);

            //----------------------------TipoTripulanteRepo---------------------------
            var stub3 = sinon.stub(tipoTripulanteRepoInstance, "exists").withArgs("7").returns(true);
            stub3.withArgs("6").returns(true);

            //----------------------------TipoViaturaRepo---------------------------
            var stub4 = sinon.stub(tipoViaturaRepoInstance, "exists").withArgs("5").returns(true);
            stub4.withArgs("9").returns(true);

            //---------------------------------NoRepo---------------------------------------
            var stub5 = sinon.stub(noRepoInstance, "exists").withArgs("1").returns(true);
            stub5.withArgs("2").returns(true);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const ctrl = new LinhaController(linhaService);

            await ctrl.criarLinha(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "a",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            }));
            sinon.restore();
        });
    });

    describe('linhasOrdenadasPor', function () {

        it('linhasOrdenadasPor: retorna as linhas ordenadas por código/nome', async function () {
            let req: Partial<Request> = {};
            req.params = { tipo: 'nome' };

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(linhaRepoInstance, "findAll").returns([stubLinha]);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const ctrl = new LinhaController(linhaService);

            await ctrl.linhasOrdenadasPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "a",
                "codigo": body.codigo,
                "nome": body.nome,
                "cor": body.cor,
                "noInicial": body.noInicial,
                "noFinal": body.noFinal,
                "viaturasPermitidas": body.viaturasPermitidas,
                "viaturasProibidas": body.viaturasProibidas,
                "tripulantesPermitidos": body.tripulantesPermitidos,
                "tripulantesProibidos": body.tripulantesProibidos
            }]));
            sinon.restore();
        });
    });

    describe('linhasComecadasPor', function () {
        it('linhasComecadasPor: retorna as linhas que começam por código/nome', async function () {
            let req: Partial<Request> = {};
            req.params = { tipo: 'codigo', iniciais: 'A' };

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(linhaRepoInstance, "findLinhaComCodigoComecadaPor").returns([stubLinha]);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const ctrl = new LinhaController(linhaService);

            await ctrl.linhasComecadasPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "a",
                "codigo": body.codigo,
                "nome": body.nome,
                "cor": body.cor,
                "noInicial": body.noInicial,
                "noFinal": body.noFinal,
                "viaturasPermitidas": body.viaturasPermitidas,
                "viaturasProibidas": body.viaturasProibidas,
                "tripulantesPermitidos": body.tripulantesPermitidos,
                "tripulantesProibidos": body.tripulantesProibidos
            }]));
            sinon.restore();
        });
    });

    describe("getLinhaById", function () {
        it("deve retornar a linha com o id correspondente passado por parametro", async function () {
            let req: Partial<Request> = {};
            req.params = { id: "a" };

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(linhaRepoInstance, "findByDomainId").returns(stubLinha);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const ctrl = new LinhaController(linhaService);

            await ctrl.getLinhaById(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "a",
                "codigo": body.codigo,
                "nome": body.nome,
                "cor": body.cor,
                "noInicial": body.noInicial,
                "noFinal": body.noFinal,
                "viaturasPermitidas": body.viaturasPermitidas,
                "viaturasProibidas": body.viaturasProibidas,
                "tripulantesPermitidos": body.tripulantesPermitidos,
                "tripulantesProibidos": body.tripulantesProibidos
            }));
            sinon.restore();
        });
    });

});
