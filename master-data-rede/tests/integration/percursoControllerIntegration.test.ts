import { Response, Request, NextFunction } from 'express';
import config from '../../config';
import { Container } from 'typedi';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import Percurso from '../../src/domain/percurso';
import Segmento from '../../src/domain/segmento';
import { SegmentoId } from '../../src/domain/segmentoId';
import IPercursoDTO from '../../src/dto/IPercursoDTO';
import ISegmentoDTO from '../../src/dto/ISegmentoDTO';
import { INoRepo } from '../../src/services/IRepos/INoRepo';
import { IPercursoRepo } from '../../src/services/IRepos/IPercursoRepo';
import PercursoService from '../../src/services/percursoService';
import PercursoController from '../../src/controllers/percursoController';
import { ILinhaRepo } from '../../src/services/IRepos/ILinhaRepo';
import ILinhaDTO from '../../src/dto/ILinhaDTO';
import Linha from '../../src/domain/linha';
import { NoId } from '../../src/domain/noId';
import { TipoViaturaId } from '../../src/domain/tipoViaturaId';
import { TipoTripulanteId } from '../../src/domain/tipoTripulanteId';

const sinon = require('sinon').createSandbox();

describe("criarPercurso", function() {

    it('criarPercurso: returns json with values', async function() {
        let body = {
            "nomePercurso": 'SaoJoaoMadeira_Espinho',
            "linhaId": "1000",
            "orientacao": "Ida",
            "listaSegmentos": ["111", "222"],
            "noInicialId": "111",
            "noFinalId": "333",
            "distanciaTotal": 12000,
            "tempoTotal": 1500
        };

        let req: Partial<Request> = {};
        req.body = body;

        let res, status, json;
        status = sinon.stub();
        json = sinon.spy();
        res = { status, json };
        status.returns(res);

        let next: Partial<NextFunction> = () => { };

        //Stub do DTO do percurso que vamos "criar". Baseado no req.body
        const stubValue1 = {
            id: "aaa",
            nomePercurso: req.body.nomePercurso,
            linhaId: req.body.linhaId,
            orientacao: req.body.orientacao,
            listaSegmentos: req.body.listaSegmentos,
            noInicialId: req.body.noInicialId,
            noFinalId: req.body.noFinalId,
            distanciaTotal: req.body.distanciaTotal,
            tempoTotal: req.body.tempoTotal
        } as IPercursoDTO;

        const stubValue2 = {
            id: "111",
            nomeSegmento: "SaoJoaoMadeira_SantaMariaFeira",
            idNoInicial: "111",
            idNoFinal: "200",
            distanciaNos: 1500,
            tempoNos: 300
        } as ISegmentoDTO;

        const stubValue3 = {
            id: "222",
            nomeSegmento: "SantaMariaFeira_Espinho",
            idNoInicial: "200",
            idNoFinal: "333",
            distanciaNos: 1400,
            tempoNos: 250
        } as ISegmentoDTO;

        const stubValue4 = {
            "id": "a",
            "codigo": "A",
            "nome": "SaoJoaoMadeira_Espinho",
            "cor": "rosa",
            "noInicial": "111",
            "noFinal": "333",
            "viaturasPermitidas": ["5"],
            "viaturasProibidas": ["9"],
            "tripulantesPermitidos": ["7"],
            "tripulantesProibidos": ["6"]
        } as ILinhaDTO;

        /**
         * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
         */
        //NoId:
        const noInicial = NoId.create(new UniqueEntityID(stubValue4.noInicial));
        const noFinal = NoId.create(new UniqueEntityID(stubValue4.noFinal));

        let index = 0;
        //Tipo de viaturas permitidas
        let viaturasPermitidas: TipoViaturaId[] = [];
        for (index = 0; index < stubValue4.viaturasPermitidas.length; ++index) {
            const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasPermitidas[index]));
            viaturasPermitidas.push(viaturaId);
        }

        //Tipo de viaturas proibidas
        let viaturasProibidas: TipoViaturaId[] = [];
        for (index = 0; index < stubValue4.viaturasProibidas.length; ++index) {
            const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasProibidas[index]));
            viaturasProibidas.push(viaturaId);
        }

        //Tipo de tripulantes permitidos
        let tripulantesPermitidos: TipoTripulanteId[] = [];
        for (index = 0; index < stubValue4.tripulantesPermitidos.length; ++index) {
            const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesPermitidos[index]));
            tripulantesPermitidos.push(tripulanteId);
        }

        //Tipo de tripulantes proibidos
        let tripulantesProibidos: TipoTripulanteId[] = [];
        for (index = 0; index < stubValue4.tripulantesProibidos.length; ++index) {
            const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesProibidos[index]));
            tripulantesProibidos.push(tripulanteId);
        }


        //Segmentos necessários para criar o percurso criados com o stub do DTO
        const stubPercurso = Percurso.create(stubValue1, [SegmentoId.create(new UniqueEntityID("111")), SegmentoId.create(new UniqueEntityID("222"))], stubValue1.distanciaTotal, stubValue1.tempoTotal, new UniqueEntityID(stubValue1.id)).getValue();
        const stubSegmento1 = Segmento.create(stubValue2, new UniqueEntityID(stubValue2.id));
        const stubSegmento2 = Segmento.create(stubValue2, new UniqueEntityID(stubValue3.id));

        const stubLinha = Linha.create(stubValue4, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue4.id)).getValue();

        //Carregar para o container o repositorio
        let percursoRepoClass = require('../../src/repos/percursoRepo').default;
        let percursoRepoInstance: IPercursoRepo = Container.get(percursoRepoClass);
        Container.set(config.repos.percurso.name, percursoRepoInstance);

        let noRepoClass = require('../../src/repos/noRepo').default;
        let noRepoInstance: INoRepo = Container.get(noRepoClass);
        Container.set(config.repos.no.name, noRepoInstance);

        let linhaRepoClass = require('../../src/repos/linhaRepo').default;
        let linhaRepoInstance: ILinhaRepo = Container.get(linhaRepoClass);
        Container.set(config.repos.linha.name, linhaRepoInstance);

        //Buscar o repositorio ao container carregado atrás
        percursoRepoInstance = Container.get(config.repos.percurso.name);
        noRepoInstance = Container.get(config.repos.no.name);
        linhaRepoInstance = Container.get(config.repos.linha.name);

        var stub1 = sinon.stub(percursoRepoInstance, "save").returns(stubPercurso);
        var stub2 = sinon.stub(noRepoInstance, "exists").returns(true);
        var stub3 = sinon.stub(percursoRepoInstance, "validarPercurso").withArgs(stubValue1.listaSegmentos[0]).returns(stubSegmento1);
        stub3.withArgs(stubValue1.listaSegmentos[1]).returns(stubSegmento2);
        var stub4 = sinon.stub(percursoRepoInstance, "validarNosPercurso").returns(true);
        var stub5 = sinon.stub(linhaRepoInstance, "exists").returns(true);
        var stub6 = sinon.stub(linhaRepoInstance, "findByDomainId").returns(stubLinha);


        const percursoService = new PercursoService(percursoRepoInstance as IPercursoRepo, noRepoInstance as INoRepo, linhaRepoInstance as ILinhaRepo);
        const percursoController = new PercursoController(percursoService);

        await percursoController.criarPercurso(<Request>req, res, <NextFunction>next);

        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match({
            "id": "aaa",
            "nomePercurso": req.body.nomePercurso,
            "linhaId": req.body.linhaId,
            "orientacao": req.body.orientacao,
            "listaSegmentos": req.body.listaSegmentos,
            "noInicialId": req.body.noInicialId,
            "noFinalId": req.body.noFinalId,
            "distanciaTotal": req.body.distanciaTotal,
            "tempoTotal": req.body.tempoTotal
        }));

        sinon.restore();

    })

    describe('listarPercursos', function() {
        it('listarPercursos: returns json with values', async function() {

            let body = {
                "nomePercurso": 'SaoJoaoMadeira_Espinho',
                "linhaId": "1000",
                "orientacao": "Ida",
                "listaSegmentos": ["111", "222"],
                "noInicialId": "111",
                "noFinalId": "333",
                "distanciaTotal": 12000,
                "tempoTotal": 1500
            };

            let req: Partial<Request> = {};
            req.params = { id: "1000" };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Stub do DTO do percurso que vamos "criar". Baseado no req.body
            const stubValue1 = {
                id: "aaa",
                nomePercurso: body.nomePercurso,
                linhaId: body.linhaId,
                orientacao: body.orientacao,
                listaSegmentos: body.listaSegmentos,
                noInicialId: body.noInicialId,
                noFinalId: body.noFinalId,
                distanciaTotal: body.distanciaTotal,
                tempoTotal: body.tempoTotal
            } as IPercursoDTO;

            const stubValue2 = {
                id: "111",
                nomeSegmento: "SaoJoaoMadeira_SantaMariaFeira",
                idNoInicial: "111",
                idNoFinal: "200",
                distanciaNos: 1500,
                tempoNos: 300
            } as ISegmentoDTO;

            const stubValue3 = {
                id: "222",
                nomeSegmento: "SantaMariaFeira_Espinho",
                idNoInicial: "200",
                idNoFinal: "333",
                distanciaNos: 1400,
                tempoNos: 250
            } as ISegmentoDTO;

            const stubValue4 = {
                id: "a",
                codigo: "A",
                nome: "SaoJoaoMadeira_Espinho",
                cor: "rosa",
                noInicial: "111",
                noFinal: "333",
                viaturasPermitidas: ["5"],
                viaturasProibidas: ["9"],
                tripulantesPermitidos: ["7"],
                tripulantesProibidos: ["6"]
            } as ILinhaDTO;
            /**
             * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
             */
            //NoId:
            const noInicial = NoId.create(new UniqueEntityID(stubValue4.noInicial));
            const noFinal = NoId.create(new UniqueEntityID(stubValue4.noFinal));

            let index = 0;
            //Tipo de viaturas permitidas
            let viaturasPermitidas: TipoViaturaId[] = [];
            for (index = 0; index < stubValue4.viaturasPermitidas.length; ++index) {
                const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasPermitidas[index]));
                viaturasPermitidas.push(viaturaId);
            }

            //Tipo de viaturas proibidas
            let viaturasProibidas: TipoViaturaId[] = [];
            for (index = 0; index < stubValue4.viaturasProibidas.length; ++index) {
                const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasProibidas[index]));
                viaturasProibidas.push(viaturaId);
            }

            //Tipo de tripulantes permitidos
            let tripulantesPermitidos: TipoTripulanteId[] = [];
            for (index = 0; index < stubValue4.tripulantesPermitidos.length; ++index) {
                const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesPermitidos[index]));
                tripulantesPermitidos.push(tripulanteId);
            }

            //Tipo de tripulantes proibidos
            let tripulantesProibidos: TipoTripulanteId[] = [];
            for (index = 0; index < stubValue4.tripulantesProibidos.length; ++index) {
                const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesProibidos[index]));
                tripulantesProibidos.push(tripulanteId);
            }


            //Segmentos necessários para criar o percurso criados com o stub do DTO
            const stubPercurso = Percurso.create(stubValue1, [SegmentoId.create(new UniqueEntityID("111")), SegmentoId.create(new UniqueEntityID("222"))], stubValue1.distanciaTotal, stubValue1.tempoTotal, new UniqueEntityID(stubValue1.id)).getValue();

            //Carregar para o container o repositorio
            let percursoRepoClass = require('../../src/repos/percursoRepo').default;
            let percursoRepoInstance: IPercursoRepo = Container.get(percursoRepoClass);
            Container.set(config.repos.percurso.name, percursoRepoInstance);

            let noRepoClass = require('../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            let linhaRepoClass = require('../../src/repos/linhaRepo').default;
            let linhaRepoInstance: ILinhaRepo = Container.get(linhaRepoClass);
            Container.set(config.repos.linha.name, linhaRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            percursoRepoInstance = Container.get(config.repos.percurso.name);
            noRepoInstance = Container.get(config.repos.no.name);
            linhaRepoInstance = Container.get(config.repos.linha.name);

            linhaRepoInstance = Container.get(config.repos.linha.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um percurso criado por percursos em cima.
            sinon.stub(linhaRepoInstance, "exists").returns(true);
            sinon.stub(percursoRepoInstance, "findByIdLinha").returns([stubPercurso]);

            const percursoService = new PercursoService(percursoRepoInstance as IPercursoRepo, noRepoInstance as INoRepo, linhaRepoInstance as ILinhaRepo);
            const percursoController = new PercursoController(percursoService);

            await percursoController.listarPercursos(<Request>req, res, <NextFunction>next);


            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomePercurso": body.nomePercurso,
                "linhaId": body.linhaId,
                "orientacao": body.orientacao,
                "listaSegmentos": body.listaSegmentos,
                "noInicialId": body.noInicialId,
                "noFinalId": body.noFinalId,
                "distanciaTotal": body.distanciaTotal,
                "tempoTotal": body.tempoTotal
            }]));

            sinon.restore();
        })
    });

    afterEach(function() {
        // completely restore all fakes created through the sandbox
        sinon.restore();
    });
})
