const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../config';
import { Result } from '../../src/core/logic/Result';
import TipoViaturaController from '../../src/controllers/tipoViaturaController';
import ITipoViaturaDTO from '../../src/dto/ITipoViaturaDTO';
import ITipoViaturaService from '../../src/services/IServices/ITipoViaturaService';
import { ITipoViaturaRepo } from '../../src/services/IRepos/ITipoViaturaRepo';
import TipoViatura from '../../src/domain/tipoViatura';
import { UniqueEntityID } from '../../src/core/domain/UniqueEntityID';
import TipoViaturaService from '../../src/services/tipoViaturaService';

describe('TipoViaturaController', function () {
    let body = {
        "codigo": "12345678901234567890",
        "descricao": "viatura1",
        "combustivel": "Gasolina",
        "autonomia": 65,
        "velocidadeMedia": 45,
        "custoPorQuilometro": 14,
        "consumoMedio": 22.352
    };

    //Carregar para o container o repositorio
    let tipoViaturaRepoClass = require('../../src/repos/tipoViaturaRepo').default;
    let tipoViaturaRepoInstance: ITipoViaturaRepo = Container.get(tipoViaturaRepoClass);
    Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);

    //Buscar o repositorio ao container carregado atrás
    tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);

    const tipoViaturaServiceInstance = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);

    const ctrl = new TipoViaturaController(tipoViaturaServiceInstance);

    describe('criarTipoViatura', function () {
        it('criarTipoViatura: returns json with values', async function () {

            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;

            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            //Stub do DTO do tipo de viatura que vamos "criar".
            const stubValue = {
                id: "aaa",
                codigo: req.body.codigo,
                descricao: req.body.descricao,
                combustivel: req.body.combustivel,
                autonomia: req.body.autonomia,
                velocidadeMedia: req.body.velocidadeMedia,
                custoPorQuilometro: req.body.custoPorQuilometro,
                consumoMedio: req.body.consumoMedio
            } as ITipoViaturaDTO;

            //Tipo Viatura criado com o stub do DTO.
            const stubTipoViatura = TipoViatura.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(tipoViaturaRepoInstance, "save").returns(stubTipoViatura);

            await ctrl.criarTipoViatura(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao,
                "combustivel": req.body.combustivel,
                "autonomia": req.body.autonomia,
                "velocidadeMedia": req.body.velocidadeMedia,
                "custoPorQuilometro": req.body.custoPorQuilometro,
                "consumoMedio": req.body.consumoMedio
            }));
            sinon.restore();
        });
    });

    describe("getTipoViatura", function () {
        it("deve retornar todos os tipos de viaturas guardados", async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;

            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            const stubValue = {
                id: "aaa",
                codigo: req.body.codigo,
                descricao: req.body.descricao,
                combustivel: req.body.combustivel,
                autonomia: req.body.autonomia,
                velocidadeMedia: req.body.velocidadeMedia,
                custoPorQuilometro: req.body.custoPorQuilometro,
                consumoMedio: req.body.consumoMedio
            } as ITipoViaturaDTO;

            //Tipo Viatura criado com o stub do DTO.
            const stubTipoViatura = TipoViatura.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(tipoViaturaRepoInstance, "findAll").returns([stubTipoViatura]);

            await ctrl.getTipoViatura(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao,
                "combustivel": req.body.combustivel,
                "autonomia": req.body.autonomia,
                "velocidadeMedia": req.body.velocidadeMedia,
                "custoPorQuilometro": req.body.custoPorQuilometro,
                "consumoMedio": req.body.consumoMedio
            }]));

            sinon.restore();
        });
    });
});