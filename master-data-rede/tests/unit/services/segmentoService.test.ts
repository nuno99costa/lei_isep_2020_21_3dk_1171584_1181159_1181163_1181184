import Container from "typedi";
import config from "../../../config";
const sinon = require('sinon');
const chai = require('chai');
import Segmento from "../../../src/domain/segmento";
import SegmentoService from '../../../src/services/segmentoService'
import { ISegmentoRepo } from "../../../src/services/IRepos/ISegmentoRepo";
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import ISegmentoDTO from "../../../src/dto/ISegmentoDTO";
import { INoRepo } from "../../../src/services/IRepos/INoRepo";
import { before } from "lodash";
import { contained } from "sequelize/types/lib/operators";

describe("SegmentoService", function () {
    const stubValue = {
        id: "aaa",
        nomeSegmento: "SãoJoãoMadeira_SantaMariaFeira",
        idNoInicial: "111",
        idNoFinal: "222",
        distanciaNos: 2500,
        tempoNos: 300
    } as ISegmentoDTO;

    const stubSegmento = Segmento.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    //Carregar para o container o repositorio
    let segmentoRepoClass = require('../../../src/repos/segmentoRepo').default;
    let segmentoRepoInstance: ISegmentoRepo = Container.get(segmentoRepoClass);
    Container.set(config.repos.segmento.name, segmentoRepoInstance);

    let noRepoClass = require('../../../src/repos/noRepo').default;
    let noRepoInstance: INoRepo = Container.get(noRepoClass);
    Container.set(config.repos.no.name, noRepoInstance);

    //Buscar o repositorio ao container carregado atrás
    segmentoRepoInstance = Container.get(config.repos.segmento.name);
    noRepoInstance = Container.get(config.repos.no.name);

    describe("criarSegmento", function () {
        it("should create a segmento", async function () {
            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um segmento criado por nós em cima.
            var stub1 = sinon.stub(segmentoRepoInstance, "save").returns(stubSegmento);
            var stub2 = sinon.stub(noRepoInstance, "exists").returns(true);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const segmentoService = new SegmentoService(segmentoRepoInstance as ISegmentoRepo, noRepoInstance as INoRepo);

            //Testar método
            const result = (await segmentoService.criarSegmento(stubValue)).getValue();

            sinon.restore();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.nomeSegmento).to.equal(stubValue.nomeSegmento);
            chai.expect(result.idNoInicial).to.equal(stubValue.idNoInicial);
            chai.expect(result.idNoFinal).to.equal(stubValue.idNoFinal);
            chai.expect(result.distanciaNos).to.equal(stubValue.distanciaNos);
            chai.expect(result.tempoNos).to.equal(stubValue.tempoNos);
        });
    });

    describe('getSegmentos', function () {
        it('deve retornar todos os segmentos guardados na base de dados', async function () {
            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um segmento criado por nós em cima.
            var stub1 = sinon.stub(segmentoRepoInstance, "findAll").returns([stubSegmento]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const segmentoService = new SegmentoService(segmentoRepoInstance as ISegmentoRepo, noRepoInstance as INoRepo);

            //Testar método
            const result = (await segmentoService.getSegmentos()).getValue();

            sinon.restore();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].nomeSegmento).to.equal(stubValue.nomeSegmento);
            chai.expect(result[0].idNoInicial).to.equal(stubValue.idNoInicial);
            chai.expect(result[0].idNoFinal).to.equal(stubValue.idNoFinal);
            chai.expect(result[0].distanciaNos).to.equal(stubValue.distanciaNos);
            chai.expect(result[0].tempoNos).to.equal(stubValue.tempoNos);
        });
    });
});
