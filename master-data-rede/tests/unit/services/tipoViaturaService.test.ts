import Container from "typedi";
import config from "../../../config";
import ITipoViaturaDTO from "../../../src/dto/ITipoViaturaDTO";
const sinon = require('sinon');
const chai = require('chai');
import TipoViatura from "../../../src/domain/tipoViatura";
import TipoViaturaService from '../../../src/services/tipoViaturaService'
import { ITipoViaturaRepo } from "../../../src/services/IRepos/ITipoViaturaRepo";
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";

describe("TipoViaturaService", function () {

    //Stub do DTO do tipo de viatura que vamos "criar".
    const stubValue = {
        id: "teste",
        "codigo": "12345678901234567890",
        "descricao": "viatura1",
        "combustivel": "Gasolina",
        "autonomia": 65,
        "velocidadeMedia": 45,
        "custoPorQuilometro": 14,
        "consumoMedio": 22.352
    } as ITipoViaturaDTO;

    //Tipo de viatura criado com o stub do DTO.
    const stubTipoViatura = TipoViatura.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    //Carregar para o container o repositorio
    let tipoViaturaRepoClass = require('../../../src/repos/tipoViaturaRepo').default;
    let tipoViaturaRepoInstance: ITipoViaturaRepo = Container.get(tipoViaturaRepoClass);
    Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);

    //Buscar o repositorio ao container carregado atrás
    tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);

    describe("criarTipoViatura", function () {

        it("should create a vehicle type", async function () {

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um tipo de viatura criado por nós em cima.
            sinon.stub(tipoViaturaRepoInstance, "save").returns(stubTipoViatura);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const tipoViaturaService = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);

            //Testar método
            const result = (await tipoViaturaService.criarTipoViatura(stubValue)).getValue();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.codigo).to.equal(stubValue.codigo);
            chai.expect(result.descricao).to.equal(stubValue.descricao);
            chai.expect(result.combustivel).to.equal(stubValue.combustivel);
            chai.expect(result.autonomia).to.equal(stubValue.autonomia);
            chai.expect(result.velocidadeMedia).to.equal(stubValue.velocidadeMedia);
            chai.expect(result.custoPorQuilometro).to.equal(stubValue.custoPorQuilometro);
            chai.expect(result.consumoMedio).to.equal(stubValue.consumoMedio);

            sinon.restore();
        });
    });

    describe("getTipoViatura", function () {

        it("deve devolver todos os tipos de viatura guardados", async function () {

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um tipo de viatura criado por nós em cima.
            sinon.stub(tipoViaturaRepoInstance, "findAll").returns([stubTipoViatura]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const tipoViaturaService = new TipoViaturaService(tipoViaturaRepoInstance as ITipoViaturaRepo);

            //Testar método
            const result = (await tipoViaturaService.getTipoViatura()).getValue();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].codigo).to.equal(stubValue.codigo);
            chai.expect(result[0].descricao).to.equal(stubValue.descricao);
            chai.expect(result[0].combustivel).to.equal(stubValue.combustivel);
            chai.expect(result[0].autonomia).to.equal(stubValue.autonomia);
            chai.expect(result[0].velocidadeMedia).to.equal(stubValue.velocidadeMedia);
            chai.expect(result[0].custoPorQuilometro).to.equal(stubValue.custoPorQuilometro);
            chai.expect(result[0].consumoMedio).to.equal(stubValue.consumoMedio);

            sinon.restore();
        });
    });
});


