import Container from "typedi";
import config from "../../../config";
import IPercursoDTO from "../../../src/dto/IPercursoDTO";
const sinon = require('sinon').createSandbox();;
const chai = require('chai');
import Percurso from "../../../src/domain/percurso";
import PercursoService from '../../../src/services/percursoService'
import { IPercursoRepo } from "../../../src/services/IRepos/IPercursoRepo";
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import { INoRepo } from "../../../src/services/IRepos/INoRepo";
import { Result } from "../../../src/core/logic/Result";
import { SegmentoId } from "../../../src/domain/segmentoId";
import { constructor } from "acorn";
import ISegmentoDTO from "../../../src/dto/ISegmentoDTO";
import Segmento from "../../../src/domain/segmento";
import { ILinhaRepo } from "../../../src/services/IRepos/ILinhaRepo";
import { TipoViaturaId } from "../../../src/domain/tipoViaturaId";
import { TipoTripulanteId } from "../../../src/domain/tipoTripulanteId";
import { NoId } from "../../../src/domain/noId";
import ILinhaDTO from "../../../src/dto/ILinhaDTO";
import Linha from "../../../src/domain/linha";

describe("PercursoService", function(){
    const stubValue1 = {
        id: "aaa",
        nomePercurso: "SãoJoãoMadeira_Espinho",
        linhaId: "a",
        orientacao: "Ida",
        listaSegmentos: ["111", "222"],
        noInicialId: "111",
        noFinalId: "333",
        distanciaTotal: 12000,
        tempoTotal: 1500
    } as IPercursoDTO;

    const stubValue2 = {
        id: "111",
        nomeSegmento: "SaoJoaoMadeira_SantaMariaFeira",
        idNoInicial: "111",
        idNoFinal: "200",
        distanciaNos: 1500,
        tempoNos: 300
    } as ISegmentoDTO;

    const stubValue3 = {
        id: "222",
        nomeSegmento: "SantaMariaFeira_Espinho",
        idNoInicial: "200",
        idNoFinal: "333",
        distanciaNos: 1400,
        tempoNos: 250
    } as ISegmentoDTO;

    const stubValue4 = {
        id: "a",
        codigo: "A",
        nome: "SaoJoaoMadeira_Espinho",
        cor: "rosa",
        noInicial: "111",
        noFinal: "333",
        viaturasPermitidas: ["5"],
        viaturasProibidas: ["9"],
        tripulantesPermitidos: ["7"],
        tripulantesProibidos: ["6"]
    } as ILinhaDTO;

    /**
     * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
     */
    //NoId:
    const noInicial = NoId.create(new UniqueEntityID(stubValue4.noInicial));
    const noFinal = NoId.create(new UniqueEntityID(stubValue4.noFinal));

    let index = 0;
    //Tipo de viaturas permitidas
    let viaturasPermitidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue4.viaturasPermitidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasPermitidas[index]));
        viaturasPermitidas.push(viaturaId);
    }

    //Tipo de viaturas proibidas
    let viaturasProibidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue4.viaturasProibidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue4.viaturasProibidas[index]));
        viaturasProibidas.push(viaturaId);
    }

    //Tipo de tripulantes permitidos
    let tripulantesPermitidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue4.tripulantesPermitidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesPermitidos[index]));
        tripulantesPermitidos.push(tripulanteId);
    }

    //Tipo de tripulantes proibidos
    let tripulantesProibidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue4.tripulantesProibidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue4.tripulantesProibidos[index]));
        tripulantesProibidos.push(tripulanteId);
    }

    const stubPercurso = Percurso.create(stubValue1, [SegmentoId.create( new UniqueEntityID("111")), SegmentoId.create( new UniqueEntityID("222"))], stubValue1.distanciaTotal, stubValue1.tempoTotal, new UniqueEntityID(stubValue1.id)).getValue();
    const stubSegmento1 = Segmento.create(stubValue2, new UniqueEntityID(stubValue2.id));
    const stubSegmento2 = Segmento.create(stubValue2, new UniqueEntityID(stubValue3.id));
    const stubLinha = Linha.create(stubValue4, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue4.id)).getValue();
    const stubPercursos = [stubValue1];

    describe("criarSegmento", function () {

        it("should create a percurso", async function () {

            //Carregar para o container o repositorio
            let percursoRepoClass = require('../../../src/repos/percursoRepo').default;
            let percursoRepoInstance: IPercursoRepo = Container.get(percursoRepoClass);
            Container.set(config.repos.percurso.name, percursoRepoInstance);

            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            let linhaRepoClass = require('../../../src/repos/linhaRepo').default;
            let linhaRepoInstance:  ILinhaRepo = Container.get(linhaRepoClass);
            Container.set(config.repos.linha.name, linhaRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            percursoRepoInstance = Container.get(config.repos.percurso.name);
            noRepoInstance = Container.get(config.repos.no.name);
            linhaRepoInstance = Container.get(config.repos.linha.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um percurso criado por percursos em cima.
            var stub1 = sinon.stub(percursoRepoInstance, "save").returns(stubPercurso);
            var stub2 = sinon.stub(noRepoInstance, "exists").returns(true);
            var stub3 = sinon.stub(percursoRepoInstance, "validarPercurso").withArgs(stubValue1.listaSegmentos[0]).returns(stubSegmento1);
            stub3.withArgs(stubValue1.listaSegmentos[1]).returns(stubSegmento2);
            var stub4 = sinon.stub(percursoRepoInstance, "validarNosPercurso").returns(true);
            var stub5 = sinon.stub(linhaRepoInstance, "exists").returns(true);
            var stub6 = sinon.stub(linhaRepoInstance, "findByDomainId").returns(stubLinha);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const percursoService = new PercursoService(percursoRepoInstance as IPercursoRepo, noRepoInstance as INoRepo, linhaRepoInstance as ILinhaRepo);

            //Testar método
            const result = (await percursoService.criarPercurso(stubValue1)).getValue();

            chai.expect(result.id).to.equal(stubValue1.id);
            chai.expect(result.nomePercurso).to.equal(stubValue1.nomePercurso);
            chai.expect(result.linhaId).to.equal(stubValue1.linhaId);
            chai.expect(result.orientacao).to.equal(stubValue1.orientacao);
            chai.expect(result.listaSegmentos[0]).to.equal(stubValue1.listaSegmentos[0]);
            chai.expect(result.listaSegmentos[1]).to.equal(stubValue1.listaSegmentos[1]);
            chai.expect(result.noInicialId).to.equal(stubValue1.noInicialId);
            chai.expect(result.noFinalId).to.equal(stubValue1.noFinalId);
            chai.expect(result.distanciaTotal).to.equal(stubValue1.distanciaTotal);
            chai.expect(result.tempoTotal).to.equal(stubValue1.tempoTotal);

            sinon.restore();
        })
    })

    describe("listaPercursos", function () {
        
        it("should list the percursos", async function() {

            //Carregar para o container o repositorio
            let percursoRepoClass = require('../../../src/repos/percursoRepo').default;
            let percursoRepoInstance: IPercursoRepo = Container.get(percursoRepoClass);
            Container.set(config.repos.percurso.name, percursoRepoInstance);

            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            let linhaRepoClass = require('../../../src/repos/linhaRepo').default;
            let linhaRepoInstance:  ILinhaRepo = Container.get(linhaRepoClass);
            Container.set(config.repos.linha.name, linhaRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            percursoRepoInstance = Container.get(config.repos.percurso.name);
            noRepoInstance = Container.get(config.repos.no.name);
            linhaRepoInstance = Container.get(config.repos.linha.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um percurso criado por percursos em cima.
            var stub1 = sinon.stub(linhaRepoInstance, "exists").returns(true);
            var stub2 = sinon.stub(percursoRepoInstance, "findByIdLinha").returns([stubPercurso]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const percursoService = new PercursoService(percursoRepoInstance as IPercursoRepo, noRepoInstance as INoRepo, linhaRepoInstance as ILinhaRepo);

            //Testar método
            const result = (await percursoService.listaPercursos(stubValue4.id)).getValue();  
            
            var index = 0;
    
            for(index = 0; index < stubPercursos.length; index++){
                chai.expect(result[index].id).to.equal(stubPercursos[index].id);
                chai.expect(result[index].linhaId).to.equal(stubPercursos[index].linhaId);
                chai.expect(result[index].noInicialId).to.equal(stubPercursos[index].noInicialId);
                chai.expect(result[index].noFinalId).to.equal(stubPercursos[index].noFinalId);
                chai.expect(result[index].nomePercurso).to.equal(stubPercursos[index].nomePercurso);
                chai.expect(result[index].orientacao).to.equal(stubPercursos[index].orientacao);
                chai.expect(result[index].tempoTotal).to.equal(stubPercursos[index].tempoTotal);
                chai.expect(result[index].distanciaTotal).to.equal(stubPercursos[index].distanciaTotal);
                chai.expect(result[index].listaSegmentos[0]).to.equal(stubPercursos[index].listaSegmentos[0]);
                chai.expect(result[index].listaSegmentos[1]).to.equal(stubPercursos[index].listaSegmentos[1]);
            }

            sinon.restore();
        })
    })

    afterEach(function () {
        // completely restore all fakes created through the sandbox
        sinon.restore();
    });

})
