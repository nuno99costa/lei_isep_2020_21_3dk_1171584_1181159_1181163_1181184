import Container from "typedi";
import config from "../../../config";
import ITipoTripulanteDTO from "../../../src/dto/ITipoTripulanteDTO";
const sinon = require('sinon');
const chai = require('chai');
import { ITipoTripulanteRepo } from '../../../src/services/IRepos/ITipoTripulanteRepo'
import TipoTripulante from "../../../src/domain/tipoTripulante";
import TipoTripulanteService from '../../../src/services/tipoTripulanteService'
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";

describe("TipoTripulanteService", function () {

    //Stub do DTO do nó que vamos "criar".
    const stubValue = {
        id: "1122",
        codigo: "abc",
        descricao: "Gosta"
    } as ITipoTripulanteDTO;

    //No criado com o stub do DTO.
    const stubTipoTripulante = TipoTripulante.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    //Carregar para o container o repositorio
    let tipoTripulanteRepoClass = require('../../../src/repos/tipoTripulanteRepo').default;
    let tipoTripulanteRepoInstance: ITipoTripulanteRepo = Container.get(tipoTripulanteRepoClass);
    Container.set(config.repos.tipoTripulante.name, tipoTripulanteRepoInstance);

    //Buscar o repositorio ao container carregado atrás
    tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);

    describe("cria;rTipoTripulante", function () {

        it("should create a tipo de tripulante", async function () {

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(tipoTripulanteRepoInstance, "save").returns(stubTipoTripulante);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const tipoTripulanteService = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            //Testar método
            const result = (await tipoTripulanteService.criarTipoTripulante(stubValue)).getValue();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.codigo).to.equal(stubValue.codigo);
            chai.expect(result.descricao).to.equal(stubValue.descricao);

        });
    });

    describe("getTipoTripulante", function () {

        it("deve devolver todos os tipos de Tripulante guardados", async function () {

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um tipo de Tripulante criado por nós em cima.
            sinon.stub(tipoTripulanteRepoInstance, "findAll").returns([stubTipoTripulante]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const tipoTripulanteService = new TipoTripulanteService(tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            //Testar método
            const result = (await tipoTripulanteService.getTipoTripulante()).getValue();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].codigo).to.equal(stubValue.codigo);
            chai.expect(result[0].descricao).to.equal(stubValue.descricao);

            sinon.restore();
        });
    });
});