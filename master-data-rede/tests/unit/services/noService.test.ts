import Container from "typedi";
import config from "../../../config";
import INoDTO from "../../../src/dto/INoDTO";
const sinon = require('sinon');
const chai = require('chai');
import No from "../../../src/domain/no";
import NoService from '../../../src/services/noService'
import { INoRepo } from "../../../src/services/IRepos/INoRepo";
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import { func } from "joi";

describe("NoService", function () {


    //Stub do DTO do nó que vamos "criar".
    const stubValue = {
        id: "aaa",
        nomeCompleto: "Espinho",
        abreviatura: "Esp",
        isPontoRendicao: true,
        isEstacaoRecolha: true,
        latitude: 12,
        longitude: -12.3
    } as INoDTO;

    const stubValue2 = {
        id: "bbb",
        nomeCompleto: "Porto",
        abreviatura: "POR",
        isPontoRendicao: true,
        isEstacaoRecolha: true,
        latitude: 12,
        longitude: -12.3
    } as INoDTO;

    //No criado com o stub do DTO.
    const stubNo = No.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();
    const stubNo2 = No.create(stubValue2, new UniqueEntityID(stubValue2.id)).getValue();

    describe("criarNo", function () {

        it("should create a node", async function () {

            //Carregar para o container o repositorio
            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(noRepoInstance, "save").returns(stubNo);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const noService = new NoService(noRepoInstance as INoRepo);

            //Testar método
            const result = (await noService.criarNo(stubValue)).getValue();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.nomeCompleto).to.equal(stubValue.nomeCompleto);
            chai.expect(result.abreviatura).to.equal(stubValue.abreviatura);
            chai.expect(result.isPontoRendicao).to.equal(stubValue.isPontoRendicao);
            chai.expect(result.isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);
            chai.expect(result.latitude).to.equal(stubValue.latitude);
            chai.expect(result.longitude).to.equal(stubValue.longitude);

            sinon.restore();

        })
    })

    describe("listaOrdenadaPor", function () {

        it("should list nos by x", async function () {

            //Carregar para o container o repositorio
            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //"Mock"/"Stub" do metodo save do repositorio. Fazemo-lo retornar um nó criado por nós em cima.
            sinon.stub(noRepoInstance, "findAll").returns([stubNo, stubNo2]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const noService = new NoService(noRepoInstance as INoRepo);

            //Testar método
            const result = (await noService.listaOrdenadaPor("nome")).getValue();

            sinon.restore();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].nomeCompleto).to.equal(stubValue.nomeCompleto);
            chai.expect(result[0].abreviatura).to.equal(stubValue.abreviatura);
            chai.expect(result[0].isPontoRendicao).to.equal(stubValue.isPontoRendicao);
            chai.expect(result[0].isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);
            chai.expect(result[0].latitude).to.equal(stubValue.latitude);
            chai.expect(result[0].longitude).to.equal(stubValue.longitude);

            chai.expect(result[1].id).to.equal(stubValue2.id);
            chai.expect(result[1].nomeCompleto).to.equal(stubValue2.nomeCompleto);
            chai.expect(result[1].abreviatura).to.equal(stubValue2.abreviatura);
            chai.expect(result[1].isPontoRendicao).to.equal(stubValue2.isPontoRendicao);
            chai.expect(result[1].isEstacaoRecolha).to.equal(stubValue2.isEstacaoRecolha);
            chai.expect(result[1].latitude).to.equal(stubValue2.latitude);
            chai.expect(result[1].longitude).to.equal(stubValue2.longitude);



        })
    })

    describe("listaComecadosPor", function () {

        it("should list nos comecados by x", async function () {

            //Carregar para o container o repositorio
            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //stubs (necessarios)
            sinon.stub(noRepoInstance, "findNoComNomeComecadoPor").returns([stubNo]);
            sinon.stub(noRepoInstance, "findNoComCodigoComecadoPor").returns([stubNo]);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const noService = new NoService(noRepoInstance as INoRepo);

            //Testar método
            const result = (await noService.listaComecadosPor("nome", "Esp")).getValue();
            const result2 = (await noService.listaComecadosPor("codigo", "a")).getValue();

            sinon.restore();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].nomeCompleto).to.equal(stubValue.nomeCompleto);
            chai.expect(result[0].abreviatura).to.equal(stubValue.abreviatura);
            chai.expect(result[0].isPontoRendicao).to.equal(stubValue.isPontoRendicao);
            chai.expect(result[0].isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);
            chai.expect(result[0].latitude).to.equal(stubValue.latitude);
            chai.expect(result[0].longitude).to.equal(stubValue.longitude);

            chai.expect(result2[0].id).to.equal(stubValue.id);
            chai.expect(result2[0].nomeCompleto).to.equal(stubValue.nomeCompleto);
            chai.expect(result2[0].abreviatura).to.equal(stubValue.abreviatura);
            chai.expect(result2[0].isPontoRendicao).to.equal(stubValue.isPontoRendicao);
            chai.expect(result2[0].isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);
            chai.expect(result2[0].latitude).to.equal(stubValue.latitude);
            chai.expect(result2[0].longitude).to.equal(stubValue.longitude);

        });
    });

    describe("getNoById", function () {
        it("deve retornar o nó com o id enviado por parametro", async function () {
            //Carregar para o container o repositorio
            let noRepoClass = require('../../../src/repos/noRepo').default;
            let noRepoInstance: INoRepo = Container.get(noRepoClass);
            Container.set(config.repos.no.name, noRepoInstance);

            //Buscar o repositorio ao container carregado atrás
            noRepoInstance = Container.get(config.repos.no.name);

            //stubs (necessarios)
            sinon.stub(noRepoInstance, "findByDomainId").withArgs("aaa").returns(stubNo);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const noService = new NoService(noRepoInstance as INoRepo);

            const result = (await noService.getNoById("aaa")).getValue();

            sinon.restore();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.nomeCompleto).to.equal(stubValue.nomeCompleto);
            chai.expect(result.abreviatura).to.equal(stubValue.abreviatura);
            chai.expect(result.isPontoRendicao).to.equal(stubValue.isPontoRendicao);
            chai.expect(result.isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);
            chai.expect(result.latitude).to.equal(stubValue.latitude);
            chai.expect(result.longitude).to.equal(stubValue.longitude);
        });
    });
});