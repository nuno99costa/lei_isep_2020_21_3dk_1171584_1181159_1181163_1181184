import Container from "typedi";
import config from "../../../config";
import ILinhaDTO from "../../../src/dto/ILinhaDTO";
const sinon = require('sinon');
const chai = require('chai');
import { ILinhaRepo } from '../../../src/services/IRepos/ILinhaRepo'
import Linha from "../../../src/domain/linha";
import LinhaService from '../../../src/services/linhaService'
import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import { NoId } from "../../../src/domain/noId";
import { TipoViaturaId } from "../../../src/domain/tipoViaturaId";
import { TipoTripulanteId } from "../../../src/domain/tipoTripulanteId";
import { ITipoTripulanteRepo } from "../../../src/services/IRepos/ITipoTripulanteRepo";
import { ITipoViaturaRepo } from "../../../src/services/IRepos/ITipoViaturaRepo";
import { INoRepo } from "../../../src/services/IRepos/INoRepo";

describe("LinhaService", function () {

    //Stub do DTO da linha que vamos "criar".
    const stubValue = {
        "id": "a",
        "codigo": "A",
        "nome": "Cristelo-Lordelo",
        "cor": "rosa",
        "noInicial": "1",
        "noFinal": "2",
        "viaturasPermitidas": ["5"],
        "viaturasProibidas": ["9"],
        "tripulantesPermitidos": ["7"],
        "tripulantesProibidos": ["6"]
    } as ILinhaDTO;

    /**
     * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
     */
    //NoId:
    const noInicial = NoId.create(new UniqueEntityID(stubValue.noInicial));
    const noFinal = NoId.create(new UniqueEntityID(stubValue.noFinal));

    let index = 0;
    //Tipo de viaturas permitidas
    let viaturasPermitidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasPermitidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasPermitidas[index]));
        viaturasPermitidas.push(viaturaId);
    }

    //Tipo de viaturas proibidas
    let viaturasProibidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasProibidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasProibidas[index]));
        viaturasProibidas.push(viaturaId);
    }

    //Tipo de tripulantes permitidos
    let tripulantesPermitidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesPermitidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesPermitidos[index]));
        tripulantesPermitidos.push(tripulanteId);
    }

    //Tipo de tripulantes proibidos
    let tripulantesProibidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesProibidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesProibidos[index]));
        tripulantesProibidos.push(tripulanteId);
    }

    //Linha criada com o stub do DTO.
    const stubLinha = Linha.create(stubValue, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue.id)).getValue();

    //----------------------------LinhaRepo------------------------------
    let linhaRepoClass = require('../../../src/repos/linhaRepo').default;
    let linhaRepoInstance: ILinhaRepo = Container.get(linhaRepoClass);
    Container.set(config.repos.linha.name, linhaRepoInstance);
    linhaRepoInstance = Container.get(config.repos.linha.name);

    //----------------------------TipoTripulanteRepo---------------------------
    let tipoTripulanteRepoClass = require('../../../src/repos/tipoTripulanteRepo').default;
    let tipoTripulanteRepoInstance: ITipoTripulanteRepo = Container.get(tipoTripulanteRepoClass);
    Container.set(config.repos.tipoTripulante.name, tipoTripulanteRepoInstance);
    tipoTripulanteRepoInstance = Container.get(config.repos.tipoTripulante.name);

    //----------------------------TipoViaturaRepo---------------------------
    let tipoViaturaRepoClass = require('../../../src/repos/tipoViaturaRepo').default;
    let tipoViaturaRepoInstance: ITipoViaturaRepo = Container.get(tipoViaturaRepoClass);
    Container.set(config.repos.tipoViatura.name, tipoViaturaRepoInstance);
    tipoViaturaRepoInstance = Container.get(config.repos.tipoViatura.name);

    //---------------------------------NoRepo---------------------------------------
    let noRepoClass = require('../../../src/repos/noRepo').default;
    let noRepoInstance: INoRepo = Container.get(noRepoClass);
    Container.set(config.repos.no.name, noRepoInstance);
    noRepoInstance = Container.get(config.repos.no.name);

    describe("criarLinha", function () {

        it("should create a Linha", async function () {

            //----------------------------LinhaRepo-------------------------------
            sinon.stub(linhaRepoInstance, "save").returns(stubLinha);

            //----------------------------TipoTripulanteRepo---------------------------
            var stub3 = sinon.stub(tipoTripulanteRepoInstance, "exists").withArgs("7").returns(true);
            stub3.withArgs("6").returns(true);

            //----------------------------TipoViaturaRepo---------------------------
            var stub4 = sinon.stub(tipoViaturaRepoInstance, "exists").withArgs("5").returns(true);
            stub4.withArgs("9").returns(true);

            //---------------------------------NoRepo---------------------------------------
            var stub5 = sinon.stub(noRepoInstance, "exists").withArgs("1").returns(true);
            stub5.withArgs("2").returns(true);

            //Chamamos a classe a testar, instanciando com o construtor e com o parametro vindo do container.
            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            //Testar método
            const result = (await linhaService.criarLinha(stubValue)).getValue();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.codigo).to.equal(stubValue.codigo);
            chai.expect(result.nome).to.equal(stubValue.nome);
            chai.expect(result.cor).to.equal(stubValue.cor);
            chai.expect(result.noInicial).to.equal(stubValue.noInicial);
            chai.expect(result.noFinal).to.equal(stubValue.noFinal);
            chai.expect(result.viaturasPermitidas[0]).to.equal(stubValue.viaturasPermitidas[0]);
            chai.expect(result.viaturasProibidas[0]).to.equal(stubValue.viaturasProibidas[0]);
            chai.expect(result.tripulantesPermitidos[0]).to.equal(stubValue.tripulantesPermitidos[0]);
            chai.expect(result.tripulantesProibidos[0]).to.equal(stubValue.tripulantesProibidos[0]);

            sinon.restore();
        })
    })

    describe("listaOrdenadaPor", function () {
        it("deve listar as linhas ordenadas por código/nome", async function () {

            sinon.stub(linhaRepoInstance, "findAll").returns([stubLinha]);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const result = (await linhaService.listaOrdenadaPor("codigo")).getValue();

            sinon.restore();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].codigo).to.equal(stubValue.codigo);
            chai.expect(result[0].nome).to.equal(stubValue.nome);
            chai.expect(result[0].cor).to.equal(stubValue.cor);
            chai.expect(result[0].noInicial).to.equal(stubValue.noInicial);
            chai.expect(result[0].noFinal).to.equal(stubValue.noFinal);
            chai.expect(result[0].viaturasPermitidas[0]).to.equal(stubValue.viaturasPermitidas[0]);
            chai.expect(result[0].viaturasProibidas[0]).to.equal(stubValue.viaturasProibidas[0]);
            chai.expect(result[0].tripulantesPermitidos[0]).to.equal(stubValue.tripulantesPermitidos[0]);
            chai.expect(result[0].tripulantesProibidos[0]).to.equal(stubValue.tripulantesProibidos[0]);

        });
    });

    describe("listaComecadasPor", function () {
        it("deve listar as linhas que começam por um código/nome", async function () {

            sinon.stub(linhaRepoInstance, "findLinhaComNomeComecadaPor").withArgs("Cris").returns([stubLinha]);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const result = (await linhaService.listaComecadasPor("nome", "Cris")).getValue();

            sinon.restore();

            chai.expect(result[0].id).to.equal(stubValue.id);
            chai.expect(result[0].codigo).to.equal(stubValue.codigo);
            chai.expect(result[0].nome).to.equal(stubValue.nome);
            chai.expect(result[0].cor).to.equal(stubValue.cor);
            chai.expect(result[0].noInicial).to.equal(stubValue.noInicial);
            chai.expect(result[0].noFinal).to.equal(stubValue.noFinal);
            chai.expect(result[0].viaturasPermitidas[0]).to.equal(stubValue.viaturasPermitidas[0]);
            chai.expect(result[0].viaturasProibidas[0]).to.equal(stubValue.viaturasProibidas[0]);
            chai.expect(result[0].tripulantesPermitidos[0]).to.equal(stubValue.tripulantesPermitidos[0]);
            chai.expect(result[0].tripulantesProibidos[0]).to.equal(stubValue.tripulantesProibidos[0]);

        });
    });

    describe("getLinhaById", function () {
        it("deve retornar a linha com o id enviado por parametro", async function () {

            sinon.stub(linhaRepoInstance, "findByDomainId").withArgs("a").returns(stubLinha);

            const linhaService = new LinhaService(linhaRepoInstance as ILinhaRepo, noRepoInstance as INoRepo, tipoViaturaRepoInstance as ITipoViaturaRepo, tipoTripulanteRepoInstance as ITipoTripulanteRepo);

            const result = (await linhaService.getLinhaById("a")).getValue();

            sinon.restore();

            chai.expect(result.id).to.equal(stubValue.id);
            chai.expect(result.codigo).to.equal(stubValue.codigo);
            chai.expect(result.nome).to.equal(stubValue.nome);
            chai.expect(result.cor).to.equal(stubValue.cor);
            chai.expect(result.noInicial).to.equal(stubValue.noInicial);
            chai.expect(result.noFinal).to.equal(stubValue.noFinal);
            chai.expect(result.viaturasPermitidas[0]).to.equal(stubValue.viaturasPermitidas[0]);
            chai.expect(result.viaturasProibidas[0]).to.equal(stubValue.viaturasProibidas[0]);
            chai.expect(result.tripulantesPermitidos[0]).to.equal(stubValue.tripulantesPermitidos[0]);
            chai.expect(result.tripulantesProibidos[0]).to.equal(stubValue.tripulantesProibidos[0]);
        });
    });

})