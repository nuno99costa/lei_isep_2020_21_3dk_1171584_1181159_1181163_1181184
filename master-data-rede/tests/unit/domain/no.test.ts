import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import No from "../../../src/domain/no";
import INoDTO from "../../../src/dto/INoDTO";

const sinon = require('sinon');
const chai = require('chai');

describe("No", function () {

    //Stub do DTO do nó que vamos "criar".
    const stubValue = {
        id: "aaa",
        nomeCompleto: "Espinho",
        abreviatura: "Esp",
        isPontoRendicao: true,
        isEstacaoRecolha: true,
        latitude: 12,
        longitude: -12.3
    } as INoDTO;

    //No criado com o stub do DTO.
    const stubNo = No.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id and NoId", function () {
        it("should return id", async function () {
            chai.expect(stubNo.id.toString()).to.equal(new UniqueEntityID("aaa").toString());

        })
    })

    describe("get NoId", function () {
        it("should return id", async function () {
            chai.expect(stubNo.noId.id).to.equal(stubValue.id);

        })
    })

    describe("get NomeCompleto", function () {
        it("should return nomeCompleto", async function () {
            chai.expect(stubNo.nomeCompleto).to.equal(stubValue.nomeCompleto);

        })
    })

    describe("get abreviatura", function () {
        it("should return abreviatura", async function () {
            chai.expect(stubNo.abreviatura).to.equal(stubValue.abreviatura);

        })
    })

    describe("get isPontoRendicao", function () {
        it("should return isPontoRendicao", async function () {
            chai.expect(stubNo.isPontoRendicao).to.equal(stubValue.isPontoRendicao);

        })
    })

    describe("get isEstacaoRecolha", function () {
        it("should return isEstacaoRecolha", async function () {
            chai.expect(stubNo.isEstacaoRecolha).to.equal(stubValue.isEstacaoRecolha);

        })
    })

    describe("get latitude", function () {
        it("should return latitude", async function () {
            chai.expect(stubNo.latitude).to.equal(stubValue.latitude);

        })
    })

    describe("get longitude", function () {
        it("should return longitude", async function () {
            chai.expect(stubNo.longitude).to.equal(stubValue.longitude);

        })
    })

    describe("create", function () {
        it("should create a new No", async function () {

            const newNo = No.create(stubValue, new UniqueEntityID(stubValue.id));
            chai.expect(newNo.isSuccess).to.be.true;

        })

    })

    describe("compareByNome", function () {
        it("should compare 2 nos", async function () {

            const no1 = No.create({
                id: "aaa",
                nomeCompleto: "Algarve",
                abreviatura: "Esp",
                isPontoRendicao: true,
                isEstacaoRecolha: true,
                latitude: 12,
                longitude: -12.3,
                nomeModelo: "busstop"
            }, new UniqueEntityID("b")).getValue();

            const no2 = No.create({
                id: "aaa",
                nomeCompleto: "Espinho",
                abreviatura: "Esp",
                isPontoRendicao: true,
                isEstacaoRecolha: true,
                latitude: 12,
                longitude: -12.3,
                nomeModelo: "busstop"
            }, new UniqueEntityID("a")).getValue();

            const result1 = No.compareByID(no1, no2);
            const result2 = No.compareByID(no2, no1);
            const result3 = No.compareByID(no1, no1);
            chai.expect(result1).to.equal(1);
            chai.expect(result2).to.equal(-1);
            chai.expect(result3).to.equal(0);

        })

    })

    describe("compareByCodigo", function () {
        it("should compare 2 nos", async function () {

            const no1 = No.create(stubValue, new UniqueEntityID("a")).getValue();
            const no2 = No.create(stubValue, new UniqueEntityID("b")).getValue();
            const no3 = No.create(stubValue, new UniqueEntityID("a")).getValue();
            const result1 = No.compareByID(no1, no2);
            const result2 = No.compareByID(no2, no1);
            const result3 = No.compareByID(no1, no3);
            chai.expect(result1).to.equal(-1);
            chai.expect(result2).to.equal(1);
            chai.expect(result3).to.equal(0);

        })

    })
})