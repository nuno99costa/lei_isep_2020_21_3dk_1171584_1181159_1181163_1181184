const sinon = require('sinon');
const chai = require('chai');
import { UniqueEntityID } from '../../../src/core/domain/UniqueEntityID';
import Linha from '../../../src/domain/linha'
import { NoId } from '../../../src/domain/noId';
import { TipoTripulanteId } from '../../../src/domain/tipoTripulanteId';
import { TipoViaturaId } from '../../../src/domain/tipoViaturaId';
import ILinhaDTO from '../../../src/dto/ILinhaDTO';

describe("Linha", function () {

    //Stub do DTO da linha que vamos "criar".
    const stubValue = {
        "id": "a",
        "codigo": "A",
        "nome": "Cristelo-Lordelo",
        "cor": "rosa",
        "noInicial": "1",
        "noFinal": "2",
        "viaturasPermitidas": ["5"],
        "viaturasProibidas": ["9"],
        "tripulantesPermitidos": ["7"],
        "tripulantesProibidos": ["6"]
    } as ILinhaDTO;

    /**
     * NESTES MÉTODOS ABAIXO, VAMOS CRIAR AS INSTÂNCIAS PARA AS ENVIARMOS POR PARÂMETRO NO MÉTODO CREATE().
     */
    //NoId:
    const noInicial = NoId.create(new UniqueEntityID(stubValue.noInicial));
    const noFinal = NoId.create(new UniqueEntityID(stubValue.noFinal));

    let index = 0;
    //Tipo de viaturas permitidas
    let viaturasPermitidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasPermitidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasPermitidas[index]));
        viaturasPermitidas.push(viaturaId);
    }

    //Tipo de viaturas proibidas
    let viaturasProibidas: TipoViaturaId[] = [];
    for (index = 0; index < stubValue.viaturasProibidas.length; ++index) {
        const viaturaId = TipoViaturaId.create(new UniqueEntityID(stubValue.viaturasProibidas[index]));
        viaturasProibidas.push(viaturaId);
    }

    //Tipo de tripulantes permitidos
    let tripulantesPermitidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesPermitidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesPermitidos[index]));
        tripulantesPermitidos.push(tripulanteId);
    }

    //Tipo de tripulantes proibidos
    let tripulantesProibidos: TipoTripulanteId[] = [];
    for (index = 0; index < stubValue.tripulantesProibidos.length; ++index) {
        const tripulanteId = TipoTripulanteId.create(new UniqueEntityID(stubValue.tripulantesProibidos[index]));
        tripulantesProibidos.push(tripulanteId);
    }

    //Linha criada com o stub do DTO.
    const stubLinha = Linha.create(stubValue, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id", function () {
        it("should get the id", async function () {
            chai.expect(stubLinha.id.toString()).to.equal(new UniqueEntityID("a").toString());
        })
    })

    describe("get linhaId", function () {
        it("should get the id", async function () {
            chai.expect(stubLinha.linhaId.id).to.equal("a");
        })
    })

    describe("get codigo", function () {
        it("should get the codigo", async function () {
            chai.expect(stubLinha.codigo).to.equal(stubValue.codigo);
        })
    })

    describe("get nome", function () {
        it("should get the nome", async function () {
            chai.expect(stubLinha.nome).to.equal(stubValue.nome);
        })
    })

    describe("get cor", function () {
        it("should get the cor", async function () {
            chai.expect(stubLinha.cor).to.equal(stubValue.cor);
        })
    })

    describe("get noInicial", function () {
        it("should get the id of the no Inicial", async function () {
            chai.expect(stubLinha.noInicial.id).to.equal(stubValue.noInicial);
        })
    })

    describe("get noFinal", function () {
        it("should get the id of the no Final", async function () {
            chai.expect(stubLinha.noFinal.id).to.equal(stubValue.noFinal);
        })
    })

    //PRoximos 4 testam o permitidas/proibidas + permitidasEmString/proibidasEmString
    describe("get viaturasPermitidas", function () {
        it("should get the id of the viaturas permitidas", async function () {
            chai.expect(stubLinha.viaturasPermitidas[0].id).to.equal(stubValue.viaturasPermitidas[0]);
            chai.expect(stubLinha.viaturasPermitidasEmString[0]).to.equal(stubValue.viaturasPermitidas[0]);
        })
    })

    describe("get viaturasProibidas", function () {
        it("should get the id of the viaturas proibidas", async function () {
            chai.expect(stubLinha.viaturasProibidas[0].id).to.equal(stubValue.viaturasProibidas[0]);
            chai.expect(stubLinha.viaturasProibidasEmString[0]).to.equal(stubValue.viaturasProibidas[0]);
        })
    })

    describe("get tripulantesPermitidas", function () {
        it("should get the id of the tipo tripulante permitido", async function () {
            chai.expect(stubLinha.tripulantesPermitidos[0].id).to.equal(stubValue.tripulantesPermitidos[0]);
            chai.expect(stubLinha.tripulantesPermitidosEmString[0]).to.equal(stubValue.tripulantesPermitidos[0]);
        })
    })

    describe("get tripulantesProibidos", function () {
        it("should get the id of the tipo de tripulante proibido", async function () {
            chai.expect(stubLinha.tripulantesProibidos[0].id).to.equal(stubValue.tripulantesProibidos[0]);
            chai.expect(stubLinha.tripulantesProibidosEmString[0]).to.equal(stubValue.tripulantesProibidos[0]);
        })
    })

    describe("create", function () {
        it("should create a new linha", async function () {

            const newLinha = Linha.create(stubValue, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(stubValue.id));
            chai.expect(newLinha.isSuccess).to.be.true;
            
        })            
    
    })
})
    

