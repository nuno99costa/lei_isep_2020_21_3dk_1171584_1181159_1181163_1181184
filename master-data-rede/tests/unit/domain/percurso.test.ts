import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import Percurso from "../../../src/domain/percurso";
import { SegmentoId } from "../../../src/domain/segmentoId";
import IPercursoDTO from "../../../src/dto/IPercursoDTO";


const sinon = require('sinon');
const chai = require('chai');

describe("Percurso", function () {

    const stubValue = {
        id: "111",
        nomePercurso: "SãoJoãoMadeira_Espinho",
        linhaId: "1000",
        orientacao: "Ida",
        listaSegmentos: ["111", "222"],
        noInicialId: "111",
        noFinalId: "333",
        distanciaTotal: 12000,
        tempoTotal: 1500
    } as IPercursoDTO;

    // criado com o stub do DTO.
    const stubPercurso = Percurso.create(stubValue, [SegmentoId.create( new UniqueEntityID("111")), SegmentoId.create( new UniqueEntityID("222"))], stubValue.distanciaTotal, stubValue.tempoTotal, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id", function (){
        it("should return id", async function () {
            chai.expect(stubPercurso.id.toString()).to.equal(new UniqueEntityID("111").toString());

        })
    })

    describe("get percursoId", function (){
        it("should return id", async function () {
            chai.expect(stubPercurso.percursoId.id).to.equal(stubValue.id);
            
        })
    })

    describe("get orientacao", function (){
        it("should return orientacao", async function () {
            chai.expect(stubPercurso.orientacao).to.equal(stubValue.orientacao);
            
        })
    })

    describe("get linhaId", function (){
        it("should return id", async function () {
            chai.expect(stubPercurso.linhaId.id).to.equal(stubValue.linhaId);
            
        })
    })

    describe("get NomePercurso", function (){
        it("should return nomePercurso", async function () {
            chai.expect(stubPercurso.nomePercurso).to.equal(stubValue.nomePercurso);
            
        })
    })

    describe("get noInicialId", function (){
        it("should return noInicialId", async function () {
            chai.expect(stubPercurso.noInicialId.id).to.equal(stubValue.noInicialId);
            
        })
    })

    describe("get noFinalId", function (){
        it("should return noFinalId", async function () {
            chai.expect(stubPercurso.noFinalId.id).to.equal(stubValue.noFinalId);
            
        })
    })

    describe("get listaSegmentos", function (){
        it("should return listaSegmentos", async function () {
            chai.expect(stubPercurso.listaSegmentos[0].id).to.equal(stubValue.listaSegmentos[0]);
            chai.expect(stubPercurso.listaSegmentos[1].id).to.equal(stubValue.listaSegmentos[1]);
        })
    })

    describe("get distanciaTotal", function (){
        it("should return distanciaTotal", async function () {
            chai.expect(stubPercurso.distanciaTotal).to.equal(stubValue.distanciaTotal);
            
        })
    })

    describe("get tempoTotal", function (){
        it("should return tempoTotal", async function () {
            chai.expect(stubPercurso.tempoTotal).to.equal(stubValue.tempoTotal);
            
        })
    })

    describe("create", function () {
        it("should create a new No", async function () {

            const newPercurso = Percurso.create(stubValue, [SegmentoId.create( new UniqueEntityID("111")), SegmentoId.create( new UniqueEntityID("222"))], stubValue.distanciaTotal, stubValue.tempoTotal, new UniqueEntityID(stubValue.id));
            chai.expect(newPercurso.isSuccess).to.be.true;
            
        })            
    
    })
})