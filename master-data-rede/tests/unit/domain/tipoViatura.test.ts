import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import TipoViatura from "../../../src/domain/tipoViatura";
import ITipoViaturaDTO from "../../../src/dto/ITipoViaturaDTO";

const sinon = require('sinon');
const chai = require('chai');

describe("TipoViatura", function () {

    const stubValue = {
        "id": "1",
        "codigo": "1",
        "descricao": "viatura1",
        "combustivel": "Gasolina",
        "autonomia": 65,
        "velocidadeMedia": 45,
        "custoPorQuilometro": 14,
        "consumoMedio": 22.352
    } as ITipoViaturaDTO;

    //Tipo de viatura criado com o stub do DTO.
    const stubTipoViatura = TipoViatura.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id", function (){
        it("should return id", async function () {
            chai.expect(stubTipoViatura.id.toString()).to.equal(new UniqueEntityID("1").toString());

        })
    })

    describe("get tipoViaturaId", function (){
        it("should return id do tipo de viatura", async function () {
            chai.expect(stubTipoViatura.tipoViaturaId.id).to.equal(stubValue.id);
            
        })
    })

    describe("get codigo", function (){
        it("should return codigo", async function () {
            chai.expect(stubTipoViatura.codigo).to.equal(stubValue.codigo);
            
        })
    })

    describe("get descricao", function (){
        it("should return descricao", async function () {
            chai.expect(stubTipoViatura.descricao).to.equal(stubValue.descricao);
            
        })
    })

    describe("get combustivel", function (){
        it("should return combustivel", async function () {
            chai.expect(stubTipoViatura.combustivel.toString()).to.equal(stubValue.combustivel);
            
        })
    })

    describe("get autonomia", function (){
        it("should return autunomia", async function () {
            chai.expect(stubTipoViatura.autonomia).to.equal(stubValue.autonomia);
            
        })
    })

    describe("get velocidadeMedia", function (){
        it("should return velocidadeMedia", async function () {
            chai.expect(stubTipoViatura.velocidadeMedia).to.equal(stubValue.velocidadeMedia);
            
        })
    })

    describe("get custoPorQuilometro", function (){
        it("should return custoPorQuilometro", async function () {
            chai.expect(stubTipoViatura.custoPorQuilometro).to.equal(stubValue.custoPorQuilometro);
            
        })
    })

    describe("get consumoMEdio", function (){
        it("should return consumoMedio", async function () {
            chai.expect(stubTipoViatura.consumoMedio).to.equal(stubValue.consumoMedio);
            
        })
    })

    describe("create", function () {
        it("should create a new Tipo Viatura", async function () {

            const newTipo = TipoViatura.create(stubValue, new UniqueEntityID(stubValue.id));  
            chai.expect(newTipo.isSuccess).to.be.true;
            
        })            
    
    })
})