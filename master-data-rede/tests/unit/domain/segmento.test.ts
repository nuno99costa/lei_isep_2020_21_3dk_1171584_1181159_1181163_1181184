import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import Segmento from "../../../src/domain/segmento";
import ISegmentoDTO from "../../../src/dto/ISegmentoDTO";

const sinon = require('sinon');
const chai = require('chai');

describe("No", function () {

    const stubValue = {
        id: "aaa",
        nomeSegmento: "SãoJoãoMadeira_SantaMariaFeira",
        idNoInicial: "111",
        idNoFinal: "222",
        distanciaNos: 2500,
        tempoNos: 300
    } as ISegmentoDTO;

    const stubSegmento = Segmento.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id", function (){
        it("should return id", async function () {
            chai.expect(stubSegmento.id.toString()).to.equal(new UniqueEntityID("aaa").toString());

        })
    })

    describe("get segmentoId", function (){
        it("should return id", async function () {
            chai.expect(stubSegmento.segmentoId.id).to.equal(stubValue.id);
            
        })
    })

    describe("get NomeSegmento", function (){
        it("should return nomeSegmento", async function () {
            chai.expect(stubSegmento.nomeSegmento).to.equal(stubValue.nomeSegmento);
            
        })
    })

    describe("get idNoInicial", function (){
        it("should return idNoInicial", async function () {
            chai.expect(stubSegmento.idNoInicial.id).to.equal(stubValue.idNoInicial);
            
        })
    })

    describe("get idNofinal", function (){
        it("should return idNoFinal", async function () {
            chai.expect(stubSegmento.idNoFinal.id).to.equal(stubValue.idNoFinal);
            
        })
    })

    describe("get distanciaNos", function (){
        it("should return distanciaNos", async function () {
            chai.expect(stubSegmento.distanciaNos).to.equal(stubValue.distanciaNos);
            
        })
    })

    describe("get tempoNos", function (){
        it("should return tempoNos", async function () {
            chai.expect(stubSegmento.tempoNos).to.equal(stubValue.tempoNos);
            
        })
    })

    describe("create", function () {
        it("should create a new Segmento", async function () {

            const newSeg = Segmento.create(stubValue, new UniqueEntityID(stubValue.id));    
            chai.expect(newSeg.isSuccess).to.be.true;
            
        })            
    
    })
})