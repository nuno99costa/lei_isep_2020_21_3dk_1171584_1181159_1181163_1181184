import { UniqueEntityID } from "../../../src/core/domain/UniqueEntityID";
import TipoTripulante from "../../../src/domain/tipoTripulante";
import ITipoTripulanteDTO from "../../../src/dto/ITipoTripulanteDTO";

const sinon = require('sinon');
const chai = require('chai');

describe("TipoTripulante", function () {

    //Stub do DTO do nó que vamos "criar".
    const stubValue = {
        id: "1122",
        codigo: "abc",
        descricao: "Gosta"
    } as ITipoTripulanteDTO;

    //No criado com o stub do DTO.
    const stubTipoTripulante = TipoTripulante.create(stubValue, new UniqueEntityID(stubValue.id)).getValue();

    describe("get id", function (){
        it("should return id", async function () {
            chai.expect(stubTipoTripulante.id.toString()).to.equal(new UniqueEntityID("1122").toString());

        })
    })

    describe("get tipoTripulanteId", function (){
        it("should return id do tipo de tripulante", async function () {
            chai.expect(stubTipoTripulante.tipoViaturaId.id).to.equal(stubValue.id);
            
        })
    })

    describe("get codigo", function (){
        it("should return codigo", async function () {
            chai.expect(stubTipoTripulante.codigo).to.equal(stubValue.codigo);
            
        })
    })

    describe("get descricao", function (){
        it("should return descricao", async function () {
            chai.expect(stubTipoTripulante.descricao).to.equal(stubValue.descricao);
            
        })
    })

    describe("create", function () {
        it("should create a new tipo de tripulante", async function () {

            const newTipo = TipoTripulante.create(stubValue, new UniqueEntityID(stubValue.id))  
            chai.expect(newTipo.isSuccess).to.be.true;
            
        })            
    
    })
})