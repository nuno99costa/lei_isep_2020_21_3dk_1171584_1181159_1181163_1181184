const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import LinhaController from '../../../src/controllers/linhaController';
import ILinhaService from '../../../src/services/IServices/ILinhaService'
import ILinhaDTO from '../../../src/dto/ILinhaDTO';
import { func } from 'joi';

describe('LinhaController', function () {
    let body = {
        "codigo": "A",
        "nome": "Cristelo-Lordelo",
        "cor": "rosa",
        "noInicial": "1",
        "noFinal": "2",
        "viaturasPermitidas": ["5"],
        "viaturasProibidas": ["9"],
        "tripulantesPermitidos": ["7"],
        "tripulantesProibidos": ["6"]
    };

    describe('criarLinha', function () {
        it('criarLinha: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let linhaServiceClass = require('../../../src/services/linhaService').default;
            let linhaServiceInstance = Container.get(linhaServiceClass)
            Container.set(config.services.linha.name, linhaServiceInstance);

            linhaServiceInstance = Container.get(config.services.linha.name);
            sinon.stub(linhaServiceInstance, "criarLinha").returns(Result.ok<ILinhaDTO>({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            } as ILinhaDTO));

            const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);

            await ctrl.criarLinha(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            }));
            sinon.restore();
        });
    });

    describe('linhasOrdenadasPor', function () {
        it('linhasOrdenadasPor: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;
            req.params = { tipo: "nome" };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let linhaServiceClass = require('../../../src/services/linhaService').default;
            let linhaServiceInstance = Container.get(linhaServiceClass)
            Container.set(config.services.linha.name, linhaServiceInstance);

            linhaServiceInstance = Container.get(config.services.linha.name);

            sinon.stub(linhaServiceInstance, "listaOrdenadaPor").returns(Result.ok<ILinhaDTO>({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            } as ILinhaDTO));

            const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);

            await ctrl.linhasOrdenadasPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            }));
            sinon.restore();
        });
    });

    describe('linhasComecadasPor', function () {
        it('linhasComecadasPor: retorna linhas com as iniciais do codigo/nome ', async function () {
            let req: Partial<Request> = {};
            req.params = { tipo: "codigo", iniciais: "A" };
            req.body = body;
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let linhaServiceClass = require('../../../src/services/linhaService').default;
            let linhaServiceInstance = Container.get(linhaServiceClass)
            Container.set(config.services.linha.name, linhaServiceInstance);

            linhaServiceInstance = Container.get(config.services.linha.name);

            sinon.stub(linhaServiceInstance, "listaComecadasPor").returns(Result.ok<ILinhaDTO>({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            } as ILinhaDTO));

            const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);

            await ctrl.linhasComecadasPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            }));
            sinon.restore();
        });
    });

    describe("getLinhaById", function () {
        it("deve retornar uma linha com o id correspondente ao que é passado por parametro", async function () {
            let req: Partial<Request> = {};
            req.params = { id: "aaa" };
            req.body = body;
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let linhaServiceClass = require('../../../src/services/linhaService').default;
            let linhaServiceInstance = Container.get(linhaServiceClass)
            Container.set(config.services.linha.name, linhaServiceInstance);

            linhaServiceInstance = Container.get(config.services.linha.name);

            sinon.stub(linhaServiceInstance, "getLinhaById").returns(Result.ok<ILinhaDTO>({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            } as ILinhaDTO));

            const ctrl = new LinhaController(linhaServiceInstance as ILinhaService);

            await ctrl.getLinhaById(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "nome": req.body.nome,
                "cor": req.body.cor,
                "noInicial": req.body.noInicial,
                "noFinal": req.body.noFinal,
                "viaturasPermitidas": req.body.viaturasPermitidas,
                "viaturasProibidas": req.body.viaturasProibidas,
                "tripulantesPermitidos": req.body.tripulantesPermitidos,
                "tripulantesProibidos": req.body.tripulantesProibidos
            }));
            sinon.restore();
        })
    });
});