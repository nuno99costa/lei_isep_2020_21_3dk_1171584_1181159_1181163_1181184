const sinon = require('sinon').createSandbox();

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import PercursoController from '../../../src/controllers/percursoController';
import IPercursoService from '../../../src/services/IServices/IPercursoService'
import IPercursoDTO from '../../../src/dto/IPercursoDTO';

describe('PercursoController', function() {

    it('criarPercurso: returns json with values', async function() {
        let body = {
            "nomePercurso": 'SaoJoaoMadeira_Espinho',
            "linhaId": "1000",
            "orientacao": "Ida",
            "listaSegmentos": ["SaoJoaoMadeira_SantaMariaFeira", "SantaMariaFeira_Espinho"],
            "noInicialId": "111",
            "noFinalId": "333",
            "distanciaTotal": 12000,
            "tempoTotal": 1500
        };
        let req: Partial<Request> = {};
        req.body = body;

        let res, status, json;
        status = sinon.stub();
        json = sinon.spy();
        res = { status, json };
        status.returns(res);

        let next: Partial<NextFunction> = () => { };

        let percursoServiceClass = require('../../../src/services/percursoService').default;
        let percursoServiceInstance = Container.get(percursoServiceClass);
        Container.set(config.services.percurso.name, percursoServiceInstance);

        percursoServiceInstance = Container.get(config.services.percurso.name);
        sinon.stub(percursoServiceInstance, "criarPercurso").returns(Result.ok<IPercursoDTO>({
            "id": "aaa",
            "nomePercurso": req.body.nomePercurso,
            "linhaId": req.body.linhaId,
            "orientacao": req.body.orientacao,
            "listaSegmentos": req.body.listaSegmentos,
            "noInicialId": req.body.noInicialId,
            "noFinalId": req.body.noFinalId,
            "distanciaTotal": req.body.distanciaTotal,
            "tempoTotal": req.body.tempoTotal
        } as IPercursoDTO));

        const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);

        await ctrl.criarPercurso(<Request>req, res, <NextFunction>next);

        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match({
            "id": "aaa",
            "nomePercurso": req.body.nomePercurso,
            "linhaId": req.body.linhaId,
            "orientacao": req.body.orientacao,
            "listaSegmentos": req.body.listaSegmentos,
            "noInicialId": req.body.noInicialId,
            "noFinalId": req.body.noFinalId,
            "distanciaTotal": req.body.distanciaTotal,
            "tempoTotal": req.body.tempoTotal
        }));

        sinon.restore();
    })

    it('listaPercursos: returns json with values', async function() {
        let body = {
            "nomePercurso": 'SaoJoaoMadeira_Espinho',
            "linhaId": "1000",
            "orientacao": "Ida",
            "listaSegmentos": ["SaoJoaoMadeira_SantaMariaFeira", "SantaMariaFeira_Espinho"],
            "noInicialId": "111",
            "noFinalId": "333",
            "distanciaTotal": 12000,
            "tempoTotal": 1500
        };
        let req: Partial<Request> = {};
        req.params = { id: "1000" };
        let res, status, json;
        status = sinon.stub();
        json = sinon.spy();
        res = { status, json };
        status.returns(res);

        let next: Partial<NextFunction> = () => { };

        let percursoServiceClass = require('../../../src/services/percursoService').default;
        let percursoServiceInstance = Container.get(percursoServiceClass);
        Container.set(config.services.percurso.name, percursoServiceInstance);

        percursoServiceInstance = Container.get(config.services.percurso.name);

        sinon.stub(percursoServiceInstance, "listaPercursos").returns(Result.ok<IPercursoDTO[]>([{
            "id": "aaa",
            "nomePercurso": body.nomePercurso,
            "linhaId": body.linhaId,
            "orientacao": body.orientacao,
            "listaSegmentos": body.listaSegmentos,
            "noInicialId": body.noInicialId,
            "noFinalId": body.noFinalId,
            "distanciaTotal": body.distanciaTotal,
            "tempoTotal": body.tempoTotal
        }] as IPercursoDTO[]));

        const ctrl = new PercursoController(percursoServiceInstance as IPercursoService);

        await ctrl.listarPercursos(<Request>req, res, <NextFunction>next);

        sinon.assert.calledOnce(res.json);
        sinon.assert.calledWith(res.json, sinon.match([{
            "id": "aaa",
            "nomePercurso": body.nomePercurso,
            "linhaId": body.linhaId,
            "orientacao": body.orientacao,
            "listaSegmentos": body.listaSegmentos,
            "noInicialId": body.noInicialId,
            "noFinalId": body.noFinalId,
            "distanciaTotal": body.distanciaTotal,
            "tempoTotal": body.tempoTotal
        }]));

        sinon.restore();

    })
})

afterEach(function() {
    // completely restore all fakes created through the sandbox
    sinon.restore();
});
;