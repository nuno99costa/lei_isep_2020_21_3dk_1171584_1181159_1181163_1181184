const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import { Role } from '../../../src/domain/user';
import { IUserDTO } from '../../../src/dto/IUserDTO';
import UserController from '../../../src/controllers/userController';
import IUserService from '../../../src/services/IServices/IUserService';


describe('UserController', function () {
    let body = {
        "name": "Nuno",
        "email": "nuno@gmail.com",
        "password": "ola123",
        "dataNascimento": "03/04/2020",
        "role": Role.Administrador
    };

    describe("registarUser", function () {
        it('registarUser: returns json with values', async function () {

            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let userServiceClass = require('../../../src/services/userService').default;
            let userServiceInstance = Container.get(userServiceClass)
            Container.set(config.services.user.name, userServiceInstance);

            userServiceInstance = Container.get(config.services.user.name);

            sinon.stub(userServiceInstance, "registarUser").returns(Result.ok<IUserDTO>({
                "name": "Nuno",
                "email": "nuno@gmail.com",
                "password": "ola123",
                "dataNascimento": "03/04/2020",
                "role": Role.Administrador
            } as IUserDTO));

            const ctrl = new UserController(userServiceInstance as IUserService);

            await ctrl.registarUser(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "name": "Nuno",
                "email": "nuno@gmail.com",
                "password": "ola123",
                "dataNascimento": "03/04/2020",
                "role": Role.Administrador
            }));

            sinon.restore();
        });
    })

    

});