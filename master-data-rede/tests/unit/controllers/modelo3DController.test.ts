const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import IModelo3DDTO from "../../../src/dto/IModelo3DDTO";
import IModelo3DService from '../../../src/services/IServices/IModelo3DService';
import Modelo3DController from '../../../src/controllers/modelo3DController';

describe('Modelo3DController', function () {
    let body = {
        "id": "Nuno"
    };

    describe("getByNome", function () {
        it('getByNome: returns json with values', async function () {

            let req: Partial<Request> = {};
            req.params = {
                id: "Nuno"
            };
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let modeloServiceClass = require('../../../src/services/modelo3DService').default;
            let modeloServiceInstance = Container.get(modeloServiceClass)
            Container.set(config.services.modelo3D.name, modeloServiceInstance);

            modeloServiceInstance = Container.get(config.services.modelo3D.name);

            sinon.stub(modeloServiceInstance, "getModelo3DByNome").returns(Result.ok<IModelo3DDTO>({
                "nome": "Nuno",
                "path": "http://localhost:3000/modelos/Nuno"
            } as IModelo3DDTO));

            const ctrl = new Modelo3DController(modeloServiceInstance as IModelo3DService);

            await ctrl.getModeloByNome(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "nome": "Nuno",
                "path": "http://localhost:3000/modelos/Nuno"
            }));

            sinon.restore();
        });
    })

    

});