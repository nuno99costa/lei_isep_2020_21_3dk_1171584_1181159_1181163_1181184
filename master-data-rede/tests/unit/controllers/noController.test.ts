const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import NoController from '../../../src/controllers/noController';
import INoService from '../../../src/services/IServices/INoService'
import INoDTO from '../../../src/dto/INoDTO';

describe('NoController', function () {
    let body = {
        "nomeCompleto": 'Espinho',
        "abreviatura": 'ESP',
        "isPontoRendicao": true,
        "isEstacaoRecolha": false,
        "latitude": 12.3,
        "longitude": -8.4
    };
    describe("criarNo", function () {
        it('criarNo: returns json with values', async function () {

            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let noServiceClass = require('../../../src/services/noService').default;
            let noServiceInstance = Container.get(noServiceClass)
            Container.set(config.services.no.name, noServiceInstance);

            noServiceInstance = Container.get(config.services.no.name);

            sinon.stub(noServiceInstance, "criarNo").returns(Result.ok<INoDTO>({
                "id": "aaa",
                "nomeCompleto": req.body.nomeCompleto,
                "abreviatura": req.body.abreviatura,
                "isPontoRendicao": req.body.isPontoRendicao,
                "isEstacaoRecolha": req.body.isEstacaoRecolha,
                "latitude": req.body.latitude,
                "longitude": req.body.longitude
            } as INoDTO));

            const ctrl = new NoController(noServiceInstance as INoService);

            await ctrl.criarNo(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "nomeCompleto": req.body.nomeCompleto,
                "abreviatura": req.body.abreviatura,
                "isPontoRendicao": req.body.isPontoRendicao,
                "isEstacaoRecolha": req.body.isEstacaoRecolha,
                "latitude": req.body.latitude,
                "longitude": req.body.longitude
            }));

            sinon.restore();
        });
    })

    describe('nosOrdenadosPor', function () {
        it('should list nos ordenados por x', async function () {

            let req: Partial<Request> = {};
            req.params = { tipo: "nome" };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let noServiceClass = require('../../../src/services/noService').default;
            let noServiceInstance = Container.get(noServiceClass)
            Container.set(config.services.no.name, noServiceInstance);

            noServiceInstance = Container.get(config.services.no.name);

            sinon.stub(noServiceInstance, "listaOrdenadaPor").returns(Result.ok<INoDTO[]>([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }] as INoDTO[]
            ));

            const ctrl = new NoController(noServiceInstance as INoService);

            await ctrl.nosOrdenadosPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }]));

            sinon.restore();

        });

    });

    describe('nosComecadosPor', function () {
        it('should list nos começados por x', async function () {

            let req: Partial<Request> = {};
            req.params = {
                tipo: "nome",
                iniciais: "E"
            };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let noServiceClass = require('../../../src/services/noService').default;
            let noServiceInstance = Container.get(noServiceClass)
            Container.set(config.services.no.name, noServiceInstance);

            noServiceInstance = Container.get(config.services.no.name);

            sinon.stub(noServiceInstance, "listaComecadosPor").returns(Result.ok<INoDTO[]>([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }] as INoDTO[]
            ));

            const ctrl = new NoController(noServiceInstance as INoService);

            await ctrl.nosComecadosPor(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }]));

            sinon.restore();

        });

    });

    describe("getNoById", function () {
        it("deve retornar um nó com o id correspondente passado por parametro", async function () {
            let req: Partial<Request> = {};
            req.params = {
                id: "aaa"
            };
            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            let noServiceClass = require('../../../src/services/noService').default;
            let noServiceInstance = Container.get(noServiceClass)
            Container.set(config.services.no.name, noServiceInstance);

            noServiceInstance = Container.get(config.services.no.name);

            sinon.stub(noServiceInstance, "getNoById").returns(Result.ok<INoDTO[]>([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }] as INoDTO[]
            ));

            const ctrl = new NoController(noServiceInstance as INoService);

            await ctrl.getNoById(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeCompleto": body.nomeCompleto,
                "abreviatura": body.abreviatura,
                "isPontoRendicao": body.isPontoRendicao,
                "isEstacaoRecolha": body.isEstacaoRecolha,
                "latitude": body.latitude,
                "longitude": body.longitude

            }]));

            sinon.restore();

        });
    });

});