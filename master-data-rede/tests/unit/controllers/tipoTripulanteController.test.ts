const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import TipoTripulanteController from '../../../src/controllers/tipoTripulanteController';
import ITipoTripulanteDTO from '../../../src/dto/ITipoTripulanteDTO';
import ITipoTripulanteService from '../../../src/services/IServices/ITipoTripulanteService';

describe('TipoTripulanteController', function () {
    let body = {
        "codigo": "abc",
        "descricao": "Gosta de conduzir"
    };

    let tipoTripulanteServiceClass = require('../../../src/services/tipoTripulanteService').default;
    let tipoTripulanteServiceInstance = Container.get(tipoTripulanteServiceClass)
    Container.set(config.services.tipoTripulante.name, tipoTripulanteServiceInstance);

    tipoTripulanteServiceInstance = Container.get(config.services.tipoTripulante.name);

    const ctrl = new TipoTripulanteController(tipoTripulanteServiceInstance as ITipoTripulanteService);

    describe('criarTipoTripulante', function () {
        it('criarTipoTripulante: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;

            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(tipoTripulanteServiceInstance, "criarTipoTripulante").returns(Result.ok<ITipoTripulanteDTO>({
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            } as ITipoTripulanteDTO));

            await ctrl.criarTipoTripulante(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            }));
            sinon.restore();
        });
    });

    describe("getTipoTripulante", function () {
        it("deve retornar todos os tipos de Tripulante guardados", async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(tipoTripulanteServiceInstance, "getTipoTripulante").returns(Result.ok<ITipoTripulanteDTO[]>([{
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            }] as ITipoTripulanteDTO[]
            ));

            await ctrl.getTipoTripulante(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao
            }]));

            sinon.restore();

        });
    });

});