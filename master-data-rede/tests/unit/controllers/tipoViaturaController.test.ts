const sinon = require('sinon');

import { Request, NextFunction } from 'express';
import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import ITipoViaturaDTO from '../../../src/dto/ITipoViaturaDTO';
import ITipoViaturaService from '../../../src/services/IServices/ITipoViaturaService';
import TipoViaturaController from '../../../src/controllers/tipoViaturaController';

describe('TipoViaturaController', function () {
    let body = {
        "codigo": "12345678901234567890",
        "descricao": "viatura1",
        "combustivel": "Gasolina",
        "autonomia": 65,
        "velocidadeMedia": 45,
        "custoPorQuilometro": 14,
        "consumoMedio": 22.352
    };

    let tipoViaturaServiceClass = require('../../../src/services/tipoViaturaService').default;
    let tipoViaturaServiceInstance = Container.get(tipoViaturaServiceClass);
    Container.set(config.services.tipoViatura.name, tipoViaturaServiceInstance);

    tipoViaturaServiceInstance = Container.get(config.services.tipoViatura.name);

    const ctrl = new TipoViaturaController(tipoViaturaServiceInstance as ITipoViaturaService);
    
    describe('criarTipoViatura', function () {
        it('criarTipoViatura: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(tipoViaturaServiceInstance, "criarTipoViatura").returns(Result.ok<ITipoViaturaDTO>({
                "id": "teste",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao,
                "combustivel": req.body.combustivel,
                "autonomia": req.body.autonomia,
                "velocidadeMedia": req.body.velocidadeMedia,
                "custoPorQuilometro": req.body.custoPorQuilometro,
                "consumoMedio": req.body.consumoMedio
            } as ITipoViaturaDTO));

            await ctrl.criarTipoViatura(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "teste",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao,
                "combustivel": req.body.combustivel,
                "autonomia": req.body.autonomia,
                "velocidadeMedia": req.body.velocidadeMedia,
                "custoPorQuilometro": req.body.custoPorQuilometro,
                "consumoMedio": req.body.consumoMedio
            }));
            sinon.restore();
        });
    });


    describe("getTipoViatura", function () {
        it("deve retornar todos os tipos de viatura guardados", async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(tipoViaturaServiceInstance, "getTipoViatura").returns(Result.ok<ITipoViaturaDTO[]>([{
                id: "aaa",
                codigo: req.body.codigo,
                descricao: req.body.descricao,
                combustivel: req.body.combustivel,
                autonomia: req.body.autonomia,
                velocidadeMedia: req.body.velocidadeMedia,
                custoPorQuilometro: req.body.custoPorQuilometro,
                consumoMedio: req.body.consumoMedio
            }] as ITipoViaturaDTO[]
            ));

            await ctrl.getTipoViatura(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "codigo": req.body.codigo,
                "descricao": req.body.descricao,
                "combustivel": req.body.combustivel,
                "autonomia": req.body.autonomia,
                "velocidadeMedia": req.body.velocidadeMedia,
                "custoPorQuilometro": req.body.custoPorQuilometro,
                "consumoMedio": req.body.consumoMedio
            }]));

            sinon.restore();

        });
    });
});