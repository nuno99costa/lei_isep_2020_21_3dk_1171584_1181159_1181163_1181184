const sinon = require('sinon');

import { Response, Request, NextFunction } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import { Result } from '../../../src/core/logic/Result';
import SegmentoController from '../../../src/controllers/segmentoController';
import ISegmentoService from '../../../src/services/IServices/ISegmentoService'
import ISegmentoDTO from '../../../src/dto/ISegmentoDTO';

describe('SegmentoController', function () {

    let body = {
        "nomeSegmento": 'SaoJoaoMadeira_SantaMariaFeira',
        "idNoInicial": "111",
        "idNoFinal": "222",
        "distanciaNos": 2500,
        "tempoNos": 300
    };

    let segmentoServiceClass = require('../../../src/services/segmentoService').default;
    let segmentoServiceInstance = Container.get(segmentoServiceClass);
    Container.set(config.services.segmento.name, segmentoServiceInstance);

    segmentoServiceInstance = Container.get(config.services.segmento.name);

    const ctrl = new SegmentoController(segmentoServiceInstance as ISegmentoService);

    describe('criarSegmento', function () {
        it('criarSegmento: returns json with values', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(segmentoServiceInstance, "criarSegmento").returns(Result.ok<ISegmentoDTO>({
                "id": "aaa",
                "nomeSegmento": req.body.nomeSegmento,
                "idNoInicial": req.body.idNoInicial,
                "idNoFinal": req.body.idNoFinal,
                "distanciaNos": req.body.distanciaNos,
                "tempoNos": req.body.tempoNos
            } as ISegmentoDTO));

            await ctrl.criarSegmento(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match({
                "id": "aaa",
                "nomeSegmento": req.body.nomeSegmento,
                "idNoInicial": req.body.idNoInicial,
                "idNoFinal": req.body.idNoFinal,
                "distanciaNos": req.body.distanciaNos,
                "tempoNos": req.body.tempoNos
            }));

            sinon.restore();
        });
    });

    describe('getSegmentos', function () {
        it('deve retornar todos os segmentos guardados na base de dados', async function () {
            let req: Partial<Request> = {};
            req.body = body;

            let res, status, json;
            status = sinon.stub();
            json = sinon.spy();
            res = { status, json };
            status.returns(res);

            let next: Partial<NextFunction> = () => { };

            sinon.stub(segmentoServiceInstance, "getSegmentos").returns(Result.ok<ISegmentoDTO[]>([{
                "id": "aaa",
                "nomeSegmento": req.body.nomeSegmento,
                "idNoInicial": req.body.idNoInicial,
                "idNoFinal": req.body.idNoFinal,
                "distanciaNos": req.body.distanciaNos,
                "tempoNos": req.body.tempoNos
            }] as ISegmentoDTO[]));

            await ctrl.getSegmentos(<Request>req, res, <NextFunction>next);

            sinon.assert.calledOnce(res.json);
            sinon.assert.calledWith(res.json, sinon.match([{
                "id": "aaa",
                "nomeSegmento": req.body.nomeSegmento,
                "idNoInicial": req.body.idNoInicial,
                "idNoFinal": req.body.idNoFinal,
                "distanciaNos": req.body.distanciaNos,
                "tempoNos": req.body.tempoNos
            }]));

            sinon.restore();
        });
    });
});