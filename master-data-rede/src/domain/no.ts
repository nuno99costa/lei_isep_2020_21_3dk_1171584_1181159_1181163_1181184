import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import INoDTO from "../dto/INoDTO";
import { NoId } from "./noId";

//Cada entidade tem isto. Obrigatório
interface NoProps {
    //Fields
    nomeCompleto: string;
    abreviatura: string;
    isPontoRendicao: boolean;
    isEstacaoRecolha: boolean;
    latitude: number;
    longitude: number;
    nomeModelo: string;
}

//Nó é uma root, logo extende AggregateRoot.
export default class No extends AggregateRoot<NoProps>{

    //GETS 

    get id(): UniqueEntityID {
        return this._id;
    }

    get noId(): NoId {
        return NoId.create(this.id);
    }

    get nomeCompleto(): string {
        return this.props.nomeCompleto;
    }

    get abreviatura(): string {
        return this.props.abreviatura;
    }

    get isPontoRendicao(): boolean {
        return this.props.isPontoRendicao;
    }

    get isEstacaoRecolha(): boolean {
        return this.props.isEstacaoRecolha;
    }

    get latitude(): number {
        return this.props.latitude;
    }

    get longitude(): number {
        return this.props.longitude;
    }

    get modelo3d(): string {
        return this.props.nomeModelo;
    }

    public static compareByNome(a: No, b: No) {
        return a.nomeCompleto.localeCompare(b.nomeCompleto);
    }

    public static compareByID(a: No, b: No) {
        return a.noId.id.localeCompare(b.noId.id);
    }

    //Construtor (privado pois é chamado pelo create) da superclasse
    private constructor(props: NoProps, id?: UniqueEntityID) {
        super(props, id);
    }

    //Create
    public static create(noDTO: INoDTO, id?: UniqueEntityID): Result<No> {

        const no = new No({
            nomeCompleto: noDTO.nomeCompleto,
            abreviatura: noDTO.abreviatura,
            isPontoRendicao: noDTO.isPontoRendicao,
            isEstacaoRecolha: noDTO.isEstacaoRecolha,
            latitude: noDTO.latitude,
            longitude: noDTO.longitude,
            nomeModelo: noDTO.nomeModelo
        }, id);

        return Result.ok<No>(no);

    }
}