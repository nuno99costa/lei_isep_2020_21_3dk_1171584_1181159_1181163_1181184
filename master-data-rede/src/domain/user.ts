import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import { UserId } from "./userId";
import { UserEmail } from "./userEmail";
import { UserPassword } from "./userPassword";
import { Guard } from "../core/logic/Guard";
import { IUserDTO } from "../dto/IUserDTO";

export enum Role {
  Administrador = "Administrador",
  Cliente = "Cliente",
  Gestor = "Gestor"
}

interface UserProps {
  name: string;
  email: UserEmail;
  password: string;
  dataNascimento: Date;
  role: Role;
}

export class User extends AggregateRoot<UserProps> {
  get id(): UniqueEntityID {
    return this._id;
  }

  get userId(): UserId {
    return UserId.caller(this.id)
  }

  get email(): UserEmail {
    return this.props.email;
  }

  get name(): string {
    return this.props.name
  }

  get password(): string {
    return this.props.password;
  }

  get dataNascimento(): Date {
    return this.props.dataNascimento;
  }

  get role(): Role {
    return this.props.role;
  }

  set role(value: Role) {
    this.props.role = value;
  }

  private constructor(props: UserProps, id?: UniqueEntityID) {
    super(props, id);
  }

  public static create(user: IUserDTO, id?: UniqueEntityID): Result<User> {
    const userToCreate = new User({
      name: user.name,
      email: UserEmail.create(user.email).getValue(),
      password: user.password,
      dataNascimento: new Date(user.dataNascimento),
      role: user.role
    }, id);

    return Result.ok<User>(userToCreate);
  }
}