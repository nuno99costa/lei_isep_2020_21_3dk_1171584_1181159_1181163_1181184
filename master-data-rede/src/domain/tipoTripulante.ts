import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";
import { TipoTripulanteId } from "./tipoTripulanteId";

interface TipoTripulanteProps {
    codigo: string;
    descricao: string;
}

export default class TipoTripulante extends AggregateRoot<TipoTripulanteProps>{
    get id(): UniqueEntityID {
        return this._id;
    }

    get tipoViaturaId(): TipoTripulanteId {
        return TipoTripulanteId.create(this.id);
    }

    get codigo(): string {
        return this.props.codigo;
    }

    get descricao(): string {
        return this.props.descricao;
    }

    private constructor(props: TipoTripulanteProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(tipoTripulanteDTO: ITipoTripulanteDTO, id?: UniqueEntityID): Result<TipoTripulante> {

        const tipoTripulante = new TipoTripulante({
            codigo: tipoTripulanteDTO.codigo,
            descricao: tipoTripulanteDTO.descricao
        }, id);

        return Result.ok<TipoTripulante>(tipoTripulante);
    }
}