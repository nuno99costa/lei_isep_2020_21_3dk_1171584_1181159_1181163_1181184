import { timeStamp } from "console";
import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import { SegmentoId } from "./segmentoId";
import { NoId } from "./noId";

interface SegmentoProps {
    //Fields
    nomeSegmento: string;
    idNoInicial: NoId;
    idNoFinal: NoId;
    distanciaNos: number;
    tempoNos: number;
}

//Segmento é um root, logo extende AggregateRoot
export default class Segmento extends AggregateRoot<SegmentoProps>{

    //GETS

    get id(): UniqueEntityID {
        return this._id;
    }

    get segmentoId(): SegmentoId {
        return SegmentoId.create(this.id);
    }

    get nomeSegmento(): string {
        return this.props.nomeSegmento;
    }

    get idNoInicial(): NoId {
        return this.props.idNoInicial;
    }

    get idNoFinal(): NoId {
        return this.props.idNoFinal;
    }

    get distanciaNos(): number {
        return this.props.distanciaNos;
    }

    get tempoNos(): number {
        return this.props.tempoNos;
    }

    //Construtor (privado pois é chamado pelo create da superclasse)
    private constructor (props: SegmentoProps, id?: UniqueEntityID) {
        super(props, id);
    }

    //Create
    public static create(segmentoDTO: ISegmentoDTO, id?: UniqueEntityID): Result<Segmento> {

        const segmento = new Segmento({nomeSegmento: segmentoDTO.nomeSegmento,
                                       idNoInicial: NoId.create(new UniqueEntityID(segmentoDTO.idNoInicial)),
                                       idNoFinal: NoId.create(new UniqueEntityID(segmentoDTO.idNoFinal)),
                                       distanciaNos: segmentoDTO.distanciaNos,
                                       tempoNos: segmentoDTO.tempoNos}, id);
        
        return Result.ok<Segmento>( segmento );
    }
}