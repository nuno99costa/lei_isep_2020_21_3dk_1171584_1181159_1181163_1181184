import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import IModelo3DDTO from "../dto/IModelo3DDTO";
import { Modelo3DId } from "./Modelo3DId";

//Cada entidade tem isto. Obrigatório
interface Modelo3DProps {
    //Fields
    nome: string,
    path: string
}

export default class Modelo3D extends Entity<Modelo3DProps>{

    get id(): UniqueEntityID {
        return this._id;
    }

    get noId(): Modelo3DId {
        return Modelo3DId.create(this.id);
    }

    get nome(): string {
        return this.props.nome;
    }

    get path(): string {
        return this.props.path;
    }
    
    //Construtor (privado pois é chamado pelo create) da superclasse
    private constructor(props: Modelo3DProps, id?: UniqueEntityID) {
        super(props, id);
    }

    //Create
    public static create(modeloDTO: IModelo3DDTO, id?: UniqueEntityID): Result<Modelo3D> {

        const modelo3D = new Modelo3D({
            nome: modeloDTO.nome,
            path: modeloDTO.path
        }, id);

        return Result.ok<Modelo3D>(modelo3D);

    }
}