import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import ILinhaDTO from "../dto/ILinhaDTO";
import { LinhaId } from "./linhaId";
import { NoId } from "./noId";
import { TipoTripulanteId } from "./tipoTripulanteId";
import { TipoViaturaId } from "./tipoViaturaId";

interface LinhaProps {
    codigo: string;
    nome: string;
    cor: string;
    noInicial: NoId;
    noFinal: NoId;
    viaturasPermitidas: TipoViaturaId[];
    viaturasProibidas: TipoViaturaId[];
    tripulantesPermitidos: TipoTripulanteId[];
    tripulantesProibidos: TipoTripulanteId[];
}

export default class Linha extends AggregateRoot<LinhaProps>{
    get id(): UniqueEntityID {
        return this._id;
    }

    get linhaId(): LinhaId {
        return LinhaId.create(this.id);
    }

    get codigo(): string {
        return this.props.codigo;
    }

    get nome(): string {
        return this.props.nome;
    }

    get cor(): string {
        return this.props.cor;
    }

    get noInicial(): NoId {
        return this.props.noInicial;
    }

    get noFinal(): NoId {
        return this.props.noFinal;
    }

    get viaturasPermitidas(): TipoViaturaId[] {
        return this.props.viaturasPermitidas;
    }

    get viaturasProibidas(): TipoViaturaId[] {
        return this.props.viaturasProibidas;
    }

    get tripulantesPermitidos(): TipoTripulanteId[] {
        return this.props.tripulantesPermitidos;
    }

    get tripulantesProibidos(): TipoTripulanteId[] {
        return this.props.tripulantesProibidos;
    }

    //Compara duas linhas pelo seu nome
    public static compareByNome(a: Linha, b: Linha) {
        return a.nome.localeCompare(b.nome);
    }

    //Compara duas linhas pelo seu código
    public static compareByCodigo(a: Linha, b: Linha) {
        return a.codigo.localeCompare(b.codigo);
    }

    /** 
     * Estes métodos abaixo devolvem as listas de viaturas e de tripulantes existentes transformadas em listas de string.
     */
    get viaturasPermitidasEmString(): string[] {
        var array: string[] = [];
        let index: number = 0;

        for (index = 0; index < this.viaturasPermitidas.length; index++) {
            array.push(this.viaturasPermitidas[index].id);
        }

        return array;
    }

    get viaturasProibidasEmString(): string[] {
        var array: string[] = [];
        let index: number = 0;

        for (index = 0; index < this.viaturasProibidas.length; index++) {
            array.push(this.viaturasProibidas[index].id);
        }

        return array;
    }

    get tripulantesPermitidosEmString(): string[] {
        var array: string[] = [];
        let index: number = 0;

        for (index = 0; index < this.tripulantesPermitidos.length; index++) {
            array.push(this.tripulantesPermitidos[index].id);
        }

        return array;
    }

    get tripulantesProibidosEmString(): string[] {
        var array: string[] = [];
        let index: number = 0;

        for (index = 0; index < this.tripulantesProibidos.length; index++) {
            array.push(this.tripulantesProibidos[index].id);
        }

        return array;
    }

    private constructor(props: LinhaProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(linhaDTO: ILinhaDTO, noInicial: NoId, noFinal: NoId, listaViaturasPermitidas: TipoViaturaId[], listaViaturasProibidas: TipoViaturaId[], listaTripulantesPermitidos: TipoTripulanteId[], listaTripulantesProibidos: TipoTripulanteId[], id?: UniqueEntityID): Result<Linha> {

        const linha = new Linha({
            codigo: linhaDTO.codigo,
            nome: linhaDTO.nome,
            cor: linhaDTO.cor,
            noInicial: noInicial,
            noFinal: noFinal,
            viaturasPermitidas: listaViaturasPermitidas,
            viaturasProibidas: listaViaturasProibidas,
            tripulantesPermitidos: listaTripulantesPermitidos,
            tripulantesProibidos: listaTripulantesProibidos
        }, id);

        return Result.ok<Linha>(linha);
    }
}