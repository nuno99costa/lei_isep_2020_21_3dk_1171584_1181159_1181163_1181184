import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

//Id do nó.
interface SegmentoIdProps{
    id: string;
}


//Id do segmento.
export class SegmentoId extends ValueObject<SegmentoIdProps> {
    get id() : string {
        return this.props.id;
    }

    public static create(id? : UniqueEntityID): SegmentoId{
        return new SegmentoId( { id: id.toString() } );
    }
}