import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

export interface LinhaIdProps {
    id: string;
}

export class LinhaId extends ValueObject<LinhaIdProps> {
    get id(): string {
        return this.props.id;
    }

    public static create(id?: UniqueEntityID): LinhaId {
        return new LinhaId({ id: id.toString() });
    }
}