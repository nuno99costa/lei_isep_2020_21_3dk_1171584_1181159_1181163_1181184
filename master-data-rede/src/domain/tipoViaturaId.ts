import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

export interface TipoViaturaIdProps {
    id: string;
}

export class TipoViaturaId extends ValueObject<TipoViaturaIdProps>{
    get id(): string {
        return this.props.id;
    }

    public static create(id?: UniqueEntityID): TipoViaturaId {
        return new TipoViaturaId({ id: id.toString() });
    }
}