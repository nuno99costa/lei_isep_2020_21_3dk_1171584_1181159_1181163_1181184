
import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

interface UserIdProps{
  id: string;
}
export class UserId extends ValueObject<UserIdProps> {

  get id (): string {
    return this.props.id;
  }

  public static create(id?: UniqueEntityID) : UserId{
    return new UserId({id: id.toString()});
  }
}