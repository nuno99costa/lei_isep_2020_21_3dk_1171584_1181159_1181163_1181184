import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

//Id do nó.
interface Modelo3DIdProps{
    id: string;
}

export class Modelo3DId extends ValueObject<Modelo3DIdProps> {
    get id(): string {
        return this.props.id;
    }

    public static create (id?: UniqueEntityID): Modelo3DId{
        return new Modelo3DId( { id: id.toString() });
    }
}