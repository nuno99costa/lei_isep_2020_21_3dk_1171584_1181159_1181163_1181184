import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import { TipoViaturaId } from "./tipoViaturaId";

export enum TipoCombustivel {
    Gasoleo = 'Gasóleo',
    Gasolina = 'Gasolina',
    Eletrico = 'Elétrico',
    GPL = 'GPL',
    Hidrogenio = 'Hidrogénio'
}

interface TipoViaturaProps {
    codigo: string;
    descricao: string;
    combustivel: TipoCombustivel;
    autonomia: number;
    velocidadeMedia: number;
    custoPorQuilometro: number;
    consumoMedio: number;
}

export default class TipoViatura extends AggregateRoot<TipoViaturaProps>{
    get id(): UniqueEntityID {
        return this._id;
    }

    get tipoViaturaId(): TipoViaturaId {
        return TipoViaturaId.create(this.id);
    }

    get codigo(): string {
        return this.props.codigo;
    }

    get descricao(): string {
        return this.props.descricao;
    }

    get combustivel(): TipoCombustivel {
        return this.props.combustivel;
    }

    get autonomia(): number {
        return this.props.autonomia;
    }

    get velocidadeMedia(): number {
        return this.props.velocidadeMedia;
    }

    get custoPorQuilometro(): number {
        return this.props.custoPorQuilometro;
    }

    get consumoMedio(): number {
        return this.props.consumoMedio;
    }

    private constructor(props: TipoViaturaProps, id?: UniqueEntityID) {
        super(props, id);
    }

    public static create(tipoViaturaDTO: ITipoViaturaDTO, id?: UniqueEntityID): Result<TipoViatura> {

        const tipoViatura = new TipoViatura({
            codigo: tipoViaturaDTO.codigo,
            descricao: tipoViaturaDTO.descricao,
            combustivel: tipoViaturaDTO.combustivel,
            autonomia: tipoViaturaDTO.autonomia,
            velocidadeMedia: tipoViaturaDTO.velocidadeMedia,
            custoPorQuilometro: tipoViaturaDTO.custoPorQuilometro,
            consumoMedio: tipoViaturaDTO.consumoMedio
        }, id);

        return Result.ok<TipoViatura>(tipoViatura);
    }
}