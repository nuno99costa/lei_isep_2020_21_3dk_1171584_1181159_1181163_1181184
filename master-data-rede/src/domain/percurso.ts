import { AggregateRoot } from "../core/domain/AggregateRoot";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import IPercursoDTO from "../dto/IPercursoDTO";
import { LinhaId } from "./linhaId";
import { NoId } from "./noId";
import { PercursoId } from "./percursoId";
import Segmento from "./segmento";
import { SegmentoId } from "./segmentoId";

//Cada entidade tem isto é obrigatório
interface PercursoProps {
    //Fields
    nomePercurso: string;
    linhaId: LinhaId;
    orientacao: Orientacao;
    noInicialId: NoId;
    noFinalId: NoId;
    listaSegmentos: SegmentoId[];
    distanciaTotal: number;
    tempoTotal: number;

}

export enum Orientacao {
    Ida = 'Ida',
    Volta = 'Volta',
    Reforço = 'Reforço',
    Vazio = 'Vazio'
}

//Percurso é uma root, logo extende AggregateRoot.
export default class Percurso extends AggregateRoot<PercursoProps>{
    //Gets
    get id(): UniqueEntityID {
        return this._id;
    }

    get percursoId(): PercursoId {
        return PercursoId.create(this.id);
    }

    get nomePercurso(): string {
        return this.props.nomePercurso;
    }

    get orientacao(): Orientacao {
        return this.props.orientacao;
    }

    get linhaId(): LinhaId {
        return this.props.linhaId;
    }

    get noInicialId(): NoId {
        return this.props.noInicialId;
    }

    get noFinalId(): NoId {
        return this.props.noFinalId;
    }

    get listaSegmentos(): Array<SegmentoId> {
        return this.props.listaSegmentos;
    }

    get distanciaTotal(): number {
        return this.props.distanciaTotal;
    }

    get tempoTotal(): number {
        return this.props.tempoTotal;
    }

    //Construtor (privado pois é chamado pelo create) da superclasse
    private constructor(props: PercursoProps, id?: UniqueEntityID) {
        super(props, id);
    }

    get segmentosEmString(): String[] {
        var array: String[] = [];
        let index: number = 0;

        for (index = 0; index < this.listaSegmentos.length; index++) {
            array.push(this.listaSegmentos[index].id);
        }

        return array;
    }

    //Create
    public static create(percursoDTO: IPercursoDTO, listaSegmentos: SegmentoId[], distanciaTotal: number, tempoTotal: number, id?: UniqueEntityID): Result<Percurso> {

        const percurso = new Percurso({
            nomePercurso: percursoDTO.nomePercurso,
            linhaId: LinhaId.create(new UniqueEntityID(percursoDTO.linhaId)),
            orientacao: percursoDTO.orientacao,
            noInicialId: NoId.create(new UniqueEntityID(percursoDTO.noInicialId)),
            noFinalId: NoId.create(new UniqueEntityID(percursoDTO.noFinalId)),
            listaSegmentos: listaSegmentos,
            distanciaTotal: distanciaTotal,
            tempoTotal: tempoTotal
        }, id);

        return Result.ok<Percurso>(percurso);
    }
}