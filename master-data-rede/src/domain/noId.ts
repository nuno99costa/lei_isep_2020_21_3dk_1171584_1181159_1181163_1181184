import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

//Id do nó.
interface NoIdProps{
    id: string;
}

export class NoId extends ValueObject<NoIdProps> {
    get id(): string {
        return this.props.id;
    }

    public static create (id?: UniqueEntityID): NoId{
        return new NoId( { id: id.toString() });
    }
}