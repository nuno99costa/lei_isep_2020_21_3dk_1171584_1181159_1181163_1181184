import { Entity } from "../core/domain/Entity";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

//Id do percurso
interface PercursoIdProps{
    id: string;
}

export class PercursoId extends ValueObject<PercursoIdProps> {
    get id(): string {
        return this.props.id;
    }

    public static create (id?: UniqueEntityID): PercursoId{
        return new PercursoId( { id: id.toString() });
    }
}