import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ValueObject } from "../core/domain/ValueObject";

export interface TipoTripulanteIdProps {
    id: string;
}

export class TipoTripulanteId extends ValueObject<TipoTripulanteIdProps>{
    get id(): string {
        return this.props.id;
    }

    public static create(id?: UniqueEntityID): TipoTripulanteId {
        return new TipoTripulanteId({ id: id.toString() });
    }
}