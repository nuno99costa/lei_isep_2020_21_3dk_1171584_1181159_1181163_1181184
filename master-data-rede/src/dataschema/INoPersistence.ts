export interface INoPersistence {
    domainId: string;
    nomeCompleto: string;
    abreviatura: string;
    isPontoRendicao: boolean;
    isEstacaoRecolha: boolean;
    latitude: number;
    longitude: number;
    nomeModelo: string;
}