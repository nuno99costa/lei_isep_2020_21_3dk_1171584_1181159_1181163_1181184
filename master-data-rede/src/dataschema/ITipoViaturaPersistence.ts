import { TipoCombustivel } from "../domain/tipoViatura";

export interface ITipoViaturaPersistence {
    domainId: string;
    codigo: string;
    descricao: string;
    combustivel: TipoCombustivel;
    autonomia: number;
    velocidadeMedia: number;
    custoPorQuilometro: number;
    consumoMedio: number;
}