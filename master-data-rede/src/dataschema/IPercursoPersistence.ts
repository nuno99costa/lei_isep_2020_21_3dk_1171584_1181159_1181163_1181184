import { Schema } from "mongoose";
import { Orientacao } from "../domain/percurso";
import Segmento from "../domain/segmento";

export interface IPercursoPersistence {
    domainId: string;
    nomePercurso: string,
    linhaId: string,
    orientacao: string,
    noInicialId: string,
    noFinalId: string,
    listaSegmentos: String[],
    distanciaTotal: number,
    tempoTotal: number
}