export interface IUserPersistence {
	domainId: string;
	name: string;
	email: string;
	password: string;
	dataNascimento: string;
	role: string
  }