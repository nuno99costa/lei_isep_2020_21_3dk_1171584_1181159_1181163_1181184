export interface ITipoTripulantePersistence {
    domainId: string;
    codigo: string;
    descricao: string;
}