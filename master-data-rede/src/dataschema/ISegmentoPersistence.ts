import { NoId } from "../domain/noId";

export interface ISegmentoPersistence {
    domainId: string;
    nomeSegmento: string;
    idNoInicial: NoId;
    idNoFinal: NoId;
    distanciaNos: number;
    tempoNos: number;
}