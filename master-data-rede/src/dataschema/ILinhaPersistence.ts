
export interface ILinhaPersistence {
    domainId: string;
    codigo: string;
    nome: string;
    cor: string;
    noInicial: string;
    noFinal: string;
    viaturasPermitidas: string[];
    viaturasProibidas: string[];
    tripulantesPermitidos: string[];
    tripulantesProibidos: string[];
}