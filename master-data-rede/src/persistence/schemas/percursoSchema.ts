import { IPercursoPersistence} from "../../dataschema/IPercursoPersistence";
import mongoose from 'mongoose';
import { Orientacao } from "../../domain/percurso";

const PercursoSchema = new mongoose.Schema(
    {
        domainId: { type: String, unique: true },
        nomePercurso: { type: String, unique: true },
        linhaId: { type: String},
        orientacao: { type: String, unique: false },
        noInicialId: { type: String },
        noFinalId: { type: String },
        listaSegmentos: [{type: String}],
        distanciaTotal: { type: Number },
        tempoTotal: { type: Number },
    }
)

export default mongoose.model<IPercursoPersistence & mongoose.Document>('Percurso', PercursoSchema);