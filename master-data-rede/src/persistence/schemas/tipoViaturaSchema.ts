import { TipoCombustivel } from "../../domain/tipoViatura";
import mongoose from 'mongoose';
import { ITipoViaturaPersistence } from "../../dataschema/ITipoViaturaPersistence";

const TipoViaturaSchema = new mongoose.Schema(
    {
        domainId: { type: String, unique: true },
        codigo: { type: String, unique: true },
        descricao: { type: String, unique: false },
        combustivel: { type: TipoCombustivel, unique: false },
        autonomia: { type: Number, unique: false },
        velocidadeMedia: { type: Number, unique: false },
        custoPorQuilometro: { type: Number, unique: false },
        consumoMedio: { type: Number, unique: false }
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ITipoViaturaPersistence & mongoose.Document>('Tipo Viatura', TipoViaturaSchema);