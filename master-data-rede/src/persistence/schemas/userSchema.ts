import { IUserPersistence } from '../../dataschema/IUserPersistence';
import mongoose from 'mongoose';

const User = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    name: { type: String, unique: false },
    email: { type: String, unique: true },
    password: { type: String, unique: false },
    dataNascimento: { type: String, unique: false },
    role: {type: String, unique: false}
  },
  { timestamps: true },
);

export default mongoose.model<IUserPersistence & mongoose.Document>('User', User);
