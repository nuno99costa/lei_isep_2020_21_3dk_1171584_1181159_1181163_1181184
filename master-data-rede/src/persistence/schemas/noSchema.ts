import { INoPersistence } from "../../dataschema/INoPersistence";
import mongoose from 'mongoose';
import { string } from "joi";


const NoSchema = new mongoose.Schema(
  {
    domainId: { type: String, unique: true },
    nomeCompleto: { type: String, unique: true },
    abreviatura: { type: String, unique: true },
    isPontoRendicao: { type: Boolean, unique: false },
    isEstacaoRecolha: { type: Boolean, unique: false },
    latitude: { type: Number, unique: false },
    longitude: { type: Number, unique: false },
    nomeModelo: {type : String, unique: false},
  },
  {
    timestamps: true
  }
);

export default mongoose.model<INoPersistence & mongoose.Document>('No', NoSchema);