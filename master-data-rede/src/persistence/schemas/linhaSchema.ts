import mongoose from 'mongoose';
import { ILinhaPersistence } from '../../dataschema/ILinhaPersistence';

const LinhaSchema = new mongoose.Schema(
    {
        domainId: { type: String, unique: true },
        codigo: { type: String, unique: true },
        nome: { type: String, unique: false },
        cor: { type: String, unique: false },
        noInicial: { type: String, unique: false },
        noFinal: { type: String, unique: false },
        viaturasPermitidas: [{ type: String }],
        viaturasProibidas: [{ type: String }],
        tripulantesPermitidos: [{ type: String }],
        tripulantesProibidos: [{ type: String }]
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ILinhaPersistence & mongoose.Document>('Linha', LinhaSchema);