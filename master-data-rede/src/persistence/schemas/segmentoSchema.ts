import { ISegmentoPersistence } from "../../dataschema/ISegmentoPersistence";
import mongoose from 'mongoose';
import { NoId } from "../../domain/noId";

const SegmentoSchema = new mongoose.Schema(
    {
        domainId: { type: String, unique: true },
        nomeSegmento: { type: String, unique: true },
        idNoInicial: { type: String, unique: false},
        idNoFinal: { type: String, unique: false},
        distanciaNos: { type: Number },
        tempoNos: { type: Number },
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ISegmentoPersistence & mongoose.Document>('Segmento', SegmentoSchema);