import mongoose from 'mongoose';
import { ITipoTripulantePersistence } from "../../dataschema/ITipoTripulantePersistence";

const TipoTripulanteSchema = new mongoose.Schema(
    {
        domainId: { type: String, unique: true },
        codigo: { type: String, unique: true },
        descricao: { type: String, unique: false }
    },
    {
        timestamps: true
    }
);

export default mongoose.model<ITipoTripulantePersistence & mongoose.Document>('Tipo Tripulante', TipoTripulanteSchema);