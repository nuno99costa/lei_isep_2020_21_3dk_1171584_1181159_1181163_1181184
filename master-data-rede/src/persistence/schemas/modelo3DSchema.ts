import mongoose from 'mongoose';
import { IModelo3DPersistence } from "../../dataschema/IModelo3DPersistence";


const Modelo3DSchema = new mongoose.Schema(
  {
    nome: { type: String, unique: true },
    path: { type: String, unique: true },
    
  },
  {
    timestamps: true
  }
);

export default mongoose.model<IModelo3DPersistence & mongoose.Document>('Modelo3D', Modelo3DSchema);