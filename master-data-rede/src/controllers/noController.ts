import { Request, Response, NextFunction } from "express";
import { Inject, Container, Service } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import INoDTO from "../dto/INoDTO";
import NoMapper from "../mappers/noMapper";
import INoService from "../services/IServices/INoService";
import INoController from './IControllers/INoController'

export default class NoController implements INoController {

    constructor(@Inject(config.services.no.name) private noServiceInstance: INoService
    ) { }

    public async criarNo(req: Request, res: Response, next: NextFunction) {
        try {

            //Validar dados do pedido
            const { error } = NoMapper.validarNo(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //Conversão do req.body (JSON) para DTO. 
            const noDTO = req.body as INoDTO;

            //Chamar service para criar e guardar o nó. Retorna o Result do DTO do nó guardado.
            const noOrError = await this.noServiceInstance.criarNo(noDTO) as Result<INoDTO>;

            //Verifica se falhou. Se sim, mensagem de erro
            if (noOrError.isFailure) {
                return res.status(402).send();
            }

            //Correu bem, logo vamos buscar o DTO do No GUARDADO ao Result. Respondemos com esse DTO em JSON.
            const savedNoDTO = noOrError.getValue();

            return res.status(201).json(savedNoDTO);

        } catch (err) {
            return next(err);
        }

    }

    public async nosOrdenadosPor(req: Request, res: Response, next: NextFunction) {

        try {

            const listaNosOrdenadosOrError = await this.noServiceInstance.listaOrdenadaPor(req.params.tipo) as Result<INoDTO[]>;

            if (listaNosOrdenadosOrError.isFailure) {
                return res.status(400).send(listaNosOrdenadosOrError.errorValue());
            }

            const listaNosOrdenados = listaNosOrdenadosOrError.getValue();

            return res.status(200).json(listaNosOrdenados);

        } catch (err) {
            return next(err);
        }

    }

    public async nosComecadosPor(req: Request, res: Response, next: NextFunction) {


        try {
            const listaNosFiltradosOrError = await this.noServiceInstance.listaComecadosPor(req.params.tipo, req.params.iniciais) as Result<INoDTO[]>;

            if (listaNosFiltradosOrError.isFailure) {
                return res.status(400).send(listaNosFiltradosOrError.errorValue());
            }

            const listaNosFiltrados = listaNosFiltradosOrError.getValue();

            return res.status(200).json(listaNosFiltrados);

        } catch (err) {
            return next(err);
        }

    }

    //Devolve o nó com o id enviado por parâmetro
    public async getNoById(req: Request, res: Response, next: NextFunction) {
        try {
            const noDTOOrError = await this.noServiceInstance.getNoById(req.params.id);

            if (noDTOOrError.isFailure) return res.status(400).send(noDTOOrError.errorValue());

            const noDTO = noDTOOrError.getValue();

            return res.status(200).json(noDTO);

        } catch (error) {
            return next(error);
        }
    }

    public async updateModelo3D(req: Request, res: Response, next: NextFunction){
        try {
            const body : { nomeModelo : string} = req.body as { nomeModelo : string};

            const nomeModelo = body.nomeModelo;

            const updatedNoOrError = await (await this.noServiceInstance.updateModelo3D(req.params.id, nomeModelo));

            if(updatedNoOrError.isFailure){
                return res.status(400).send(updatedNoOrError.errorValue());
            }

            return res.status(200).json(updatedNoOrError.getValue());

        } catch (error) {
            return next(error);
        }
    }

}