import { NextFunction, Request, Response } from "express";
import { Inject } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import TipoViaturaMapper from "../mappers/tipoViaturaMapper";
import ITipoViaturaService from "../services/IServices/ITipoViaturaService";
import ITipoViaturaController from "./IControllers/ITipoViaturaController";

export default class TipoViaturaController implements ITipoViaturaController {
    constructor(@Inject(config.services.tipoViatura.name) private tipoViaturaServiceInstance: ITipoViaturaService) { }

    public async criarTipoViatura(req: Request, res: Response, next: NextFunction) {
        try {
            const { error } = TipoViaturaMapper.validarTipoViatura(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            const tipoViaturaDTO = req.body as ITipoViaturaDTO;

            const tipoViaturaOrError = await this.tipoViaturaServiceInstance.criarTipoViatura(tipoViaturaDTO) as Result<ITipoViaturaDTO>;

            if (tipoViaturaOrError.isFailure) return res.status(402).send();

            const savedTipoViaturaDTO = tipoViaturaOrError.getValue();

            return res.status(201).json(savedTipoViaturaDTO);

        } catch (err) {
            return next(err);
        }
    }

    public async getTipoViatura(req: Request, res: Response, next: NextFunction) {
        try {
            const listaDTOOrError = await this.tipoViaturaServiceInstance.getTipoViatura();

            if (listaDTOOrError.isFailure) return res.status(400).send(listaDTOOrError.errorValue());

            const listaDTO = listaDTOOrError.getValue();

            return res.status(200).json(listaDTO);

        } catch (error) {
            return next(error);
        }
    }
}
