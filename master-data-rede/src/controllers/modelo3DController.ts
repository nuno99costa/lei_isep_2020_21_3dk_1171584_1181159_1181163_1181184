
import { NextFunction, Request, Response } from 'express';
import { Inject } from 'typedi';
import config from '../../config';
import { Result } from '../core/logic/Result';
import IModelo3DDTO from '../dto/IModelo3DDTO';
import Modelo3DMapper from '../mappers/modelo3DMapper';
import IModelo3DService from '../services/IServices/IModelo3DService';
import IModelo3DController from "./IControllers/IModelo3DController";
import formidable from 'formidable';
import extract from 'extract-zip';
import { pathToFileURL } from 'url';
import path from 'path';


export default class Modelo3DController implements IModelo3DController{

    constructor(@Inject(config.services.modelo3D.name) private modelo3DServiceInstance: IModelo3DService
    ) { } 

    public async criarModelo3D(req: Request, res: Response, next: NextFunction){
        try {

            var form = new formidable.IncomingForm();
            form.keepExtensions = false;
            form.parse(req);

            form.on('file', async (name, file) => {
                
                await extract(file.path, { dir: path.join(__dirname, '..', '/modelos')});
                const modeloDTO :IModelo3DDTO = {nome: file.name, path:'https://lei-isep-lapr5-g05-mdr.herokuapp.com/modelos/' + name + '/scene.gltf'};
                const modeloOrError = await this.modelo3DServiceInstance.criarModelo3D(modeloDTO) as Result<IModelo3DDTO>;
                if (modeloOrError.isFailure) {
                    return res.status(402).send("Problema a guardar o modelo");
                }
    
                //Correu bem, logo vamos buscar o DTO do No GUARDADO ao Result. Respondemos com esse DTO em JSON.
                const savedModeloDTO = modeloOrError.getValue();
    
                return res.status(201).json(savedModeloDTO); 

            })

        
            
            
            /* //Validar dados do pedido
            const { error } = Modelo3DMapper.validarModelo(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //Conversão do req.body (JSON) para DTO. 
            const modeloDTO = req.body as IModelo3DDTO;

            //Chamar service para criar e guardar o nó. Retorna o Result do DTO do nó guardado.
            const modeloOrError = await this.modelo3DServiceInstance.criarModelo3D(modeloDTO) as Result<IModelo3DDTO>;

            //Verifica se falhou. Se sim, mensagem de erro
            if (modeloOrError.isFailure) {
                return res.status(402).send();
            }

            //Correu bem, logo vamos buscar o DTO do No GUARDADO ao Result. Respondemos com esse DTO em JSON.
            const savedModeloDTO = modeloOrError.getValue();

            return res.status(201).json(savedModeloDTO); */

        } catch (err) {
            return next(err);
        }

       
    }

    public async getModeloByNome(req: Request, res: Response, next: NextFunction) {
        try {
            const modeloDTOOrError = await this.modelo3DServiceInstance.getModelo3DByNome(req.params.id);

            if (modeloDTOOrError.isFailure) return res.status(400).send(modeloDTOOrError.errorValue());

            const modeloDTO = modeloDTOOrError.getValue();

            return res.status(200).json(modeloDTO);

        } catch (error) {
            return next(error);
        }
    }

    public async getAllModelos3D(req: Request, res: Response, next: NextFunction){
        try {
            const modelos = await this.modelo3DServiceInstance.getAllModelos3D();
            

            return res.status(200).json(modelos.getValue());

        } catch (error) {
            return next(error);
        }
    }

}


