import { NextFunction, Request, Response } from "express";
import { Inject } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import IInserirGLXController from "./IControllers/IInserirGLXController";

//TipoViatura
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import TipoViaturaMapper from "../mappers/tipoViaturaMapper";
import ITipoViaturaService from "../services/IServices/ITipoViaturaService";

//SEGMENTO
import ISegmentoDTO from "../dto/ISegmentoDTO";
import SegmentoMapper from "../mappers/segmentoMapper";
import ISegmentoService from "../services/IServices/ISegmentoService";


//NO
import INoDTO from "../dto/INoDTO";
import INoService from "../services/IServices/INoService";
import NoMapper from "../mappers/noMapper";
import { INoRepo } from "../services/IRepos/INoRepo";

//PERCURSO
import IPercursoDTO from "../dto/IPercursoDTO";
import PercursoMapper from "../mappers/percursoMapper";
import IPercursoService from "../services/IServices/IPercursoService";
import IPercursoController from './IControllers/IPercursoController'
import { IPercursoRepo } from "../services/IRepos/IPercursoRepo";

//LINHA
import ILinhaDTO from "../dto/ILinhaDTO";
import LinhaMapper from "../mappers/linhaMapper";
import ILinhaService from "../services/IServices/ILinhaService";
import ILinhaController from "./IControllers/ILinhaController";
import { exist } from "joi";

export default class InserirGLXController implements IInserirGLXController {
    constructor(
        @Inject(config.services.tipoViatura.name) private tipoViaturaServiceInstance: ITipoViaturaService,
        @Inject(config.services.no.name) private noServiceInstance: INoService,
        @Inject(config.repos.no.name) private noRepoInstance: INoRepo,
        @Inject(config.services.segmento.name) private segmentoServiceInstance: ISegmentoService,
        @Inject(config.services.percurso.name) private percursoServiceInstance: IPercursoService,
        @Inject(config.repos.percurso.name) private percursoRepoInstance: IPercursoRepo,
        @Inject(config.services.linha.name) private linhaServiceInstance: ILinhaService
    ) {
    }

    public async ingerirGLX(req: Request, res: Response, next: NextFunction) {
        try {
            var network = req.body.gldocumentinfo.world[0].gldocument[0].gldocumentnetwork[0].network[0];

            await this.guardarTiposViaturas(network.vehicletypes[0].vehicletype);

            await this.guardarNos(network.nodes[0].node);

            //Estrutura do element
            //console.log(network.lines[0].line[0]);
            await this.guardarLinhas(network.lines[0].line, network.nodes[0].node, network.paths[0].path);

            //afinal não existe tipotripulante https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=1652 e https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=1346

            return res.status(200).send('Ficheiro GLX ingerido com sucesso');
        } catch (err) {
            return res.status(500).send(err);
        }
    }

    private async guardarTiposViaturas(vehicletypes) {
        for (var i = vehicletypes.length - 1; i >= 0; i--) {
            await this.guardarTipoViatura(vehicletypes[i]['$']);
        }
        console.log("*-- Tipos de Viaturas ingeridos");
    };

    //Estrutura do element
    //console.log(network.vehicletypes[0].vehicletype[0]);
    private async guardarTipoViatura(element) {
        var tipoCombustivelAtual;
        switch (element.EnergySource) {
            case "23":
                tipoCombustivelAtual = "Gasóleo"
                break;
            case "20":
                tipoCombustivelAtual = "GPL"
                break;
            case "50":
                tipoCombustivelAtual = "Hidrogénio"
                break;
            case "75":
                tipoCombustivelAtual = "Elétrico"
                break;
            case "1":
                tipoCombustivelAtual = "Gasolina"
                break;
        }

        const tipoViaturaDTO = {
            //n tem código no glx (https://moodle.isep.ipp.pt/mod/forum/discuss.php?d=1290)
            id: element.key,
            codigo: element.Name,
            descricao: "",

            combustivel: tipoCombustivelAtual,
            autonomia: element.Autonomy,
            velocidadeMedia: element.AverageSpeed,
            custoPorQuilometro: element.Cost,
            consumoMedio: element.Consumption
        } as ITipoViaturaDTO;

        const { error } = TipoViaturaMapper.validarTipoViatura(tipoViaturaDTO);
        if (error) throw new Error("Dados de tipo de viatura " + element.Name + " inválidos");

        const tipoViaturaOrError = await this.tipoViaturaServiceInstance.criarTipoViatura(tipoViaturaDTO) as Result<ITipoViaturaDTO>;

        if (tipoViaturaOrError.isFailure) throw new Error("Erro ao gravar tipo de viatura " + element.Name);

        console.log("+---- Gravado tipo de viatura " + tipoViaturaOrError.getValue().codigo);
    };

    private async guardarLinhas(lines, nodeList, pathList) {
        for (var i = lines.length - 1; i >= 0; i--) {
            await this.guardarLinha(nodeList, pathList, lines[i]);
        }
        console.log("*-- Linhas ingeridas");
    };

    private async guardarLinha(nodeList, pathList, element) {
        var noInicial, noFinal;
        var percursosLinha = [];

        const initialPath = await pathList.find(x => x["$"].key == element.linepaths[0].linepath[0]["$"].Path);
        const noInicialShortName = await nodeList.find(x => x["$"].key == initialPath.pathnodes[0].pathnode[0]["$"].Node)["$"].ShortName;
        const noFinalShortName = await nodeList.find(x => x["$"].key == initialPath.pathnodes[0].pathnode[initialPath.pathnodes[0].pathnode.length - 1]["$"].Node)["$"].ShortName;
        noInicial = await this.noRepoInstance.findByAbreviatura(noInicialShortName);
        noFinal = await this.noRepoInstance.findByAbreviatura(noFinalShortName);

        const linhaDTO = {
            //O que é o código
            id: element["$"].key,
            codigo: element["$"].Name.substr(0, 20),
            nome: element["$"].Name,
            cor: element["$"].Color,
            noInicial: noInicial.id,
            noFinal: noFinal.id,
            viaturasPermitidas: [],
            viaturasProibidas: [],
            tripulantesPermitidos: [],
            tripulantesProibidos: []
        } as ILinhaDTO;

        const linhaOrError = await this.linhaServiceInstance.criarLinha(linhaDTO) as Result<ILinhaDTO>;

        if (linhaOrError.isFailure) throw linhaOrError.error;

        const savedLineDTO = linhaOrError.getValue();

        //criar percursos associados à linha
        for (var i = 0; i < element.linepaths[0].linepath.length; ++i) {
            this.guardarPercurso(nodeList, pathList, element.linepaths[0].linepath[i], savedLineDTO.id);
        }

        console.log("+---- Gravada linha " + linhaOrError.getValue().nome);
    };

    private async guardarPercurso(nodelist, pathList, element, linhaID) {
        //get path from linepath id
        const infPercurso = await pathList.find(x => x["$"].key == element["$"].Path);

        var listaSegmentosID = new Array();
        var listaSegmentos = new Array();
        for (var i = 0; i < infPercurso["pathnodes"].length; ++i) {
            const segmentos = infPercurso["pathnodes"][i].pathnode;
            for (var i = 1; i < segmentos.length; i++) {
                const segmentoGuardado = await this.guardarSegmento(nodelist, segmentos[i - 1], segmentos[i]);
                console.log("+---- Gravado segmento " + segmentoGuardado.nomeSegmento);
                await listaSegmentosID.push(segmentoGuardado.id);
                await listaSegmentos.push(segmentoGuardado);
            }

            var totalDistancia = 0;
            var totalTempo = 0;
            listaSegmentos.forEach(async element => {
                totalDistancia += element.distanciaNos;
            });
            listaSegmentos.forEach(async element => {
                totalTempo += element.tempoNos;
            });

            const noInicial = listaSegmentos[0].nomeSegmento.substr(0, listaSegmentos[0].nomeSegmento.indexOf('_'));
            const noFinal = listaSegmentos[listaSegmentos.length - 1].nomeSegmento.split("_")[1].split("(")[0];

            const nomePercurso = noInicial + '_' + noFinal;

            var orientacao;
            switch (infPercurso.Orientation) {
                case "Go":
                    orientacao = "Ida";
                    break;
                case "Return":
                    orientacao = "Volta";
                    break;
                default:
                    orientacao = infPercurso.Orientation;
                    break;
            }

            const percursoDTO = {
                id: element["$"].Path,
                nomePercurso: nomePercurso,
                listaSegmentos: listaSegmentosID,
                noInicialId: listaSegmentos[0].idNoInicial,
                noFinalId: listaSegmentos[listaSegmentos.length - 1].idNoFinal,
                linhaId: linhaID,
                orientacao: orientacao,
                distanciaTotal: totalDistancia,
                tempoTotal: totalTempo
            } as IPercursoDTO;

            //Chamar service para criar e guardar o percurso. Retorna o Result do DTO do percurso guardado.
            const percursoOrError = await this.percursoServiceInstance.criarPercurso(percursoDTO) as Result<IPercursoDTO>;

            //Verifica se falhou. Se sim, mensagem de erro
            if (percursoOrError.isFailure) throw percursoOrError.error;

            console.log("+---- Gravado percurso " + percursoOrError.getValue().nomePercurso);
        }
        ;
    };

    //Estrutura do element
    //console.log(network.nodes[0].node[0]);
    private async guardarNos(nodes) {
        for (var i = 0; i < nodes.length; i++) {
            await this.guardarNo(nodes[i]['$']);
        }
        console.log("*-- Nós ingeridos");
    };

    private async guardarNo(element) {
        var isrelief;
        if (element.IsReliefPoint == "True") {
            isrelief = true;
        } else {
            isrelief = false;
        }
        ;
        var isrendition;
        if (element.IsDepot == "True") {
            isrendition = true;
        } else {
            isrendition = false;
        }
        ;

        const noDTO = {
            id: element.key,
            nomeCompleto: element.Name,
            abreviatura: element.ShortName,
            isPontoRendicao: isrelief,
            isEstacaoRecolha: isrendition,
            latitude: element.Latitude,
            longitude: element.Longitude
        } as INoDTO;
        //Validar dados do pedido
        const { error } = NoMapper.validarNo(noDTO);

        if (error) throw error;

        const noOrError = await this.noServiceInstance.criarNo(noDTO) as Result<INoDTO>;

        if (noOrError.isFailure) throw new Error("Erro ao guardar nó " + element.Name);

        console.log("+---- Guardado nó " + noOrError.getValue().nomeCompleto);

    };

    private async guardarSegmento(nodeList, startElement, endElement): Promise<ISegmentoDTO> {
        //get two nodes based on first and last elements
        const noInicio = await this.noRepoInstance.findByAbreviatura(nodeList.find(element => element["$"].key == startElement["$"].Node)["$"].ShortName);
        const noFinal = await this.noRepoInstance.findByAbreviatura(nodeList.find(element => element["$"].key == endElement["$"].Node)["$"].ShortName);

        const existeSegmento: Result<ISegmentoDTO> = await this.segmentoServiceInstance.segmentoByNome(noInicio.abreviatura + "_" + noFinal.abreviatura);
        if (!existeSegmento.isFailure) {
            console.log("Encontrei o segmento: " + existeSegmento.getValue().nomeSegmento);
            return existeSegmento.getValue();
        } else {
            const segmentoDTO = {
                nomeSegmento: noInicio.abreviatura + "_" + noFinal.abreviatura,
                idNoInicial: noInicio.id.toString(),
                idNoFinal: noFinal.id.toString(),
                distanciaNos: endElement["$"].Distance,
                tempoNos: endElement["$"].Duration
            } as ISegmentoDTO;

            const { error } = SegmentoMapper.validarSegmento(segmentoDTO);
            if (error) throw error;

            const segmentoOrError = await this.segmentoServiceInstance.criarSegmento(segmentoDTO) as Result<ISegmentoDTO>;

            if (segmentoOrError.isFailure) throw segmentoOrError.error;

            const savedSegmentoDTO = await segmentoOrError.getValue();

            return savedSegmentoDTO;
        }


    };
}
