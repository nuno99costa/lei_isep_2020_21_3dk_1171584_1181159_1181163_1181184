import { Request, Response, NextFunction } from 'express';

export default interface IInserirGLXController {
    ingerirGLX(req: Request, res: Response, next: NextFunction);
}