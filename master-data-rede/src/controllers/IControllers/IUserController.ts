import { Request, Response, NextFunction } from 'express';

export default interface IUserController {
    registarUser(req: Request, res: Response, next: NextFunction);
    login(req: Request, res: Response, next: NextFunction);
    getDadosUser(req: Request, res: Response, next: NextFunction);
    updateDadosUser(req: Request, res: Response, next: NextFunction);
    deleteDadosUser(req: Request, res: Response, next: NextFunction);
}