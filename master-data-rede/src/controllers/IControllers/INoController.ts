import { Request, Response, NextFunction } from 'express';

export default interface INoController {
    criarNo(req: Request, res: Response, next: NextFunction);
    nosOrdenadosPor(req: Request, res: Response, next: NextFunction);
    nosComecadosPor(req: Request, res: Response, next: NextFunction);
    getNoById(req: Request, res: Response, next: NextFunction);
    updateModelo3D(req: Request, res: Response, next: NextFunction);
}