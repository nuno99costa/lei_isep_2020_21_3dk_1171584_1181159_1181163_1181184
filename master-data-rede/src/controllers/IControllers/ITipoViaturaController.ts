import { NextFunction, Request, Response } from "express";

export default interface ITipoViaturaController {
    criarTipoViatura(req: Request, res: Response, next: NextFunction);
    getTipoViatura(req: Request, res: Response, next: NextFunction);
}