import { Request, Response, NextFunction } from 'express';

export default interface ISegmentoController {
    criarSegmento(req: Request, res: Response, next: NextFunction);
    getSegmentos(req: Request, res: Response, next: NextFunction);
}