import { Request, Response, NextFunction } from 'express';

export default interface IPercursoController {
    criarPercurso(req: Request, res: Response, next: NextFunction);
    listarPercursos(req: Request, res: Response, next: NextFunction);
    dadosParaMapa(req: Request, res: Response, next: NextFunction);
    dadosPercurso(req: Request, res: Response, next: NextFunction);
    getNosPercursoId(req: Request, res: Response, next: NextFunction);
    getSegmentosPercurso(req: Request, res: Response, next: NextFunction);
}
