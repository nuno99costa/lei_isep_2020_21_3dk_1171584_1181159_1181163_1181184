import { Request, Response, NextFunction } from 'express';

export default interface IModelo3DController {
    criarModelo3D(req: Request, res: Response, next: NextFunction);
    getModeloByNome(req: Request, res: Response, next: NextFunction);
    getAllModelos3D(req: Request, res: Response, next: NextFunction);
}