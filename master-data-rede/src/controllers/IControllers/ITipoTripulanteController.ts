import { NextFunction, Request, Response } from "express";
import { Result } from "../../core/logic/Result";
import ITipoTripulanteDTO from "../../dto/ITipoTripulanteDTO";

export default interface ITipoTripulanteController {
    criarTipoTripulante(req: Request, res: Response, next: NextFunction);
    getTipoTripulante(req: Request, res: Response, next: NextFunction);
}