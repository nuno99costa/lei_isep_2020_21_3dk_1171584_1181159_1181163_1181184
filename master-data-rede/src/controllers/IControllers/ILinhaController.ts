import { Request, Response, NextFunction } from "express";

export default interface ILinhaController {
    criarLinha(req: Request, res: Response, next: NextFunction);
    linhasOrdenadasPor(req: Request, res: Response, next: NextFunction);
    linhasComecadasPor(req: Request, res: Response, next: NextFunction);
    getLinhaById(req: Request, res: Response, next: NextFunction);
}