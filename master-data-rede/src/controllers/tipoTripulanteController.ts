import { NextFunction, Request, Response } from "express";
import { Inject } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";
import TipoTripulanteMapper from "../mappers/tipoTripulanteMapper";
import ITipoTripulanteService from "../services/IServices/ITipoTripulanteService";
import ITipoTripulanteController from "./IControllers/ITipoTripulanteController";

export default class TipoTripulanteController implements ITipoTripulanteController {
    constructor(@Inject(config.services.tipoTripulante.name) private tipoTripulanteServiceInstance: ITipoTripulanteService) { }

    public async criarTipoTripulante(req: Request, res: Response, next: NextFunction) {
        try {

            const { error } = TipoTripulanteMapper.validarTipoTripulante(req.body);
            if (error) { return res.status(400).send(error.details[0].message); }

            const tipoTripulanteDTO = req.body as ITipoTripulanteDTO;

            const tipoTripulanteOrError = await this.tipoTripulanteServiceInstance.criarTipoTripulante(tipoTripulanteDTO) as Result<ITipoTripulanteDTO>;

            if (tipoTripulanteOrError.isFailure) return res.status(402).send();

            const savedTipoTripulanteDTO = tipoTripulanteOrError.getValue();

            return res.status(201).json(savedTipoTripulanteDTO);

        } catch (err) {
            return next(err);
        }
    }

    public async getTipoTripulante(req: Request, res: Response, next: NextFunction) {
        try {
            const listaDTOOrError = await this.tipoTripulanteServiceInstance.getTipoTripulante();

            if (listaDTOOrError.isFailure) return res.status(400).send(listaDTOOrError.errorValue());

            const listaDTO = listaDTOOrError.getValue();

            return res.status(200).json(listaDTO);

        } catch (error) {
            return next(error);
        }
    }

}
