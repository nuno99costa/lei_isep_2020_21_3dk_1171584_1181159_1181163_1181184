import { Request, Response, NextFunction } from "express";
import { Inject, Container, Service } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import SegmentoMapper from "../mappers/segmentoMapper";
import ISegmentoService from "../services/IServices/ISegmentoService";
import ISegmentoController from './IControllers/ISegmentoController'

export default class SegmentoController implements ISegmentoController {

    constructor(@Inject(config.services.segmento.name) private segmentoServiceInstance: ISegmentoService
    ) { }

    public async criarSegmento(req: Request, res: Response, next: NextFunction) {
        try {

            //Validar dados do pedido
            const { error } = SegmentoMapper.validarSegmento(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //Conversão do req.body (JSON) para DTO
            const segmentoDTO = req.body as ISegmentoDTO;

            //Chamar service para criar e guardar o nó. Retorna o Result do DTO do segmento guardado.
            const segmentoOrError = await this.segmentoServiceInstance.criarSegmento(segmentoDTO) as Result<ISegmentoDTO>;

            console.log(segmentoOrError);
            //Verifica se falhou. Se sim, mensagem de erro
            if (segmentoOrError.isFailure) {
                return res.status(402).send();
            }

            //Correu bem, logo vamos buscar o DTO do segmento guardado ao Result. Respondemos com esse DTO em JSON.~
            const savedSegmentoDTO = segmentoOrError.getValue();

            return res.status(201).json(savedSegmentoDTO);

        } catch (err) {
            return next(err);
        }
    }

    public async getSegmentos(req: Request, res: Response, next: NextFunction) {
        try {
            const listaDTOOrError = await this.segmentoServiceInstance.getSegmentos();

            if (listaDTOOrError.isFailure) return res.status(400).send(listaDTOOrError.errorValue());

            const listaDTO = listaDTOOrError.getValue();

            return res.status(200).json(listaDTO);

        } catch (error) {
            return next(error);
        }
    }
}