import { Request, Response, NextFunction } from "express";
import { Inject, Container, Service } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import IDadosLinhaMapaDTO from "../dto/IDadosLinhaMapaDTO";
import { ILinhaPercursosDTO } from "../dto/ILinhaPercursosDTO";
import INoDTO from "../dto/INoDTO";
import IPercursoDTO from "../dto/IPercursoDTO";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import PercursoMapper from "../mappers/percursoMapper";
import IPercursoService from "../services/IServices/IPercursoService";
import IPercursoController from './IControllers/IPercursoController'

export default class PercursoController implements IPercursoController {

    constructor(@Inject(config.services.percurso.name) private percursoServiceInstance: IPercursoService
    ) { }

    public async criarPercurso(req: Request, res: Response, next: NextFunction) {
        try {
            //Validar dados do pedido
            const { error } = PercursoMapper.validarPercurso(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //Conversão do req.body (JSON) pra DTO
            const percursoDTO = req.body as IPercursoDTO;

            //Chamar service para criar e guardar o percurso. Retorna o Result do DTO do percurso guardado.
            const percursoOrError = await this.percursoServiceInstance.criarPercurso(percursoDTO) as Result<IPercursoDTO>;

            //Verifica se falhou. Se sim, mensagem de erro
            if (percursoOrError.isFailure) {
                return res.status(402).send(percursoOrError.errorValue());
            }

            //Correu bem, logo vamos buscar o DTO do percurso guardado ao Result. Respondemos com esse DTO em JSON.
            const savedPercursoDTO = percursoOrError.getValue();


            return res.status(201).json(savedPercursoDTO);

        } catch (err) {
            return next(err);
        }
    }

    public async listarPercursos(req: Request, res: Response, next: NextFunction) {

        try {

            const listaPercursosOrError = await this.percursoServiceInstance.listaPercursos(req.params.id) as Result<IPercursoDTO[]>;

            if (listaPercursosOrError.isFailure) {
                return res.status(402).send(listaPercursosOrError.errorValue());
            }

            const listaPercursos = listaPercursosOrError.getValue();

            return res.status(200).json(listaPercursos);

        } catch (err) {
            return next(err);
        }

    }

    public async dadosParaMapa(req: Request, res: Response, next: NextFunction) {
        try {

            const listaDadosOrError = await this.percursoServiceInstance.dadosDePercursosParaMapa() as Result<IDadosLinhaMapaDTO[]>;

            if (listaDadosOrError.isFailure) {
                return res.status(400).send(listaDadosOrError.errorValue());
            }

            const listaDados = listaDadosOrError.getValue();


            return res.status(200).json(listaDados);

        } catch (err) {
            return next(err);
        }
    }

    public async dadosPercurso(req: Request, res: Response, next: NextFunction) {
        try {

            const dadosPercursoOrError = await this.percursoServiceInstance.dadosPercursoParaPlaneamento() as Result<ILinhaPercursosDTO[]>;

            if (dadosPercursoOrError.isFailure) {
                return res.status(400).send(dadosPercursoOrError.errorValue());
            }

            const dadosPercurso = dadosPercursoOrError.getValue();


            return res.status(200).json(dadosPercurso);

        } catch (err) {
            return next(err);
        }
    }

    public async getNosPercursoId(req: Request, res: Response, next: NextFunction) {
        try {
            const nosOrError = await this.percursoServiceInstance.getNosPercursoId(req.params.percursoId) as Result<INoDTO[]>;

            if (nosOrError.isFailure) {
                return res.status(400).send(nosOrError.errorValue());
            }

            const nos = nosOrError.getValue();

            return res.status(200).json(nos);

        } catch (err) {
            return next(err);
        }
    }

    public async getSegmentosPercurso(req: Request, res: Response, next: NextFunction) {
        try {
            const segmentosOrError = await this.percursoServiceInstance.getSegmentosPercurso(req.params.percursoId) as Result<ISegmentoDTO[]>;

            if (segmentosOrError.isFailure) {
                return res.status(400).send(segmentosOrError.errorValue());
            }

            const segmentos = segmentosOrError.getValue();

            return res.status(200).json(segmentos);

        } catch (err) {
            return next(err);
        }
    }

}