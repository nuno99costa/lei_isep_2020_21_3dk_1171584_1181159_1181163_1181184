import { Response, Request, NextFunction } from 'express';
import { Inject, Token } from 'typedi';
import config from '../../config';
import { UserEmail } from '../domain/userEmail';
import ILoginDTO from '../dto/ILoginDTO';
import { IUserDTO } from '../dto/IUserDTO';
import { UserMapper } from '../mappers/userMapper';
import IUserService from '../services/IServices/IUserService';
import IUserController from './IControllers/IUserController';
import jwt, { decode } from 'jsonwebtoken';

export default class UserController implements IUserController {

    constructor(@Inject(config.services.user.name) private userService: IUserService) { }

    public async registarUser(req: Request, res: Response, next: NextFunction) {
        try {

            //Validar dados do utilizador
            const { error } = UserMapper.validarUser(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //Conversão do body para DTO
            const userDTO = req.body as IUserDTO;

            //Chamar service
            const userOrError = await this.userService.registarUser(userDTO);

            if (userOrError.isFailure) {
                return res.status(500).send(userOrError.errorValue());
            }

            const savedUserDTO = userOrError.getValue();

            return res.status(201).json(savedUserDTO);
        } catch (err) {
            return next(err);
        }


    }

    public async login(req: Request, res: Response, next: NextFunction) {
        try {

            const loginDTO = req.body as ILoginDTO;

            const tokenOrError = await this.userService.login(loginDTO);

            if (tokenOrError.isFailure) {
                return res.status(500).send(tokenOrError.errorValue());
            }

            const tokenString = tokenOrError.getValue();

            return res.status(200).json({ token: tokenString });

        } catch (err) {
            return next(err);
        }

    }

    public async getDadosUser(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.header('auth-token');
            var decoded = jwt.decode(token) as { email: string };

            const email = decoded.email;

            const dadosOrError = await this.userService.getDadosUser(email);

            if (dadosOrError.isFailure) {
                return res.status(500).send(dadosOrError.errorValue());
            }

            const dadosString = dadosOrError.getValue();

            return res.status(200).json(dadosString);

        } catch (err) {
            return next(err);
        }

    }

    public async updateDadosUser(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.header('auth-token');
            var decoded = jwt.decode(token) as { email: string };
            
            const email = decoded.email;

            const userUpdate = req.body as IUserDTO;

            const dadosOrError = await this.userService.updateDadosUser(email, userUpdate);

            if (dadosOrError.isFailure) {
                return res.status(500).send(dadosOrError.errorValue());
            }

            const dadosString = dadosOrError.getValue();

            return res.status(200).json(dadosString);

        } catch (err) {
            return next(err);
        }

    }

    public async deleteDadosUser(req: Request, res: Response, next: NextFunction) {
        try {
            const token = req.header('auth-token');
            var decoded = jwt.decode(token) as { email: string };

            const email = decoded.email;

            const dadosOrError = await this.userService.deleteDadosUser(email);

            if (dadosOrError.isFailure) {
                return res.status(500).send(dadosOrError.errorValue());
            }

            const dadosString = dadosOrError.getValue();

            return res.status(200).json(dadosString);

        } catch (err) {
            return next(err);
        }

    }
}