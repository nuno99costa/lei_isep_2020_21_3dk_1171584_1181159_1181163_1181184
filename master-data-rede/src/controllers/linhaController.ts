import { NextFunction, Request, Response } from "express";
import { Inject } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import ILinhaDTO from "../dto/ILinhaDTO";
import LinhaMapper from "../mappers/linhaMapper";
import ILinhaService from "../services/IServices/ILinhaService";
import ILinhaController from "./IControllers/ILinhaController";

export default class LinhaController implements ILinhaController {

    constructor(@Inject(config.services.linha.name) private linhaServiceInstance: ILinhaService) { }

    public async criarLinha(req: Request, res: Response, next: NextFunction) {
        try {
            //valida dados do pedido
            const { error } = LinhaMapper.validarLinha(req.body);
            if (error) return res.status(400).send(error.details[0].message);

            //converte json(req.body) para dto 
            const linhaDTO = req.body as ILinhaDTO;

            //cria e guarda a linha através do service. Retorna Result do DTO da linha guardada
            const linhaOrError = await this.linhaServiceInstance.criarLinha(linhaDTO) as Result<ILinhaDTO>;

            if (linhaOrError.isFailure) return res.status(402).send(linhaOrError.errorValue());

            //guarda o valor 
            const savedLinhaDTO = linhaOrError.getValue();

            //devolve o dto guardado
            return res.status(201).json(savedLinhaDTO);

        } catch (err) {
            return next(err);
        }
    }

    public async linhasOrdenadasPor(req: Request, res: Response, next: NextFunction) {

        try {
            const listaLinhasOrdenadosOrError = await this.linhaServiceInstance.listaOrdenadaPor(req.params.tipo) as Result<ILinhaDTO[]>;

            if (listaLinhasOrdenadosOrError.isFailure) {
                return res.status(400).send(listaLinhasOrdenadosOrError.errorValue());
            }

            const listaLinhasOrdenados = listaLinhasOrdenadosOrError.getValue();

            return res.status(200).json(listaLinhasOrdenados);

        } catch (err) {
            return next(err);
        }

    }

    public async linhasComecadasPor(req: Request, res: Response, next: NextFunction) {

        try {
            const listaLinhasFiltradasOrError = await this.linhaServiceInstance.listaComecadasPor(req.params.tipo, req.params.iniciais) as Result<ILinhaDTO[]>;

            if (listaLinhasFiltradasOrError.isFailure) {
                return res.status(400).send(listaLinhasFiltradasOrError.errorValue());
            }

            const listaLinhasFiltradas = listaLinhasFiltradasOrError.getValue();

            return res.status(200).json(listaLinhasFiltradas);

        } catch (err) {
            return next(err);
        }

    }

    public async getLinhaById(req: Request, res: Response, next: NextFunction) {
        try {
            const linhaDTOOrError = await this.linhaServiceInstance.getLinhaById(req.params.id) as Result<ILinhaDTO>;

            if (linhaDTOOrError.isFailure) return res.status(400).send(linhaDTOOrError.errorValue());

            const linhaDTO = linhaDTOOrError.getValue();

            return res.status(200).json(linhaDTO);
        } catch (error) {
            return next(error);
        }
    }
}