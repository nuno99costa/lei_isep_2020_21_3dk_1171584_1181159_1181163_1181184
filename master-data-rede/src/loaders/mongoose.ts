import mongoose from 'mongoose';
import { Db } from 'mongodb';
import config from '../../config';

export default async (isTest : boolean): Promise<Db> => {
  if(isTest){
    const connection = await mongoose.connect(config.testDatabaseURL, { useNewUrlParser: true, useCreateIndex: true });
    return connection.connection.db;
  }else{
    const connection = await mongoose.connect(config.databaseURL, { useNewUrlParser: true, useCreateIndex: true });
    return connection.connection.db;
  }
  
  
};
