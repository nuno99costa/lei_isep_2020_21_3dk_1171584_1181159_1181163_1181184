import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import mongooseLoader from './mongoose';
import Logger from './logger';
import config from '../../config';
import * as readline from 'readline';

export default async ({ expressApp }) => {

  let isTeste : boolean = false;

  function askQuestion(query){
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })

    return new Promise<string>(resolve => rl.question(query, answer => {
      rl.close();
      resolve(answer);
    }))
  }

  // const answer = await askQuestion('Usar a base de dados de teste?');
  
  // switch(answer.toLowerCase()){
  //   case 'y':
  //     isTeste = true;
  //   break;
  // }

  isTeste = false;

  const mongoConnection = await mongooseLoader(isTeste);
  Logger.info('✌️ DB loaded and connected!');

  //USER
  const userSchema = {
    // compare with the approach followed in repos and services
    name: 'userSchema',
    schema: '../persistence/schemas/userSchema',
  };

  const userService = {
    name: config.services.user.name,
    path: config.services.user.path
  }

  const userController = {
    name: config.controller.user.name,
    path: config.controller.user.path
  }

  const userRepo = {
    name: config.repos.user.name,
    path: config.repos.user.path
  }

  //ROLE
  const roleSchema = {
    // compare with the approach followed in repos and services
    name: 'roleSchema',
    schema: '../persistence/schemas/roleSchema',
  };

  const roleService = {
    name: config.services.role.name,
    path: config.services.role.path
  }

  const roleController = {
    name: config.controller.role.name,
    path: config.controller.role.path
  }

  const roleRepo = {
    name: config.repos.role.name,
    path: config.repos.role.path
  }

  //NÓ
  const noSchema = {
    name: 'noSchema',
    schema: '../persistence/schemas/noSchema'
  }

  const noController = {
    name: config.controller.no.name,
    path: config.controller.no.path
  }

  const noService = {
    name: config.services.no.name,
    path: config.services.no.path
  }

  const noRepo = {
    name: config.repos.no.name,
    path: config.repos.no.path
  }

  //TIPO DE VIATURA
  const tipoViaturaSchema = {
    name: 'tipoViaturaSchema',
    schema: '../persistence/schemas/tipoViaturaSchema'
  }

  const tipoViaturaService = {
    name: config.services.tipoViatura.name,
    path: config.services.tipoViatura.path
  }

  const tipoViaturaController = {
    name: config.controller.tipoViatura.name,
    path: config.controller.tipoViatura.path
  }

  const tipoViaturaRepo = {
    name: config.repos.tipoViatura.name,
    path: config.repos.tipoViatura.path
  }

  //TIPO DE TRIPULANTE
  const tipoTripulanteSchema = {
    name: 'tipoTripulanteSchema',
    schema: '../persistence/schemas/tipoTripulanteSchema'
  }

  const tipoTripulanteService = {
    name: config.services.tipoTripulante.name,
    path: config.services.tipoTripulante.path
  }

  const tipoTripulanteController = {
    name: config.controller.tipoTripulante.name,
    path: config.controller.tipoTripulante.path
  }

  const tipoTripulanteRepo = {
    name: config.repos.tipoTripulante.name,
    path: config.repos.tipoTripulante.path
  }

  //LINHA
  const linhaSchema = {
    name: 'linhaSchema',
    schema: '../persistence/schemas/linhaSchema'
  }

  const linhaController = {
    name: config.controller.linha.name,
    path: config.controller.linha.path
  }

  const linhaService = {
    name: config.services.linha.name,
    path: config.services.linha.path
  }

  const linhaRepo = {
    name: config.repos.linha.name,
    path: config.repos.linha.path
  }

  //PERCURSO
  const percursoSchema = {
    name: 'percursoSchema',
    schema: '../persistence/schemas/percursoSchema'
  }

  const percursoService = {
    name: config.services.percurso.name,
    path: config.services.percurso.path
  }

  const percursoController = {
    name: config.controller.percurso.name,
    path: config.controller.percurso.path
  }

  const percursoRepo = {
    name: config.repos.percurso.name,
    path: config.repos.percurso.path
  }

  //SEGMENTO
  const segmentoSchema = {
    name: 'segmentoSchema',
    schema: '../persistence/schemas/segmentoSchema'
  }

  const segmentoService = {
    name: config.services.segmento.name,
    path: config.services.segmento.path
  }

  const segmentoController = {
    name: config.controller.segmento.name,
    path: config.controller.segmento.path
  }

  const segmentoRepo = {
    name: config.repos.segmento.name,
    path: config.repos.segmento.path
  }

  //INSERIRGLX
  const inserirGLXController = {
    name: config.controller.inserirGLX.name,
    path: config.controller.inserirGLX.path
  }

  //Modelo 3D
  const modelo3DController = {
    name: config.controller.modelo3D.name,
    path: config.controller.modelo3D.path
  }

  const modelo3DService = {
    name: config.services.modelo3D.name,
    path: config.services.modelo3D.path
  }

  const modelo3DRepo = {
    name: config.repos.modelo3D.name,
    path: config.repos.modelo3D.path
  }

  const modelo3DSchema = {
    name: 'modelo3DSchema',
    schema: '../persistence/schemas/modelo3DSchema'
  }

  await dependencyInjectorLoader({
    mongoConnection,
    schemas: [
      userSchema,
      roleSchema,
      noSchema,
      tipoViaturaSchema,
      tipoTripulanteSchema,
      segmentoSchema,
      percursoSchema,
      linhaSchema,
      modelo3DSchema
    ],
    controllers: [
      roleController,
      userController,
      noController,
      tipoViaturaController,
      tipoTripulanteController,
      segmentoController,
      percursoController,
      linhaController,
      inserirGLXController,
      modelo3DController
    ],
    repos: [
      roleRepo,
      userRepo,
      noRepo,
      tipoViaturaRepo,
      tipoTripulanteRepo,
      segmentoRepo,
      percursoRepo,
      linhaRepo,
      modelo3DRepo
    ],
    services: [
      roleService,
      userService,
      noService,
      tipoViaturaService,
      tipoTripulanteService,
      segmentoService,
      percursoService,
      linhaService,
      modelo3DService
    ]
  });
  Logger.info('✌️ Schemas, Controllers, Repositories, Services, etc. loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');
};
