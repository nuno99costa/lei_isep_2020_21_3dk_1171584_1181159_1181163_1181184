import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import { INoRepo } from "../services/IRepos/INoRepo";
import { Result } from "../core/logic/Result";
import { IModelo3DPersistence } from "../dataschema/IModelo3DPersistence";
import Modelo3D from "../domain/Modelo3D";
import { Modelo3DId } from "../domain/Modelo3DId";
import { IModelo3DRepo } from "../services/IRepos/IModelo3DRepo";
import Modelo3DMapper from "../mappers/modelo3DMapper";


@Service()
export default class Modelo3DRepo implements IModelo3DRepo {

    constructor(@Inject('modelo3DSchema') private modelo3DSchema: Model<IModelo3DPersistence & Document>,) { }

    async findByNome(nome : string): Promise<Modelo3D>{
        const query = { nome: nome };
        const modeloRecord = await this.modelo3DSchema.findOne(query);

        if (modeloRecord != null) {
            return Modelo3DMapper.toDomain(modeloRecord);
        }
        else {
            return null;
        }
    }


    //TODO:
    public async exists(modelo3DId: Modelo3DId | string): Promise<boolean> {
        const idX = modelo3DId instanceof Modelo3DId ? (<Modelo3DId>modelo3DId).id.toString() : modelo3DId;
        const query = { nome: idX };
        const noDocument = await this.modelo3DSchema.findOne(query);

        return !!noDocument === true;
    }

    //Guardar o nó.
    public async save(modelo3D: Modelo3D): Promise<Modelo3D> {

        //Verificar se já existe um nó igual
        const query = { nome: modelo3D.id.toString() }; //Vai buscar o id do nó

        const modelo3DDocument = await this.modelo3DSchema.findOne(query); //Verifica se já existe

        try {

            if (modelo3DDocument === null) {
                //Transformar em schema (ver schema em persistence/schemas)
                const rawNo: any = Modelo3DMapper.toPersistence(modelo3D);

                //Juro que sei porque
                const modelo3DCriado = await this.modelo3DSchema.create(rawNo); //Gjarda na bd

                //Schema -> Domain
                return Modelo3DMapper.toDomain(modelo3DCriado);
            } else {

                //Se houver igual (???)
                modelo3DDocument.nome = modelo3D.nome;
                modelo3DDocument.path = modelo3D.path;
                
                //Guardar
                await modelo3DDocument.save();

                return modelo3D;
            }

        } catch (err) {
            throw (err);
        }
    }

    public async findAll() : Promise<Modelo3D[]>{

        var result : Modelo3D[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const modelos = await this.modelo3DSchema.find(query);

        for(index = 0; index < modelos.length; index++){
            var modelo3DDomain = Modelo3DMapper.toDomain(modelos[index]);
            result.push(modelo3DDomain);
        }

        return result;

    }



}