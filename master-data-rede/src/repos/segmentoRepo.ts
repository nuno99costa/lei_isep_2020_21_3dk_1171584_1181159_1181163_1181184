import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import Segmento from "../domain/segmento";
import { ISegmentoRepo } from "../services/IRepos/ISegmentoRepo";
import SegmentoMapper from "../mappers/segmentoMapper";
import { ISegmentoPersistence } from "../dataschema/ISegmentoPersistence";
import { SegmentoId } from "../domain/segmentoId";
import { NoId } from "../domain/noId";

@Service()
export default class SegmentoRepo implements ISegmentoRepo {

    constructor(@Inject('segmentoSchema') private segmentoSchema: Model<ISegmentoPersistence & Document>,) { }

    public async findAll(): Promise<Segmento[]> {

        var result: Segmento[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const segmentos = await this.segmentoSchema.find(query);

        for (index = 0; index < segmentos.length; index++) {
            var segmentoDomain = SegmentoMapper.toDomain(segmentos[index]);
            result.push(segmentoDomain);
        }
        return result;
    }

    async findByNome(nome: string): Promise<Segmento> {
        const query = { nomeSegmento: nome };
        const roleRecord = await this.segmentoSchema.findOne(query);

        if (roleRecord != null) {
            return SegmentoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }
    }

    async findByDomainId(segmentoId: string): Promise<Segmento> {
        const query = { domainId: segmentoId };
        const roleRecord = await this.segmentoSchema.findOne(query);

        if (roleRecord != null) {
            return SegmentoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }
    }

    //TODO:
    public async exists(segmentoId: SegmentoId | string): Promise<boolean> {
        const idX = segmentoId instanceof SegmentoId ? (<SegmentoId>segmentoId).id.toString() : segmentoId;
        const query = { domainId: idX };
        const segmentoDocument = await this.segmentoSchema.findOne(query);

        return !!segmentoDocument === true;
    }

    //Guardar o segmento
    public async save(segmento: Segmento): Promise<Segmento> {

        //Verificar se já existe um segmento igual
        const query = { domainId: segmento.id.toString() }; //Vai buscar o id do segmento

        const segmentoDocument = await this.segmentoSchema.findOne(query); //Verifica se já existe

        try {

            if (segmentoDocument === null) {
                //Transformar em schema (ver schema em persistence/schemas)
                const rawSegmento: any = SegmentoMapper.toPersistence(segmento);

                //Guarda na base de dados
                const segmentoCriado = await this.segmentoSchema.create(rawSegmento);

                return SegmentoMapper.toDomain(segmentoCriado);
            } else {

                //Se houver igual 
                segmentoDocument.nomeSegmento = segmento.nomeSegmento;
                segmentoDocument.idNoInicial = segmento.idNoInicial;
                segmentoDocument.idNoFinal = segmento.idNoFinal;
                segmentoDocument.distanciaNos = segmento.distanciaNos;
                segmentoDocument.tempoNos = segmento.tempoNos;

                //Guardar
                await segmentoDocument.save();

                return segmento;
            }
        } catch (err) {
            throw (err);
        }
    }
}