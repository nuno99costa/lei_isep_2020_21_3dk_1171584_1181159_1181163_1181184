import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import { ITipoViaturaPersistence } from "../dataschema/ITipoViaturaPersistence";
import TipoViatura from "../domain/tipoViatura";
import { TipoViaturaId } from "../domain/tipoViaturaId";
import TipoViaturaMapper from "../mappers/tipoViaturaMapper";
import { ITipoViaturaRepo } from "../services/IRepos/ITipoViaturaRepo";

@Service()
export default class TipoViaturaRepo implements ITipoViaturaRepo {

    constructor(@Inject('tipoViaturaSchema') private tipoViaturaSchema: Model<ITipoViaturaPersistence & Document>,) { }

    public async findAll(): Promise<TipoViatura[]> {

        var result: TipoViatura[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const tiposViatura = await this.tipoViaturaSchema.find(query);

        for (index = 0; index < tiposViatura.length; index++) {
            var tipoViaturaDomain = TipoViaturaMapper.toDomain(tiposViatura[index]);
            result.push(tipoViaturaDomain);
        }

        return result;
    }

    public async findByDomainId(tipoViaturaId: string): Promise<TipoViatura> {
        const query = { domainId: tipoViaturaId };
        const tipoViaturaRecord = await this.tipoViaturaSchema.findOne(query);

        if (tipoViaturaRecord != null) {
            return TipoViaturaMapper.toDomain(tipoViaturaRecord);
        }
        else {
            return null;
        }
    }

    public async exists(tipoViaturaId: TipoViaturaId | string): Promise<boolean> {

        const idX = tipoViaturaId instanceof TipoViaturaId ? (<TipoViaturaId>tipoViaturaId).id.toString() : tipoViaturaId;
        const query = { domainId: idX };
        const tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);

        return !!tipoViaturaDocument === true;
    }

    public async save(tipoViatura: TipoViatura): Promise<TipoViatura> {
        const query = { domainId: tipoViatura.id.toString() }; //Vai buscar o id do nó

        const tipoViaturaDocument = await this.tipoViaturaSchema.findOne(query);

        try {
            if (tipoViaturaDocument === null) {
                const rawTipoViatura: any = TipoViaturaMapper.toPersistence(tipoViatura);

                const tipoViaturaCriada = await this.tipoViaturaSchema.create(rawTipoViatura);

                return TipoViaturaMapper.toDomain(tipoViaturaCriada);
            } else {
                tipoViaturaDocument.codigo = tipoViatura.codigo;
                tipoViaturaDocument.descricao = tipoViatura.descricao;
                tipoViaturaDocument.combustivel = tipoViatura.combustivel;
                tipoViaturaDocument.consumoMedio = tipoViatura.consumoMedio;
                tipoViaturaDocument.autonomia = tipoViatura.autonomia;
                tipoViaturaDocument.velocidadeMedia = tipoViatura.velocidadeMedia;
                tipoViaturaDocument.custoPorQuilometro = tipoViatura.custoPorQuilometro;

                await tipoViaturaDocument.save();

                return tipoViatura;
            }

        } catch (err) {
            throw (err);
        }
    }
}