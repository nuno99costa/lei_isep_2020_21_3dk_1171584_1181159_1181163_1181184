import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import No from "../domain/no";
import { INoRepo } from "../services/IRepos/INoRepo";
import NoMapper from "../mappers/noMapper";
import { INoPersistence } from "../dataschema/INoPersistence";
import { NoId } from "../domain/noId";
import { Result } from "../core/logic/Result";


@Service()
export default class NoRepo implements INoRepo {

    constructor(@Inject('noSchema') private noSchema: Model<INoPersistence & Document>,) { }

    async updateNoModelo3D(no: string, modeloNome: string): Promise<No> {
        const query = { domainId: no };

        const noDocument = await this.noSchema.findOne(query);

        noDocument.nomeModelo = modeloNome;

        await noDocument.save();

        return NoMapper.toDomain(noDocument);
    }

    async findByAbreviatura(abv : string): Promise<No>{
        const query = { abreviatura: abv };
        const roleRecord = await this.noSchema.findOne(query);

        if (roleRecord != null) {
            return NoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }
    }
    
    async findByDomainId(noId: string): Promise<No> {
        const query = { domainId: noId };
        const roleRecord = await this.noSchema.findOne(query);

        if (roleRecord != null) {
            return NoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }

    }

    //TODO:
    public async exists(noId: NoId | string): Promise<boolean> {
        const idX = noId instanceof NoId ? (<NoId>noId).id.toString() : noId;
        const query = { domainId: idX };
        const noDocument = await this.noSchema.findOne(query);

        return !!noDocument === true;
    }

    //Guardar o nó.
    public async save(no: No): Promise<No> {

        //Verificar se já existe um nó igual
        const query = { domainId: no.id.toString() }; //Vai buscar o id do nó

        const noDocument = await this.noSchema.findOne(query); //Verifica se já existe

        try {

            if (noDocument === null) {
                //Transformar em schema (ver schema em persistence/schemas)
                const rawNo: any = NoMapper.toPersistence(no);

                //Juro que sei porque
                const noCriado = await this.noSchema.create(rawNo); //Gjarda na bd

                //Schema -> Domain
                return NoMapper.toDomain(noCriado);
            } else {

                //Se houver igual (???)
                noDocument.nomeCompleto = no.nomeCompleto;
                noDocument.abreviatura = no.abreviatura;
                noDocument.isPontoRendicao = no.isPontoRendicao;
                noDocument.isEstacaoRecolha = no.isEstacaoRecolha;
                noDocument.latitude = no.latitude;
                noDocument.longitude = no.longitude;

                //Guardar
                await noDocument.save();

                return no;
            }

        } catch (err) {
            throw (err);
        }
    }

    public async findAll() : Promise<No[]>{

        var result : No[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const nos = await this.noSchema.find(query);

        for(index = 0; index < nos.length; index++){
            var noDomain = NoMapper.toDomain(nos[index]);
            result.push(noDomain);
        }

        return result;

    }

    public async findNoComNomeComecadoPor(iniciais : String): Promise<No[]>{
        var result : No[] = [];
        var index = 0;

        var regexp = new RegExp("^" + iniciais); //REgex de começam por iniciais
        const query = {nomeCompleto: regexp}; 
        const nos = await this.noSchema.find(query);

        for(index = 0; index < nos.length; index++){
            var noDomain = NoMapper.toDomain(nos[index]);
            result.push(noDomain);
        }

        return result;
    }

    public async findNoComCodigoComecadoPor(iniciais : String): Promise<No[]>{
        var result : No[] = [];
        var index = 0;

        var regexp = new RegExp("^" + iniciais); //REgex de começam por iniciais
        const query = {domainId: regexp}; 
        const nos = await this.noSchema.find(query);

        for(index = 0; index < nos.length; index++){
            var noDomain = NoMapper.toDomain(nos[index]);
            result.push(noDomain);
        }

        return result;
    }

}