import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import { ILinhaPersistence } from "../dataschema/ILinhaPersistence";
import Linha from "../domain/linha";
import { LinhaId } from "../domain/linhaId";
import LinhaMapper from "../mappers/linhaMapper";
import { ILinhaRepo } from "../services/IRepos/ILinhaRepo";

@Service()
export default class LinhaRepo implements ILinhaRepo {

    constructor(@Inject('linhaSchema') private linhaSchema: Model<ILinhaPersistence & Document>,) { }

    //Devolve a linha com o LinhaId que é passado por parâmetro
    public async findByDomainId(linhaId: string): Promise<Linha> {
        const query = { domainId: linhaId };
        const linhaRecord = await this.linhaSchema.findOne(query);

        if (linhaRecord != null) {
            return LinhaMapper.toDomain(linhaRecord);
        }
        else {
            return null;
        }
    }

    //Verifica se a linha com o LinhaId passado por parâmetro já existe
    public async exists(linhaId: LinhaId | string): Promise<boolean> {

        const idX = linhaId instanceof LinhaId ? (<LinhaId>linhaId).id.toString() : linhaId;
        const query = { domainId: idX };
        const linhaDocument = await this.linhaSchema.findOne(query);

        return !!linhaDocument === true;
    }

    //Guarda a linha na DB
    public async save(linha: Linha): Promise<Linha> {
        const query = { domainId: linha.id.toString() }; //Vai buscar o id do nó

        const linhaDocument = await this.linhaSchema.findOne(query);

        try {
            if (linhaDocument === null) {
                const rawLinha: any = LinhaMapper.toPersistence(linha);

                const linhaCriada = await this.linhaSchema.create(rawLinha);

                return LinhaMapper.toDomain(linhaCriada);
            } else {
                linhaDocument.codigo = linha.codigo;
                linhaDocument.nome = linha.nome;
                linhaDocument.cor = linha.cor;
                linhaDocument.noInicial = linha.noInicial.id;
                linhaDocument.noFinal = linha.noFinal.id;
                linhaDocument.viaturasPermitidas = linha.viaturasPermitidasEmString;
                linhaDocument.viaturasProibidas = linha.viaturasProibidasEmString;
                linhaDocument.tripulantesPermitidos = linha.tripulantesPermitidosEmString;
                linhaDocument.tripulantesProibidos = linha.tripulantesProibidosEmString;

                await linhaDocument.save();

                return linha;
            }

        } catch (err) {
            throw (err);
        }
    }

    //Devolve todas as linhas
    public async findAll(): Promise<Linha[]> {

        var result: Linha[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const linhas = await this.linhaSchema.find(query);

        for (index = 0; index < linhas.length; index++) {
            var linhaDomain = LinhaMapper.toDomain(linhas[index]);
            result.push(linhaDomain);
        }

        return result;

    }

    //Devolve uma promise de uma lista de linhas onde o seu nome tem as iniciais enviadas por parâmetro
    public async findLinhaComNomeComecadaPor(iniciais: String): Promise<Linha[]> {
        var result: Linha[] = [];
        var index = 0;

        var regexp = new RegExp("^" + iniciais); //Regex: começam por iniciais dadas por parâmetro
        const query = { nome: regexp };
        const linhas = await this.linhaSchema.find(query);

        for (index = 0; index < linhas.length; index++) {
            var linhaDomain = LinhaMapper.toDomain(linhas[index]);
            result.push(linhaDomain);
        }

        return result;
    }

    //Devolve uma promise de uma lista de linhas onde o seu código tem as iniciais enviadas por parâmetro
    public async findLinhaComCodigoComecadaPor(iniciais: String): Promise<Linha[]> {
        var result: Linha[] = [];
        var index = 0;

        var regexp = new RegExp("^" + iniciais); //Regex: começam por iniciais dadas por parâmetro
        const query = { codigo: regexp };
        const linhas = await this.linhaSchema.find(query);

        for (index = 0; index < linhas.length; index++) {
            var linhaDomain = LinhaMapper.toDomain(linhas[index]);
            result.push(linhaDomain);
        }

        return result;
    }
}