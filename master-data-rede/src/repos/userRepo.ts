import { Service, Inject } from 'typedi';

import { Document, Model } from 'mongoose';
import { IUserPersistence } from '../dataschema/IUserPersistence';

import { IUserRepo } from "../services/IRepos/IUserRepo";
import { User } from "../domain/user";
import { UserId } from "../domain/userId";
import { UserEmail } from "../domain/userEmail";
import { UserMapper } from "../mappers/userMapper";
import { IUserDTO } from '../dto/IUserDTO';
const bcrypt = require('bcryptjs');

@Service()
export default class UserRepo implements IUserRepo {
  private models: any;

  constructor(
    @Inject('userSchema') private userSchema: Model<IUserPersistence & Document>,
  ) { }

  public async update(email: UserEmail | string, userUpdate: User): Promise<User> {
    try {

      const query = { email: email.toString() };

      const userDocument = await this.userSchema.findOne(query);

      if (userUpdate.name) userDocument.name = userUpdate.name;

      if (userUpdate.email) userDocument.email = userUpdate.email.props.value;

      if (userUpdate.dataNascimento) userDocument.dataNascimento = userUpdate.dataNascimento.toString();

      if (userUpdate.password != userDocument.password) {
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(userUpdate.password, salt);
        userDocument.password = hashedPassword;
      }

      await userDocument.save();

      return UserMapper.toDomain(userDocument);

    } catch (error) {
      throw error;
    }
  }

  public async delete(email: UserEmail | string): Promise<User> {
    const query = { email: email.toString() };

    const user = await this.userSchema.findOneAndDelete(query);

    if (user != null) {
      return UserMapper.toDomain(user);
    }
    else
      return null;
  }

  public async exists(userId: UserId | string): Promise<boolean> {

    const idX = userId instanceof UserId ? (<UserId>userId).id.toString() : userId;

    const query = { domainId: idX };

    const userDocument = await this.userSchema.findOne(query);

    return !!userDocument === true;
  }

  public async save(user: User): Promise<User> {

    const query = { email: user.email.value };

    const userDocument = await this.userSchema.findOne(query);

    try {
      if (userDocument === null) {
        const rawUser: any = UserMapper.toPersistence(user);

        const userCreated = await this.userSchema.create(rawUser);

        return await UserMapper.toDomain(userCreated);

      } else {
        userDocument.name = user.name;
        await userDocument.save();

        return user;
      }
    } catch (err) {
      throw err;
    }
  }

  public async findByEmail(email: UserEmail | string): Promise<User> {
    const query = { email: email.toString() };
    const userRecord = await this.userSchema.findOne(query);

    if (userRecord != null) {
      return UserMapper.toDomain(userRecord);
    }
    else
      return null;
  }
}