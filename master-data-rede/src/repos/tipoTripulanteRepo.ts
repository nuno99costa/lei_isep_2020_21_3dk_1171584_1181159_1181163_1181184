import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import { ITipoTripulantePersistence } from "../dataschema/ITipoTripulantePersistence";
import TipoTripulante from "../domain/tipoTripulante";
import { TipoTripulanteId } from "../domain/tipoTripulanteId";
import TipoTripulanteMapper from "../mappers/tipoTripulanteMapper";
import { ITipoTripulanteRepo } from "../services/IRepos/ITipoTripulanteRepo";

@Service()
export default class TipoTripulanteRepo implements ITipoTripulanteRepo {

    constructor(@Inject('tipoTripulanteSchema') private tipoTripulanteSchema: Model<ITipoTripulantePersistence & Document>,) { }

    public async findAll(): Promise<TipoTripulante[]> {

        var result: TipoTripulante[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const tiposTripulante = await this.tipoTripulanteSchema.find(query);

        for (index = 0; index < tiposTripulante.length; index++) {
            var tipoTripulanteDomain = TipoTripulanteMapper.toDomain(tiposTripulante[index]);
            result.push(tipoTripulanteDomain);
        }

        return result;
    }


    public async findByDomainId(tipoTripulanteId: string): Promise<TipoTripulante> {
        const query = { domainId: tipoTripulanteId };
        const tipoTripulanteRecord = await this.tipoTripulanteSchema.findOne(query);

        if (tipoTripulanteRecord != null) {
            return TipoTripulanteMapper.toDomain(tipoTripulanteRecord);
        }
        else {
            return null;
        }
    }

    public async exists(tipoTripulanteId: TipoTripulanteId | string): Promise<boolean> {

        const idX = tipoTripulanteId instanceof TipoTripulanteId ? (<TipoTripulanteId>tipoTripulanteId).id.toString() : tipoTripulanteId;
        const query = { domainId: idX };
        const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);

        return !!tipoTripulanteDocument === true;
    }

    public async save(tipoTripulante: TipoTripulante): Promise<TipoTripulante> {
        const query = { domainId: tipoTripulante.id.toString() }; //Vai buscar o id do tipo de tripulante

        const tipoTripulanteDocument = await this.tipoTripulanteSchema.findOne(query);

        try {
            if (tipoTripulanteDocument === null) {
                const rawTipoTripulante: any = TipoTripulanteMapper.toPersistence(tipoTripulante);

                const tipoTripulanteCriada = await this.tipoTripulanteSchema.create(rawTipoTripulante);

                return TipoTripulanteMapper.toDomain(tipoTripulanteCriada);
            } else {
                tipoTripulanteDocument.codigo = tipoTripulante.codigo;
                tipoTripulanteDocument.descricao = tipoTripulante.descricao;

                await tipoTripulanteDocument.save();

                return tipoTripulante;
            }

        } catch (err) {
            throw (err);
        }
    }
}