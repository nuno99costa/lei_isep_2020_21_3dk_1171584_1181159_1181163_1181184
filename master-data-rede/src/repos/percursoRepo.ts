import { Document, Model } from "mongoose";
import { Inject, Service } from "typedi";
import Percurso, { Orientacao } from "../domain/percurso";
import { IPercursoRepo } from "../services/IRepos/IPercursoRepo";
import PercursoMapper from "../mappers/percursoMapper";
import { IPercursoPersistence } from "../dataschema/IPercursoPersistence";
import { PercursoId } from "../domain/percursoId";
import { Result } from "../core/logic/Result";
import config from "../../config";
import { ISegmentoRepo } from "../services/IRepos/ISegmentoRepo";
import Segmento from "../domain/segmento";
import { result } from "lodash";
import { INoRepo } from "../services/IRepos/INoRepo";
import { NoId } from "../domain/noId";
import { query } from "express";


@Service()
export default class PercursoRepo implements IPercursoRepo {

    constructor(@Inject('percursoSchema') private percursoSchema: Model<IPercursoPersistence & Document>,
        @Inject(config.repos.segmento.name) private segmentoRepo?: ISegmentoRepo,
    ) { }

    async findByNome(nome: string): Promise<Percurso> {
        const query = { nomePercurso: nome };
        const roleRecord = await this.percursoSchema.findOne(query);

        if (roleRecord != null) {
            return PercursoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }
    }

    public async findAll(): Promise<Percurso[]> {

        var result: Percurso[] = [];
        var index = 0;
        const query = {}; //Vazia porque não há restrições
        const percursos = await this.percursoSchema.find(query);

        for (index = 0; index < percursos.length; index++) {
            var percursoDomain = PercursoMapper.toDomain(percursos[index]);
            result.push(percursoDomain);
        }
        return result;
    }

    async findByDomainId(percursoId: string): Promise<Percurso> {
        const query = { domainId: percursoId };
        const roleRecord = await this.percursoSchema.findOne(query);

        if (roleRecord != null) {
            return PercursoMapper.toDomain(roleRecord);
        }
        else {
            return null;
        }
    }

    async findByIdLinha(linhaId: string): Promise<Percurso[]> {
        var result: Percurso[] = [];
        var index = 0;
        const query = { linhaId: linhaId }; //vazia porque não existem restrições
        const percursos = await this.percursoSchema.find(query);

        for (index = 0; index < percursos.length; index++) {
            var percursoDomain = PercursoMapper.toDomain(percursos[index]);
            result.push(percursoDomain);
        }

        return result;
    }

    public async exists(percursoId: PercursoId | string): Promise<boolean> {
        const idX = percursoId instanceof PercursoId ? (<PercursoId>percursoId).id.toString() : percursoId;
        const query = { domainId: idX };
        const percursoDocument = await this.percursoSchema.findOne(query);

        return !!percursoDocument === true;
    }

    //Guardar o segmento
    public async save(percurso: Percurso): Promise<Percurso> {


        //Verificar se á existe um percurso igual
        const query = { domainId: percurso.id.toString() } //Vai buscar o id do percurso

        const percursoDocument = await this.percursoSchema.findOne(query); //Verifica se já existe


        try {

            if (percursoDocument === null) {
                //Transformar em schema (ver schema em persisence/schemas)
                const rawPercurso: any = PercursoMapper.toPersistence(percurso);


                //Guarda na bd
                const percursoCriado = await this.percursoSchema.create(rawPercurso);

                //Schema -> Domain

                return PercursoMapper.toDomain(percursoCriado);
            } else {

                //Se houver percurso igual
                percursoDocument.nomePercurso = percurso.nomePercurso;
                percursoDocument.listaSegmentos = percurso.segmentosEmString;
                percursoDocument.distanciaTotal = percurso.distanciaTotal;
                percursoDocument.tempoTotal = percurso.tempoTotal;

                //gUARDAR
                await percursoDocument.save();

                return percurso;
            }

        } catch (err) {
            throw (err);
        }
    }

    public async validarPercurso(segmentoId: String, index: number, array: String[]): Promise<Segmento> {

        const segmentoOrNull = await this.segmentoRepo.findByDomainId(segmentoId);


        if (segmentoOrNull === null) {
            return null;
        }

        if (index == 0) {
            return segmentoOrNull;
        }

        const segmentoAnterior = await this.segmentoRepo.findByDomainId(array[index - 1]);

        const noFinalAnterior = segmentoAnterior.idNoFinal.id;
        const noInicialAtual = segmentoOrNull.idNoInicial.id;

        if (noFinalAnterior != noInicialAtual) {
            return null;
        }

        return segmentoOrNull;
    }

    //Aqui vamos verificar se os nós inicial e final coincidem com os de cada percurso
    public async validarNosPercurso(noInicialLinha: string, noFinalLinha: string, noInicialPercurso: string, noFinalPercurso: string, orientacao: Orientacao): Promise<boolean> {
        switch (orientacao) {
            case Orientacao.Ida: {
                return ((noInicialLinha == noInicialPercurso) && (noFinalLinha == noFinalPercurso));
            }

            case Orientacao.Volta: {
                return ((noInicialLinha == noFinalPercurso) && !(noFinalLinha == noInicialPercurso));
            }

            default: { //para Reforço e Vazio (podem vir de qualquer lado)
                if (!(noInicialLinha == noInicialPercurso) || !(noFinalLinha == noFinalPercurso)) return true;
                if (!(noInicialLinha == noFinalPercurso) || !(noFinalLinha == noInicialPercurso)) return true;
                break;
            }

        }
        return true;
    }
}