export interface ILinhaPercursosDTO {
    nomeLinha: string;
    idPercurso: string;
    abreviaturasNos: string[];
    tempo: number;
    distancia: number;
}