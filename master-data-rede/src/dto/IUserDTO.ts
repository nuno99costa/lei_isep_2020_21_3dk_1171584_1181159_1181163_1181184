import { Role } from "../domain/user";

export interface IUserDTO {
  name: string;
  email: string;
  password: string;
  dataNascimento: string;
  role: Role;
}
