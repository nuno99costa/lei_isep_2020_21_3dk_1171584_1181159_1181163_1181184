//Para colocarmos os diferentes percursos no mapa, precisamos de devolver um objeto com uma matriz de coordenadas.
//Cada linha da matriz é um array de coordenadas de cada percurso de uma linha
//Assim ,minimizamos a quantidade de pedidos HTTP, liminando os mesmos a um por linha.
export default interface IDadosLinhaMapaDTO {
    cor: string;
    matrizCoordenadas: [number, number][][];
    nomeLinha: string;
    nomesPercursos: string[];
}