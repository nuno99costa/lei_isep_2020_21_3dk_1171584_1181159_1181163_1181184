import { TipoCombustivel } from "../domain/tipoViatura";

export default interface ITipoViaturaDTO {
    id: string;
    codigo: string;
    descricao: string;
    combustivel: TipoCombustivel;
    autonomia: number;
    velocidadeMedia: number;
    custoPorQuilometro: number;
    consumoMedio: number;
}