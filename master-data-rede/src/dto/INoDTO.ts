

//DTO do No (o id tem de estar + nomes dos atributos iguais a NoProps)
export default interface INoDTO {
  id: string;

  nomeCompleto: string;

  abreviatura: string;

  isPontoRendicao: boolean;

  isEstacaoRecolha: boolean;

  latitude: number;

  longitude: number;

  nomeModelo: string;

}