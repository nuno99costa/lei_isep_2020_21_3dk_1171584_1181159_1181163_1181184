import { Orientacao } from "../domain/percurso";

//DTO do percurso
export default interface IPercursoDTO {
    id: string,
    
    nomePercurso: string,

    linhaId: string,

    orientacao: Orientacao,

    listaSegmentos: String[],

    noInicialId: string,

    noFinalId: string,

    distanciaTotal: number,

    tempoTotal: number
}