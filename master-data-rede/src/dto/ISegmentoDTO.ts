
//DTO do Segmento (o id tem de estar + nomes dos atributos iguais a SegmentoProps)
export default interface ISegmentoDTO{
    id: string;

    nomeSegmento: string;

    idNoInicial: string;

    idNoFinal: string;

    distanciaNos: number;

    tempoNos: number;

}