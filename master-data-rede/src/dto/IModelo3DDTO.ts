import { Parser } from "acorn";

export default interface IModelo3DDTO {
    nome: string;
    path: string;
}