export default class NumberUtils{
    public static euclidianDistance(xi : number,yi : number, xj : number, yj : number) : number{
        return Math.sqrt( Math.pow((xj-xi), 2) + Math.pow((yj-yi), 2) );
    }
}