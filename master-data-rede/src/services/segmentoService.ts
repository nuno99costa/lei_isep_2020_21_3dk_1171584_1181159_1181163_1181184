import e from "express";
import { Container, Service, Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import { NoId } from "../domain/noId";
import Segmento from "../domain/segmento";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import SegmentoMapper from "../mappers/segmentoMapper";
import { INoRepo } from "./IRepos/INoRepo";
import { ISegmentoRepo } from "./IRepos/ISegmentoRepo";
import ISegmentoService from "./IServices/ISegmentoService";

export default class SegmentoService implements ISegmentoService {

    constructor(
        @Inject(config.repos.segmento.name) private segmentoRepo?: ISegmentoRepo,
        @Inject(config.repos.no.name) private noRepo?: INoRepo//Injetar repositorio do config
    ) { }

    public async criarSegmento(segmentoDTO: ISegmentoDTO): Promise<Result<ISegmentoDTO>> { //Promise pois é async.

        try {

            //Os nós existem?
            const noInicial = await this.noRepo.exists(segmentoDTO.idNoInicial);
            const noFinal = await this.noRepo.exists(segmentoDTO.idNoFinal);

            if (noInicial == false) {
                return Result.fail<ISegmentoDTO>("O nó inicial não existe");
            } else if (noFinal == false) {
                return Result.fail<ISegmentoDTO>("O nó final não existe");
            }

            //Criar instância do segmento usando o metodo create(dto). Retorna um result
            var segmentoOrError;
            if (segmentoDTO.id === undefined) {
                segmentoOrError = await Segmento.create(segmentoDTO);

            } else {
                segmentoOrError = await Segmento.create(segmentoDTO, new UniqueEntityID(segmentoDTO.id));

            }

            //Se der erro, manda fail
            if (segmentoOrError.isFailure) {
                return Result.fail<ISegmentoDTO>(segmentoOrError.errorValue());
            }

            //Se der certo, vamos buscar o value (segmento)
            const segmentoResult = segmentoOrError.getValue();

            //Chamamos o repositorio para guardar o segmento criado. Retorna o segmento guardado
            const savedSegmento = await this.segmentoRepo.save(segmentoResult);

            //Transformar segmento guardado em DTO
            const savedSegmentoDTOResult = SegmentoMapper.toDTO(savedSegmento) as ISegmentoDTO;

            //Retornar DTO do segmento guardado
            return Result.ok<ISegmentoDTO>(savedSegmentoDTOResult);
        } catch (err) {
            throw err;
        }
    }

    public async getSegmentos(): Promise<Result<ISegmentoDTO[]>> {
        const listaSegmentos = await this.segmentoRepo.findAll();
        if (listaSegmentos.length == 0) return Result.fail<ISegmentoDTO[]>("⚠️ Não existem segmentos! ⚠️");

        let result: ISegmentoDTO[] = [];
        let index = 0;

        for (index = 0; index < listaSegmentos.length; index++) {
            const segmento = SegmentoMapper.toDTO(listaSegmentos[index]);
            result.push(segmento);
        }

        return Result.ok<ISegmentoDTO[]>(result);
    }

    public segmentoByNosInList(segmentos : Segmento[], idNoInicial : NoId, idNoFinal : NoId) : Segmento{
        let i = 0;
        let found : Segmento = segmentos.find(seg => seg.idNoInicial === idNoInicial && seg.idNoFinal === idNoFinal);
        return found;
    }

    public async segmentoByNome(nome : string) : Promise<Result<ISegmentoDTO>>{
        try {
            const segmento = await this.segmentoRepo.findByNome(nome);
            if (segmento === null) return Result.fail<ISegmentoDTO>("⚠️ Não existe nenhum segmento com o ID introduzido! ⚠️");
            const segmentoDTO = SegmentoMapper.toDTO(segmento);
            return Result.ok<ISegmentoDTO>(segmentoDTO);
        } catch (error) {
            throw error;
        }
    }

}