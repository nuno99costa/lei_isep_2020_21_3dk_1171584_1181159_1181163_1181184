import { Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import Linha from "../domain/linha";
import { NoId } from "../domain/noId";
import { TipoTripulanteId } from "../domain/tipoTripulanteId";
import { TipoViaturaId } from "../domain/tipoViaturaId";
import ILinhaDTO from "../dto/ILinhaDTO";
import LinhaMapper from "../mappers/linhaMapper";
import { ILinhaRepo } from "./IRepos/ILinhaRepo";
import { INoRepo } from "./IRepos/INoRepo";
import { IPercursoRepo } from "./IRepos/IPercursoRepo";
import { ISegmentoRepo } from "./IRepos/ISegmentoRepo";
import { ITipoTripulanteRepo } from "./IRepos/ITipoTripulanteRepo";
import { ITipoViaturaRepo } from "./IRepos/ITipoViaturaRepo";
import ILinhaService from "./IServices/ILinhaService";

export default class LinhaService implements ILinhaService {
    constructor(@Inject(config.repos.linha.name) private linhaRepo?: ILinhaRepo,
        @Inject(config.repos.no.name) private noRepo?: INoRepo, //Injetar repositorio do config.
        @Inject(config.repos.tipoViatura.name) private tipoViaturaRepo?: ITipoViaturaRepo,
        @Inject(config.repos.tipoTripulante.name) private tipoTripulanteRepo?: ITipoTripulanteRepo,
    ) { }

    public async criarLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>> {
        try {

            /**Aqui vamos buscar os tripulantes viaturas das listas de strings que estão no DTO 
            e transformamo-los em listas de ids (viatura e tripulante) e percursosLinha. Depois mandamos por
            parâmetro para o método create**/

            //Verificar se os nós existem
            const nosAreValid = await this.validarIdNos(linhaDTO);
            if (!nosAreValid) return Result.fail<ILinhaDTO>("⚠️ Um dos nós introduzidos não existe! ⚠️");
            const noInicial = NoId.create(new UniqueEntityID(linhaDTO.noInicial));
            const noFinal = NoId.create(new UniqueEntityID(linhaDTO.noFinal));

            //Verificar se os nós são iguais (se é uma linha circular)
            if (noInicial.equals(noFinal)) return Result.fail<ILinhaDTO>("⚠️ O nó inicial tem de ser diferente do final!⚠️");

            //Verificar se as listas de tipos de viaturas são válidas
            const viaturasPermitidasAreValid = await this.validarTiposViatura(linhaDTO.viaturasPermitidas);
            if (viaturasPermitidasAreValid.isFailure) return Result.fail<ILinhaDTO>(viaturasPermitidasAreValid.errorValue());
            const listaViaturasPermitidas = viaturasPermitidasAreValid.getValue();

            const viaturasProibidasAreValid = await this.validarTiposViatura(linhaDTO.viaturasProibidas);
            if (viaturasProibidasAreValid.isFailure) return Result.fail<ILinhaDTO>(viaturasProibidasAreValid.errorValue());
            const listaViaturasProibidas = viaturasProibidasAreValid.getValue();

            //Verificar se as listas dos tripulantes são válidas
            const tripulantesPermitidosAreValid = await this.validarTiposTripulante(linhaDTO.tripulantesPermitidos);
            if (tripulantesPermitidosAreValid.isFailure) return Result.fail<ILinhaDTO>(tripulantesPermitidosAreValid.errorValue());
            const listaTripulantesPermitidos = tripulantesPermitidosAreValid.getValue();

            const tripulantesProibidosAreValid = await this.validarTiposTripulante(linhaDTO.tripulantesProibidos);
            if (tripulantesProibidosAreValid.isFailure) return Result.fail<ILinhaDTO>(tripulantesProibidosAreValid.errorValue());
            const listaTripulantesProibidos = tripulantesProibidosAreValid.getValue();

            //O DTO veio com id?
            var linhaOrError;
            if (!linhaDTO.id || linhaDTO.id === undefined) {
                linhaOrError = await Linha.create(linhaDTO, noInicial, noFinal, listaViaturasPermitidas, listaViaturasProibidas, listaTripulantesPermitidos, listaTripulantesProibidos);
            } else {
                linhaOrError = await Linha.create(linhaDTO, noInicial, noFinal, listaViaturasPermitidas, listaViaturasProibidas, listaTripulantesPermitidos, listaTripulantesProibidos, new UniqueEntityID(linhaDTO.id));
            }

            //Verificamos se houve erro no create
            if (linhaOrError.isFailure) return Result.fail<ILinhaDTO>(linhaOrError.errorValue());

            //Se não, guardamos o valor
            const linhaResult = linhaOrError.getValue();

            //Save na DB
            const savedLinha = await this.linhaRepo.save(linhaResult);

            //Transformamos em DTO e devolvemos
            const savedLinhaDTOResult = LinhaMapper.toDTO(savedLinha) as ILinhaDTO;

            return Result.ok<ILinhaDTO>(savedLinhaDTOResult);
        } catch (error) {
            throw (error);
        }
    }

    //Devolve a lista de linhas ordenadas por nome ou por codigo
    public async listaOrdenadaPor(ordenacao: String): Promise<Result<ILinhaDTO[]>> {

        try {
            var result: ILinhaDTO[] = [];

            var todasAsLinhas = await this.linhaRepo.findAll(); // vai buscar todas as linhas

            //Verifica qual é o tipo de ordenação que veio por parâmetro: nome ou código
            if (ordenacao === "nome") {
                todasAsLinhas = todasAsLinhas.sort(Linha.compareByNome); //sort por nome
            } else if (ordenacao === "codigo") {
                todasAsLinhas = todasAsLinhas.sort(Linha.compareByCodigo); //sort por codigo da linha
            } else {
                return Result.fail<ILinhaDTO[]>("⚠️ Tipo de ordenação não é válido! ⚠️");
            }

            var index = 0;
            //Transforma a linha em DTO e depois guarda no array
            for (index = 0; index < todasAsLinhas.length; index++) {
                var linhaDTO = LinhaMapper.toDTO(todasAsLinhas[index]);
                result.push(linhaDTO);
            }

            return Result.ok<ILinhaDTO[]>(result);

        } catch (err) {
            throw err;
        }

    }

    public async listaComecadasPor(tipo: String, iniciais: String): Promise<Result<ILinhaDTO[]>> {
        try {
            var result: ILinhaDTO[] = [];
            var todasAsLinhas: Linha[];
            if (tipo === "nome") {
                todasAsLinhas = await this.linhaRepo.findLinhaComNomeComecadaPor(iniciais);
            } else if (tipo === "codigo") {
                todasAsLinhas = await this.linhaRepo.findLinhaComCodigoComecadaPor(iniciais);
            } else {
                return Result.fail<ILinhaDTO[]>("⚠️ O tipo de filtro não é válido! ⚠️");
            }

            var index = 0;

            for (index = 0; index < todasAsLinhas.length; index++) {
                var linhaDTO = LinhaMapper.toDTO(todasAsLinhas[index]);
                result.push(linhaDTO);
            }

            if (result.length == 0) return Result.fail<ILinhaDTO[]>("⚠️ Não existe nenhuma linha com essas iniciais! ⚠️");

            return Result.ok<ILinhaDTO[]>(result);
        } catch (err) {
            throw err;
        }
    }

    //Devolve uma linha correspondente ao id
    public async getLinhaById(id: string): Promise<Result<ILinhaDTO>> {
        try {
            const linha = await this.linhaRepo.findByDomainId(id);
            if (linha === null) Result.fail<ILinhaDTO>("⚠️ Não existe nenhuma linha com o ID introduzido! ⚠️");
            const linhaDTO = LinhaMapper.toDTO(linha);
            return Result.ok<ILinhaDTO>(linhaDTO);
        } catch (error) {
            throw error;
        }
    }



    //Validamos os tipos de tripulantes (permitidos ou proibidos)
    private async validarTiposTripulante(tipoTripulantes: String[]): Promise<Result<TipoTripulanteId[]>> {

        let tripulantes: TipoTripulanteId[] = [];

        let index = 0;

        for (index = 0; index < tipoTripulantes.length; ++index) {
            const tripulanteExists = this.tipoTripulanteRepo.exists(tipoTripulantes[index].toString());
            if (!tripulanteExists) return Result.fail<TipoTripulanteId[]>('⚠️ O tipo de tripulante não existe! ⚠️');

            const tripulante = TipoTripulanteId.create(new UniqueEntityID(tipoTripulantes[index].toString()));

            tripulantes.push(tripulante);
        }

        return Result.ok<TipoTripulanteId[]>(tripulantes);

    }

    //Valida os tipos de viatura (permitidos ou proibidos)
    private async validarTiposViatura(tipoViaturas: String[]): Promise<Result<TipoViaturaId[]>> {

        let viaturas: TipoViaturaId[] = [];

        let index = 0;

        for (index = 0; index < tipoViaturas.length; ++index) {
            const viaturaExists = this.tipoViaturaRepo.exists(tipoViaturas[index].toString());
            if (!viaturaExists) return Result.fail<TipoViaturaId[]>('⚠️ O tipo de viatura não existe! ⚠️');

            const viatura = TipoViaturaId.create(new UniqueEntityID(tipoViaturas[index].toString()));

            viaturas.push(viatura);
        }

        return Result.ok<TipoViaturaId[]>(viaturas);
    }

    //Valida os ids dos nós
    private async validarIdNos(linhaDTO: ILinhaDTO): Promise<boolean> {
        const existeNoInicial = await this.noRepo.exists(linhaDTO.noInicial);
        const existeNoFinal = await this.noRepo.exists(linhaDTO.noFinal);

        if (!existeNoFinal || !existeNoInicial) {
            return false;
        }
        return true;
    }


}