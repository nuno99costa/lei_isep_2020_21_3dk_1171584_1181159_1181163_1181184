import { Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import TipoViatura from "../domain/tipoViatura";
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";
import TipoViaturaMapper from "../mappers/tipoViaturaMapper";
import { ITipoViaturaRepo } from "./IRepos/ITipoViaturaRepo";
import ITipoViaturaService from "./IServices/ITipoViaturaService";

export default class TipoViaturaService implements ITipoViaturaService {
    constructor(@Inject(config.repos.tipoViatura.name) private tipoViaturaRepo?: ITipoViaturaRepo) { }

    public async criarTipoViatura(tipoViaturaDTO: ITipoViaturaDTO): Promise<Result<ITipoViaturaDTO>> {
        try {

            var tipoViaturaOrError;
            if (!tipoViaturaDTO.id || tipoViaturaDTO.id === undefined) {
                tipoViaturaOrError = await TipoViatura.create(tipoViaturaDTO);
            } else {
                tipoViaturaOrError = await TipoViatura.create(tipoViaturaDTO, new UniqueEntityID(tipoViaturaDTO.id));
            }

            if (tipoViaturaOrError.isFailure) {
                return Result.fail<ITipoViaturaDTO>(tipoViaturaOrError.errorValue());
            }

            const tipoViaturaResult = tipoViaturaOrError.getValue();

            const savedTipoViatura = await this.tipoViaturaRepo.save(tipoViaturaResult);

            const savedTipoViaturaDTOResult = TipoViaturaMapper.toDTO(savedTipoViatura) as ITipoViaturaDTO;

            return Result.ok<ITipoViaturaDTO>(savedTipoViaturaDTOResult);
        } catch (err) {
            throw err;
        }
    }

    public async getTipoViatura(): Promise<Result<ITipoViaturaDTO[]>> {
        try {
            const listaTiposViatura = await this.tipoViaturaRepo.findAll();
            if (listaTiposViatura.length == 0) return Result.fail<ITipoViaturaDTO[]>("⚠️ Não existem tipos de viatura! ⚠️");

            var result: ITipoViaturaDTO[] = [];
            var index = 0;

            for (index = 0; index < listaTiposViatura.length; index++) {
                const dto = TipoViaturaMapper.toDTO(listaTiposViatura[index]);
                result.push(dto);
            }

            return Result.ok<ITipoViaturaDTO[]>(result);
        } catch (error) {
            throw error;
        }
    }
}