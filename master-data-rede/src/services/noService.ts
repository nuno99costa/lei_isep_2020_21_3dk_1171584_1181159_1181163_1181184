import e from "express";
import { Container, Service, Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import No from "../domain/no";
import { NoId } from "../domain/noId";
import INoDTO from "../dto/INoDTO";
import NoMapper from "../mappers/noMapper";
import { INoRepo } from "./IRepos/INoRepo";
import INoService from "./IServices/INoService";

export default class NoService implements INoService {

    constructor(
        @Inject(config.repos.no.name) private noRepo?: INoRepo //Injetar repositorio do config.
    ) { }

    async updateModelo3D(id: string, nomeModelo: string): Promise<Result<INoDTO>> {
        const updatedNo = await this.noRepo.updateNoModelo3D(id, nomeModelo);
        return Result.ok<INoDTO>(NoMapper.toDTO(updatedNo));
    }

    public async criarNo(noDTO: INoDTO): Promise<Result<INoDTO>> { //Promise pois é async.

        try {

            //Criar instância do nó usando o metodo create(dto). Retorna um result
            var noOrError;
            if (!noDTO.id || noDTO.id === undefined) {
                noOrError = await No.create(noDTO);

            } else {
                noOrError = await No.create(noDTO, new UniqueEntityID(noDTO.id));

            }

            //Se der erro, manda fail.
            if (noOrError.isFailure) {
                return Result.fail<INoDTO>(noOrError.errorValue());
            }

            //Se der certo, vamos buscar o Value (nó).
            const noResult = noOrError.getValue();

            //Chamamos o repositorio para guardar o no criado. Retorna o nó guardado.
            const savedNo = await this.noRepo.save(noResult);

            //Transformar no guardado em DTO.
            const savedNoDTOResult = NoMapper.toDTO(savedNo) as INoDTO;

            //Retornar DTO do no guardado.
            return Result.ok<INoDTO>(savedNoDTOResult);
        } catch (err) {
            throw err;
        }



    }

    public async listaOrdenadaPor(ordenacao: String): Promise<Result<INoDTO[]>> {

        try {
            var result: INoDTO[] = [];

            var todosOsNos = await this.noRepo.findAll();

            if (ordenacao === "nome") {
                todosOsNos = todosOsNos.sort(No.compareByNome);
            } else if (ordenacao === "codigo") {
                todosOsNos = todosOsNos.sort(No.compareByID);
            } else {
                return Result.fail<INoDTO[]>("Tipo de ordenação não encontrado");
            }

            var index = 0;

            for (index = 0; index < todosOsNos.length; index++) {
                var noDTO = NoMapper.toDTO(todosOsNos[index]);
                result.push(noDTO);
            }


            return Result.ok<INoDTO[]>(result);
        } catch (err) {
            throw err;
        }

    }

    public async listaComecadosPor(tipo: String, iniciais: String): Promise<Result<INoDTO[]>> {
        try {
            var result: INoDTO[] = [];
            var todosOsNos: No[];
            if (tipo === "nome") {
                todosOsNos = await this.noRepo.findNoComNomeComecadoPor(iniciais);
            } else if (tipo === "codigo") {
                todosOsNos = await this.noRepo.findNoComCodigoComecadoPor(iniciais);
            } else {
                return Result.fail<INoDTO[]>("Tipo de filtro não encontrado!");
            }

            var index = 0;

            for (index = 0; index < todosOsNos.length; index++) {
                var noDTO = NoMapper.toDTO(todosOsNos[index]);
                result.push(noDTO);
            }


            return Result.ok<INoDTO[]>(result);
        } catch (err) {
            throw err;
        }
    }

    //Devolve o nó correspondente ao id
    public async getNoById(id: string): Promise<Result<INoDTO>> {
        try {
            const no = await this.noRepo.findByDomainId(id);
            if (no === null) return Result.fail<INoDTO>("⚠️ Não existe nenhum nó com o ID introduzido! ⚠️");
            const noDTO = NoMapper.toDTO(no);
            return Result.ok<INoDTO>(noDTO);
        } catch (error) {
            throw error;
        }
    }
}