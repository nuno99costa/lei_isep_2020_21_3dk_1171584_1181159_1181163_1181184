import { Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import TipoTripulante from "../domain/tipoTripulante";
import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";
import TipoTripulanteMapper from "../mappers/tipoTripulanteMapper";
import { ITipoTripulanteRepo } from "./IRepos/ITipoTripulanteRepo";
import ITipoTripulanteService from "./IServices/ITipoTripulanteService";

export default class TipoTripulanteService implements ITipoTripulanteService {
    constructor(@Inject(config.repos.tipoTripulante.name) private tipoTripulanteRepo?: ITipoTripulanteRepo) { }

    public async criarTipoTripulante(tipoTripulanteDTO: ITipoTripulanteDTO): Promise<Result<ITipoTripulanteDTO>> {
        try {

            var tipoTripulanteOrError
            if (tipoTripulanteDTO.id === undefined) {

                tipoTripulanteOrError = await TipoTripulante.create(tipoTripulanteDTO);

            } else {
                tipoTripulanteOrError = await TipoTripulante.create(tipoTripulanteDTO, new UniqueEntityID(tipoTripulanteDTO.id));

            }

            if (tipoTripulanteOrError.isFailure) {
                return Result.fail<ITipoTripulanteDTO>(tipoTripulanteOrError.errorValue());
            }

            const tipoTripulanteResult = tipoTripulanteOrError.getValue();

            const savedTipoTripulante = await this.tipoTripulanteRepo.save(tipoTripulanteResult);

            const savedTipoTripulanteDTOResult = TipoTripulanteMapper.toDTO(savedTipoTripulante) as ITipoTripulanteDTO;

            return Result.ok<ITipoTripulanteDTO>(savedTipoTripulanteDTOResult);
        } catch (err) {
            throw err;
        }
    }

    public async getTipoTripulante(): Promise<Result<ITipoTripulanteDTO[]>> {
        try {
            const listaTiposTripulante = await this.tipoTripulanteRepo.findAll();
            if (listaTiposTripulante.length == 0) return Result.fail<ITipoTripulanteDTO[]>("⚠️ Não existem tipos de tripulante! ⚠️");

            var result: ITipoTripulanteDTO[] = [];
            var index = 0;

            for (index = 0; index < listaTiposTripulante.length; index++) {
                const dto = TipoTripulanteMapper.toDTO(listaTiposTripulante[index]);
                result.push(dto);
            }

            return Result.ok<ITipoTripulanteDTO[]>(result);
        } catch (error) {
            throw error;
        }
    }

}