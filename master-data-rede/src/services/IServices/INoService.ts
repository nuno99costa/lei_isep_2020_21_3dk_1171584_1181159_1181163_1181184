import { Result } from "../../core/logic/Result";
import INoDTO from "../../dto/INoDTO";

export default interface INoService {
    criarNo(noDTO: INoDTO): Promise<Result<INoDTO>>; //Promise porque é async.
    listaOrdenadaPor(ordenacao: String): Promise<Result<INoDTO[]>>;
    listaComecadosPor(tipo: String, iniciais: String): Promise<Result<INoDTO[]>>;
    getNoById(id: string): Promise<Result<INoDTO>>;
    updateModelo3D(id : string, nomeModelo : string): Promise<Result<INoDTO>>;
}