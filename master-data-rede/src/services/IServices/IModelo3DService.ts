import { Result } from "../../core/logic/Result";
import IModelo3DDTO from "../../dto/IModelo3DDTO";

export default interface IModelo3DService {
    criarModelo3D(noDTO: IModelo3DDTO): Promise<Result<IModelo3DDTO>>; //Promise porque é async.
    getModelo3DByNome(id: string): Promise<Result<IModelo3DDTO>>;
    getAllModelos3D(): Promise<Result<IModelo3DDTO[]>>;
}