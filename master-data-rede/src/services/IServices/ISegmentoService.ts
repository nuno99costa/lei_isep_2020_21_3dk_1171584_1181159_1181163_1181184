import { Result } from "../../core/logic/Result";
import ISegmentoDTO from "../../dto/ISegmentoDTO";

export default interface ISegmentoService {
    criarSegmento(segmentoDTO: ISegmentoDTO): Promise<Result<ISegmentoDTO>>; //Promise porque é async.
    getSegmentos(): Promise<Result<ISegmentoDTO[]>>; //Devolve todos os segmentos guardados
    segmentoByNome(nome : string) : Promise<Result<ISegmentoDTO>>;
}