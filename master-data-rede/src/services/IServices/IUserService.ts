import { Result } from "../../core/logic/Result";
import { UserEmail } from "../../domain/userEmail";
import ILoginDTO from "../../dto/ILoginDTO";
import { IUserDTO } from "../../dto/IUserDTO";

export default interface IUserService {
    registarUser(userDTO: IUserDTO): Promise<Result<IUserDTO>>;
    login(loginDTO: ILoginDTO): Promise<Result<string>>;
    getDadosUser(email: UserEmail | string): Promise<Result<IUserDTO>>;
    updateDadosUser(email: UserEmail | string, userUpdate: IUserDTO): Promise<Result<IUserDTO>>;
    deleteDadosUser(email: UserEmail | string): Promise<Result<IUserDTO>>;
}