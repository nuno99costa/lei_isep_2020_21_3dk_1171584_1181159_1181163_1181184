import { Result } from "../../core/logic/Result";
import Percurso from "../../domain/percurso";
import IDadosLinhaMapaDTO from "../../dto/IDadosLinhaMapaDTO";
import { ILinhaPercursosDTO } from "../../dto/ILinhaPercursosDTO";
import INoDTO from "../../dto/INoDTO";
import IPercursoDTO from "../../dto/IPercursoDTO";
import ISegmentoDTO from "../../dto/ISegmentoDTO";

export default interface IPercursoService {
    criarPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>>; //Promise porque é async.
    listaPercursos(idLinha: string): Promise<Result<IPercursoDTO[]>>; //Promise porque é async.
    dadosDePercursosParaMapa(): Promise<Result<IDadosLinhaMapaDTO[]>>
    dadosPercursoParaPlaneamento(): Promise<Result<ILinhaPercursosDTO[]>>; //devolve os dados de um percurso para o módulo do planeamento
    getNosPercursoId(percursoId: string): Promise<Result<INoDTO[]>>;
    getSegmentosPercurso(percursoId: string): Promise<Result<ISegmentoDTO[]>>;//devolve todos os segmentos de um percurso
}