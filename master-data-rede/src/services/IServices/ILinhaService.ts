import { Result } from "../../core/logic/Result";
import ILinhaDTO from "../../dto/ILinhaDTO";

export default interface ILinhaService {
    criarLinha(linhaDTO: ILinhaDTO): Promise<Result<ILinhaDTO>>;
    listaOrdenadaPor(ordenacao: String): Promise<Result<ILinhaDTO[]>>;
    listaComecadasPor(tipo: String, iniciais: String): Promise<Result<ILinhaDTO[]>>;
    getLinhaById(id: String): Promise<Result<ILinhaDTO>>;
}