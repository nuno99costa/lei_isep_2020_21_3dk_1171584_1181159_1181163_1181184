import { Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import Modelo3D from "../domain/Modelo3D";
import IModelo3DDTO from "../dto/IModelo3DDTO";
import Modelo3DMapper from "../mappers/modelo3DMapper";
import { IModelo3DRepo } from "./IRepos/IModelo3DRepo";
import IModelo3DService from "./IServices/IModelo3DService";


export default class Modelo3DService implements IModelo3DService {

    constructor(
        @Inject(config.repos.modelo3D.name) private modelo3DRepo?: IModelo3DRepo //Injetar repositorio do config.
    ) { }

    public async criarModelo3D(modelo3DDTO: IModelo3DDTO): Promise<Result<IModelo3DDTO>> { //Promise pois é async.

        try {

            //Criar instância do nó usando o metodo create(dto). Retorna um result
            var modeloOrError;
            modeloOrError = await Modelo3D.create(modelo3DDTO, new UniqueEntityID(modelo3DDTO.nome));


            //Se der erro, manda fail.
            if (modeloOrError.isFailure) {
                return Result.fail<IModelo3DDTO>(modeloOrError.errorValue());
            }

            //Se der certo, vamos buscar o Value (nó).
            const modeloResult = modeloOrError.getValue();

            //Chamamos o repositorio para guardar o no criado. Retorna o nó guardado.
            const savedModelo = await this.modelo3DRepo.save(modeloResult);

            //Transformar no guardado em DTO.
            const savedModeloDTOResult = Modelo3DMapper.toDTO(savedModelo) as IModelo3DDTO;

            //Retornar DTO do no guardado.
            return Result.ok<IModelo3DDTO>(savedModeloDTOResult);
        } catch (err) {
            throw err;
        }



    }


    //Devolve o nó correspondente ao id
    public async getModelo3DByNome(nome: string): Promise<Result<IModelo3DDTO>> {
        try {
            const modelo3D = await this.modelo3DRepo.findByNome(nome);
            if (modelo3D === null) return Result.fail<IModelo3DDTO>("⚠️ Não existe nenhum modelo com o nome introduzido! ⚠️");
            const modelo3DDTO = Modelo3DMapper.toDTO(modelo3D);
            return Result.ok<IModelo3DDTO>(modelo3DDTO);
        } catch (error) {
            throw error;
        }
    }

    public async getAllModelos3D() : Promise<Result<IModelo3DDTO[]>> {
        try{

            const modelos = await this.modelo3DRepo.findAll();
            let modelosDTO : IModelo3DDTO[] = [];
            let i = 0;
            for(i = 0; i < modelos.length; i++){
                modelosDTO.push(Modelo3DMapper.toDTO(modelos[i]));
            }

            return Result.ok<IModelo3DDTO[]>(modelosDTO);

        }catch (error) {
            throw error;
        }
    }
}