import { Inject } from "typedi";
import config from "../../config";
import { Result } from "../core/logic/Result";
import Percurso, { Orientacao } from "../domain/percurso";
import IPercursoDTO from "../dto/IPercursoDTO";
import PercursoMapper from "../mappers/percursoMapper";
import { IPercursoRepo } from "./IRepos/IPercursoRepo";
import IPercursoService from "./IServices/IPercursoService";
import { SegmentoId } from "../domain/segmentoId";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { INoRepo } from "./IRepos/INoRepo";
import { ILinhaRepo } from "./IRepos/ILinhaRepo";
import IDadosLinhaMapaDTO from "../dto/IDadosLinhaMapaDTO";
import Linha from "../domain/linha";
import Segmento from "../domain/segmento";
import { ISegmentoRepo } from "./IRepos/ISegmentoRepo";
import No from "../domain/no";
import { ILinhaPercursosDTO } from "../dto/ILinhaPercursosDTO";
import INoDTO from "../dto/INoDTO";
import NoMapper from "../mappers/noMapper";
import ISegmentoDTO from "../dto/ISegmentoDTO";
import SegmentoMapper from "../mappers/segmentoMapper";
import NumberUtils from "../utils/NumberUtils";

export default class PercursoService implements IPercursoService {

    constructor(
        @Inject(config.repos.percurso.name) private percursoRepo?: IPercursoRepo,
        @Inject(config.repos.no.name) private noRepo?: INoRepo, //Injetar repositorio do config.
        @Inject(config.repos.linha.name) private linhaRepo?: ILinhaRepo,
        @Inject(config.repos.segmento.name) private segmentoRepo?: ISegmentoRepo
    ) { }

    private segmentosConsulta: { noInicial: string, noFinal: string, coeficiente: number }[] = [];


    public async criarPercurso(percursoDTO: IPercursoDTO): Promise<Result<IPercursoDTO>> { //Promise pois é async

        //Validar se a linha existe
        const existeLinha = await this.linhaRepo.exists(percursoDTO.linhaId);

        if (!existeLinha) {
            return Result.fail<IPercursoDTO>("A linha introduzida não existe");
        }

        //Validar se os nós existem
        const existeNoInicial = await this.noRepo.exists(percursoDTO.noInicialId);
        const existeNoFinal = await this.noRepo.exists(percursoDTO.noFinalId);

        if (!existeNoFinal || !existeNoInicial) {
            return Result.fail<IPercursoDTO>("Um dos nós introduzidos não existe");
        }

        //Validar se o nó inicial e final do percurso corresponde ao do nó inicial e final da linha respetivamente 
        const linha = await this.linhaRepo.findByDomainId(percursoDTO.linhaId);
        const noInicialLinha = linha.noInicial.id;
        const noFinalLinha = linha.noFinal.id;

        //const nosCorrespondem = await this.percursoRepo.validarNosPercurso(noInicialLinha, noFinalLinha, percursoDTO.noInicialId, percursoDTO.noFinalId, percursoDTO.orientacao)
        const nosCorrespondem = true;
        if (!nosCorrespondem) {
            return Result.fail<IPercursoDTO>("Um dos nós inicial ou final não correspondem à linha");
        }

        //Valida a lista de segmentos (se estao pela ordem certa)
        let listaSegmentos: SegmentoId[] = [];
        let distanciaTotal: number = 0;
        let tempoTotal: number = 0;
        let index = 0;

        for (index = 0; index < percursoDTO.listaSegmentos.length; ++index) {

            const segmento = await this.percursoRepo.validarPercurso(percursoDTO.listaSegmentos[index], index, percursoDTO.listaSegmentos);

            if (segmento === null) {
                return Result.fail<IPercursoDTO>("Segmento número " + index + "não existe ou não está de acordo com o antererior");
            }

            distanciaTotal += segmento.distanciaNos;
            tempoTotal += segmento.tempoNos;
            listaSegmentos.push(segmento.segmentoId);

        }

        try {

            var percursoOrError;

            //Criar instância do percurso usando o método create(dto).
            if (percursoDTO.id === undefined) {
                percursoOrError = await Percurso.create(percursoDTO, listaSegmentos, distanciaTotal, tempoTotal);
            } else {
                percursoOrError = await Percurso.create(percursoDTO, listaSegmentos, distanciaTotal, tempoTotal, new UniqueEntityID(percursoDTO.id));
            }



            //Se der erro, manda fail.
            if (percursoOrError.isFailure) {
                return Result.fail<IPercursoDTO>(percursoOrError.errorValue());
            }


            //Se der certo, vamos buscar o Value(percurso)
            const percursoResult = percursoOrError.getValue();

            //Chamamos o repositorio para guardar o percurso criado. Retorna o percurso guardado
            const savedPercurso = await this.percursoRepo.save(percursoResult);

            //Transformar percurso guardado em DTO
            const savedPercursoDTOResult = PercursoMapper.toDTO(savedPercurso);

            //Retornar DTO do percurso guardado.
            return Result.ok<IPercursoDTO>(savedPercursoDTOResult);
        } catch (err) {
            throw err;
        }
    }

    //------------------------------------DEVOLVE COORDENADAS DE PERCURSOS DE LINHA-----------------
    public async dadosDePercursosParaMapa(): Promise<Result<IDadosLinhaMapaDTO[]>> {


        try {

            await this.inicializarSegmentosConsulta();

            //Definir resultado final vazio
            var result: IDadosLinhaMapaDTO[] = [];

            //Vamos buscar as linhas existentes
            const linhas: Linha[] = await this.linhaRepo.findAll();
            let indexLinhas = 0;
            let indexPercursos = 0;

            //Percorremos as linhas e vamos buscar os percursos de cada uma
            for (indexLinhas = 0; indexLinhas < linhas.length; indexLinhas++) {

                console.log("--------------------------------LINHA" + linhas[indexLinhas].nome + "-------------------------------")

                //Iniciamos instancia de dados para a linha em questão
                let matrix: [number, number][][] = [];
                let nomesPercursos : string[]= [];
                let dadosLinhaMapa: IDadosLinhaMapaDTO = { cor: linhas[indexLinhas].cor, matrizCoordenadas: matrix, nomeLinha:linhas[indexLinhas].nome, nomesPercursos:nomesPercursos};

                //Vamos buscar os percursos de uma linha
                const percursosDeLinha: Percurso[] = await this.percursoRepo.findByIdLinha(linhas[indexLinhas].linhaId.id);

                //Para cada percurso da linha
                for (indexPercursos = 0; indexPercursos < percursosDeLinha.length; indexPercursos++) {

                    //Vamos buscar o array de coordenadas e damos push para a matriz

                    if (percursosDeLinha.length >= 2) {
                        let coordenadasPercurso = await this.coordenadasDePercurso(percursosDeLinha[0]);

                        dadosLinhaMapa.matrizCoordenadas.push(coordenadasPercurso);
                        dadosLinhaMapa.nomesPercursos.push(percursosDeLinha[0].nomePercurso);

                        coordenadasPercurso = await this.coordenadasDePercurso(percursosDeLinha[1]);
 
                        dadosLinhaMapa.matrizCoordenadas.push(coordenadasPercurso); 

                        break;
                    }

                }

                result.push(dadosLinhaMapa);

            }

            return Result.ok<IDadosLinhaMapaDTO[]>(result);

        } catch (err) {
            return Result.fail<IDadosLinhaMapaDTO[]>(err);

        }


    }

    //Para aliviar a responsabilidade da função acima
    private async coordenadasDePercurso(percurso: Percurso): Promise<[number, number][]> {

        var coordenadasDePercurso: [number, number][] = [];
        let indexSegmentos = 0;
        for (indexSegmentos = 0; indexSegmentos < percurso.listaSegmentos.length; indexSegmentos++) {
            const segmento: Segmento = await this.segmentoRepo.findByDomainId(percurso.listaSegmentos[indexSegmentos].id);
            let coordenadasCorretas = await this.calculoDasCoordenadas(segmento);
            coordenadasDePercurso.push([coordenadasCorretas[1], coordenadasCorretas[0]]);

            if (indexSegmentos === (percurso.listaSegmentos.length - 1)) {


                coordenadasDePercurso.push([coordenadasCorretas[3], coordenadasCorretas[2]]);

            }

        }

        return coordenadasDePercurso;
    }

    private async inicializarSegmentosConsulta(): Promise<void> {
        this.segmentosConsulta = []; //Voltar a esvaziar o array just in case
        const segmentos = await this.segmentoRepo.findAll();
        let i = 0;
        for (i = 0; i < segmentos.length; i++) {
            //Verifica se o array SegmentosConsulta já tem um segmento com o par de nos em avaliação (segmentos[i]). Se NÃÃÃO tiver, acrescenta um!
            if (await this.segmentosConsulta.find(s => (s.noInicial === segmentos[i].idNoInicial.id && s.noFinal === segmentos[i].idNoFinal.id) || (s.noInicial === segmentos[i].idNoFinal.id && s.noFinal === segmentos[i].idNoInicial.id)) == undefined) {
                let objeto = { noInicial: segmentos[i].idNoInicial.id, noFinal: segmentos[i].idNoFinal.id, coeficiente: 0 };
                this.segmentosConsulta.push(objeto);
            }
        }

    }

    //Calcula as coordenadas dos nós conforme os coeficientes de SegmentosConsulta.
    private async calculoDasCoordenadas(segmento: Segmento): Promise<[number, number, number, number]> {
        let noInicial: No = await this.noRepo.findByDomainId(segmento.idNoInicial.id);
        let noFinal: No = await this.noRepo.findByDomainId(segmento.idNoFinal.id);

        //Coeficiente
        let indexSegmento = await this.segmentosConsulta.findIndex(s => (s.noInicial === segmento.idNoInicial.id && s.noFinal === segmento.idNoFinal.id) || (s.noInicial === segmento.idNoFinal.id && s.noFinal === segmento.idNoInicial.id));

        let coeficiente = this.segmentosConsulta[indexSegmento].coeficiente

        if (this.segmentosConsulta[indexSegmento].noInicial === segmento.idNoFinal.id && this.segmentosConsulta[indexSegmento].noFinal === segmento.idNoInicial.id) {
            console.log("Troca efetuada!")
            let temp = noFinal;
            noFinal = noInicial;
            noInicial = temp;
        }

        //Coordenadas dos nos
        let xi = noInicial.latitude;
        let yi = noInicial.longitude;
        let xj = noFinal.latitude;
        let yj = noFinal.longitude;
        //Distancia euclidiana
        const distancia = Math.sqrt(Math.pow(xj - xi, 2) + Math.pow(yj - yi, 2));

        //Angulos
        let cosBeta = (-(yj - yi)) / distancia;
        let sinBeta = (xj - xi) / distancia;

        //Novas coordenadas
        let xa = xi + (coeficiente * 0.0001 * cosBeta);
        let ya = yi + (coeficiente * 0.0001 * sinBeta);
        let xb = xj + (coeficiente * 0.0001 * cosBeta);
        let yb = yj + (coeficiente * 0.0001 * sinBeta);
        console.log("Nó inicial: " + noInicial.nomeCompleto + "     Coordenadas calc: " + [xa,ya] + "\nNó Final: " + noFinal.nomeCompleto + "     Coordenadas calc: " + [xb,yb] + "\n Coeficiente = " + coeficiente  + "\ncosBeta: " + cosBeta + "\nsinBeta: " + sinBeta + "\n\n\n");
        //Atualizar segmentosConsulta
        if (coeficiente <= 0) {
            this.segmentosConsulta[indexSegmento].coeficiente = 1 - coeficiente;
        } else {
            this.segmentosConsulta[indexSegmento].coeficiente = -coeficiente;
        }


        //Return
        if (this.segmentosConsulta[indexSegmento].noInicial === segmento.idNoFinal.id && this.segmentosConsulta[indexSegmento].noFinal === segmento.idNoInicial.id) {
            return [xb, yb, xa, ya];
        }
        return [xa, ya, xb, yb];

    }



    //------------------------------------------------------------------------------------------------------------

    /**
     * @returns Devolve um array com a informação de todos os percursos para o módulo do planeamento.
     */
    public async dadosPercursoParaPlaneamento(): Promise<Result<ILinhaPercursosDTO[]>> {
        try {

            var result: ILinhaPercursosDTO[] = [];

            //Vamos buscar os percursos todos
            const percursos: Percurso[] = await this.percursoRepo.findAll();
            let index = 0;

            for (index = 0; index < percursos.length; index++) {
                //Vamos buscar toda a informação
                let nomeLinha = await (await this.linhaRepo.findByDomainId(percursos[index].linhaId.id)).nome;
                let idPercurso = percursos[index].id.toString();
                let abreviaturasNos = await this.getNosPercurso(percursos[index]);
                let tempo = percursos[index].tempoTotal;
                let distancia = percursos[index].distanciaTotal;

                let dadosLinhaPercurso: ILinhaPercursosDTO = { nomeLinha, idPercurso, abreviaturasNos, tempo, distancia };

                result.push(dadosLinhaPercurso);
            }

            return Result.ok<ILinhaPercursosDTO[]>(result);

        } catch (err) {
            return Result.fail<ILinhaPercursosDTO[]>(err);
        }
    }

    /**
     * 
     * @param percursoId recebe um id de percurso e devolve todos os nós do mesmo
     */
    public async getNosPercursoId(percursoId: string): Promise<Result<INoDTO[]>> {
        try {
            const percurso = await this.percursoRepo.findByDomainId(percursoId);

            if (percurso === null) return Result.fail<INoDTO[]>("O percurso com o id introduzido não existe.");

            var result: INoDTO[] = [];

            let indexSegmentos = 0;
            for (indexSegmentos = 0; indexSegmentos < percurso.listaSegmentos.length; indexSegmentos++) {
                const segmento: Segmento = await this.segmentoRepo.findByDomainId(percurso.listaSegmentos[indexSegmentos].id);
                const noInicial: No = await this.noRepo.findByDomainId(segmento.idNoInicial.id);

                result.push(NoMapper.toDTO(noInicial));

                if (indexSegmentos === (percurso.listaSegmentos.length - 1)) {
                    const noFinal: No = await this.noRepo.findByDomainId(segmento.idNoFinal.id);

                    result.push(NoMapper.toDTO(noFinal));
                }
            }

            return Result.ok<INoDTO[]>(result);
        } catch (error) {
            return error;
        }
    }

    /**
    * 
    * @param percursoId recebe um id de percurso e devolve todos os segmentos do mesmo
    */
    public async getSegmentosPercurso(percursoId: string): Promise<Result<ISegmentoDTO[]>> {
        try {
            const percurso = await this.percursoRepo.findByDomainId(percursoId);

            if (percurso === null) return Result.fail<ISegmentoDTO[]>("O percurso com o id introduzido não existe.");

            var result: ISegmentoDTO[] = [];

            let indexSegmentos = 0;
            for (indexSegmentos = 0; indexSegmentos < percurso.listaSegmentos.length; indexSegmentos++) {
                const segmento: Segmento = await this.segmentoRepo.findByDomainId(percurso.listaSegmentos[indexSegmentos].id);
                result.push(SegmentoMapper.toDTO(segmento));
            }

            return Result.ok<ISegmentoDTO[]>(result);
        } catch (error) {
            return error;
        }
    }

    /**
     * @param percurso recebe um percurso e devolve uma lista com todas as abreviaturas de todos os nós
     */
    private async getNosPercurso(percurso: Percurso): Promise<string[]> {
        var abreviaturasNos: string[] = [];
        let indexSegmentos = 0;
        for (indexSegmentos = 0; indexSegmentos < percurso.listaSegmentos.length; indexSegmentos++) {
            const segmento: Segmento = await this.segmentoRepo.findByDomainId(percurso.listaSegmentos[indexSegmentos].id);
            const noInicial: No = await this.noRepo.findByDomainId(segmento.idNoInicial.id);

            abreviaturasNos.push(noInicial.abreviatura);

            if (indexSegmentos === (percurso.listaSegmentos.length - 1)) {
                const noFinal: No = await this.noRepo.findByDomainId(segmento.idNoFinal.id);

                abreviaturasNos.push(noFinal.abreviatura);
            }
        }
        return abreviaturasNos;
    }

    //--------------------------LISTA PERCURSOS DE UMA LINHA--------------------------------------------
    public async listaPercursos(idLinha: string): Promise<Result<IPercursoDTO[]>> {
        //Validar se a linha existe
        const existeLinha = await this.linhaRepo.exists(idLinha);

        if (!existeLinha) {
            return Result.fail<IPercursoDTO[]>("A linha introduzida não existe");
        }

        try {
            var result: IPercursoDTO[] = [];

            var todosOsPercursos = await this.percursoRepo.findByIdLinha(idLinha);

            var index = 0;


            for (index = 0; index < todosOsPercursos.length; index++) {
                var percursoDTO = PercursoMapper.toDTO(todosOsPercursos[index]);
                result.push(percursoDTO);
            }

            return Result.ok<IPercursoDTO[]>(result);
        } catch (err) {
            return err;
        }
    }

}
