import { Inject } from "typedi";
import config from "../../config";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { Result } from "../core/logic/Result";
import { User } from "../domain/user";
import ILoginDTO from "../dto/ILoginDTO";
import { IUserDTO } from "../dto/IUserDTO";
import { UserMapper } from "../mappers/userMapper";
import { IUserRepo } from "./IRepos/IUserRepo";
import IUserService from "./IServices/IUserService";
const bcrypt = require('bcryptjs');
import jwt from 'jsonwebtoken';
import { rest, result } from "lodash";
import { UserEmail } from "../domain/userEmail";

export default class UserService implements IUserService {

    constructor(@Inject(config.repos.user.name) private userRepo?: IUserRepo) { }



    public async registarUser(userDTO: IUserDTO): Promise<Result<IUserDTO>> {
        try {

            var userOrError;

            //Encriptar a password
            const salt = await bcrypt.genSalt(10);
            const hashedPassword = await bcrypt.hash(userDTO.password, salt);
            userDTO.password = hashedPassword;

            //Criar objeto
            userOrError = await User.create(userDTO, new UniqueEntityID());

            //Validar
            if (userOrError.isFailure) {
                return Result.fail<IUserDTO>(userOrError.errorValue());
            }

            //Buscar value
            const userResult = userOrError.getValue();

            const savedUser = await this.userRepo.save(userResult);

            const savedUserDTO = UserMapper.toDTO(savedUser) as IUserDTO;

            return Result.ok<IUserDTO>(savedUserDTO);

        } catch (err) {
            throw err;
        }


    }

    public async login(loginDTO: ILoginDTO): Promise<Result<string>> {

        //Validar se o email existe
        const user: User = await this.userRepo.findByEmail(loginDTO.email);

        if (user === null) {
            return Result.fail<string>('E-mail não encontrado');
        }

        //Validar a password
        const isValid = await this.comparePasswords(loginDTO.password, user.password);
        if (!isValid) {
            return Result.fail<string>("Password inválida"); //Falhou, ggwp
        }

        //Criar o token
        var token;

        switch (user.role) {
            case 'Administrador':
                token = jwt.sign({ email: user.email.value, isAdministrador: true, isCliente: false, isGestor: false }, config.jwtSecret);
                break;

            case 'Cliente':
                token = jwt.sign({ email: user.email.value, isAdministrador: false, isCliente: true, isGestor: false }, config.jwtSecret);
                break;

            default:
                token = jwt.sign({ email: user.email.value, isAdministrador: false, isCliente: false, isGestor: true }, config.jwtSecret);
                break;

        }

        return Result.ok<string>(token);

    }

    //Devolve os dados do utilizador
    public async getDadosUser(email: UserEmail | string): Promise<Result<IUserDTO>> {
        try {
            const user = await this.userRepo.findByEmail(email);
            if (user === null) return Result.fail<IUserDTO>("Erro: user não existe!");

            const userDTO = UserMapper.toDTO(user);
            return Result.ok<IUserDTO>(userDTO);
        } catch (error) {
            throw error;
        }
    }

    //Altera os dados do user
    public async updateDadosUser(email: UserEmail | string, userUpdateDTO: IUserDTO): Promise<Result<IUserDTO>> {
        try {
            
            const userUpdate = await UserMapper.toDomain(userUpdateDTO);

            const user = await this.userRepo.update(email, userUpdate);
            if (user === null) return Result.fail<IUserDTO>("Erro: user não existe!");

            const userDTO = UserMapper.toDTO(user);
            return Result.ok<IUserDTO>(userDTO);
        } catch (error) {
            throw error;
        }
    }

    //Elimina dados da base de dados
    public async deleteDadosUser(email: UserEmail | string): Promise<Result<IUserDTO>> {
        try {
            const user = await this.userRepo.delete(email);
            if (user === null) return Result.fail<IUserDTO>("Não foi possível eliminar o user.");

            const userDTO = UserMapper.toDTO(user);

            return Result.ok<IUserDTO>(userDTO);
        } catch (error) {
            throw error;
        }
    }

    //bcrypt.compare é async. Para esperar pela validação, temos de a envolver numa promise.
    private async comparePasswords(loginPass: string, userPass: string): Promise<string> {

        return new Promise(function (resolve, reject) {
            bcrypt.compare(loginPass, userPass, function (err, res) {
                if (err) {
                    reject(err);
                } else {
                    resolve(res);
                }
            });
        });
    }

}