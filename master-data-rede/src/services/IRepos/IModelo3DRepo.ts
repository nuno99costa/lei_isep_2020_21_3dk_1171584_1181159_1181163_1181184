import { Repo } from "../../core/infra/Repo";
import Modelo3D from "../../domain/Modelo3D";
import { Modelo3DId } from "../../domain/Modelo3DId";


export interface IModelo3DRepo extends Repo<Modelo3D> {
    save(modelo:Modelo3D):Promise<Modelo3D>;
    exists(modeloNome: Modelo3DId | string):Promise<boolean>;
    findByNome(nomeModelo: string): Promise<Modelo3D>;
    findAll() : Promise<Modelo3D[]>;
}