import { Repo } from "../../core/infra/Repo";
import Linha from "../../domain/linha";
import { LinhaId } from "../../domain/linhaId";

export interface ILinhaRepo extends Repo<Linha> {
    save(linha:Linha):Promise<Linha>;
    exists(linha: LinhaId | string):Promise<boolean>;
    findByDomainId(linhaId: string): Promise<Linha>;
    findAll(): Promise<Linha[]>;
    findLinhaComNomeComecadaPor(iniciais : String) : Promise<Linha[]>;
    findLinhaComCodigoComecadaPor(iniciais : String) : Promise<Linha[]>;
}