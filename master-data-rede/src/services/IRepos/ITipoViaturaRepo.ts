import { Repo } from "../../core/infra/Repo";
import TipoViatura from "../../domain/tipoViatura";
import { TipoViaturaId } from "../../domain/tipoViaturaId";

export interface ITipoViaturaRepo extends Repo<TipoViatura> {
    save(tipoViatura: TipoViatura): Promise<TipoViatura>;
    exists(tipoViatura: TipoViaturaId | string):Promise<boolean>;
    findByDomainId(tipoViaturaId: string): Promise<TipoViatura>;
    findAll(): Promise<TipoViatura[]>;
}