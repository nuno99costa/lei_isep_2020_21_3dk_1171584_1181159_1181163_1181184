import { Repo } from "../../core/infra/Repo";
import TipoTripulante from "../../domain/tipoTripulante";
import { TipoTripulanteId } from "../../domain/tipoTripulanteId";

export interface ITipoTripulanteRepo extends Repo<TipoTripulante> {
    save(tipoTripulante: TipoTripulante): Promise<TipoTripulante>;
    exists(tipoTripulante: TipoTripulanteId | string): Promise<boolean>;
    findByDomainId(tipoTripulanteId: string): Promise<TipoTripulante>;
    findAll(): Promise<TipoTripulante[]>;
}