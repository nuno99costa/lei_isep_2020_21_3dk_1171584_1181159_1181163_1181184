import { Repo } from "../../core/infra/Repo";

import { User } from "../../domain/user";
import { UserEmail } from "../../domain/userEmail";
import { IUserDTO } from "../../dto/IUserDTO";

export interface IUserRepo extends Repo<User> {
	findByEmail(email: UserEmail | string): Promise<User>;
	save(user: User): Promise<User>;
	delete(email: UserEmail | string): Promise<User>;
	update(email: UserEmail | string, userUpdate: User): Promise<User>;
}
