import { Repo } from "../../core/infra/Repo";
import Percurso, { Orientacao } from "../../domain/percurso";
import { PercursoId } from "../../domain/percursoId";
import Segmento from "../../domain/segmento";

export interface IPercursoRepo extends Repo<Percurso> {
    save(percurso: Percurso): Promise<Percurso>;
    exists(percurso: PercursoId | string): Promise<boolean>;
    findAll(): Promise<Percurso[]>;
    findByDomainId(percursoId: string): Promise<Percurso>;
    validarPercurso(segmentoId: String, index: number, array: String[]): Promise<Segmento>;
    findByNome(nome: string): Promise<Percurso>;
    validarNosPercurso(noInicialLinha: string, noFinalLinha: string, noInicialPercurso: string, noFinalPercurso: string, orientacao: Orientacao): Promise<boolean>;
    findByIdLinha(linhaId: string): Promise<Percurso[]>
}