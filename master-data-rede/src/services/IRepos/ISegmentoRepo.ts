import { Repo } from "../../core/infra/Repo";
import Segmento from "../../domain/segmento";
import { SegmentoId } from "../../domain/segmentoId";


export interface ISegmentoRepo extends Repo<Segmento> {
    save(segmento: Segmento): Promise<Segmento>;
    findByDomainId(segmentoId: String): Promise<Segmento>;
    exists(segmentoId: SegmentoId | String): Promise<boolean>;
    findByNome(nome: string): Promise<Segmento>;
    findAll(): Promise<Segmento[]>;
}