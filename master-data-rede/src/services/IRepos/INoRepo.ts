import { Repo } from "../../core/infra/Repo";
import { Result } from "../../core/logic/Result";
import No from "../../domain/no";
import { NoId } from "../../domain/noId";


export interface INoRepo extends Repo<No> {
    save(no:No):Promise<No>;
    exists(no: NoId | string):Promise<boolean>;
    findByDomainId(noId: string): Promise<No>;
    findByAbreviatura(abreviatura: string): Promise<No>;
    findAll(): Promise<No[]>;
    findNoComNomeComecadoPor(iniciais : String) : Promise<No[]>;
    findNoComCodigoComecadoPor(iniciais : String) : Promise<No[]>;
    updateNoModelo3D(no: string, modeloNome : String) : Promise<No>;
}