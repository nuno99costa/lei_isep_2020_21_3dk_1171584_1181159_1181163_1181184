import { NextFunction, Request, Response } from "express";
import jwt from 'jsonwebtoken';
import config from "../../../config";

module.exports = function (req: Request, res: Response, next: NextFunction) {
  const token = req.header('auth-token');
  if (token === "secret-shhh") {
    next();
  } else {
    if (!token) return res.status(401).send('Access Denied');


    try {
      const verified = jwt.verify(token, config.jwtSecret);
      next();
    } catch (err) {
      res.status(400).send('Invalid token');
    }
  }

}