import { Router } from 'express';
//import auth from './routes/authRoute';
import user from './routes/userRoute';
import role from './routes/roleRoute';
import no from './routes/noRoute';
import tipoViatura from './routes/tipoViaturaRoute';
import tipoTripulante from './routes/tipoTripulanteRoute';
import percurso from './routes/percursoRoute';
import segmento from './routes/segmentoRoute';
import linha from './routes/linhaRoute';
import inserirGLX from './routes/inserirGLXRoute';
import modelo3D from './routes/modelo3DRoute';
import Modelo3DController from '../controllers/modelo3DController';

export default () => {
	const app = Router();

	//auth(app);
	user(app);
	role(app);
	no(app);
	tipoViatura(app);
	tipoTripulante(app);
	percurso(app);
	segmento(app);
	linha(app);
	user(app);
	inserirGLX(app);
	modelo3D(app);

	return app;
}
