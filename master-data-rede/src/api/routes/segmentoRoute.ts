import { Router } from 'express';
import { Container } from 'typedi';
import config from '../../../config';
import ISegmentoController from '../../controllers/IControllers/ISegmentoController';
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/segmento', route); //Endereço base

    const ctrl = Container.get(config.controller.segmento.name) as ISegmentoController//Ir ao container(config) buscar o controller (DI)

    route.post('',isAuth, (req, res, next) => ctrl.criarSegmento(req, res, next));//Todos os pedidos post no endereço /segmento são para criar os segmentos

    route.get('', isAuth,(req, res, next) => ctrl.getSegmentos(req, res, next));//Devolve todos os segmentos na bd
}