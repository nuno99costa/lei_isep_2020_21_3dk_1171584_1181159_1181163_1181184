import { resolve } from 'dns';
import { Router } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import INoController from '../../controllers/IControllers/INoController';
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/no', route); //Endereço base 

    const ctrl = Container.get(config.controller.no.name) as INoController; //Ir ao container(config) buscar o controller (DI)

    route.post('', isAuth,(req, res, next) => ctrl.criarNo(req, res, next)); //Todos os pedidos post no endereço /no são para criar os nós

    route.get('/all/ordenados/:tipo', isAuth,(req, res, next) => ctrl.nosOrdenadosPor(req, res, next));

    route.get('/all/filtrados/:tipo/:iniciais', isAuth,(req, res, next) => ctrl.nosComecadosPor(req, res, next));

    route.get('/:id', isAuth,(req, res, next) => ctrl.getNoById(req, res, next)); //Devolve o nó com o id

    route.put('/modelo3d/:id', isAuth, (req, res, next) => ctrl.updateModelo3D(req,res,next));

}