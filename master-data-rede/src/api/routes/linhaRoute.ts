import { Router } from "express";
import Container from "typedi";
import config from "../../../config";
import ILinhaController from "../../controllers/IControllers/ILinhaController";
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/linha', route);

    const ctrl = Container.get(config.controller.linha.name) as ILinhaController; //Ir ao container(config) buscar o controller (DI)

    route.post('', isAuth, (req, res, next) => ctrl.criarLinha(req, res, next));

    route.get('/all/ordenados/:tipo', isAuth,(req, res, next) => ctrl.linhasOrdenadasPor(req, res, next)); //get com ordenação pelo código ou pelo nome

    route.get('/all/filtrados/:tipo/:iniciais', isAuth,(req, res, next) => ctrl.linhasComecadasPor(req, res, next));

    route.get('/:id', isAuth,(req, res, next) => ctrl.getLinhaById(req, res, next)); //Devolve uma linha com o id 

}