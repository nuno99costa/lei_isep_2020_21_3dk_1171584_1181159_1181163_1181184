import { Router } from "express";
import { Container } from "typedi";
import config from "../../../config";
import ITipoTripulanteController from "../../controllers/IControllers/ITipoTripulanteController";
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/tipoTripulante', route);

    const ctrl = Container.get(config.controller.tipoTripulante.name) as ITipoTripulanteController;

    route.post('', isAuth, (req, res, next) => ctrl.criarTipoTripulante(req, res, next));

    route.get('', isAuth, (req, res, next) => ctrl.getTipoTripulante(req, res, next));
}