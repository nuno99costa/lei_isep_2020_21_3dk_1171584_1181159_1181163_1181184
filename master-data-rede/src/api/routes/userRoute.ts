import { Router, Request, Response } from 'express';
import Container from 'typedi';
import config from '../../../config';
import IUserController from '../../controllers/IControllers/IUserController';
var controller = require('../../controllers/userController');
const isAuth = require('../middlewares/isAuth');

const route = Router();

export default (app: Router) => {
  app.use('/user', route);

  const ctrl = Container.get(config.controller.user.name) as IUserController; //Ir ao container(config) buscar o controller (DI)

  route.post('/register', (req, res, next) => ctrl.registarUser(req, res, next));

  route.post('/login', (req, res, next) => ctrl.login(req, res, next));

  route.get('', (req, res, next) => ctrl.getDadosUser(req, res, next));

  route.put('', isAuth, (req, res, next) => ctrl.updateDadosUser(req, res, next));

  route.delete('', isAuth, (req, res, next) => ctrl.deleteDadosUser(req, res, next));

}
