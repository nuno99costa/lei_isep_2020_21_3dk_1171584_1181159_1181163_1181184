
import express, { Router, Request, Response } from 'express';
import Container from 'typedi';
import config from '../../../config';
import IModelo3DController from '../../controllers/IControllers/IModelo3DController';
var controller = require('../../controllers/userController');


const route = Router();

export default (app: Router) => {
  app.use('/modelo3D', route);

  const ctrl = Container.get(config.controller.modelo3D.name) as IModelo3DController; //Ir ao container(config) buscar o controller (DI)

  route.post('/', (req, res, next) => ctrl.criarModelo3D(req, res ,next));

  route.get('/', (req, res, next) => ctrl.getAllModelos3D(req, res ,next));
  
}

        