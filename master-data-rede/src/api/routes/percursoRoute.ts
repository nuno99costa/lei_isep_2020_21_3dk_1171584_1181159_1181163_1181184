import { Router } from 'express';

import { Container } from 'typedi';
import config from '../../../config';
import IPercursoController from '../../controllers/IControllers/IPercursoController';
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/percurso', route);//Endereço base

    const ctrl = Container.get(config.controller.percurso.name) as IPercursoController //Ir ao container(config) buscar o controller (DI)

    route.post('', isAuth,(req, res, next) => ctrl.criarPercurso(req, res, next)); //Todos os pedidos post no endereço /percurso são para criar os percursos

    route.get('/linha/:id', isAuth,(req, res, next) => ctrl.listarPercursos(req, res, next));

    route.get('/dadosMapa', isAuth,(req, res, next) => ctrl.dadosParaMapa(req, res, next));

    route.get('/dadosPercurso', isAuth,(req, res, next) => ctrl.dadosPercurso(req, res, next));

    route.get('/nos/:percursoId', isAuth,(req, res, next) => ctrl.getNosPercursoId(req, res, next));

    route.get('/segmentos/:percursoId', isAuth,(req, res, next) => ctrl.getSegmentosPercurso(req, res, next));
}