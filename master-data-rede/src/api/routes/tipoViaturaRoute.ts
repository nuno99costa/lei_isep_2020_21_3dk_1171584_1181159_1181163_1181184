import { Router } from "express";
import { Container } from "typedi";
import config from "../../../config";
import ITipoViaturaController from "../../controllers/IControllers/ITipoViaturaController";
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
    app.use('/tipoViatura', route);

    const ctrl = Container.get(config.controller.tipoViatura.name) as ITipoViaturaController;

    route.post('', isAuth,(req, res, next) => ctrl.criarTipoViatura(req, res, next));

    route.get('', isAuth,(req, res, next) => ctrl.getTipoViatura(req, res, next)); //devolve todos os tipos de viatura
}