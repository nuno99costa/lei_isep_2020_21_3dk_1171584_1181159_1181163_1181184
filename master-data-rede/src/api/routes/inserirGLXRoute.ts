import { Router } from "express";
import { Container } from "typedi";
import config from "../../../config";
import IInserirGLXController from "../../controllers/IControllers/IInserirGLXController";
const isAuth = require('../middlewares/isAuth') ;

const route = Router();

export default (app: Router) => {
	var xmlparser = require("express-xml-bodyparser");

    app.use("/inserirGLX", route); //Endereço base

    const ctrl = Container.get(config.controller.inserirGLX.name) as IInserirGLXController;//Ir ao container(config) buscar o controller (DI)

	//route.post("", (req, res, next) => ctrl.ingerirGLX);//Todos os pedidos post no endereço /segmento são para inserir o ficheiro glx
	
	route.post('', isAuth, xmlparser({trim: false, explicitArray: false}), function(req, res, next) {
 		ctrl.ingerirGLX(req,res,next);
	});

    //assumo aqui que o documento glx é enviado no body do pedido http com MIME-TYPE de application/xml
}