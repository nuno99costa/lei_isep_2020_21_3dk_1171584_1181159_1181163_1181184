
import { Joi } from "celebrate";
import { join, noConflict } from "lodash";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ISegmentoPersistence } from "../dataschema/ISegmentoPersistence";
import { NoId } from "../domain/noId";
import Segmento from "../domain/segmento";
import ISegmentoDTO from "../dto/ISegmentoDTO";

export default class SegmentoMapper {

    constructor() {}

    //Domain -> DTO.
    public static toDTO(segmento: Segmento): ISegmentoDTO {
        return {
            id: segmento.id.toString(),

            nomeSegmento: segmento.nomeSegmento,
            
            idNoInicial: segmento.idNoInicial.id,

            idNoFinal: segmento.idNoFinal.id,

            distanciaNos: segmento.distanciaNos,

            tempoNos: segmento.tempoNos
        }
    }

    public static toDomain(segmento: any | Model<ISegmentoPersistence & Document>) : Segmento {
        const segmentoOrError = Segmento.create(segmento, new UniqueEntityID(segmento.domainId));

        if(segmentoOrError.isFailure){
            console.log(segmentoOrError.error)
        }

        return segmentoOrError.isSuccess ? segmentoOrError.getValue() : null;
    }

    public static toPersistence(segmento: Segmento): any {
        return{
            domainId: segmento.id.toString(),
            nomeSegmento: segmento.nomeSegmento,
            idNoInicial: segmento.idNoInicial.id.toString(),
            idNoFinal: segmento.idNoFinal.id.toString(),
            distanciaNos: segmento.distanciaNos,
            tempoNos: segmento.tempoNos
        }
    }

    public static validarSegmento(data) {
        const schema = Joi.object({
            id: Joi.string(),
            nomeSegmento: Joi.string().required(),
            idNoInicial: Joi.string().required(),
            idNoFinal: Joi.string().required(),
            distanciaNos: Joi.number().required(),
            tempoNos: Joi.number().required()
        });

        return schema.validate(data);
    }
}