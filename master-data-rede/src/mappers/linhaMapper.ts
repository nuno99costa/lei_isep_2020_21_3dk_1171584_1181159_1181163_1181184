import { Joi } from "celebrate";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ILinhaPersistence } from "../dataschema/ILinhaPersistence";
import Linha from "../domain/linha";
import { NoId } from "../domain/noId";
import { PercursoId } from "../domain/percursoId";
import { TipoTripulanteId } from "../domain/tipoTripulanteId";
import { TipoViaturaId } from "../domain/tipoViaturaId";
import ILinhaDTO from "../dto/ILinhaDTO";

export default class LinhaMapper {
    constructor() { }

    //Transforma em DTO
    public static toDTO(linha: Linha): ILinhaDTO {
        return {
            id: linha.id.toString(),
            codigo: linha.codigo,
            nome: linha.nome,
            cor: linha.cor,
            noInicial: linha.noInicial.id,
            noFinal: linha.noFinal.id,
            viaturasPermitidas: linha.viaturasPermitidasEmString,
            viaturasProibidas: linha.viaturasProibidasEmString,
            tripulantesPermitidos: linha.tripulantesPermitidosEmString,
            tripulantesProibidos: linha.tripulantesProibidosEmString
        }
    }

    //Devolve a linha
    public static toDomain(linha: any | Model<ILinhaPersistence & Document>): Linha {
        const noInicial = this.nosEmId(linha.noInicial);
        const noFinal = this.nosEmId(linha.noFinal);
        const viaturasPermitidas = this.viaturasEmId(linha.viaturasPermitidas);
        const viaturasProibidas = this.viaturasEmId(linha.viaturasProibidas);
        const tripulantesPermitidos = this.tripulantesEmId(linha.tripulantesPermitidos);
        const tripulantesProibidos = this.tripulantesEmId(linha.tripulantesProibidos);

        const linhaOrError = Linha.create(linha, noInicial, noFinal, viaturasPermitidas, viaturasProibidas, tripulantesPermitidos, tripulantesProibidos, new UniqueEntityID(linha.domainId));

        if (linhaOrError.isFailure) {
            console.log(linhaOrError.error)
        }

        return linhaOrError.isSuccess ? linhaOrError.getValue() : null;
    }

    //Recebe um id de nó e transforma em NoId
    private static nosEmId(no: string): NoId {
        return NoId.create(new UniqueEntityID(no));
    }

    //Devolve uma lista de TripoTripulanteId através da transformação da string
    private static tripulantesEmId(listaTripulantes: string[]): TipoTripulanteId[] {
        var array: TipoTripulanteId[] = [];
        let index: number = 0;
        for (index = 0; index < listaTripulantes.length; index++) {
            array.push(TipoTripulanteId.create(new UniqueEntityID(listaTripulantes[index])));
        }

        return array;
    }

    //Devolve uma lista de TripoViaturaId através da transformação da string
    private static viaturasEmId(listaViaturas: string[]): TipoViaturaId[] {
        var array: TipoViaturaId[] = [];
        let index: number = 0;
        for (index = 0; index < listaViaturas.length; index++) {
            array.push(TipoViaturaId.create(new UniqueEntityID(listaViaturas[index])));
        }

        return array;
    }

    public static toPersistence(linha: Linha): any {
        return {
            domainId: linha.id,
            codigo: linha.codigo,
            nome: linha.nome,
            cor: linha.cor,
            noInicial: linha.noInicial.id,
            noFinal: linha.noFinal.id,
            viaturasPermitidas: linha.viaturasPermitidasEmString,
            viaturasProibidas: linha.viaturasProibidasEmString,
            tripulantesPermitidos: linha.tripulantesPermitidosEmString,
            tripulantesProibidos: linha.tripulantesProibidosEmString
        }
    }

    //Valida a linha recebida
    public static validarLinha(data) {

        const schemaLinha = Joi.object({
            id: Joi.string(),
            codigo: Joi.string().required().min(1),
            nome: Joi.string().required(),
            cor: Joi.string().required(),
            noInicial: Joi.string().required(),
            noFinal: Joi.string().required(),
            viaturasPermitidas: Joi.array().items(Joi.string()),
            viaturasProibidas: Joi.array().items(Joi.string()),
            tripulantesPermitidos: Joi.array().items(Joi.string()),
            tripulantesProibidos: Joi.array().items(Joi.string())
        });

        return schemaLinha.validate(data);
    }
}