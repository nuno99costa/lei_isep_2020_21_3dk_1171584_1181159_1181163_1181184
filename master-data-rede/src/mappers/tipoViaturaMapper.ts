import { Joi } from "celebrate";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ITipoViaturaPersistence } from "../dataschema/ITipoViaturaPersistence";
import TipoViatura, { TipoCombustivel } from "../domain/tipoViatura";
import ITipoViaturaDTO from "../dto/ITipoViaturaDTO";

export default class TipoViaturaMapper {
    constructor() { }

    //Transformação de Domain para DTO.
    public static toDTO(tipoViatura: TipoViatura): ITipoViaturaDTO {
        return {
            id: tipoViatura.id.toString(),
            codigo: tipoViatura.codigo,
            descricao: tipoViatura.descricao,
            combustivel: tipoViatura.combustivel,
            autonomia: tipoViatura.autonomia,
            velocidadeMedia: tipoViatura.velocidadeMedia,
            custoPorQuilometro: tipoViatura.custoPorQuilometro,
            consumoMedio: tipoViatura.consumoMedio
        }
    }

    public static toDomain(tipoViatura: any | Model<ITipoViaturaPersistence & Document>): TipoViatura {
        const tViaturaOrError = TipoViatura.create(tipoViatura, new UniqueEntityID(tipoViatura.domainId));

        if (tViaturaOrError.isFailure) {
            console.log(tViaturaOrError.error)
        }

        return tViaturaOrError.isSuccess ? tViaturaOrError.getValue() : null;
    }

    public static toPersistence(tipoViatura: TipoViatura): any {
        return {
            domainId: tipoViatura.id,
            codigo: tipoViatura.codigo,
            descricao: tipoViatura.descricao,
            combustivel: tipoViatura.combustivel,
            autonomia: tipoViatura.autonomia,
            velocidadeMedia: tipoViatura.velocidadeMedia,
            custoPorQuilometro: tipoViatura.custoPorQuilometro,
            consumoMedio: tipoViatura.consumoMedio
        }
    }

    public static validarTipoViatura(data){
        const schema = Joi.object({
            id: Joi.string().allow(null, ''),
            codigo: Joi.string().max(20),
            descricao: Joi.string().required().max(250).allow(null, ''),
            combustivel: Joi.string().required().valid(TipoCombustivel.Gasoleo, TipoCombustivel.Eletrico, TipoCombustivel.GPL, TipoCombustivel.Hidrogenio, TipoCombustivel.Gasolina),
            autonomia: Joi.number().integer().positive().required(),
            velocidadeMedia: Joi.number().integer().positive().required(),
            custoPorQuilometro: Joi.number().positive(), 
            consumoMedio: Joi.number().precision(3).required()
        });

        return schema.validate(data);
    }
}