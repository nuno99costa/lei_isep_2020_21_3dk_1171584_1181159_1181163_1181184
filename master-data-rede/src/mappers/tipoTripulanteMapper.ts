import { Joi } from "celebrate";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { ITipoTripulantePersistence } from "../dataschema/ITipoTripulantePersistence";
import TipoTripulante from "../domain/tipoTripulante";
import ITipoTripulanteDTO from "../dto/ITipoTripulanteDTO";

export default class TipoTripulanteMapper {
    constructor() { }

    //Transformação de Domain para DTO.
    public static toDTO(tipoTripulante: TipoTripulante): ITipoTripulanteDTO {
        return {
            id: tipoTripulante.id.toString(),
            codigo: tipoTripulante.codigo,
            descricao: tipoTripulante.descricao
        }
    }

    public static toDomain(tipoTripulante: any | Model<ITipoTripulantePersistence & Document>): TipoTripulante {
        const tTripulanteOrError = TipoTripulante.create(tipoTripulante, new UniqueEntityID(tipoTripulante.domainId));

        if (tTripulanteOrError.isFailure) {
            console.log(tTripulanteOrError.error)
        }

        return tTripulanteOrError.isSuccess ? tTripulanteOrError.getValue() : null;
    }

    public static toPersistence(tipoTripulante: TipoTripulante): any {
        return {
            domainId: tipoTripulante.id,
            codigo: tipoTripulante.codigo,
            descricao: tipoTripulante.descricao
        }
    }

    public static validarTipoTripulante(data){

        const schema = Joi.object({
            id: Joi.string(),
            codigo: Joi.string().required().max(20),
            descricao: Joi.string().required().max(250)
        });

        return schema.validate(data);
    }
}