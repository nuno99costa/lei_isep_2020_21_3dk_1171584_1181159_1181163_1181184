
import { Joi } from "celebrate";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { INoPersistence } from "../dataschema/INoPersistence";
import No from "../domain/no";
import INoDTO from "../dto/INoDTO";

export default class NoMapper {
    
    


    constructor() { }

    //Domain -> DTO. 
    public static toDTO(no: No): INoDTO {
        return {
            id: no.id.toString(),

            nomeCompleto: no.nomeCompleto,

            abreviatura: no.abreviatura,

            isPontoRendicao: no.isPontoRendicao,

            isEstacaoRecolha: no.isEstacaoRecolha,

            latitude: no.latitude,

            longitude: no.longitude,

            nomeModelo: no.modelo3d
        }
    }

    public static toDomain(no: any | Model<INoPersistence & Document> ): No {
        const noOrError = No.create(no, new UniqueEntityID(no.domainId));

        if(noOrError.isFailure){
            console.log(noOrError.error)
        }

        return noOrError.isSuccess ? noOrError.getValue() : null;

    }

    public static toPersistence(no: No): any {
        return {
            domainId: no.id.toString(),
            nomeCompleto: no.nomeCompleto,
            abreviatura: no.abreviatura,
            isPontoRendicao: no.isPontoRendicao,
            isEstacaoRecolha: no.isEstacaoRecolha,
            latitude: no.latitude,
            longitude: no.longitude,
            nomeModelo: no.modelo3d
        }
    }

    public static validarNo(data) {
        const schema = Joi.object({
            id: Joi.string().allow(null, ""),
            nomeCompleto: Joi.string().required(),
            abreviatura: Joi.string().required(),
            isPontoRendicao: Joi.boolean().required(),
            isEstacaoRecolha: Joi.boolean().required(),
            latitude: Joi.number().required(),
            longitude: Joi.number().required(),
            nomeModelo: Joi.string().allow(null, "")
        });

        return schema.validate(data);
    }
}