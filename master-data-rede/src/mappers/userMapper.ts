import { Container } from 'typedi';

import { Mapper } from "../core/infra/Mapper";

import { IUserDTO } from "../dto/IUserDTO";

import { Role, User } from "../domain/user";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";

import { UserEmail } from "../domain/userEmail";
import { UserPassword } from "../domain/userPassword";

import RoleRepo from "../repos/roleRepo";
import Joi from 'joi';
import { IUserPersistence } from '../dataschema/IUserPersistence';
import { Document, Model } from 'mongoose';

export class UserMapper {

  public static validarUser(data) {
    const schema = Joi.object({
      id: Joi.string().allow(null, ""),
      name: Joi.string().required(),
      email: Joi.string().email().required(),
      password: Joi.string().required(),
      dataNascimento: Joi.date().required(),
      role: Joi.string().required().valid(Role.Administrador, Role.Cliente, Role.Gestor)
    });

    return schema.validate(data);
  }

  public static toDTO(user: User): IUserDTO {
    return {
      id: user.id.toString(),
      name: user.name,
      email: user.email.value,
      password: user.password,
      dataNascimento: user.dataNascimento.toString(),
      role: user.role
    } as IUserDTO;
  }

  public static async toDomain(user: any | Model<IUserPersistence & Document>): Promise<User> {
    const userOrError = User.create(user, new UniqueEntityID(user.domainId));

    if (userOrError.isFailure) {
      console.log(userOrError.error)
    }

    return userOrError.isSuccess ? userOrError.getValue() : null;

  }

  public static toPersistence(user: User): any {
    const a = {
      domainId: user.id.toString(),
      email: user.email.value,
      password: user.password,
      dataNascimento: user.dataNascimento,
      name: user.name,
      role: user.role,
    }
    return a;
  }
}