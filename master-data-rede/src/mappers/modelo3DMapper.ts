import Joi from "joi";
import { Document, Model } from "mongoose";
import { pathToFileURL } from "url";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { IModelo3DPersistence } from "../dataschema/IModelo3DPersistence";
import Modelo3D from "../domain/Modelo3D";
import IModelo3DDTO from "../dto/IModelo3DDTO";

export default class Modelo3DMapper {

    constructor() { }

    //Domain -> DTO. 
    public static toDTO(modelo: Modelo3D): IModelo3DDTO {
        return {
            nome: modelo.nome,
            path: modelo.path
        }
    }

    public static toDomain(modelo: any | Model<IModelo3DPersistence & Document> ): Modelo3D {
        const modeloOrError = Modelo3D.create(modelo, new UniqueEntityID(modelo.domainId));

        if(modeloOrError.isFailure){
            console.log(modeloOrError.error)
        }

        return modeloOrError.isSuccess ? modeloOrError.getValue() : null;

    }

    public static toPersistence(modelo: Modelo3D): any {
        return {
            nome: modelo.nome,
            path: modelo.path
        }
    }

    public static validarModelo(data) {
        const schema = Joi.object({
            nome: Joi.string().required(),
            path: Joi.string().required(),
        });

        return schema.validate(data);
    }
}