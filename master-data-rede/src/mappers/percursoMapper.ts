
import { Joi } from "celebrate";
import { Document, Model } from "mongoose";
import { UniqueEntityID } from "../core/domain/UniqueEntityID";
import { IPercursoPersistence } from "../dataschema/IPercursoPersistence";
import Percurso, { Orientacao } from "../domain/percurso";
import Segmento from "../domain/segmento";
import { SegmentoId } from "../domain/segmentoId";
import IPercursoDTO from "../dto/IPercursoDTO";

export default class PercursoMapper {

    constructor() { }

    //Domain -> DTO
    public static toDTO(percurso: Percurso): IPercursoDTO {

        return {
            id: percurso.id.toString(),

            nomePercurso : percurso.nomePercurso,

            linhaId : percurso.linhaId.id,

            orientacao: percurso.orientacao,

            listaSegmentos: percurso.segmentosEmString,

            noInicialId: percurso.noInicialId.id,

            noFinalId: percurso.noFinalId.id,

            distanciaTotal: percurso.distanciaTotal,

            tempoTotal: percurso.tempoTotal
        }
    }

    private static segmentosEmId(listaSegmentos: string[]): SegmentoId[]{
        var array : SegmentoId[] = [];
        let index : number = 0;
        for(index = 0; index < listaSegmentos.length; index++){
            array.push(SegmentoId.create(new UniqueEntityID(listaSegmentos[index])));
        }

        return array;
    }

    public static toDomain(percurso: any| Model<IPercursoPersistence & Document>): Percurso {

        const segmentosId = this.segmentosEmId(percurso.listaSegmentos);
        
        const percursoOrError = Percurso.create(percurso, segmentosId, percurso.distanciaTotal, percurso.tempoTotal, new UniqueEntityID(percurso.domainId));

        if(percursoOrError.isFailure){
            console.log(percursoOrError.error)
        }

        return percursoOrError.isSuccess ? percursoOrError.getValue() : null;

    }

    public static toPersistence(percurso : Percurso): any {
        return {
            domainId: percurso.id.toString(),
            linhaId: percurso.linhaId.id,
            orientacao: percurso.orientacao,
            nomePercurso: percurso.nomePercurso,
            noInicialId: percurso.noInicialId.id,
            noFinalId: percurso.noFinalId.id,
            listaSegmentos: percurso.segmentosEmString,
            distanciaTotal: percurso.distanciaTotal,
            tempoTotal: percurso.tempoTotal
        }
    }

    public static validarPercurso(data) {
        const schema = Joi.object({
            id: Joi.string(),
            nomePercurso: Joi.string().required(),
            linhaId: Joi.string().required(),
            orientacao: Joi.string().required().valid(Orientacao.Ida, Orientacao.Volta, Orientacao.Reforço, Orientacao.Vazio),
            noInicialId: Joi.string().required(),
            noFinalId: Joi.string().required(),
            listaSegmentos: Joi.array().items(Joi.string()).min(2),
            distanciaTotal: Joi.number(),
            tempoTotal: Joi.number()
        });

        return schema.validate(data);
    }
}