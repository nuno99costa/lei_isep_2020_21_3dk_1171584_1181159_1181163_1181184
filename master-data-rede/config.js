import dotenv from 'dotenv';

// Set the NODE_ENV to 'development' by default
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config();
if (!envFound) {
  // This error should crash whole process

  throw new Error("⚠️  Couldn't find .env file  ⚠️");
}

export default {
  /**
   * Your favorite port
   */
  port: parseInt(process.env.PORT, 10),

  /**
   * That long string from mlab
   */
  databaseURL: process.env.MONGODB_URI,

  /*
  * URL da base de dados de teste
  */
  testDatabaseURL: process.env.TESTMONGODB_URI,

  /**
   * Your secret sauce
   */
  jwtSecret: process.env.JWT_SECRET,

  /**
   * Used by winston logger
   */
  logs: {
    level: process.env.LOG_LEVEL || 'silly',
  },

  /**
   * API configs
   */
  api: {
    prefix: '/api',
  },

  controller: {
    role: {
      name: "RoleController",
      path: "../controllers/roleController"
    },
    no: {
      name: "NoController",
      path: "../controllers/noController"
    },

    tipoViatura: {
      name: "TipoViaturaController",
      path: "../controllers/tipoViaturaController"
    },

    tipoTripulante: {
      name: "TipoTripulanteController",
      path: "../controllers/tipoTripulanteController"
    },

    percurso: {
      name: "PercursoController",
      path: "../controllers/percursoController"
    },

    segmento: {
      name: "SegmentoController",
      path: "../controllers/segmentoController"
    },

    linha: {
      name: "LinhaController",
      path: "../controllers/linhaController"
    },

    user: {
      name: "UserController",
      path: "../controllers/userController"
    },

    inserirGLX: {
      name: "InserirGLXController",
      path: "../controllers/inserirGLXController"
    },

    modelo3D: {
      name: "Modelo3DController",
      path: "../controllers/modelo3DController"
    }
  },

  repos: {
    role: {
      name: "RoleRepo",
      path: "../repos/roleRepo"
    },
    user: {
      name: "UserRepo",
      path: "../repos/userRepo"
    },
    no: {
      name: "NoRepo",
      path: "../repos/noRepo"
    },
    tipoViatura: {
      name: "TipoViaturaRepo",
      path: "../repos/tipoViaturaRepo"
    },
    tipoTripulante: {
      name: "TipoTripulanteRepo",
      path: "../repos/tipoTripulanteRepo"
    },
    percurso: {
      name: "PercursoRepo",
      path: "../repos/percursoRepo"
    },
    segmento: {
      name: "SegmentoRepo",
      path: "../repos/segmentoRepo"
    },
    linha: {
      name: "LinhaRepo",
      path: "../repos/linhaRepo"
    },

    user: {
      name: "UserRepo",
      path: "../repos/userRepo"
    },
    
    modelo3D: {
      name: "Modelo3DRepo",
      path: "../repos/modelo3DRepo"
    }

  },

  services: {
    role: {
      name: "RoleService",
      path: "../services/roleService"
    },
    no: {
      name: "NoService",
      path: "../services/noService"
    },
    tipoViatura: {
      name: "TipoViaturaService",
      path: "../services/tipoViaturaService"
    },
    tipoTripulante: {
      name: "TipoTripulanteService",
      path: "../services/tipoTripulanteService"
    },
    percurso: {
      name: "PercursoService",
      path: "../services/percursoService"
    },
    segmento: {
      name: "SegmentoService",
      path: "../services/segmentoService"
    },
    linha: {
      name: "LinhaService",
      path: "../services/linhaService"
    },
    user: {
      name: "UserService",
      path: "../services/userService"
    },

    modelo3D: {
      name: "Modelo3DService",
      path: "../services/modelo3DService"
    }
  },


};
